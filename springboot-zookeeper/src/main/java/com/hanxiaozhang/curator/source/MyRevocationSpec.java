package com.hanxiaozhang.curator.source;

import java.util.concurrent.Executor;

/**
 * 〈一句话功能简述〉<br>
 * 〈撤销范围〉
 *
 * @author hanxinghua
 * @create 2022/8/31
 * @since 1.0.0
 */
public class MyRevocationSpec {

    private final Runnable runnable;
    private final Executor executor;

    MyRevocationSpec(Executor executor, Runnable runnable) {
        this.runnable = runnable;
        this.executor = executor;
    }

    Runnable getRunnable() {
        return runnable;
    }

    Executor getExecutor() {
        return executor;
    }
}
