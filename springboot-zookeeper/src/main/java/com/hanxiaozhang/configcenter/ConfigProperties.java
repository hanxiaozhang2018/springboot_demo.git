package com.hanxiaozhang.configcenter;

/**
 * 功能描述: <br>
 * 〈配置信息〉
 *
 * @Author:hanxinghua
 * @Date: 2021/6/8
 */
public class ConfigProperties {

    private  String conf ;

    public String getConf() {
        return conf;
    }

    public void setConf(String conf) {
        this.conf = conf;
    }
}
