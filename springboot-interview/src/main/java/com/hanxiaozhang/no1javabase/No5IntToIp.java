package com.hanxiaozhang.no1javabase;

/**
 * 功能描述: <br>
 * 〈面试题：请用代码实现ip地址与int之间互换？〉
 *
 * @Author:hanxinghua
 * @Date: 2019/3/7
 */
public class No5IntToIp {

	public static void main(String[] args) {
		
		   System.out.println(ip2Int("193.11.223.3"));
		   System.out.println(int2Ip1(ip2Int("193.11.223.3")));
	}
	
	/**
	 * 将 ip 字符串转换为 int 类型的数字
	 * <p>
	 * 思路就是将 ip 的每一段数字转为 8 位二进制数，并将它们放在结果的适当位置上
	 *
	 * @param ipString ip字符串，如 127.0.0.1
	 * @return ip字符串对应的 int 值
	 */
	public static int ip2Int(String ipString) {
	    // 取 ip 的各段
	    String[] ipSlices = ipString.split("\\.");
	    int rs = 0;
	    for (int i = 0; i < ipSlices.length; i++) {
	        // 将 ip 的每一段解析为 int，并根据位置左移 8 位
	        int intSlice = Integer.parseInt(ipSlices[i]) << 8 * i;
	        // 求与
	        rs = rs | intSlice;
	    }
	    return rs;
	}
	
	/**
	 * 将 int 转换为 ip 字符串
	 *
	 * @param ipInt 用 int 表示的 ip 值
	 * @return ip字符串，如 127.0.0.1
	 */
	public static String int2Ip(int ipInt) {
	    String[] ipString = new String[4];
	    for (int i = 0; i < 4; i++) {
	        // 每 8 位为一段，这里取当前要处理的最高位的位置
	        int pos = i * 8;
	        // 取当前处理的 ip 段的值
	        int and = ipInt & (255 << pos);
	        // 将当前 ip 段转换为 0 ~ 255 的数字，注意这里必须使用无符号右移
	        ipString[i] = String.valueOf(and >>> pos);
	    }
	    return String.join(".", ipString);
	}
	
	/**
	 * 有问题，有时间在研究吧
	 * 20190312
	 * 
	 * @param ipInt
	 * @return
	 */
	public static String int2Ip1(int ipInt) {
		 StringBuffer binaryInt1 = new StringBuffer(Integer.toBinaryString(ipInt));  
         String binaryInt=binaryInt1.reverse().toString();
		 int length=binaryInt.length();
		 String[] ipString = new String[4];
		 for(int i=0;i<4;i++){
		  if(length>=8*(i+1)){
			  ipString[i]=Integer.valueOf(binaryInt.substring(8*i, 8*(i+1)-1),2).toString();
		  }else{
			  int c=8*i-length;
			  if(c<8&&c>0){
				  if(c==7){
					  ipString[i]="1"; 
				  }else{
					  ipString[i]=Integer.valueOf(binaryInt.substring(8*i, length),2).toString();  
				  }
			 }else{
				ipString[i]="0"; 
			 }
		  }
		 }
		 return String.join(".", ipString);
		
	}

}
