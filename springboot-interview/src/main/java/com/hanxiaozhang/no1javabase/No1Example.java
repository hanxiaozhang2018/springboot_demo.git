package com.hanxiaozhang.no1javabase;

/**
 * 〈一句话功能简述〉<br>
 * 〈i++与++i的区别〉
 *
 * @author hanxinghua
 * @create 2020/5/22
 * @since 1.0.0
 */
public class No1Example {

    public static void main(String[] args) {
        // 考题1：
        int i=1;
        // j=1 i=2
        int j=i++;
        // i==(++j) 成立  j=2 =>  (i++)==j 成立  i=3
        if ((i==(++j))&&((i++)==j)) {
            // i =i+j
            i+=j;
        }
        System.out.println("i = "+i);


        // 考点：i++与++i的区别（赋值与自增顺序不一样）、在面试紧张环境正确算出值
        // 结果：5
        // 如果在多线程下，i++与++i会出现线程安全问题，因为它分两步，不是原子操作，解决方式很多，加锁、使用Atomic原子类


        // 考题2：
        int a = 0;
        a = a++ + 1;
        System.out.println(a);
        int b = 0;
        b = ++b + 1;
        System.out.println(b);

    }


}
