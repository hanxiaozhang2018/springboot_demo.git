package com.hanxiaozhang.no1javabase.no2extend;

/**
 * 〈一句话功能简述〉<br>
 * 〈B〉
 *
 * @author hanxinghua
 * @create 2019/8/9
 * @since 1.0.0
 */
public class B extends A {

    @Override
    public void whoAmI() {
        System.out.println("I am B");
    }

}
