package com.hanxiaozhang.no1javabase.no2extend;

/**
 * 〈一句话功能简述〉<br>
 * 〈继承中对象上转型相关问题〉
 *
 * @author hanxinghua
 * @create 2019/8/9
 * @since 1.0.0
 */
public class Test {

    public static void main(String[] args) {
        A a = new B();
        //  A a = new A();
        test(a);
    }

    public static void test(A a) {
        System.out.println("test a");
        a.whoAmI();
    }

    public static void test(B b) {
        System.out.println("test b");
        b.whoAmI();
    }


}
