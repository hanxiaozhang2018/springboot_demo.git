package com.hanxiaozhang.no1javabase.no2extend;

/**
 * @author hanxinghua
 * @create 2019/8/9
 * @since 1.0.0
 */
public class A {

    public void whoAmI() {
        System.out.println("I am A");
    }

}
