package com.hanxiaozhang.no1javabase;

/**
 * 〈一句话功能简述〉<br>
 * 〈for循环，可以支持多个参数〉
 *
 * @author hanxinghua
 * @create 2020/3/18
 * @since 1.0.0
 */
public class No2Example {

    public static void main(String[] args) {

        // for循环，可以支持多个参数
        for (int i = 0, sum = 0; i < 10; ++i, sum += i) {
            System.out.println("i=" + i + " sum=" + sum);
        }

        int sum1 = 0;
        for (int i = 0; i < 10; ++i) {
            sum1 += i;
            System.out.println("i=" + i + " sum1=" + sum1);
        }

    }
}
