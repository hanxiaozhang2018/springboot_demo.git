package com.hanxiaozhang.no1javabase.no1equal;

/**
 * 功能描述: <br>
 * 面试：System.out.println(b1==b2); System.out.println(b1.equals(b2));？
 * <p>
 * 运算符“==”与equals()比较对象的区别是什么？
 * https://zhidao.baidu.com/special/view?id=42795a24626975510400
 *
 * @Author:hanxinghua
 * @Date: 2022/5/6
 */
public class Equal {

    /**
     * 当使用关系运算符“==”比较两个对象时，是比较两个对象使用的内存地址和内容是否相同，
     * 如果两个对象使用的是同一个内存地址，并且内容相同，则结果为true，否则结果为false。
     * <p>
     * 当使用equals()方法比较两个对象时，则是比较两个对象的内容是否相同，而与对象的内存地址无关，
     * 如果两个对象的内容相同，则结果为true，否则结果为false。
     */
    public static void main(String[] args) {
        B b1 = new B(5);
        B b2 = new B(5);
        // b1对象引用和b2对象引用指向对象不同，所以内存地址不同  false
        System.out.println(b1 == b2);
        // 自己创建类需要重写equals()和hashCode()方法，才能使用自己equals()，不然使用Object类的equals()
        System.out.println(b1.equals(b2));
    }


}

