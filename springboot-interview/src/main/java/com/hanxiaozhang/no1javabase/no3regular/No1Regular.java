package com.hanxiaozhang.no1javabase.no3regular;

/**
 * 〈一句话功能简述〉<br>
 * 〈将字符串中数字替换成X？〉
 *
 * @author hanxinghua
 * @create 2022/5/6
 * @since 1.0.0
 */
public class No1Regular {

    public static void main(String[] args) {

        // 将字符串中数字替换成X?
        String str = "It's 8 a.m";
        // str.replaceAll("[0-9]", "X") 不用加^和$符号
        System.out.println(str.replaceAll("\\d", "X"));
    }

}
