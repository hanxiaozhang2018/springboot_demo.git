package com.hanxiaozhang.no1javabase.no3regular;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 〈一句话功能简述〉<br>
 * 〈将“12bb3hfd22dd33”字符串中的数字打印出来？〉
 *
 * @author hanxinghua
 * @create 2022/5/6
 * @since 1.0.0
 */
public class No2Regular {

    public static void main(String[] args) {

        // 将“12bb3hfd22dd33”字符串中的数字打印出来？
        String str1 = "12bb3hfd22dd33";
        // "[0-9]{1,st1.length()}",\\d{1,st1.length()},\\d+
        Pattern p = Pattern.compile("[0-9]+");
        Matcher m = p.matcher(str1);
        List<String> list = new ArrayList<String>();
        while (m.find()) {
            list.add(m.group());
        }
        System.out.println(list.toString());

    }

}
