package com.hanxiaozhang.no1javabase;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2020/5/22
 * @since 1.0.0
 */
public class No3Example {

    public static void main(String[] args) {

        String s1 = "ab";
        String s2 = "a" + "b";
        String s3 = "a";
        String s4 = "b";
        String s5 = s3 + s4;

        System.out.println(s1 == s2);
        System.out.println(s1 == s5);

        // 栈是线程私有的，每个线程包含一个栈，它的生命周期与线程相同，栈中只保存基础数据类型对象、对象引用和returnAddress类型；
        // 堆内存中最大的一块，它被所有线程共享，堆中只存放对象实例和数组，堆是GC管理的主要区域。

        //s1: 1.在栈中创建了一个名为s1的变量（对象引用）；2.判断String池是否存在"ab"，不存在就创建一个"ab"对象，让s1对象指向String池的"ab"对象；
        // 3.判断常量池是否存在"ab"，不存在创建，然后让"ab"对象指向常量池的"ab"对象。
        //s2: 0."a" + "b"在编译期已经常量折叠为"ab";1.在栈中创建了一个名为s2的变量（对象引用）;2.判断String池是否存在"ab"，存在，
        // 让s2对象指向String池"ab"对象。所有s1==s2是true
        //s3、s4：同s1
        //s5:s3 + s4的时候，在堆中会重分配一个内层空间，及String池中新对象，这个新对象指向常量池中"ab"（常量池中不会产生重复的对象）

        //String池：这部分内存应该在堆区jvm自动分配的，有些书上会写成匿名(String类)对象，这部分内存应该是有java自己的（GC- java回收机制）自己去回收，它在堆中


    }
}
