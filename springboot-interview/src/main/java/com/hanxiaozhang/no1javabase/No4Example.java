package com.hanxiaozhang.no1javabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 〈一句话功能简述〉<br>
 * 〈面试：统计A字符串在B字符串中出现的次数？〉
 *
 * @author hanxinghua
 * @create 2022/5/6
 * @since 1.0.0
 */
public class No4Example {


    public static void main(String[] args) {
        String regex = "ab";
        String target = "acabcabcbab";
        System.out.println(countString(regex, target));
    }

    /**
     * 使用正则
     *
     * @param regex 表达式
     * @param target
     * @return
     */
    private static int countString(String regex, String target) {

        // 创建Pattern对象，将给定的正则表达式编译为模式
        Pattern p = Pattern.compile(regex);
        // 创建一个匹配器，该匹配器将根据此模式匹配给定的输入。
        Matcher m = p.matcher(target);
        int count = 0;
        while (m.find()) {
            count++;
        }
        return count;
    }

}
