package com.hanxiaozhang.no3algorithm.recursion;

/**
 * 〈一句话功能简述〉<br>
 * 〈N的阶乘〉
 *
 * @author hanxinghua
 * @create 2022/5/6
 * @since 1.0.0
 */
public class No1Factorial {

    public static void main(String[] args) {

        System.out.println(factorial(5));

    }

    private static int factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return factorial(n - 1) * n;
        }
    }

}
