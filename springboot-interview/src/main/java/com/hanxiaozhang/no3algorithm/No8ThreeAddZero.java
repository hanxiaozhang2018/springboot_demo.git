package com.hanxiaozhang.no3algorithm;

import java.util.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈给定一个整数数组，判断数组中是否包含a，b，c，使得a+b+c=0。
 * 请找出所有和为0，并且不重复的三元组。〉
 * <p>
 * https://blog.csdn.net/weixin_40741512/article/details/113751515
 *
 * LeetCode 15题重复
 *
 * @author hanxinghua
 * @create 2023/3/1
 * @since 1.0.0
 */
@Deprecated
public class No8ThreeAddZero {

    public static void main(String[] args) {

        int[] nums = {-1, 0, 1, 2, -1, -4};
        method1(nums);
        System.out.println("---- ---- ---- ----");
        method2(nums);
    }


    /**
     * 双指针思想
     *
     * @param nums
     */
    public static void method2(int[] nums) {
        // 数组排序
        Arrays.sort(nums);
        // 左指针
        int leftIndex = 0;
        for (int i = 0; i < nums.length - 2; i++) {
            // 如果前一个位置的元素值与当前位置的元素值相等，结束本次循环
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            // 左指针赋初始值
            leftIndex = i + 1;
            // 循环
            for (int rightIndex = nums.length - 1; rightIndex > leftIndex; ) {
                // 如果后一个位置的元素值与当前位置的元素值相等，右指针减一，结束本次循环。
                if (rightIndex < nums.length - 1 && nums[rightIndex] == nums[rightIndex + 1]) {
                    rightIndex--;
                    continue;
                }
                // 如果等于0，打印记录，leftIndex++，rightIndex--
                if (nums[i] + nums[leftIndex] + nums[rightIndex] == 0) {
                    System.out.println("[" + nums[i] + ", " + nums[leftIndex] + ", " + nums[rightIndex] + "]");
                    leftIndex++;
                    rightIndex--;
                }
                // 如果大于0，rightIndex--
                if (nums[i] + nums[leftIndex] + nums[rightIndex] > 0) {
                    rightIndex--;
                }
                // 如果小于0，leftIndex--
                if (nums[i] + nums[leftIndex] + nums[rightIndex] < 0) {
                    leftIndex++;
                }
            }
        }
    }


    private static void method1(int[] nums) {
        if (nums == null || nums.length < 2) {
            return;
        }
        Map<String, List<String>> map = new HashMap<>();
        int length = nums.length;
        for (int i = 0; i < length - 2; i++) {
            for (int rightIndex = 1; rightIndex < length - 1; rightIndex++) {
                for (int k = 2; k < length; k++) {
                    // 三个元素值相等
                    if ((nums[i] + nums[rightIndex] + nums[k] == 0) &&
                            // 避免同一个位置元素使用多次
                            !(i == rightIndex || rightIndex == k || k == rightIndex)
                    ) {
                        List<String> list = new ArrayList<>();
                        list.add(String.valueOf(nums[k]));
                        list.add(String.valueOf(nums[rightIndex]));
                        list.add(String.valueOf(nums[i]));
                        Collections.sort(list);
                        //System.out.println(list);
                        map.put(String.join(".", list), list);
                    }
                }
            }
        }
        map.forEach((k, v) -> {
            System.out.println(v);
        });

    }
}
