package com.hanxiaozhang.no3algorithm;

/**
 * 〈一句话功能简述〉<br>
 * 〈String字符去重〉
 *
 * @author hanxinghua
 * @create 2023/2/28
 * @since 1.0.0
 */
public class No7StringRepeat {

    public static void main(String[] args) {

        String str = "abacr4df4";
        System.out.println(repeat(str));

    }

    private static String repeat(String str) {
        String newStr = "";
        for (int i = 0; i < str.length(); i++) {
            char target = str.charAt(i);
            if(str.indexOf(target)==i){
                newStr+=target;
            }
        }
        return newStr;
    }
}
