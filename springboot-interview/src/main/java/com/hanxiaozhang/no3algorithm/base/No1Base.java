package com.hanxiaozhang.no3algorithm.base;

/**
 * 〈一句话功能简述〉<br>
 * 〈基础知识〉
 *
 * @author hanxinghua
 * @create 2024/2/19
 * @since 1.0.0
 */
public class No1Base {

    public static void main(String[] args) {

        int i1 = 1;
        int i2 = 1;
        int i3 = 1;
        System.out.println("==== ==== ==== ====");
        // i++ 、++i 自增操作 ；而 i+1 只会影响到一个方法，不会影响到后续对i的使用
        method1(i1++);
        method1(++i2);
        method1(i3 + 1);
        System.out.println("==== ==== ==== ====");
        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);
    }


    private static void method1(int i) {
        System.out.println(i);
    }
}
