# 目录
+ backtrack：回溯
    + No1NQueen：N皇后
+ dynamicProgramming：动态规划
    + No1ZeroOnePack：0-1背包
+ recursion: 递归
    + No1Factorial：N的阶乘
    + No2FindSubArray：给一个数组：[1, 2, 2]，找出所有子数组，例如这个数组的子数组有：[], [1], [2], [1, 2], [2, 2], [1, 2, 2]
+ No1SingleNodeReverse：单列表逆序
+ No2Example：找出最大利润