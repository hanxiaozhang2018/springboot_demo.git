package com.hanxiaozhang.no3algorithm;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈给一个字符串，输出频率最高且最先出现的字符。〉
 *
 * @author hanxinghua
 * @create 2023/12/7
 * @since 1.0.0
 */
public class No12StringFirstMaxChar {


    public static void main(String[] args) {

        String str = "sdfsbdddsss";
        String s = method_other1(str);
        System.out.println(s);

    }

    /**
     * 给一个字符串，输出频率最高的字符。
     * <p>
     * 1. String转换成char数组
     * 2. java的字符种类有128个（ASCII码表）
     * 3. 创建数组results，容量128。每一位的值代表该ASCII码在字符串中的数量
     * 4. 循环results，寻找值最大的
     *
     * @param str
     * @return
     */
    public static String method_other1(String str) {

        char[] chars = str.toCharArray();
        int[] results = new int[128];
        for (int i = 0; i < chars.length; i++) {
            results[chars[i]] = results[chars[i]] + 1;
        }
        int max = 0, pos = 0;
        for (int i = 0; i < results.length; i++) {
            if (results[i] > max) {
                max = results[i];
                pos = i;
            }
        }
        return String.valueOf((char) pos);
    }


    /**
     * 笨方法1
     *
     * @param str
     * @return
     */
    private static String method1(String str) {

        String[] split = str.split("");
        Map<String, Integer> map = new LinkedHashMap<>();
        for (String s : split) {
            if (map.containsKey(s)) {
                map.put(s, map.get(s) + 1);
            } else {
                map.put(s, 1);
            }
        }
        int max = 0;
        String s = null;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                s = entry.getKey();
            }
        }
        return s;
    }
}
