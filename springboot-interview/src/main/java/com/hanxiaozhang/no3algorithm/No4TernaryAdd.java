package com.hanxiaozhang.no3algorithm;

/**
 * 〈一句话功能简述〉<br>
 * 〈三进制相加〉
 * <p>
 * 输入：nums1="121" nums2="1212"
 * 输出："2110"
 *
 * @author hanxinghua
 * @create 2023/6/11
 * @since 1.0.0
 */
public class No4TernaryAdd {

    public static void main(String[] args) {


        System.out.println(ternaryAdd1("121", "1212"));
        System.out.println(ternaryAdd2("121", "1212"));
        // 记住
        System.out.println(ternaryAdd3("121", "1212"));

    }


    /**
     * 简单方法
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public static String ternaryAdd3(String nums1, String nums2) {
        int num1 = Integer.parseInt(nums1, 3);
        int num2 = Integer.parseInt(nums2, 3);
        int sum = num1 + num2;
        return Integer.toString(sum, 3);
    }


    /**
     * 最笨一般
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public static String ternaryAdd1(String nums1, String nums2) {
        String result = "";
        String[] array1 = nums1.split("");
        String[] array2 = nums2.split("");
        int len1 = array1.length, len2 = array2.length;
        int len = len1 > len2 ? len1 : len2;
        int sum = 0, yu = 0;
        Integer[] array3 = new Integer[len + 1];

        for (int i = 0; i < len; i++) {
            if (len1 - i > 0) {
                if (len2 - i > 0) {
                    sum = Integer.parseInt(array1[len1 - 1 - i]) + Integer.parseInt(array2[len2 - 1 - i]) + yu;
                    if (sum < 3) {
                        array3[i] = sum;
                        yu = 0;
                    } else {
                        array3[i] = sum % 3;
                        yu = sum / 3;
                    }
                } else {
                    sum = Integer.parseInt(array1[len1 - 1 - i]) + yu;
                    if (sum < 3) {
                        array3[i] = sum;
                        yu = 0;
                    } else {
                        array3[i] = sum % 3;
                        yu = sum / 3;
                    }
                }
            } else {
                if (len2 - i > 0) {
                    sum = Integer.parseInt(array2[len2 - 1 - i]) + yu;
                    if (sum < 3) {
                        array3[i] = sum;
                        yu = 0;
                    } else {
                        array3[i] = sum % 3;
                        yu = sum / 3;
                    }
                } else {
                    sum = yu;
                    if (sum < 3) {
                        array3[i] = sum;
                        yu = 0;
                    } else {
                        array3[i] = sum % 3;
                        yu = sum / 3;
                    }
                }
            }
        }

        if (yu > 0) {
            array3[len] = yu;
        }

        for (int i = len; i >= 0; i--) {
            if (array3[i] != null) {
                result += String.valueOf(array3[i]);
            }
        }

        return result;
    }


    /**
     * 优化一般
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public static String ternaryAdd2(String nums1, String nums2) {
        String result = "";
        int len1 = nums1.length(), len2 = nums2.length();
        int len = len1 > len2 ? len1 : len2;
        int sum = 0, jin = 0;
        Integer[] array = new Integer[len + 1];
        for (int i = 0; i < len; i++) {
            if (len1 - i > 0) {
                if (len2 - i > 0) {
                    sum = Integer.parseInt(String.valueOf(nums1.charAt(len1 - 1 - i))) +
                            Integer.parseInt(String.valueOf(nums2.charAt(len2 - 1 - i))) +
                            jin;
                    if (sum < 3) {
                        array[i] = sum;
                        jin = 0;
                    } else {
                        array[i] = sum % 3;
                        jin = sum / 3;
                    }
                } else {
                    sum = Integer.parseInt(String.valueOf(nums1.charAt(len1 - 1 - i))) +
                            jin;
                    if (sum < 3) {
                        array[i] = sum;
                        jin = 0;
                    } else {
                        array[i] = sum % 3;
                        jin = sum / 3;
                    }
                }
            } else {
                if (len2 - i > 0) {
                    sum = Integer.parseInt(String.valueOf(nums2.charAt(len2 - 1 - i))) +
                            jin;
                    if (sum < 3) {
                        array[i] = sum;
                        jin = 0;
                    } else {
                        array[i] = sum % 3;
                        jin = sum / 3;
                    }
                } else {
                    sum = jin;
                    if (sum < 3) {
                        array[i] = sum;
                        jin = 0;
                    } else {
                        array[i] = sum % 3;
                        jin = sum / 3;
                    }
                }
            }
        }
        if (jin > 0) {
            array[len] = jin;
        }
        for (int i = len; i >= 0; i--) {
            if (array[i] != null) {
                result += String.valueOf(array[i]);
            }
        }

        return result;
    }


}
