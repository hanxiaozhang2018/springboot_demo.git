package com.hanxiaozhang.no10leetcode.tree;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/3/21
 * @since 1.0.0
 */
public class TreeNode {

    Integer val;

    TreeNode left;

    TreeNode right;

    TreeNode(Integer val) {
        this.val = val;
    }
}
