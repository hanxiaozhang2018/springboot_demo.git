package com.hanxiaozhang.no10leetcode.tree;

/**
 * 〈一句话功能简述〉<br>
 * 〈给一棵二叉树，判断其结构是否对称〉
 * <p>
 * 思路：
 * 1. 判断root是否为空，为空时也认为对称
 * 2. 判断root的左孩子树 与 root的右孩子树 是否相等，则对称。
 * -> 参考 com.hanxiaozhang.no10leetcode.tree.No100SameTree
 *
 * @author hanxinghua
 * @create 2024/4/7
 * @since 1.0.0
 */
public class No101SymmetricTree {


    public static void main(String[] args) {

        TreeNode tree1 = new TreeNode(5);
        tree1.left = new TreeNode(4);
        tree1.left.left = new TreeNode(3);
        tree1.left.right = new TreeNode(2);
        tree1.right = new TreeNode(4);
        tree1.right.left = new TreeNode(3);
        tree1.right.right = new TreeNode(2);


        TreeNode tree2 = new TreeNode(5);
        tree2.left = new TreeNode(1);
        tree2.right = new TreeNode(4);
        tree2.right.left = new TreeNode(3);
        tree2.right.right = new TreeNode(6);

        System.out.println(method1(tree1));
        System.out.println(method1(tree2));

    }

    /**
     * 方法1
     *
     * @param root
     * @return
     */
    private static boolean method1(TreeNode root) {

        if (root == null) {
            return true;
        }

        return recursion(root.left, root.right);
    }


    private static boolean recursion(TreeNode tree1, TreeNode tree2) {

        /*
        if (tree1 == null && tree2 != null) {
            return false;
        }
        if (tree2 == null && tree1 != null) {
            return false;
        }
        if (tree1 == null && tree2 == null) {
            return true;
        }
         */

        // 上述三步的优化写法：
        if (tree1 == null || tree2 == null) {
            return tree1 == tree2;
        }

        return tree1.val.equals(tree2.val) &&
                recursion(tree1.left, tree2.left) &&
                recursion(tree1.right, tree2.right);
    }


}
