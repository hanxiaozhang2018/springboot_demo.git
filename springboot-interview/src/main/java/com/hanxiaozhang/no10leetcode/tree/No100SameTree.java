package com.hanxiaozhang.no10leetcode.tree;

import java.util.Objects;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * <p>
 * 写一个功能判断两棵树是否相同。
 * 相同要考虑到结构相同和节点值相同
 *
 * @author hanxinghua
 * @create 2024/3/22
 * @since 1.0.0
 */
public class No100SameTree {


    public static void main(String[] args) {

        TreeNode tree1 = new TreeNode(5);
        tree1.left = new TreeNode(1);
        tree1.right = new TreeNode(4);
        tree1.right.left = new TreeNode(3);
        tree1.right.right = new TreeNode(6);

        TreeNode tree2 = new TreeNode(5);
        tree2.left = new TreeNode(1);
        tree2.right = new TreeNode(4);
        tree2.right.left = new TreeNode(3);
        tree2.right.right = new TreeNode(6);

        TreeNode tree3 = new TreeNode(2);
        tree3.left = new TreeNode(1);
        tree3.right = new TreeNode(3);

        System.out.println(method1(tree1, tree2));

        System.out.println(method1(tree2, tree3));

    }


    /**
     * 方法1
     *
     * @param node1
     * @param node2
     * @return
     */
    public static boolean method1(TreeNode node1, TreeNode node2) {

        // node1 或 node2 不为空时，判断两个是否为空。
        if (node1 == null || node2 == null) {
            return node1 == node2;
        }

        // 判断当前值是否相等 && 递归左孩子结点的结果 && 递归右孩子结点的结果
        return Objects.equals(node1.val, node2.val) &&
                method1(node1.left, node2.left) &&
                method1(node1.right, node2.right);
    }


}
