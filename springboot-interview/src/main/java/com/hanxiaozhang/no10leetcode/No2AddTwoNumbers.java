package com.hanxiaozhang.no10leetcode;

/**
 * 〈一句话功能简述〉<br>
 * 〈两数相加〉
 *
 * @author hanxinghua
 * @create 2022/12/5
 * @since 1.0.0
 */
public class No2AddTwoNumbers {


    /*
    给你两个非空的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，
    并且每个节点只能存储一位数字。
    请你将两个数相加，并以相同形式返回一个表示和的链表。
    你可以假设除了数字0之外，这两个数都不会以0 开头。
    示例 1：
    输入：l1 = [2,4,3], l2 = [5,6,4]
    输出：[7,0,8]
    解释：342 + 465 = 807.

    示例 2：
    输入：l1 = [0], l2 = [0]
    输出：[0]

    示例 3：
    输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
    输出：[8,9,9,9,0,0,0,1]

     */


    public static void main(String[] args) {


    }


    /**
     * 自己的写法
     *
     * @param l1
     * @param l2
     * @return
     */
    public ListNode addTwoNumbers1(ListNode l1, ListNode l2) {

        // 进位值,当前l1值,当前l2值,当前汇总和,本次循环是否有元素标识
        int carry = 0, l1value = 0,l2value = 0,sum = 0,flag = 0;
        // 头结点，当前节点
        ListNode head = null, node = null;

        for (int i = 0; i < 100; i++) {
            if (l1 != null) {
                l1value = l1.val;
                l1 = l1.next;
                flag++;
            }
            if (l2 != null) {
                l2value = l2.val;
                l2 = l2.next;
                flag++;
            }
            // Tips:第一次写考虑到没有节点停止循环，但是没有考虑到carry不等于0需要创建一个节点的情况
            if (flag == 0 && carry == 0) {
                break;
            }
            sum = l1value + l2value + carry;
            if (sum > 9) {
                carry = sum / 10;
                sum = sum % 10;
            }else {
                // Tips：不要忘记恢复成0
                carry =0;
            }
            // 头结点为空 Tips：第一次思考，忘记每次处理每次下移一个节点
            if (head == null) {
                head = new ListNode(sum, null);
                node=head;
            } else {
                node.next = new ListNode(sum, null);
                node =node.next;
            }
            // 恢复初始值
            l1value = 0;l2value = 0; sum = 0;flag = 0;
        }
        // Tips：不要忘记写返回值
        return head;
    }


    /**
     * 大神的写法
     *
     * @param l1
     * @param l2
     * @return
     */
    public ListNode addTwoNumbers2(ListNode l1, ListNode l2) {
        ListNode dummy = new ListNode(-1);
        ListNode cur = dummy;
        int carry = 0;
        // 循环条件的优化写法
        while (l1 != null || l2 != null || carry != 0) {
            // 获取l1、l2节点值，并相加
            int val1 = l1 != null ? l1.val : 0;
            int val2 = l2 != null ? l2.val : 0;
            int sum = val1 + val2 + carry;

            // 处理节点值和进位值的优化写法
            ListNode node = new ListNode(sum % 10);
            carry = sum / 10;

            // 移动到下一个节点
            cur.next = node;
            cur = cur.next;

            // 移动到下一个节点
            if (l1 != null){
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
        }
        return dummy.next;

    }

}

class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}