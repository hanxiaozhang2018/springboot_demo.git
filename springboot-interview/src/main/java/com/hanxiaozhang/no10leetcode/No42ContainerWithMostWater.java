package com.hanxiaozhang.no10leetcode;

/**
 * 〈一句话功能简述〉<br>
 * 〈接雨水〉
 * <p>
 * 题目描述：
 * 给定一个长度为n的整数数组height。有n条垂线，第i条线的两个端点是(i, 0)和(i,height[i])。
 * 找出其中的两条线，使得它们与x轴共同构成的容器可以容纳最多的水。返回容器可以储存的最大水量。
 * <p>
 * 说明：
 * 你不能倾斜容器。
 * <p>
 * 示例1：
 * 输入：[1,8,6,2,5,4,8,3,7]
 * 输出：49
 * 解释：图中垂直线代表输入数组 [1,8,6,2,5,4,8,3,7]。在此情况下，
 * 容器能够容纳水（表示为蓝色部分）的最大值为 49。
 * <p>
 * 参考文章：
 * https://leetcode-cn.com/problems/container-with-most-water
 * 思路：双指针思路
 * 参考method2
 *
 * @author hanxinghua
 * @create 2023/11/19
 * @since 1.0.0
 */
public class No42ContainerWithMostWater {


    public static void main(String[] args) {

        int[] height = new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7};

        System.out.println("method1 result is " + method1(height));
        System.out.println("method2 result is " + method2(height));
    }

    /**
     * 双指针算法：
     * <p>
     * 思路：
     * 1. 在每个状态下，无论长板或短板向中间收窄一格，都会导致水槽底边宽度−1​变短；
     * 2. 若向内，移动短板，水槽的短板min(h[i], h[j])不变或变大，因此下个水槽的面积可能增大；
     * 3. 若向内，移动长板，水槽的短板min(h[i], h[j])不变或变小，因此下个水槽的面积一定变小。
     * 因此，初始化双指针分列水槽左右两端，循环每轮将短板向内移动一格，并更新面积最大值，
     * 直到两指针相遇时跳出；即可获得最大面积。
     * <p>
     * 算法流程：
     * 1. 初始化双指针i、j分列在水槽左右两端；
     * 2. 循环收窄，并更新面积最大值max，直至双指针相遇时跳出；
     * 3. 选定两板高度中的短板，向中间收窄。
     *
     * @param height
     * @return
     */
    private static int method2(int[] height) {
        // 初始化双指针
        int i = 0, j = height.length - 1;
        // 定义返回结果
        int max = 0;

        while (i < j) {
            if (height[i] > height[j]) {
                max = Math.max(max, height[j] * (j - i));
                j--;
            } else {
                max = Math.max(max, height[i] * (j - i));
                i++;
            }
        }
        return max;
    }


    /**
     * 暴利破解
     * <p>
     * 计算面积公式：
     * 最短边 *（i - j）
     *
     * @param height
     * @return
     */
    private static int method1(int[] height) {
        int max = 0, temp = 0;
        for (int i = 0; i <= height.length; i++) {
            for (int j = i + 1; j < height.length; j++) {
                if (height[i] >= height[j]) {
                    temp = (j - i) * height[j];
                } else {
                    temp = (j - i) * height[i];
                }
                if (temp > max) {
                    max = temp;
                }
            }
        }
        return max;
    }

}
