package com.hanxiaozhang.no10leetcode.array;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * <p>
 * 给一个非空的正整数数组，除了一个元素之外，剩下的所有元素都出现了两次，找到这个唯一的元素。
 * 算法要求时间复杂度为线性，即O(n)且不使用多余内存
 * 实例1:
 * 输入: nums = [2,2,1]
 * 输出: 1
 * 实例2:
 * 输入: nums = [4,1,2,1,2]
 * 输出: 4
 * 实例3:
 * 输入: nums = [1]
 * 输出: 1
 * <p>
 * 思路：
 * 使用异或运算性质：N ^ N == 0 -> 两个元素异或运算为0； 0 ^ N == N  -> 0与任意元素异或运算为本身，根据题意将所有元素
 * 异或运算就可以得到一个元素值。
 *
 * @author hanxinghua
 * @create 2024/1/30
 * @since 1.0.0
 */
public class No136SingleNumber {

    public static void main(String[] args) {

        int[] nums1 = {2, 2, 1};
        int[] nums2 = {4, 1, 2, 1, 2};
        int[] nums3 = {1};

        System.out.println(method1(nums1));
        System.out.println(method1(nums2));
        System.out.println(method1(nums3));


    }

    private static int method1(int[] nums) {
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            result ^= nums[i];
        }
        return result;
    }

}
