package com.hanxiaozhang.no10leetcode.array;

import java.util.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈三个数之和，等于0〉
 * 给定一个由整数组成的数组nums，找出所有三个元素相加等于0的组合。
 * 例如：
 * nums = [-1, 0, 1, 2, -1, -4]
 * 输出：
 * [ [-1, 0, 1], [-1, -1, 2] ]
 * <p>
 * 思路：
 * 1.三层循环，每层控制一个数的位置。
 * 2.三层for循环，每层递增+1，不会出现重复的组合。
 * 3.处理重复答案，list排序后，放入map中去重。
 *
 *
 * @author hanxinghua
 * @create 2024/1/10
 * @since 1.0.0
 */
public class No15ThreeSum {

    public static void main(String[] args) {

        int[] nums = {-1, 0, 1, 2, -1, -4};
        method1(nums);

    }

    /**
     * 双指针思想
     * <p>
     * https://blog.csdn.net/weixin_40741512/article/details/113751515
     * <p>
     * 难 先pass
     *
     * @param nums
     */
    public static void method2(int[] nums) {
        // 数组排序
        Arrays.sort(nums);
        // 左指针
        int leftIndex = 0;
        for (int i = 0; i < nums.length - 2; i++) {
            // 如果前一个位置的元素值与当前位置的元素值相等，结束本次循环
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            // 左指针赋初始值
            leftIndex = i + 1;
            // 循环
            for (int rightIndex = nums.length - 1; rightIndex > leftIndex; ) {
                // 如果后一个位置的元素值与当前位置的元素值相等，右指针减一，结束本次循环。
                if (rightIndex < nums.length - 1 && nums[rightIndex] == nums[rightIndex + 1]) {
                    rightIndex--;
                    continue;
                }
                // 如果等于0，打印记录，leftIndex++，rightIndex--
                if (nums[i] + nums[leftIndex] + nums[rightIndex] == 0) {
                    System.out.println("[" + nums[i] + ", " + nums[leftIndex] + ", " + nums[rightIndex] + "]");
                    leftIndex++;
                    rightIndex--;
                }
                // 如果大于0，rightIndex--
                if (nums[i] + nums[leftIndex] + nums[rightIndex] > 0) {
                    rightIndex--;
                }
                // 如果小于0，leftIndex--
                if (nums[i] + nums[leftIndex] + nums[rightIndex] < 0) {
                    leftIndex++;
                }
            }
        }
    }

    private static void method1(int[] nums) {

        if (nums == null || nums.length < 2) {
            return;
        }
        Map<String, List<String>> map = new HashMap<>();

        // 知识：三层for循环，每层递增+1，不会出现重复的组合。
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                for (int k = j + 1; k < nums.length; k++) {
                    if (nums[i] + nums[j] + nums[k] == 0) {
                        List<String> list = new ArrayList<>();
                        list.add(String.valueOf(nums[i]));
                        list.add(String.valueOf(nums[j]));
                        list.add(String.valueOf(nums[k]));
                        Collections.sort(list);
                        map.put(String.join(".", list), list);
                    }
                }
            }
        }

        map.forEach((k, v) -> {
            System.out.println(v);
        });
    }

}
