package com.hanxiaozhang.no10leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * 给一个正整数n，输出杨辉三角的第n行
 * <p>
 * 实例1:
 * 输入: rowIndex = 3
 * 输出: [1,3,3,1]
 * 实例2:
 * 输入: rowIndex = 0
 * 输出: [1]
 * 实例3:
 * 输入: rowIndex = 1
 * 输出: [1,1]
 *
 * @author hanxinghua
 * @create 2024/1/24
 * @since 1.0.0
 */
public class No119PascalTriangleII {

    public static void main(String[] args) {

        System.out.println(method1(1));

    }

    /*
   0          1
   1        1  1
   2      1  2  2 1
   3    1  3  4  3  1


  第0行：
  数组：[0]=1
  第1行:
  数组：[0]=1
  进入循环处理：nums[1] = nums[1] + nums[0] = 0 + 1 = 1
  第2行：
  数组：[0]=1 [1]=1
  进入循环处理：
  1. nums[2] = nums[2] + nums[1] = 0 + 1 = 1
  2. nums[1] = nums[1] + nums[0] = 1 + 1 = 2
  第3行：
  数组：[0]=1 [1]=2 [2]=1
  进入循环处理：
  1. nums[3] = nums[3] + nums[2] = 0 + 1 = 1
  2. nums[2] = nums[2] + nums[1] = 1 + 2 = 3
  2. nums[1] = nums[1] + nums[0] = 2 + 1 = 3
  第4行：
  数组：[0]=1 [1]=3 [2]=3  [3]=1
  ... ...

     */


    /**
     * 方法1
     *
     * @param rowIndex 行的索引 从0开始
     * @return
     */
    public static List<Integer> method1(int rowIndex) {
        List<Integer> res = new ArrayList<>();
        if (rowIndex < 0) {
            return res;
        }
        // 声明一个数组，容量是 rowIndex + 1。原因是：行索引是0开始。0行存储1个。1行存储2个。以此类推
        // 注意点：int类型数组的默认值都是0
        int[] nums = new int[rowIndex + 1];
        // 存储第0个元素
        nums[0] = 1;
        // 从第1个元素开始处理。
        // 特殊点：第0行不进入该循环
        for (int i = 1; i <= rowIndex; i++) {
            // 从后往前，计算元素，计算到元素索引值为1时。 条件表达式 j>0，即最小值为1
            // 倒数第1个元素(最后一个元素)：nums[j] = nums[j] + nums[j - 1] = 0 + 1 = 1  。 nums[j]=0的原因是当前还没有，nums[j]默认值是0
            // 倒数第2个元素：以此类推
            for (int j = i; j > 0; j--) {
                nums[j] = nums[j] + nums[j - 1];
            }
        }
        // 赋值给集合
        for (int i : nums) {
            res.add(i);
        }
        return res;
    }

}
