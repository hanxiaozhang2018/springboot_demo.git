package com.hanxiaozhang.no10leetcode.array;

import java.util.HashMap;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈  + 测试数组写法〉
 *
 * @author hanxinghua
 * @create 2022/12/5
 * @since 1.0.0
 */
public class No1TwoSum {

    public static void main(String[] args) {
        int[] array1 = new int[]{1, 2};
        int[] array2 = {1, 2};

        int[] nums = {3, 2, 4};
        twoSum1(nums, 6);
        twoSum2(nums, 6);


    }

    /**
     * 解题思路
     * 1. 申请一个HashMap，其中 key：数组中元素值  value：数组的索引值
     * 2. 循环把元素放入到HashMap中，并且每次都跟已经存入的数据做比较
     *
     * @param nums
     * @param target
     * @return
     */
    public static int[] twoSum2(int[] nums, int target) {

        int sub = 0;
        // key：数组中元素值  value：数组的索引值
        Map<Integer, Integer> map = new HashMap<>();
        int length = nums.length;
        // 循环把元素放入到HashMap中，并且每次都跟已经存入的数据做比较
        // 即，所以输出顺序是{map.get(sub),i}
        for (int i = 0; i < length; i++) {
            sub = target - nums[i];
            if (map.containsKey(sub)) {
                return new int[]{map.get(sub), i};
            }
            map.put(nums[i], i);
        }
        return null;
    }


    /**
     * 暴利破解
     *
     * @param nums
     * @param target
     * @return
     */
    public static int[] twoSum1(int[] nums, int target) {

        int[] relust = null;

        Map<Integer, Integer> map = new HashMap<>();
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            map.put(nums[i], i);
        }

        for (int i = 0; i < length; i++) {
            if (map.containsKey(target - nums[i]) && map.get(target - nums[i]) != i) {
                relust = new int[2];
                relust[0] = i;
                relust[1] = map.get(target - nums[i]);
                break;
            }
        }
        return relust;
    }

}
