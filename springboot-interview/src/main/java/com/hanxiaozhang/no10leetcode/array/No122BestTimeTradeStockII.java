package com.hanxiaozhang.no10leetcode.array;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * 给一个数组，数组中的元素股票在当天的价格，允许进行多次交易的情况下，设计一个算法找出最大收益
 * （可以多次交易了，约束条件是必须卖了才能再买）
 * 实例1:
 * 输入: [7,1,5,3,6,4]
 * 输出: 7
 * 解释: 第2天买入（价格=1），第3天卖出（价格=5），利润=5-1=4。然后在第4天买入（价格=3），在第5天卖出（价格=6），利润=6-3=3  -> 4+3 =7
 * 实例2:
 * 输入: [1,2,3,4,5]
 * 输出: 4
 * 解释:  第一天买入（价格=1），第五天卖出（价格=5），利润=5-1=4。请注意，您不能在第一天买入，第二天买入，然后再卖出，因为您同时进行多笔交易。你必须先卖掉再买。
 * 实例3:
 * 输入: [7,6,4,3,1]
 * 输出: 0
 * 解析: 在这种情况下，不进行任何交易，即最大利润=0
 * <p>
 * 思路：
 * 最大收益 = sum(前面元素-当前元素 > 0)。
 *
 * @author hanxinghua
 * @create 2024/1/29
 * @since 1.0.0
 */
public class No122BestTimeTradeStockII {

    public static void main(String[] args) {

        int[] arr1 = {7, 1, 5, 3, 6, 4};
        System.out.println(maxProfit(arr1));
    }


    /**
     * 推论出  最大收益 = sum(前面元素-当前元素 > 0)
     *
     * @param arr
     * @return
     */
    public static int maxProfit(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        int sum = 0;
        for (int i = 1; i < arr.length; i++) {
            sum += Math.max(0, arr[i] - arr[i - 1]);
        }
        return sum;
    }


}
