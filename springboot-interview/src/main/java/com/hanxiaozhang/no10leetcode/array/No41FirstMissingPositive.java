package com.hanxiaozhang.no10leetcode.array;

import java.util.HashSet;
import java.util.Set;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * 给一个未排序的整数数组，寻找缺失的最小正整数
 * 实例1:
 * 输入: [1,2,0]
 * 输出: 3
 * 实例2:
 * 输入: [3,4,-1,1]
 * 输出: 2
 * 实例3:
 * 输入: [7,8,9,11,12]
 * 输出: 1
 * <p>
 * 思路：
 * 1. 循环将数组放入Set中
 * 2. 循环从1开始判断Set是否添加该元素，可以添加直接返回。否则继续循环。
 * 3. 如果循环都没有返回set.size() + 1 
 *
 * @author hanxinghua
 * @create 2024/1/13
 * @since 1.0.0
 */
public class No41FirstMissingPositive {

    public static void main(String[] args) {

        int[] nums1 = {1, 2, 0};

        int[] nums2 = {3, 4, -1, 1};

        int[] nums3 = {7, 8, 9, 11, 12};

        int[] nums4 = {1, 2, 3};



        System.out.println(method1(nums1));
        System.out.println(method1(nums2));
        System.out.println(method1(nums3));
        System.out.println(method1(nums4));
    }


    /**
     * 方法1
     *
     * @param nums
     * @return
     */
    public static int method1(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) {
                set.add(nums[i]);
            }
        }
        //  边界条件  i<= nums.length  和  return  set.size() + 1   只有nums4可以用上
        for (int i = 1; i <= nums.length; i++) {
            // Tips：Set.add() 如果Set集合中不包含要添加的对象，则添加对象并返回true，否则返回false
            if (set.add(i)) {
                return i;
            }
        }
        return set.size() + 1;
    }


}
