package com.hanxiaozhang.no10leetcode.array;

import java.util.HashMap;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * 给一个非空正整数数组，除了一个特定元素外，其他每个元素出现三次，找出这个特定元素。
 * 算法要求线性时间，且不适用额外存储空间。
 * 实例1:
 * 输入: [2,2,3,2]
 * 输出: 3
 * 实例2:
 * 输入: [0,1,0,1,0,1,99]
 * 输出: 99
 * <p>
 * 思路：用的笨方法
 * 1. 使用Map，存储 Map<元素,元素出现次数>
 * 2. 遍历Map中value==1，返回该key
 *
 * @author hanxinghua
 * @create 2024/1/30
 * @since 1.0.0
 */
public class No137SingleNumberII {

    public static void main(String[] args) {

        int[] nums1 = {2, 2, 3, 2};
        int[] nums2 = {0, 1, 0, 1, 0, 1, 99};

        System.out.println(method1(nums1));
        System.out.println(method1(nums2));

    }

    /**
     * 方法1
     *
     * @param nums
     * @return
     */
    private static int method1(int[] nums) {

        Integer result = 0;

        Map<Integer, Integer> map = new HashMap<>(16);

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                map.put(nums[i], map.get(nums[i]) + 1);
            } else {
                map.put(nums[i], 1);
            }
        }

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                result = entry.getKey();
                break;
            }
        }

        return result;
    }

}
