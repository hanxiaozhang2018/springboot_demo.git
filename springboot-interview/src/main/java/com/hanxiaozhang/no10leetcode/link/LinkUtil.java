package com.hanxiaozhang.no10leetcode.link;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈链表工具类〉
 *
 * @author hanxinghua
 * @create 2024/1/31
 * @since 1.0.0
 */
public class LinkUtil {


    /**
     * 循环打印
     *
     * @param head
     */
    public static void printLink(Node head) {
        List<String> list = new ArrayList<>();
        System.out.println("----------");
        while (head != null) {
            list.add(String.valueOf(head.data));
            head = head.next;
        }
        System.out.println(String.join(" -> ", list));
    }
}
