package com.hanxiaozhang.no10leetcode.link;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * 给一个已排序的链表，删除所有重复的元素使得每个元素只出现一次
 * 实例1:
 * 输入: 1->1->2
 * 输出: 1->2
 * 实例2:
 * 输入: 1->1->2->3->3
 * 输出: 1->2->3
 *
 * @author hanxinghua
 * @create 2024/1/31
 * @since 1.0.0
 */
public class No83RemoveDuplicatesFromSortedList {


    public static void main(String[] args) {


        Node<Integer> head1 = new Node(1);
        head1.next = new Node(1);
        head1.next.next = new Node(2);


        Node head2 = new Node(1);
        head2.next = new Node(1);
        head2.next.next = new Node(2);
        head2.next.next.next = new Node(3);
        head2.next.next.next.next = new Node(3);

        Node node = method1(head2);

        LinkUtil.printLink(node);

    }

    private static Node method1(Node<Integer> head) {
        // 傀儡头节点
        Node dummyHead = new Node(-1);
        dummyHead.next = head;
        Node cur = dummyHead;
        //  cur != null 是无效条件，应该去除
        while (cur != null && cur.next != null) {
            // 如果当前节点值等于后继节点值，删除后继节点
            if (cur.data.equals(cur.next.data)) {
                cur.next = cur.next.next;
                // 否则，将当前节点后移一位
            } else {
                cur = cur.next;
            }
        }
        return dummyHead.next;
    }
}
