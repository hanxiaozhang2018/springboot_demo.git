package com.hanxiaozhang.no2thread;

/**
 * 一道java的面试题：  D
 * 当编译并运行下面程序时会发生什么结果：
 * a)	编译错误，指明run方法没有定义
 * b)	运行错误，指明run方法没有定义
 * c)	编译通过并输出0到9
 * d)	编译通过但无输出
 * <p>
 * d
 * <p>
 * 2018-12-12
 *
 * @author hxh
 */

/**
 * 功能描述: <br>
 * 〈〉
 * 一道java的面试题：  D
 * 当编译并运行下面程序时会发生什么结果：
 * a)	编译错误，指明run方法没有定义
 * b)	运行错误，指明run方法没有定义
 * c)	编译通过并输出0到9
 * d)	编译通过但无输出
 *
 * @Author:hanxinghua
 * @Date: 2018/12/12
 */
public class No3Example extends Thread {

    public static void main(String[] args) {
        No3Example b = new No3Example();
        b.run();
        // b.start();
    }


    @Override
    public void start() {
        for (int i = 0; i < 10; i++) {
            System.out.print("i= " + i);
        }
    }

}
