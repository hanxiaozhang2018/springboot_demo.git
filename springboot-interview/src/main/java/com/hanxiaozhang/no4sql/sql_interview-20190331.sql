/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50556
Source Host           : localhost:3306
Source Database       : sql_interview

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2019-03-31 20:34:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hrmresource
-- ----------------------------
DROP TABLE IF EXISTS `hrmresource`;
CREATE TABLE `hrmresource` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `deptid` varchar(20) DEFAULT NULL,
  `superior` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hrmresource
-- ----------------------------
INSERT INTO `hrmresource` VALUES ('1', 'a1', '男', '001', null);
INSERT INTO `hrmresource` VALUES ('2', 'a2', '女', '001', '1');
INSERT INTO `hrmresource` VALUES ('3', 'a3', '男', '002', '1');
INSERT INTO `hrmresource` VALUES ('4', 'a4', '女', '003', '2');
INSERT INTO `hrmresource` VALUES ('5', 'a5', '男', '002', '3');
INSERT INTO `hrmresource` VALUES ('6', 'a6', '男', '003', '2');
INSERT INTO `hrmresource` VALUES ('7', 'a7', '男', '001', '5');

-- ----------------------------
-- Table structure for hrmroles
-- ----------------------------
DROP TABLE IF EXISTS `hrmroles`;
CREATE TABLE `hrmroles` (
  `id` varchar(10) NOT NULL,
  `rolesname` varchar(30) DEFAULT NULL,
  `rolesmembers` varchar(30) DEFAULT NULL,
  `roleslevel` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hrmroles
-- ----------------------------
INSERT INTO `hrmroles` VALUES ('001', '管理员', '1,', '01');
INSERT INTO `hrmroles` VALUES ('002', '开发经理', '2,3,4,', '02');
INSERT INTO `hrmroles` VALUES ('003', '项目经理', '3,4,5,6,', '03');
INSERT INTO `hrmroles` VALUES ('004', '会计', '7,', '04');
INSERT INTO `hrmroles` VALUES ('005', '新员工', '', '05');

-- ----------------------------
-- Table structure for hrm_topic_answer
-- ----------------------------
DROP TABLE IF EXISTS `hrm_topic_answer`;
CREATE TABLE `hrm_topic_answer` (
  `id` int(11) DEFAULT NULL,
  `topic` varchar(1000) DEFAULT NULL,
  `answer` varchar(10000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hrm_topic_answer
-- ----------------------------
INSERT INTO `hrm_topic_answer` VALUES ('1', '统计每个部门男女生人数?(考点：MySQL函数）', 'SELECT   h.deptid AS 部门id,SUM(IF(h.sex=\'男\',1,0)) AS 男生数量,SUM(IF(h.sex=\'女\',1,0)) AS 女生数量   FROM `hrmresource` h   GROUP BY h.deptid;');
INSERT INTO `hrm_topic_answer` VALUES ('2', '查询出人数最多部门id？', 'SELECT  h.deptid AS 人数最多部门,COUNT(h.deptid) AS 人数   FROM `hrmresource` h  GROUP BY h.deptid  ORDER BY  COUNT(h.deptid) DESC LIMIT 0,1;');
INSERT INTO `hrm_topic_answer` VALUES ('3', '当新增加人力信息的时候，请自动更新人力角色信息，添加至“新员工”角色中。（考点：MySQL触发器）', 'CREATE TRIGGER autoUpdate AFTER INSERT ON hrmresource\r\nFOR EACH ROW\r\nBEGIN\r\nUPDATE hrmroles SET rolesmembers=IF(rolesmembers=NULL,CONCAT(new.id,\',\'),CONCAT(rolesmembers,new.id,\',\')) WHERE id=\'005\';\r\nEND');
INSERT INTO `hrm_topic_answer` VALUES ('4', '查询a7所有上级的id，name，sex，deptid（考点：MySQL存储过程、自定义函数）', 'DELIMITER //\r\nCREATE FUNCTION `getParentList`(hrmName varchar(100))   \r\nRETURNS varchar(1000)   \r\nBEGIN   \r\nDECLARE fid varchar(100) default \'\';   \r\nDECLARE str varchar(1000) default \'\';\r\nDECLARE superior1 VARCHAR(100) DEFAULT \'\';\r\n\r\nSET superior1 =(SELECT superior FROM hrmresource  WHERE `name`=hrmName);\r\n\r\nIF superior1 is not null THEN \r\n    SET str=superior1;\r\n		WHILE superior1 is not null  DO   \r\n				SET fid =(SELECT superior  FROM hrmresource WHERE id = superior1);   \r\n				IF fid is not null THEN   \r\n						SET str = concat(str, \',\', fid);   \r\n						SET superior1 = fid;   \r\n				ELSE   \r\n						SET superior1 = fid;   \r\n				END IF;   \r\n		END WHILE;   \r\nEND IF;\r\nreturn str;  \r\nEND //     ----SELECT  h.id,h.`name`,h.sex,h.deptid  FROM hrmresource h WHERE FIND_IN_SET(h.id,`getParentList`(\'a7\'));');
INSERT INTO `hrm_topic_answer` VALUES ('5', '请查询出每个人的角色名称的ID NAME RoleName', 'SELECT  a.id,a.`name`,a.sex,b.rolesname FROM hrmresource a ,(SELECT rolesmembers,rolesname FROM hrmroles )AS b WHERE FIND_IN_SET(a.id,b.rolesmembers);');

-- ----------------------------
-- Table structure for Ideabinder_department
-- ----------------------------
DROP TABLE IF EXISTS `Ideabinder_department`;
CREATE TABLE `Ideabinder_department` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Ideabinder_department
-- ----------------------------
INSERT INTO `Ideabinder_department` VALUES ('1', 'IT');
INSERT INTO `Ideabinder_department` VALUES ('2', 'Sales');

-- ----------------------------
-- Table structure for Ideabinder_emloyee
-- ----------------------------
DROP TABLE IF EXISTS `Ideabinder_emloyee`;
CREATE TABLE `Ideabinder_emloyee` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `salary` varchar(20) DEFAULT NULL,
  `departmentId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Ideabinder_emloyee
-- ----------------------------
INSERT INTO `Ideabinder_emloyee` VALUES ('1', 'joe', '70000', '1');
INSERT INTO `Ideabinder_emloyee` VALUES ('2', 'tom', '80000', '2');
INSERT INTO `Ideabinder_emloyee` VALUES ('3', 'sam', '60000', '2');
INSERT INTO `Ideabinder_emloyee` VALUES ('4', 'max', '90000', '1');
INSERT INTO `Ideabinder_emloyee` VALUES ('5', 'jack', '69000', '1');
INSERT INTO `Ideabinder_emloyee` VALUES ('6', 'car', '85000', '1');

-- ----------------------------
-- Table structure for Ideabinder_topic_answer
-- ----------------------------
DROP TABLE IF EXISTS `Ideabinder_topic_answer`;
CREATE TABLE `Ideabinder_topic_answer` (
  `id` int(11) NOT NULL,
  `topic` varchar(1000) DEFAULT NULL,
  `answer` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Ideabinder_topic_answer
-- ----------------------------
INSERT INTO `Ideabinder_topic_answer` VALUES ('1', '找出每个部门工资前三高的员工?', 'SELECT d.`name` AS departmentName,e1.`name`,e1.Salary FROM Ideabinder_emloyee AS e1,Ideabinder_department AS d \r\n      WHERE e1.DepartmentId=d.Id AND (SELECT COUNT(1) FROM Ideabinder_emloyee e2 WHERE e2.DepartmentId=d.Id AND e2.Salary>e1.Salary)<3\r\n             ORDER BY d.`name`,e1.Salary DESC;');
INSERT INTO `Ideabinder_topic_answer` VALUES ('2', '找出每个部门工资最高的员工？', 'SELECT e1.id,e1.`name`,e1.MaxSalary,d.`name` AS departmentName  FROM Ideabinder_department AS d, (SELECT e.id,e.`name`,e.departmentId,MAX(e.salary) AS MaxSalary from Ideabinder_emloyee e GROUP BY e.departmentId) AS e1 WHERE d.id=e1.departmentId;');

-- ----------------------------
-- Table structure for mintejiaoyu_course
-- ----------------------------
DROP TABLE IF EXISTS `mintejiaoyu_course`;
CREATE TABLE `mintejiaoyu_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='课程表';

-- ----------------------------
-- Records of mintejiaoyu_course
-- ----------------------------
INSERT INTO `mintejiaoyu_course` VALUES ('1', '语文');
INSERT INTO `mintejiaoyu_course` VALUES ('2', '数学');
INSERT INTO `mintejiaoyu_course` VALUES ('3', '英语');

-- ----------------------------
-- Table structure for mintejiaoyu_student
-- ----------------------------
DROP TABLE IF EXISTS `mintejiaoyu_student`;
CREATE TABLE `mintejiaoyu_student` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mintejiaoyu_student
-- ----------------------------
INSERT INTO `mintejiaoyu_student` VALUES ('1', '小李');
INSERT INTO `mintejiaoyu_student` VALUES ('2', '小王');
INSERT INTO `mintejiaoyu_student` VALUES ('3', '小韩');

-- ----------------------------
-- Table structure for mintejiaoyu_stu_course
-- ----------------------------
DROP TABLE IF EXISTS `mintejiaoyu_stu_course`;
CREATE TABLE `mintejiaoyu_stu_course` (
  `id` int(11) NOT NULL,
  `stu_id` int(11) DEFAULT NULL,
  `cou_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stu_id` (`stu_id`),
  KEY `cou_id` (`cou_id`),
  CONSTRAINT `mintejiaoyu_stu_course_ibfk_1` FOREIGN KEY (`stu_id`) REFERENCES `mintejiaoyu_student` (`id`),
  CONSTRAINT `mintejiaoyu_stu_course_ibfk_2` FOREIGN KEY (`cou_id`) REFERENCES `mintejiaoyu_course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生课程表';

-- ----------------------------
-- Records of mintejiaoyu_stu_course
-- ----------------------------
INSERT INTO `mintejiaoyu_stu_course` VALUES ('1', '1', '1');
INSERT INTO `mintejiaoyu_stu_course` VALUES ('2', '1', '2');
INSERT INTO `mintejiaoyu_stu_course` VALUES ('3', '1', '3');
INSERT INTO `mintejiaoyu_stu_course` VALUES ('4', '2', '1');
INSERT INTO `mintejiaoyu_stu_course` VALUES ('5', '2', '2');
INSERT INTO `mintejiaoyu_stu_course` VALUES ('6', '3', '1');

-- ----------------------------
-- Table structure for mintejiaoyu_topic_answer
-- ----------------------------
DROP TABLE IF EXISTS `mintejiaoyu_topic_answer`;
CREATE TABLE `mintejiaoyu_topic_answer` (
  `id` int(11) DEFAULT NULL,
  `topic` varchar(1000) DEFAULT NULL,
  `answer` varchar(10000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mintejiaoyu_topic_answer
-- ----------------------------
INSERT INTO `mintejiaoyu_topic_answer` VALUES ('1', '写出创表语句：', 'CREATE TABLE mintejiaoyu_stu_course(\r\nid INT auto_increment PRIMARY KEY,\r\nstu_id INT,\r\ncou_id INT,\r\nFOREIGN KEY(stu_id) REFERENCES mintejiaoyu_student(id),\r\nFOREIGN KEY(cou_id) REFERENCES mintejiaoyu_course(id),\r\n)ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT ‘学生课程表’;');
INSERT INTO `mintejiaoyu_topic_answer` VALUES ('2', '查询出学习过两门课程的学生列表？', 'SELECT a.`name` FROM  mintejiaoyu_student a,mintejiaoyu_course b,mintejiaoyu_stu_course c WHERE c.cou_id=b.id AND c.stu_id=a.id GROUP BY a.id HAVING COUNT(b.id)>1;');

-- ----------------------------
-- Table structure for zhongzhiqifu_A
-- ----------------------------
DROP TABLE IF EXISTS `zhongzhiqifu_A`;
CREATE TABLE `zhongzhiqifu_A` (
  `studentId` int(11) NOT NULL AUTO_INCREMENT,
  `classId` int(11) DEFAULT NULL,
  PRIMARY KEY (`studentId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zhongzhiqifu_A
-- ----------------------------
INSERT INTO `zhongzhiqifu_A` VALUES ('1', '1');
INSERT INTO `zhongzhiqifu_A` VALUES ('2', '1');
INSERT INTO `zhongzhiqifu_A` VALUES ('3', '2');
INSERT INTO `zhongzhiqifu_A` VALUES ('4', '2');
INSERT INTO `zhongzhiqifu_A` VALUES ('5', '2');
INSERT INTO `zhongzhiqifu_A` VALUES ('6', '3');
INSERT INTO `zhongzhiqifu_A` VALUES ('7', '4');

-- ----------------------------
-- Table structure for zhongzhiqifu_B
-- ----------------------------
DROP TABLE IF EXISTS `zhongzhiqifu_B`;
CREATE TABLE `zhongzhiqifu_B` (
  `studentId` int(11) DEFAULT NULL,
  `courseName` varchar(30) DEFAULT NULL,
  `score` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zhongzhiqifu_B
-- ----------------------------
INSERT INTO `zhongzhiqifu_B` VALUES ('1', '语文', '80');
INSERT INTO `zhongzhiqifu_B` VALUES ('2', '数学', '65');
INSERT INTO `zhongzhiqifu_B` VALUES ('3', '语文', '81');
INSERT INTO `zhongzhiqifu_B` VALUES ('3', '数学', '70');
INSERT INTO `zhongzhiqifu_B` VALUES ('4', '语文', '99');
INSERT INTO `zhongzhiqifu_B` VALUES ('4', '数学', '20');
INSERT INTO `zhongzhiqifu_B` VALUES ('5', '数学', '70');
INSERT INTO `zhongzhiqifu_B` VALUES ('5', '语文', '100');
INSERT INTO `zhongzhiqifu_B` VALUES ('6', '数学', '65');
INSERT INTO `zhongzhiqifu_B` VALUES ('6', '体育', '33');
INSERT INTO `zhongzhiqifu_B` VALUES ('6', '历史', '89');
INSERT INTO `zhongzhiqifu_B` VALUES ('7', 'Java', '85');
INSERT INTO `zhongzhiqifu_B` VALUES ('1', '数学', '90');
INSERT INTO `zhongzhiqifu_B` VALUES ('1', '英语', '60');
INSERT INTO `zhongzhiqifu_B` VALUES ('2', '语文', '100');
INSERT INTO `zhongzhiqifu_B` VALUES ('2', '英语', '66');

-- ----------------------------
-- Table structure for zhongzhiqifu_topic_answer
-- ----------------------------
DROP TABLE IF EXISTS `zhongzhiqifu_topic_answer`;
CREATE TABLE `zhongzhiqifu_topic_answer` (
  `id` int(11) DEFAULT NULL,
  `topic` varchar(1000) DEFAULT NULL,
  `answer` varchar(10000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zhongzhiqifu_topic_answer
-- ----------------------------
INSERT INTO `zhongzhiqifu_topic_answer` VALUES ('1', '1.查询每个班每门课程的最低成绩？', 'SELECT a.classId,b.courseName,MIN(b.score ) FROM  zhongzhiqifu_A AS a,zhongzhiqifu_B AS b WHERE a.studentId=b.studentId  GROUP BY a.classId,b.courseName;');
INSERT INTO `zhongzhiqifu_topic_answer` VALUES ('2', '2.查询每个班的课程个数？', 'SELECT a.classId,COUNT(DISTINCT(courseName)) FROM  zhongzhiqifu_A AS a,zhongzhiqifu_B AS b WHERE a.studentId=b.studentId  GROUP BY a.classId;');

-- ----------------------------
-- Function structure for getParentList
-- ----------------------------
DROP FUNCTION IF EXISTS `getParentList`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `getParentList`(hrmName varchar(100)) RETURNS varchar(1000) CHARSET utf8
BEGIN   
DECLARE fid varchar(100) default '';   
DECLARE str varchar(1000) default '';
DECLARE superior1 VARCHAR(100) DEFAULT '';

SET superior1 =(SELECT superior FROM hrmresource  WHERE `name`=hrmName);

IF superior1 is not null THEN 
    SET str=superior1;
		WHILE superior1 is not null  DO   
				SET fid =(SELECT superior  FROM hrmresource WHERE id = superior1);   
				IF fid is not null THEN   
						SET str = concat(str, ',', fid);   
						SET superior1 = fid;   
				ELSE   
						SET superior1 = fid;   
				END IF;   
		END WHILE;   
END IF;
return str;  
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `autoUpdate`;
DELIMITER ;;
CREATE TRIGGER `autoUpdate` AFTER INSERT ON `hrmresource` FOR EACH ROW BEGIN
UPDATE hrmroles SET rolesmembers=IF(rolesmembers=NULL,CONCAT(new.id,','),CONCAT(rolesmembers,new.id,',')) WHERE id='005';
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `autoDelete`;
DELIMITER ;;
CREATE TRIGGER `autoDelete` AFTER DELETE ON `hrmresource` FOR EACH ROW BEGIN
UPDATE hrmroles  SET rolesmembers=REPLACE(rolesmembers,CONCAT(old.id,','),'') WHERE  id='005';
END
;;
DELIMITER ;
