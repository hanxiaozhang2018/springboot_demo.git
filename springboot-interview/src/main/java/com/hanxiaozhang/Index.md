# 目录
+ 



# 算法重点看：
+ lru算法
com.hanxiaozhang.no3algorithm.No3Lru

+ 三进制相加
com.hanxiaozhang.no3algorithm.No4TernaryAdd


+ 0-1背包
com.hanxiaozhang.no3algorithm.dynamicprogramming.No1ZeroOnePack
+ 最大子字段和
com.hanxiaozhang.no3algorithm.dynamicprogramming.No2MaxSubArraySum



+ 两数之和（HashMap<数组中元素值,组的索引值> + 循环把元素放入到HashMap中，并且每次都跟已经存入的数据做比较）：
com.hanxiaozhang.no10leetcode.array.No1TwoSum
+ 接雨水（双指针）：
com.hanxiaozhang.no10leetcode.No42ContainerWithMostWater



+ N皇后
com.hanxiaozhang.no3algorithm.backtrack.No1NQueen