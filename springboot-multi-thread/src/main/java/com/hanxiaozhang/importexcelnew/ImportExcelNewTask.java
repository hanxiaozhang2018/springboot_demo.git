package com.hanxiaozhang.importexcelnew;


import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 〈功能描述〉<br>
 * 〈导入Excel任务〉
 *
 * @author hanxinghua
 * @create 2020/2/23
 * @since 1.0.0
 */
@Slf4j
public class ImportExcelNewTask<T> implements Callable<ErrorInfoEntity> {

    /**
     * 保存Excel服务
     */
    private SaveExcelNewService excelService;

    /**
     * 数据集合
     */
    private List<T> list;


    /**
     * 屏障
     */
    private CyclicBarrier barrier;

    /**
     * 回滚标识
     */
    private AtomicBoolean rollbackFlag;


    /**
     * 构造函数
     *
     * @param excelService
     * @param list
     * @param barrier
     * @param rollbackFlag
     */
    public ImportExcelNewTask(SaveExcelNewService<T> excelService, List<T> list, CyclicBarrier barrier, AtomicBoolean rollbackFlag) {

        this.excelService = excelService;
        this.list = list;
        this.barrier = barrier;
        this.rollbackFlag = rollbackFlag;
    }


    @Override
    public ErrorInfoEntity call() throws Exception {

        return excelService.batchSave(list, barrier,rollbackFlag);
    }


}
