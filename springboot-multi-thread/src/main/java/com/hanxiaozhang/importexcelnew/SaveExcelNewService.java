package com.hanxiaozhang.importexcelnew;

import java.util.List;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * 〈一句话功能简述〉<br>
 * 〈保存ExcelService〉
 *
 * @author hanxinghua
 * @create 2021/12/12
 * @since 1.0.0
 */

public interface SaveExcelNewService<T> {


    /**
     * 批量保存
     *
     * @param list
     * @param barrier
     * @param rollbackFlag
     * @return
     * @throws Exception
     */
    ErrorInfoEntity batchSave(List<T> list, CyclicBarrier barrier, AtomicBoolean rollbackFlag) throws Exception;

}
