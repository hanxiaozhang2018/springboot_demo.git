package com.hanxiaozhang.threadbase1ndedition.no1synchronizedkeyword;

/**
 * 〈一句话功能简述〉<br>
 * 〈synchronized 不具备继承性〉
 *
 * @author hanxinghua
 * @create 2021/11/9
 * @since 1.0.0
 */
public class No7SynchNoExtends {

    public static class Parent {

        public synchronized void method() {
            System.out.println("Parent.method begin threadName = "
                    + Thread.currentThread().getName() + " time = "
                    + System.currentTimeMillis());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Parent.method end threadName = "
                    + Thread.currentThread().getName() + " time = "
                    + System.currentTimeMillis());
        }

    }


    public static class Son extends Parent {

        @Override
        public /*synchronized*/ void method()  {
            try {
                System.out.println("Son.method begin threadName = "
                        + Thread.currentThread().getName() + " time = "
                        + System.currentTimeMillis());
                Thread.sleep(5000);
                System.out.println("Son.method end threadName = "
                        + Thread.currentThread().getName() + " time = "
                        + System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            super.method();
        }

    }


    public static void main(String[] args) {

        Son son = new Son();

        new Thread(son::method,"THREAD-1").start();
        new Thread(son::method,"THREAD-2").start();

    }



}
