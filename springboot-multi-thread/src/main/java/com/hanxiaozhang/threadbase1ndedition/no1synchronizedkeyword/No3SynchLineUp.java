package com.hanxiaozhang.threadbase1ndedition.no1synchronizedkeyword;

/**
 * 〈一句话功能简述〉<br>
 * 〈synchronized排队运行,即按顺序运行〉
 * <p>
 * 添加synchronized关键字：
 * begin method threadName = THREAD-1
 * end
 * begin method threadName = THREAD-2
 * end
 * 不添加synchronized关键字：
 * begin method threadName = THREAD-1
 * begin method threadName = THREAD-2
 * end
 * end
 *
 * @author hanxinghua
 * @create 2021/11/9
 * @since 1.0.0
 */
public class No3SynchLineUp {

    public static synchronized void method() {
        try {
            System.out.println("begin method threadName = "
                    + Thread.currentThread().getName());
            Thread.sleep(10);
            System.out.println("end");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        // No3SynchLineUp.method();
        new Thread(No3SynchLineUp::method, "THREAD-1").start();
        new Thread(No3SynchLineUp::method, "THREAD-2").start();

    }
}
