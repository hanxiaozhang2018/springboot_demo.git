package com.hanxiaozhang.threadbase1ndedition.no1synchronizedkeyword;

/**
 * 〈一句话功能简述〉<br>
 * 〈synchronized 可重入锁〉
 * <p>
 * 自己可以再次获取自己的内部锁,以下例子
 * 子父类继承的环境下
 *
 * @author hanxinghua
 * @create 2021/11/9
 * @since 1.0.0
 */
public class No5SynchReentrant {


    public static void main(String[] args) {

        No5SynchReentrant example = new No5SynchReentrant();
        new Thread(example::method1).start();

    }

    public synchronized void method1() {
        System.out.println("method1 start! ");
        this.method2();
    }

    public synchronized void method2() {
        System.out.println("method2 start! ");
    }

}
