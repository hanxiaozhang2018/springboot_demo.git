package com.hanxiaozhang.threadbase1ndedition.no1synchronizedkeyword;

/**
 * 〈一句话功能简述〉<br>
 * 〈同步与非同步方法是否可以同时调用？ 可以〉
 *
 * @author hanxinghua
 * @create 2021/11/8
 * @since 1.0.0
 */
public class No4SynchAndNonSynchMethod {



    public static void main(String[] args) {
        No4SynchAndNonSynchMethod t = new No4SynchAndNonSynchMethod();

        new Thread(t::method1, "THREAD-1").start();
        new Thread(t::method2, "THREAD-2").start();

    }

    /**
     * 同步方法
     */
    public synchronized void method1() {
        System.out.println(Thread.currentThread().getName() + " method1 start...");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " method1 end");
    }

    /**
     * 非同步方法
     */
    public void method2() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " method2 ");
    }

}
