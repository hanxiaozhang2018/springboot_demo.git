package com.hanxiaozhang.threadbase1ndedition.no3compareandswap;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 〈一句话功能简述〉<br>
 * 〈CAS在java中的应用〉
 *
 * @author hanxinghua
 * @create 2021/11/9
 * @since 1.0.0
 */
public class No1AtomicInteger {

    AtomicInteger count = new AtomicInteger(0);

    public void method() {
        for (int i = 0; i < 10000; i++) {

            count.incrementAndGet();
        }
    }

    public static void main(String[] args) {
        No1AtomicInteger example = new No1AtomicInteger();

        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            threads.add(new Thread(example::method, "THREAD-" + i));
        }

        threads.forEach((o) -> o.start());

        threads.forEach((o) -> {
            try {
                o.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        System.out.println(example.count);

    }
}
