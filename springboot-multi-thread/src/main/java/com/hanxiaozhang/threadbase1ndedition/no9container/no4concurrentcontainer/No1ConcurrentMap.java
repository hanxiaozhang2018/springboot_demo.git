package com.hanxiaozhang.threadbase1ndedition.no9container.no4concurrentcontainer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

/**
 * 功能描述: <br>
 * 〈ConcurrentMap〉
 * <p>
 * 这个就是需要了解一下1.8源码
 * https://blog.csdn.net/programmer_at/article/details/79715177
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No1ConcurrentMap {


    public static void main(String[] args) {

        Map<String, String> map = new ConcurrentHashMap<>();

        Random random = new Random();
        Thread[] threads = new Thread[100];
        CountDownLatch latch = new CountDownLatch(threads.length);

        long start = System.currentTimeMillis();
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(() -> {
                for (int j = 0; j < 10000; j++) {
                    map.put(UUID.randomUUID().toString(), "a" + random.nextInt(100000));
                }
                latch.countDown();
            });
        }
        Arrays.asList(threads).forEach(t -> t.start());
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("执行时间：" + (end - start));
        System.out.println(map.size());

    }
}
