package com.hanxiaozhang.threadbase1ndedition.no9container.no4concurrentcontainer;

import java.util.Arrays;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CountDownLatch;

/**
 * 功能描述: <br>
 * 〈ConcurrentSkipListMap〉
 * <p>
 *  ConcurrentSkipListMap它替代了TreeMap
 * <p>
 * http://blog.csdn.net/sunxianghuang/article/details/52221913 文章不太好，这个源码先放弃了 20211213
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No2ConcurrentSkipListMap {


    public static void main(String[] args) {

        Map<String, String> map = new ConcurrentSkipListMap<>();

        Random r = new Random();
        Thread[] ths = new Thread[100];
        CountDownLatch latch = new CountDownLatch(ths.length);
        long start = System.currentTimeMillis();
        for (int i = 0; i < ths.length; i++) {
            ths[i] = new Thread(() -> {
                for (int j = 0; j < 10000; j++) {
                    map.put("a" + r.nextInt(100000), "a" + r.nextInt(100000));
                }
                latch.countDown();
            });
        }

        Arrays.asList(ths).forEach(t -> t.start());
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println(end - start);
        System.out.println(map.size());

    }
}
