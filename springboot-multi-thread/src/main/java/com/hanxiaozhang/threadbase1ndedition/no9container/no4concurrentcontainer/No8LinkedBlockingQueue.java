package com.hanxiaozhang.threadbase1ndedition.no9container.no4concurrentcontainer;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 〈LinkedBlockingQueue〉
 *
 * 可选择的有边界队列。
 * 使用ReentrantLock实现线程安全。
 * 支持读写同时操作，入队使用一把锁，出队使用一把锁。
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No8LinkedBlockingQueue {

	private static LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<>();

	private static Random random = new Random();

	public static void main(String[] args) {
		new Thread(() -> {
			for (int i = 0; i < 100; i++) {
				try {
					//如果满了，就会等待
					queue.put("a" + i);
					TimeUnit.MILLISECONDS.sleep(random.nextInt(1000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}, "p1").start();

		for (int i = 0; i < 5; i++) {
			new Thread(() -> {
				for (;;) {
					try {
						//如果空了，就会等待
						System.out.println(Thread.currentThread().getName() + " take -" + queue.take());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}, "c" + i).start();

		}
	}
}
