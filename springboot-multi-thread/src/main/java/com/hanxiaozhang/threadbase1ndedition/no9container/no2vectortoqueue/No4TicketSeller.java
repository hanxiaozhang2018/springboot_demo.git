package com.hanxiaozhang.threadbase1ndedition.no9container.no2vectortoqueue;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 功能描述: <br>
 * 〈使用ConcurrentLinkedQueue〉
 * <p>
 * 正确
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No4TicketSeller {
    static Queue<String> tickets = new ConcurrentLinkedQueue<>();

    static {
        for (int i = 0; i < 1000; i++) {
            tickets.add("票编号：" + i);
        }
    }

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (true) {
                    String s = tickets.poll();
                    if (s == null) {
                        break;
                    } else {
                        System.out.println("销售了--" + s);
                    }
                }
            }).start();
        }
    }
}
