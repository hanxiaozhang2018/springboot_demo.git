package com.hanxiaozhang.threadbase1ndedition.no9container.no2vectortoqueue;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述: <br>
 * 〈使用ArrayList〉
 * 有N张火车票，每张票都有一个编号，同时有10个窗口对外售票。请写一个模拟程序。
 * <p>
 * 问题：ArrayList线程不安全，并且tickets.size()与tickets.remove()之间不是原子操作
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No1TicketSeller {

    static List<String> tickets = new ArrayList<>();

    static {
        for (int i = 0; i < 10000; i++) {
            tickets.add("票编号：" + i);
        }
    }


    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (tickets.size() > 0) {
                    System.out.println("销售了--" + tickets.remove(0));
                }
            }).start();
        }
    }
}
