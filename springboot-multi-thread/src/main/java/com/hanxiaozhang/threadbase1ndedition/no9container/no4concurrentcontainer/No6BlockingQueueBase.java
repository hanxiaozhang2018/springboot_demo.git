package com.hanxiaozhang.threadbase1ndedition.no9container.no4concurrentcontainer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈阻塞队列〉
 *
 * @author hanxinghua
 * @create 2021/12/5
 * @since 1.0.0
 */
public class No6BlockingQueueBase {

    public static void main(String[] args) throws InterruptedException {

        BlockingQueue queue = new ArrayBlockingQueue(3);
        // 添加元素：
        // 1.添加空元素，报NullPointerException
        System.out.println("---- 添加null元素");
        // queue.add(null);
        // queue.offer(null);
        // queue.put(null);

        // 2.正常添加元素：
        System.out.println("---- 正常添加元素");
        queue.add("a");
        queue.offer("b");
        queue.put("c");

        // 3.在队列满的情况下，添加元素：
        System.out.println("---- 在队列满的情况下，添加元素");
        // 抛IllegalStateException: Queue full
        // queue.add("d");
        // 添加失败，返回 false
        System.out.println(queue.offer("d"));
        // 设置一个等待时间，如果超时，添加失败，返回 false
        System.out.println(queue.offer("d",2, TimeUnit.MILLISECONDS));
        // 队列满了一直阻塞
        // queue.put("d");
        System.out.println(queue);

        // peek()
        System.out.println("---- peek()得到头部元素，但是不移除元素");
        System.out.println(queue.peek());
        System.out.println(queue);

        // poll()
        System.out.println("---- poll()得到头部元素，并且不移除元素");
        System.out.println(queue.poll());
        System.out.println(queue);
        System.out.println(queue.poll(2,TimeUnit.MILLISECONDS));
        System.out.println(queue);


        // take()
        System.out.println("---- take()得到头部元素，并且不移除元素，队列为空的时候阻塞");
        System.out.println(queue.take());
        System.out.println(queue);

        // 队列为空时
        System.out.println("---- 队列为空时");
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        // 当前队列为空，会阻塞到这里
//        System.out.println(queue.take());
//        System.out.println(queue);


    }

}
