package com.hanxiaozhang.threadbase1ndedition.no9container.no2vectortoqueue;

import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 〈使用Vector或Collections.synchronizedXXX〉
 * 有N张火车票，每张票都有一个编号，同时有10个窗口对外售票。请写一个模拟程序。
 * <p>
 * 问题： tickets.size()与tickets.remove()之间不是原子操作
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No2TicketSeller {
    static Vector<String> tickets = new Vector<>();


    static {
        for (int i = 0; i < 1000; i++) {
            tickets.add("票编号：" + i);
        }
    }

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (tickets.size() > 0) {

                    try {
                        TimeUnit.MILLISECONDS.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    System.out.println("销售了--" + tickets.remove(0));
                }
            }).start();
        }
    }
}
