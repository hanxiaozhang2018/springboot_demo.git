package com.hanxiaozhang.threadbase1ndedition.no9container.no3synchronizecontainer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 功能描述: <br>
 * 〈Collections.synchronizedList的简单使用〉
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No4SynchronizedList {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		List<String> syncList = Collections.synchronizedList(list);
		syncList.add("a");

	}
}
