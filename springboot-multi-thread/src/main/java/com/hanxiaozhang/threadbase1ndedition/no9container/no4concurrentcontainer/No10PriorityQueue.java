package com.hanxiaozhang.threadbase1ndedition.no9container.no4concurrentcontainer;

import java.util.PriorityQueue;

/**
 * 功能描述: <br>
 * 〈PriorityQueue〉
 * 带有优先级的阻塞队列，它是在取数据的时候遵循优先级。
 * 无界的队列，没有长度限制，但是你在不指定长度的时候，默认长度是11，
 * 随着数据的加入，底层自动扩容。
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No10PriorityQueue {


    public static void main(String[] args) {
        PriorityQueue<String> queue = new PriorityQueue<>();

        queue.add("c");
        queue.add("e");
        queue.add("a");
        queue.add("d");
        queue.add("z");
        // 这里是无序的
        System.out.println(queue);

        for (int i = 0; i < 5; i++) {
            System.out.print(queue.poll() + " ");
        }

    }
}
