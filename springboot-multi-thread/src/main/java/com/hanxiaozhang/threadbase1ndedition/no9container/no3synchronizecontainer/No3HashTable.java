package com.hanxiaozhang.threadbase1ndedition.no9container.no3synchronizecontainer;

import java.util.Hashtable;

/**
 * 〈一句话功能简述〉<br>
 * 〈HashTable〉
 *
 * @author hanxinghua
 * @create 2021/12/4
 * @since 1.0.0
 */
public class No3HashTable {


    /*
     Hashtable是同步，安全线程，但速度慢
     Hashtable的key value 不可以null
     Hashtable默认容量大小是11
     Hashtable 直接使用 hashCode
     */

    public static void main(String[] args) {

        Hashtable<String, String> map = new Hashtable<>();
        map.put(null, null);
        map.put("null", null);

    }

}
