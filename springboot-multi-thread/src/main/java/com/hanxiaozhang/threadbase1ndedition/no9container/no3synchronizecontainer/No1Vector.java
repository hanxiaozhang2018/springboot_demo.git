package com.hanxiaozhang.threadbase1ndedition.no9container.no3synchronizecontainer;

import java.util.Vector;

/**
 * 〈一句话功能简述〉<br>
 * 〈Vector〉
 *
 *
 * @author hanxinghua
 * @create 2021/12/4
 * @since 1.0.0
 */
public class No1Vector {

    /*
    Vector线程安全，每个方法使用synchronized关键字修饰，效率低已经废弃。
    Vector底层使用的数组(Object[] elementData)，初始化容量是10。
    Vector数组可扩容倍数是2倍(默认情况下)，但也可以指定(int initialCapacity, int capacityIncrement)
     */

    public static void main(String[] args) {

        Vector<Integer> vector = new Vector();
        vector.add(1);
        vector.add(2);
        vector.add(3);
        vector.add(4);
        vector.add(5);
        vector.add(6);
        vector.add(7);
        vector.add(8);
        vector.add(9);
        vector.add(10);
        vector.add(11);
        vector.add(12);
        vector.add(13);
        vector.add(14);
        vector.add(15);
        vector.add(16);
        vector.add(17);
        vector.add(18);
        vector.add(19);
        vector.add(20);
        vector.add(21);
    }
}
