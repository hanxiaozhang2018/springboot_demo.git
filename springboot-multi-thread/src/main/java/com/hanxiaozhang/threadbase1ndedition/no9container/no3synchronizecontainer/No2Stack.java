package com.hanxiaozhang.threadbase1ndedition.no9container.no3synchronizecontainer;

import java.util.Stack;

/**
 * 〈一句话功能简述〉<br>
 * 〈栈〉
 *
 * @author hanxinghua
 * @create 2021/12/4
 * @since 1.0.0
 */
public class No2Stack {

    /*
    Stack是Vector的子类
    Stack底层是一个数组 Object[] elementData
    Stack初始化数组大小为10
     */

    public static void main(String[] args) {

        Stack<Integer> stack = new Stack<>();
        // 添加元素
        stack.push(1);
        stack.push(2);
        stack.push(3);
        // 检查栈顶元素
        System.out.println("检查栈顶元素: " + stack.peek());
        // 弹出栈顶元素
        System.out.println("弹出栈顶元素: " + stack.pop());
        // 判断栈是否为空
        System.out.println("判断栈是否为空: " + stack.empty());

    }
}
