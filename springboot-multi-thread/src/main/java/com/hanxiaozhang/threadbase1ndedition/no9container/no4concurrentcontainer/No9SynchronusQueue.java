package com.hanxiaozhang.threadbase1ndedition.no9container.no4concurrentcontainer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;

/**
 * 功能描述: <br>
 * 〈SynchronusQueue〉
 * <p>
 * 特点：
 * i.容量为0，即没有容量，put()添加一个元素后，会等待task()删除一个元素。
 * ii.使用add() 会报 Exception in thread "main" java.lang.IllegalStateException: Queue full
 * 优点：
 * 提高的进行线程间数据的传输效率，不会产生队列数据争抢问题。
 * 底层：
 * SynchronousQueue使用两个队列(一个用于正在等待的生产者、另一个用于正在等待的消费者)和一个用来保护两个队列的锁
 *
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No9SynchronusQueue {


    public static void main(String[] args) throws InterruptedException {

        // 容量为0
        SynchronousQueue<String> queue = new SynchronousQueue<>();

        // 启动一个线程获取数据
        new Thread(() -> {
            while (true){
                try {
                    System.out.println(queue.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        // 如果添加一个元素，现在没有消费者去消费，就一直阻塞。
        queue.put("a");
//        // 如果再添加一个元素，现在没有消费者去消费，就一直阻塞。
//        queue.put("b");
//        // 容量为0，不能添加元素，报 Exception in thread "main" java.lang.IllegalStateException: Queue full
//        queue.add("c");

        System.out.println(queue.size());
    }
}
