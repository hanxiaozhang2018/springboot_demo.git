package com.hanxiaozhang.threadbase1ndedition.no9container.no1map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/11/20
 * @since 1.0.0
 */
public class Constants {

    public static final int COUNT = 10_0000;
    public static final int THREAD_COUNT = 100;
}
