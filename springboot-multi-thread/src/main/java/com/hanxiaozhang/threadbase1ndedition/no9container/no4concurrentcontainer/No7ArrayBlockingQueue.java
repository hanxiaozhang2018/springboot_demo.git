package com.hanxiaozhang.threadbase1ndedition.no9container.no4concurrentcontainer;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 〈ArrayBlockingQueue〉
 * <p>
 * 有边界队列，可以选择按FIFO顺序处理添加或删除（ ArrayBlockingQueue(int capacity, boolean fair) ）。
 * 底层是一个数组。
 * 使用ReentrantLock实现线程安全。
 * 不支持读写同时操作，原因是底层使用同一把锁控制进队出队。
 *
 * @Author:hanxinghua
 * @Date: 2021/11/21
 */
public class No7ArrayBlockingQueue {

    private static ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<>(10);

    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i < 10; i++) {
            queue.put("a" + i);
        }
        // 满了就会等待，程序阻塞
        queue.put("aaa");
        System.out.println(queue);

    }
}
