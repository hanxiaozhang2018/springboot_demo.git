package com.hanxiaozhang.threadbase1ndedition.no9container.no4concurrentcontainer;

import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 〈一句话功能简述〉<br>
 * 〈CopyOnWriteArraySet〉
 * <p>
 * 内部new CopyOnWriteArrayList
 * add()是调用的CopyOnWriteArrayList.addIfAbsent(e)
 *
 * @author hanxinghua
 * @create 2021/12/4
 * @since 1.0.0
 */
public class No4CopyOnWriteArraySet {

    public static void main(String[] args) {
        CopyOnWriteArraySet<Integer> set = new CopyOnWriteArraySet<>();
        set.add(1);
        set.add(2);
        set.add(2);
        System.out.println(set);
    }

}
