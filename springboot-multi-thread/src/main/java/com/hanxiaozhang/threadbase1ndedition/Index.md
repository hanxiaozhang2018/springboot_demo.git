# 多线程与高并发第一版 
## no1synchronizedkeyword synchronized
+ No1NonThreadSafe 非线程安全举例
+ No2DirtyRead 脏读举例
+ No3SynchLineUp synchronized排队运行
+ No4SynchAndNonSynchMethod 同步与非同步方法是否可以同时调用？可以
+ No5SynchReentrant synchronized可重入锁
+ No6SynchExceptionRelease 锁异常自动释放
+ No7SynchNoExtends synchronized不具备继承性
+ No8SynchAtomic synchronized原子性

## no2volatilekeyword volatile
+ No1VolatileVisibility volatile的可见性
+ No2VolatileDisOrder volatile的禁止重排序
+ No3VolatileNoAtomic volatile不具备原子性


## no3compareandswap CAS
+ No1AtomicInteger CAS在java中的应用

## no4lock 锁
+ No1ReentrantLockBase ReentrantLock的基本使用
+ No2ReentrantLockTryLock ReentrantLock的tryLock
+ No3ReentrantLockLockInterruptibly ReentrantLock的lockInterruptibly
+ No4ReentrantLockFair ReentrantLock还可以指定为公平锁
+ No5ReentrantLockAwaitUninterruptibly
+ No6ReentrantLockAwaitUntil 
+ No10ReadWriteLock 读写锁

## no5juccommonclass juc常见类
+ No1LongAdder LongAdder的使用
+ No2CountDownLatch CountdownLatch门闩，闭锁
+ No3CyclicBarrier CyclicBarrier循环栅栏
+ No4Phaser Phaser移相器
+ No5Semaphore Semaphore信号量
+ No6Exchanger Exchanger交换器
+ No7LockSupport LockSupport

## no6threadlocal ThreadLocal
+ No1ThreadLocal  ThreadLocal

## no8fourreference 四种引用关系
+ No1StrongReference 强引用
+ No2SoftReference 软引用
+ No3WeakReference 弱引用 
+ No4PhantomReference 需引用

## no9container 容器
+ no1map
  + No1Hashtable 
  + No2HashMap
  + No3SynchronizedHashMap 
  + No4ConcurrentHashMap
+ no2vectortoqueue vector到queue解决售票问题
  + No1TicketSeller 使用ArrayList
  + No2TicketSeller 使用Vector或Collections.synchronizedXXX  
  + No3TicketSeller 使用synchronized同步锁
  + No4TicketSeller 使用ConcurrentLinkedQueue
+ no3synchronizecontainer 同步容器
  + No1Vector
  + No2Stack
  + No3HashTable
  + No4SynchronizedList
+ no3concurrentcontainer 并发容器
  + No1ConcurrentMap
  + No2ConcurrentSkipListMap
  + No3CopyOnWriteArrayList
  + No4CopyOnWriteArraySet
  + No5ConcurrentLinkedQueue
  + No6BlockingQueueBase
  + No7ArrayBlockingQueue
  + No8LinkedBlockingQueue
  + No9SynchronusQueue
  + No10PriorityQueue
  + No11DelayQueue
  + No12TransferQueue
  
## no10threadpool 线程池
+ No1CustomExecutor 认识Executor接口，自定义实现Executor接口
+ No2ExecutorService 认识ExecutorService，它接口继承了Executor接口
+ No3Executors 认识Executors，Executors工具类，用来方便的创建线程池
+ No4Future 认识Future
+ No5CompletableFuture 认识CompletableFuture，管理多个Future结果
+ No6ThreadPoolPolicy 线程池拒绝策略
+ No7FixedThreadPool 固定大小线程池
+ No8CachedThreadPool 可缓存线程池
+ No9SingleThreadPool 单线程的线程池
+ No10SingleScheduledThreadPool 定时任务单线程池
+ No11ScheduledThreadPool 定时任务线程池
+ No12WorkStealingPool 工作窃取线程池，底层是ForkJoinPool
+ No13ForkJoinPool ForkJoinPool
+ No14ParallelStreamUseForkJoinPool ParallelStream底层使用ForkJoinPool

## 98producerandconsumer 消费者与生产者
+ No1UseSynchronized
+ No2UseLock
+ No3UseBlockingQueue 

## 99other 其他
+ MultiThreadVisibilityProblem 多线程下可见性的问题
