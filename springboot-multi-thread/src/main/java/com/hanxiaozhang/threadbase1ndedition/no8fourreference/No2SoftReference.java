package com.hanxiaozhang.threadbase1ndedition.no8fourreference;

import java.lang.ref.SoftReference;

/**
 * 〈一句话功能简述〉<br>
 * 〈软引用〉
 * <p>
 * 执行前配置堆内存：-Xms20M -Xmx20M
 * <p>
 * 概念：用来描述一些还有用但并非必需的对象，使用SoftReference类来实现软引用。如果对象只具有软引用，
 * 内存空间足够，GC就不会回收它；如果内存空间不足了，GC就会回收这些对象的内存。只要GC没有回收它，该对象就可以被程序使用。
 * <p>
 * 软引用非常适合缓存使用
 *
 * @author hanxinghua
 * @create 2021/11/18
 * @since 1.0.0
 */
public class No2SoftReference {

    /**
     * @param args
     */
    public static void main(String[] args) {

        SoftReference<byte[]> m = new SoftReference<>(new byte[1024 * 1024 * 10]);
        //m = null;
        System.out.println(m.get());
        System.gc();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(m.get());

        // 再分配一个数组，heap将装不下，这时候系统会垃圾回收，先回收一次，如果不够，会把软引用干掉，需要证明 2021-11-18
        byte[] b = new byte[1024 * 1024 * 12];
        System.out.println(m.get());

    }

}
