package com.hanxiaozhang.threadbase1ndedition.no8fourreference;

import java.lang.ref.WeakReference;

/**
 * 〈一句话功能简述〉<br>
 * 〈弱引用〉
 * <p>
 * 概念：用来描述一些非必需对象的，使用WeakReference类来实现弱引用，被弱引用关联的对象只能生存到下一次垃圾收集发生之前。
 * 在GC线程扫描它所管辖的内存区域的过程中，一旦发现了只具有弱引用的对象，不管当前内存空间足够与否，都会回收它的内存。
 * <p>
 * 场景：一般用在容器中。
 * <p>
 * WeakHashMap了解一下 2021-11-18
 *
 * @author hanxinghua
 * @create 2021/11/18
 * @since 1.0.0
 */
public class No3WeakReference {

    public static void main(String[] args) {
        WeakReference<ReferenceObject> object = new WeakReference<>(new ReferenceObject());

        System.out.println(object.get());
        // 调用gc，就被回收
        System.gc();
        System.out.println(object.get());

        // 弱引用的一个应用
        ThreadLocal<ReferenceObject> tl = new ThreadLocal<>();
        tl.set(new ReferenceObject());
        tl.remove();

    }

}
