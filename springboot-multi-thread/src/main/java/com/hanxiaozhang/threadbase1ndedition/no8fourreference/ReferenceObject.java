package com.hanxiaozhang.threadbase1ndedition.no8fourreference;

/**
 * 〈一句话功能简述〉<br>
 * 〈引用测试对象〉
 *
 * @author hanxinghua
 * @create 2021/11/18
 * @since 1.0.0
 */
public class ReferenceObject {

    /**
     * 重写finalize()，这个方法GC会用到，正常是不用重写的。
     *
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        System.out.println("gc execute finalize method");
    }
}
