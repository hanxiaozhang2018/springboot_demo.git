package com.hanxiaozhang.threadbase1ndedition.no8fourreference;

import java.io.IOException;

/**
 * 〈一句话功能简述〉<br>
 * 〈强引用〉
 * <p>
 * 概念：创建一个对象并把这个对象赋给一个引用变量（对象引用），
 * 只要强引用还存在，GC永远不会回收掉被引用的对象。
 *
 * @author hanxinghua
 * @create 2021/11/18
 * @since 1.0.0
 */
public class No1StrongReference {

    public static void main(String[] args) throws IOException {

        ReferenceObject object = new ReferenceObject();
        // 只有指向null，gc才可以回收，不然强引用永远不会回收
        object = null;
        // 手动执行gc
        System.gc();
        // 阻塞main线程，不让他停止
        System.in.read();

    }

}
