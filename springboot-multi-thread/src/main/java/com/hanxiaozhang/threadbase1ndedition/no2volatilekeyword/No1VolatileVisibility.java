package com.hanxiaozhang.threadbase1ndedition.no2volatilekeyword;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈volatile的可见性〉
 *
 * @author hanxinghua
 * @create 2021/11/11
 * @since 1.0.0
 */
public class No1VolatileVisibility {


    private static volatile boolean running = true;

    public static void main(String[] args) throws InterruptedException {

        new Thread(() -> {
            System.out.println("THREAD-1 start");
            while (running) {

            }
            System.out.println("THREAD-1 end");
        }, "THREAD-1").start();

        TimeUnit.SECONDS.sleep(1);

        running = false;
    }


}
