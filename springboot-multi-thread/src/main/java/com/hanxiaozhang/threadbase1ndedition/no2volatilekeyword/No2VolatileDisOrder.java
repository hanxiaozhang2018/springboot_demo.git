package com.hanxiaozhang.threadbase1ndedition.no2volatilekeyword;

/**
 * 〈一句话功能简述〉<br>
 * 〈volatile的禁止重排序〉
 *
 *  单例模式-DCL（双重检测机制）
 *
 *
 * @author hanxinghua
 * @create 2021/11/11
 * @since 1.0.0
 */
public class No2VolatileDisOrder {

    private static volatile No2VolatileDisOrder instance = null;

    private No2VolatileDisOrder() {

    }

    public static No2VolatileDisOrder getInstance() {

        if (instance == null) {

            synchronized (No2VolatileDisOrder.class) {
                if (instance == null) {
                    instance =new No2VolatileDisOrder();
                }
            }
        }

        return instance;
    }


    public static void main(String[] args) {
        No2VolatileDisOrder.getInstance();
    }

}
