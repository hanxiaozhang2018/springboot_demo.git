package com.hanxiaozhang.threadbase1ndedition.no2volatilekeyword;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈volatile不具备原子性〉
 *
 * @author hanxinghua
 * @create 2021/11/10
 * @since 1.0.0
 */
public class No3VolatileNoAtomic {

    private volatile int count = 0;

    public  void add() {
        count++;
    }

    public static void main(String[] args) {

        List<Thread> list = new ArrayList<>();

        No3VolatileNoAtomic example = new No3VolatileNoAtomic();

        for (int i = 0; i < 15; i++) {
            Thread thread = new Thread(() -> {
                for (int j = 0; j < 2000; j++) {
                    example.add();
                }
            }, "THREAD-" + i);
            list.add(thread);
        }

        list.forEach(t -> t.start());
        list.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        System.out.println("count = " + example.count);


    }

}
