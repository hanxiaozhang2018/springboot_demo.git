package com.hanxiaozhang.threadbase1ndedition.no5juccommonclass;

import java.util.concurrent.Semaphore;

/**
 * 〈一句话功能简述〉<br>
 * 〈Semaphore 信号量〉
 * <p>
 * 概念：
 * 用来控制同时访问特定资源的线程数量，它通过协调各个线程，以保证合理的使用公共资源。
 * <p>
 * 常见方法：
 * Semaphore(int permits, boolean fair)：构造函数 permits 许可证的数量 fair 公平策略：true 公平
 * acquire()：从该信号量获取许可证
 * release()：发放许可证
 *
 *
 * @author hanxinghua
 * @create 2021/11/14
 * @since 1.0.0
 */
public class No5Semaphore {

    public static void main(String[] args) {
        //Semaphore s = new Semaphore(2);
        Semaphore s = new Semaphore(2, true);
        //允许一个线程同时执行
        //Semaphore s = new Semaphore(1);

        new Thread(() -> {
            try {
                s.acquire();

                System.out.println("T1 running...");
                Thread.sleep(200);
                System.out.println("T1 running...");

            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                s.release();
            }
        }).start();

        new Thread(() -> {
            try {
                s.acquire();

                System.out.println("T2 running...");
                Thread.sleep(200);
                System.out.println("T2 running...");

                s.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
