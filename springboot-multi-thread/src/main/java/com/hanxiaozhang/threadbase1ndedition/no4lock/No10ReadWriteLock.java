package com.hanxiaozhang.threadbase1ndedition.no4lock;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 〈一句话功能简述〉<br>
 * 〈读写锁〉
 * <p>
 * 共享锁（读锁）：在同一段时间内，多个用户可以读取一个资源，读取过程中数据不会发生任何变化。
 * 排它锁（写锁）：在任何时候只能有一个用户写入资源，当进行写锁时会堵塞其他的读锁或写锁操作。
 *
 * @author hanxinghua
 * @create 2021/11/14
 * @since 1.0.0
 */
public class No10ReadWriteLock {

    private static Lock lock = new ReentrantLock();
    private static int value;

    private static ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock readLock = readWriteLock.readLock();
    private static Lock writeLock = readWriteLock.writeLock();


    public static void main(String[] args) {

        // ---- ---- ---- ---- 20240516 ---- ---- ---- ---- ----
        // 使用读写锁简单例子：getData()使用读锁来保护读操作，而setData()使用写锁来保证写操作的互斥。
        // 这样，多个线程可以同时读取data字段，但写操作将会被序列化(串行)，从而保证数据的一致性。
        Cache cache = new Cache();
        cache.setData("11");
        System.out.println(cache.getData());

        // ---- ---- ---- ---- 以前代码---- ---- ---- ---- ----

        //Runnable readR = ()-> read(lock);
        Runnable readR = () -> read(readLock);

        //Runnable writeR = ()->write(lock, new Random().nextInt());
        Runnable writeR = () -> write(writeLock, new Random().nextInt());

        for (int i = 0; i < 18; i++) {
            new Thread(readR).start();
        }
        for (int i = 0; i < 2; i++) {
            new Thread(writeR).start();
        }


    }

    public static void read(Lock lock) {
        lock.lock();
        try {
            Thread.sleep(1000);
            System.out.println("read over!");
            //模拟读取操作
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public static void write(Lock lock, int v) {
        lock.lock();
        try {
            Thread.sleep(1000);
            value = v;
            System.out.println("write over!");
            //模拟写操作
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }


    public static class Cache {
        private Object data;
        private ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();

        public Object getData() {
            rwl.readLock().lock();
            try {
                return data;
            } finally {
                rwl.readLock().unlock();
            }
        }

        public void setData(Object data) {
            rwl.writeLock().lock();
            try {
                this.data = data;
            } finally {
                rwl.writeLock().unlock();
            }
        }
    }

}
