package com.hanxiaozhang.threadbase1ndedition.no99other;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.Executors.defaultThreadFactory;

/**
 * 〈一句话功能简述〉<br>
 * 〈测试〉
 *
 * @author hanxinghua
 * @create 2021/11/14
 * @since 1.0.0
 */
public class Test {

    public static void main(String[] args) throws InterruptedException {

        //创建一个固定线程池
        ThreadPoolExecutor executorService = new ThreadPoolExecutor(10, 10, 0, TimeUnit.MINUTES,
                new LinkedBlockingQueue<Runnable>(), defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());

        final Object object =new Object();

        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(1);
                    object.wait();
                    System.out.println("1-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(2);
                    object.wait();
                    System.out.println("2-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(3);
                    object.wait();
                    System.out.println("3-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(4);
                    object.wait();
                    System.out.println("4-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(5);
                    object.wait();
                    System.out.println("5-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(6);
                    object.wait();
                    System.out.println("6-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(7);
                    object.wait();
                    System.out.println("7-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(8);
                    object.wait();
                    System.out.println("8-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(9);
                    object.wait();
                    System.out.println("9-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executorService.submit(()->{
            synchronized (object){
                try {
                    System.out.println(10);
                    object.wait();
                    System.out.println("10-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        TimeUnit.SECONDS.sleep(1);
        synchronized (object){
            object.notifyAll();
        }

    }

}
