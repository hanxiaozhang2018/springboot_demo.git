package com.hanxiaozhang.threadbase1ndedition.no99other;

/**
 * 〈一句话功能简述〉<br>
 * 〈多线程下可见性的问题〉
 *
 *  加synchronized同步方法 或者加volatile各个线程可见变量
 *
 *
 * @author hanxinghua
 * @create 2021/11/8
 * @since 1.0.0
 */
public class MultiThreadVisibilityProblem implements Runnable {


    private /*volatile*/ int  count = 100;

    @Override
    public /*synchronized*/ void run() {
        count--;
        System.out.println(Thread.currentThread().getName() + " count= " + count);
    }


    public static void main(String[] args) {
        MultiThreadVisibilityProblem threadProblem = new MultiThreadVisibilityProblem();
        for (int i = 0; i < 100; i++) {
            new Thread(threadProblem, "THREAD-" + i).start();
        }
    }

}
