package com.hanxiaozhang.threadbase1ndedition.no10threadpool;

import java.util.concurrent.Executor;

/**
 * 〈一句话功能简述〉<br>
 * 〈认识Executor接口，自定义实现Executor接口〉
 *
 * @author hanxinghua
 * @create 2021/11/23
 * @since 1.0.0
 */
public class No1CustomExecutor implements Executor {


    public static void main(String[] args) {
        new No1CustomExecutor().execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello executor");
            }
        });
    }

    @Override
    public void execute(Runnable command) {
        System.out.println("模拟执行线程前,干一些事情!");
        command.run();
        System.out.println("模拟执行线程后,干一些事情!");
    }

}
