package com.hanxiaozhang.threadbase1ndedition.no10threadpool;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈认识Future〉
 *
 * @author hanxinghua
 * @create 2021/11/23
 * @since 1.0.0
 */
public class No4Future {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        FutureTask<Integer> task = new FutureTask<>(
                new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        TimeUnit.MILLISECONDS.sleep(500);
                        return 1000;
                    }
                });

        new Thread(task).start();
        // 阻塞
        System.out.println(task.get());


    }
}
