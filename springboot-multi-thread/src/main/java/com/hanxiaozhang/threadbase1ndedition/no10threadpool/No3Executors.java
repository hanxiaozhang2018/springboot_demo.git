package com.hanxiaozhang.threadbase1ndedition.no10threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * 〈一句话功能简述〉<br>
 * 〈认识Executors〉
 * <p>
 * Executors工具类，用来方便的创建线程池，但是根据阿里开发规范来说，线程池参数自己定义。
 *
 * @author hanxinghua
 * @create 2021/11/23
 * @since 1.0.0
 */
public class No3Executors {

    public static void main(String[] args) {

        // 创建默认线程工厂
        ThreadFactory threadFactory = Executors.defaultThreadFactory();

        // 创建各种类型线程池
        ExecutorService executorService1 = Executors.newSingleThreadExecutor();

        ExecutorService executorService2 = Executors.newFixedThreadPool(10);

    }
}
