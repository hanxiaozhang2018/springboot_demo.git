package com.hanxiaozhang.threadbase1ndedition.no10threadpool;

import java.util.concurrent.ExecutorService;

import static java.util.concurrent.Executors.newSingleThreadExecutor;

/**
 * 〈一句话功能简述〉<br>
 * 〈单线程的线程池〉
 * <p>
 * 启动一个线程负责按顺序执行任务，先提交的任务先执行
 *
 * @author hanxinghua
 * @create 2021/11/25
 * @since 1.0.0
 */
public class No9SingleThreadPool {

    public static void main(String[] args) {

        ExecutorService service = newSingleThreadExecutor();

        for (int i = 0; i < 5; i++) {
            final int j = i;
            service.execute(() -> {
                System.out.println(j + " " + Thread.currentThread().getName());
            });
        }

    }
}
