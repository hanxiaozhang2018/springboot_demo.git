package com.hanxiaozhang.threadbase1ndedition.no10threadpool;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;

/**
 * 〈一句话功能简述〉<br>
 * 〈定时任务的单线程池〉
 *
 * @author hanxinghua
 * @create 2021/11/25
 * @since 1.0.0
 */
public class No10SingleScheduledThreadPool {

    public static void main(String[] args) {

        ScheduledExecutorService service = newSingleThreadScheduledExecutor();

        service.scheduleAtFixedRate(() -> {
            System.out.println(Thread.currentThread().getName() + " hello word!");
        }, 0, 1, TimeUnit.SECONDS);

        // 这个不会去执行
        service.scheduleAtFixedRate(() -> {
            System.out.println(Thread.currentThread().getName() + " hello word!");
        }, 0, 10, TimeUnit.MILLISECONDS);


    }

}
