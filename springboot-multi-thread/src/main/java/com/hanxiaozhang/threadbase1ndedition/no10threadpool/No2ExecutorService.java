package com.hanxiaozhang.threadbase1ndedition.no10threadpool;

import java.util.concurrent.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈认识ExecutorService〉
 * <p>
 * Executor是线程池的核心接口，用来执行提交任务；
 * 而ExecutorService接口继承了Executor接口，提供了异步执行和关闭线程池的方法。
 * <p>
 * ThreadPoolExecutor是ExecutorService接口的实现类
 *
 * ThreadPoolExecutor的源码可以参考{@link com.hanxiaozhang.sourcecode.executor.MyThreadPoolExecutor }
 *
 *
 * @author hanxinghua
 * @create 2021/11/23
 * @since 1.0.0
 */
public class No2ExecutorService {


    /**
     * 创建一个线程池(完整入参):
     * 核心线程数为5 (corePoolSize),
     * 最大线程数为10 (maximumPoolSize),
     * 存活时间为60分钟(keepAliveTime),
     * 工作队列为LinkedBlockingQueue (workQueue),
     * 线程工厂为默认的DefaultThreadFactory (threadFactory),
     * 饱和策略(拒绝策略)为AbortPolicy: 抛出异常(handler).
     */
    private static ThreadPoolExecutor THREAD_POOL = new ThreadPoolExecutor(2, 5, 60, TimeUnit.MINUTES,
            new LinkedBlockingQueue(), Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy());


    public static void main(String[] args) throws ExecutionException, InterruptedException {

        // 1.execute()提交任务，无返回值
        THREAD_POOL.execute(() -> {
            System.out.println("execute1 is hello word!");
        });
        THREAD_POOL.execute(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("execute2 is hello word!");
        });

        // 2. submit()提交任务，有Future类型的返回值
        Future<String> future = THREAD_POOL.submit(() -> {
            return "submit is hello word!";
        });
        System.out.println(future.get());

        //3.一些统计数据
        System.out.println("线程池中线程的数量: " + THREAD_POOL.getPoolSize());
        System.out.println("线程池中活跃线程的数量(大致): " + THREAD_POOL.getActiveCount());
        System.out.println("线程池中最大的线程数量(动态变量): " + THREAD_POOL.getLargestPoolSize());
        System.out.println("线程池执行的任务总数(大致): " + THREAD_POOL.getTaskCount());
        System.out.println("线程池完成的任务总数(大致): " + THREAD_POOL.getCompletedTaskCount());
        System.out.println("线程池toString(): " + THREAD_POOL.toString());


        // 4.线程池参数修改或获取方法：
        /*
        setThreadFactory(ThreadFactory threadFactory)
        getThreadFactory()
        setRejectedExecutionHandler(RejectedExecutionHandler handler)
        getRejectedExecutionHandler()
        setCorePoolSize(int corePoolSize)
        getCorePoolSize()
        setMaximumPoolSize(int maximumPoolSize)
        getMaximumPoolSize()
        setKeepAliveTime(long time, TimeUnit unit)
        getKeepAliveTime(TimeUnit unit)

        allowsCoreThreadTimeOut() 获取线程池是否允许核心线程超时
        allowCoreThreadTimeOut(boolean value) 设置线程池是否允许核心线程超时

        prestartCoreThread() 启动核心线程
        ensurePrestart() 安排至少启动一个线程
        prestartAllCoreThreads() 启动所有核心线程

         */

        // 5.关闭线程池
        THREAD_POOL.shutdown();

        // 6.查看线程池是否为关闭状态
        System.out.println("THREAD_POOL has shutdown status is " + THREAD_POOL.isShutdown());
    }
}
