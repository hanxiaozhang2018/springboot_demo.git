package com.hanxiaozhang.threadbase1ndedition.no10threadpool;

import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.Executors.newScheduledThreadPool;

/**
 * 〈一句话功能简述〉<br>
 * 〈定时任务线程池〉
 *
 * @author hanxinghua
 * @create 2021/11/25
 * @since 1.0.0
 */
public class No11ScheduledThreadPool {

    public static void main(String[] args) {

        ScheduledExecutorService service = newScheduledThreadPool(4);


        // 延迟一定时间后启动
        service.schedule(() -> {
            System.out.println(Thread.currentThread().getName() + " schedule");
        }, 500, TimeUnit.MILLISECONDS);

        // 固定时间周期启动
        // 特点：在上一个任务开始时计时，period时间过去后，检测上一个任务是否执行完毕，
        // 如果上一个任务执行完毕，则当前任务立即执行，如果上一个任务没有执行完毕，
        // 则需要等上一个任务执行完毕后立即执行。
        service.scheduleAtFixedRate(() -> {
            try {
                TimeUnit.MILLISECONDS.sleep(new Random().nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " scheduleAtFixedRate");
        }, 0, 500, TimeUnit.MILLISECONDS);


        // 固定时间周期启动
        // 特点：在上一个任务结束时计时，period时间过去后，立即执行。
        service.scheduleWithFixedDelay(() -> {
            try {
                TimeUnit.MILLISECONDS.sleep(new Random().nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " scheduleWithFixedDelay");
        }, 0, 500, TimeUnit.MILLISECONDS);


    }
}
