package com.hanxiaozhang.threadbase1ndedition.no10threadpool;

import java.util.concurrent.ExecutorService;

import static java.util.concurrent.Executors.newCachedThreadPool;

import java.util.concurrent.TimeUnit;


/**
 * 〈一句话功能简述〉<br>
 * 〈可缓存线程池〉
 * <p>
 * 使用场景：执行时间短，量大的线程
 *
 * @author hanxinghua
 * @create 2021/11/25
 * @since 1.0.0
 */
public class No8CachedThreadPool {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService service = newCachedThreadPool();
        System.out.println(service);

        for (int i = 0; i < 2; i++) {
            service.execute(() -> {
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
            });
        }

        System.out.println(service);

        TimeUnit.SECONDS.sleep(80);

        System.out.println(service);

    }
}
