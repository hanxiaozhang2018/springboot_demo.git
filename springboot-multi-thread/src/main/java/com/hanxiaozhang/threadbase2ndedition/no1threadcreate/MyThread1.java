package com.hanxiaozhang.threadbase2ndedition.no1threadcreate;

/**
 * 〈一句话功能简述〉<br>
 * 〈继承方式〉
 *
 * @author hanxinghua
 * @create 2021/11/2
 * @since 1.0.0
 */
public class MyThread1 extends Thread {

    @Override
    public void run() {
        System.out.println("extends Thread way !");
    }
}
