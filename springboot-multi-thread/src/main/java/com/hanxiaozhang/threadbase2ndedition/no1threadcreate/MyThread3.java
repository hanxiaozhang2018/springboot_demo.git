package com.hanxiaozhang.threadbase2ndedition.no1threadcreate;

import java.util.concurrent.Callable;

/**
 * 〈一句话功能简述〉<br>
 * 〈implements Callable〉
 *
 * @author hanxinghua
 * @create 2021/11/2
 * @since 1.0.0
 */
public class MyThread3 implements Callable<String> {


    @Override
    public String call() throws Exception {
        return "implements Callable way!";
    }

}
