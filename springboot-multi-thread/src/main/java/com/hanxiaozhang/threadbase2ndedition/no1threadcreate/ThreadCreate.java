package com.hanxiaozhang.threadbase2ndedition.no1threadcreate;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.FutureTask;

import static java.util.concurrent.Executors.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈创建线程〉
 *
 * @author hanxinghua
 * @create 2021/11/2
 * @since 1.0.0
 */
public class ThreadCreate {

    public static void main(String[] args) {

        // 方式1
        MyThread1 myThread1 = new MyThread1();
        myThread1.start();

        // 方式2
        Thread myThread2 = new Thread(new MyThread2());
        myThread2.start();

        // 方式3
        try {
            FutureTask<String> futureTask = new FutureTask<>(new MyThread3());
            Thread myThread3 = new Thread(futureTask);
            myThread3.start();
            System.out.println(futureTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        // 方式4
        Thread myThread4 = new Thread(() -> {
            System.out.println("Lamda way!");
        });
        myThread4.start();

        // 方式5
        ExecutorService executorService = newFixedThreadPool(1);
        executorService.execute(() -> {
            System.out.println("Thread pool way!");
        });

    }


    /*
     Object object = new Object()
     new Thread(Object::method, "t1").start();
     等价于
     new Thread(()->object.object(), "t1").start();
     等价于1.8之前的写法
     new Thread(new Runnable() {
         @Override
         public void run() {
          object.object();
        }
     })
     */
}
