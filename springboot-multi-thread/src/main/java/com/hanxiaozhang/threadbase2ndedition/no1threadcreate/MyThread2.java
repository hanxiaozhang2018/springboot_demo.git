package com.hanxiaozhang.threadbase2ndedition.no1threadcreate;

/**
 * 〈一句话功能简述〉<br>
 * 〈实现Runnable〉
 *
 * @author hanxinghua
 * @create 2021/11/2
 * @since 1.0.0
 */
public class MyThread2 implements Runnable {

    @Override
    public void run() {
        System.out.println("implements Runnable way!");
    }

}
