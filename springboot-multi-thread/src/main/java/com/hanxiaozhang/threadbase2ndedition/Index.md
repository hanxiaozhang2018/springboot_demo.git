# 多线程与高并发第二版 
## no1threadcreate 线程创建方式
+ ThreadCreate

## no2threadstate 线程的6种状态
+ ThreadState 

## no3threadinterrupt 线程中断
+ No1InterruptAndIsInterrupted 设置标志位 + 查询标志位
+ No2InterruptAndInterrupted 设置标志位 + 静态查询标志位并重置标志位
+ No3InterruptAndSleep Interrupt与Sleep会抛InterruptedException异常
+ No4InterruptAndWait  Interrupt与Wait会抛InterruptedException异常
+ No5InterruptAndSynchronized Synchronized同步锁竞争的过程，不可能被interrupt打断
+ No6InterruptAndLock Lock锁竞争的过程，不可能被interrupt打断
+ No7InterruptAndLockInterruptibly 锁竞争的过程，想被interrupt打断，请使用ReentrantLock的lockInterruptibly

## no4threadend 线程结束方式
+ No1Stop  stop()停止线程
+ No2SuspendAndResume 暂停(Suspend)和继续(Resume)线程
+ No3VolatileFlag 使用Volatile标识，停止线程
+ No4Interrupt 使用Interrupt，停止线程

## no5volatilekeyword volatile关键字
+ No1HelloVolatile Volatile保持线程的可见性
+ No2VolatileReference Volatile 修饰引用类型（包括数组）只能保证引用本身的可见性，不能保证内部字段的可见性

## no6cacheline 缓存行
+ No1CacheLinePadding 使用缓存对齐

## no7programorder 程序排序
+ No1DisOrder 验证程序执行乱序
+ No2NoVisibilityAndDisorder 可见性，有序性有问题的程序
+ No3ThisEscape this对象溢出
+ No4IntroduceJClassLibPlugin 介绍JClassLib插件的使用 
