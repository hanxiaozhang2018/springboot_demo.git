package com.hanxiaozhang.threadbase2ndedition.no2threadstate;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 〈一句话功能简述〉<br>
 * 〈线程状态〉
 *
 * @author hanxinghua
 * @create 2021/11/2
 * @since 1.0.0
 */
public class ThreadState {

    public static void main(String[] args) throws Exception {

        System.out.println("---- NEW -> RUNNABLE -> TERMINATED ----");
        Thread t1 = new Thread(() -> {
            System.out.println("2: " + Thread.currentThread().getState());
            for (int i = 0; i < 3; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print(i + " ");
            }
            System.out.println();
        });
        System.out.println("1: " + t1.getState());
        t1.start();
        t1.join();
        System.out.println("3: " + t1.getState());

        System.out.println("---- NEW -> WAITING -> TIMED_WAITING -> TERMINATED ----");
        Thread t2 = new Thread(() -> {
            try {
                LockSupport.park();
                System.out.println("t2  go on");
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println("1: " + t2.getState());
        t2.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("2: " + t2.getState());
        LockSupport.unpark(t2);
        TimeUnit.SECONDS.sleep(1);
        System.out.println("3: " + t2.getState());
        t2.join();
        System.out.println("4: " + t2.getState());


        System.out.println("----synchronized is NEW -> BLOCKED -> TERMINATED ----");
        final Object o = new Object();
        Thread t3 = new Thread(() -> {
            synchronized (o) {
                System.out.println("t3 get lock o");
            }
        });
        new Thread(() -> {
            synchronized (o) {
                try {
                    Thread.sleep(3500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        TimeUnit.SECONDS.sleep(1);
        System.out.println("1: " + t3.getState());
        t3.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("2: " + t3.getState());
        t3.join();
        System.out.println("3: " + t3.getState());


        System.out.println("---- Lock is NEW -> WAITING -> TERMINATED ----");

        final Lock lock = new ReentrantLock();
        Thread t4 = new Thread(() -> {
            lock.lock();
            try {
                System.out.println("t4 get lock");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        });

        new Thread(() -> {
            lock.lock();
            try {
                Thread.sleep(3500);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();

        System.out.println("1: " + t4.getState());
        TimeUnit.SECONDS.sleep(1);
        t4.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("2: " + t4.getState());
        t4.join();
        System.out.println("3: " + t4.getState());

        System.out.println("---- LockSupport is NEW -> WAITING -> TERMINATED ----");

        Thread t5 = new Thread(() -> {
            LockSupport.park();
        });
        System.out.println("1: " + t5.getState());
        t5.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("2: " + t5.getState());
        LockSupport.unpark(t5);
        System.out.println("3: " + t5.getState());

    }

}
