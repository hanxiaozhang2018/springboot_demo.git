package com.hanxiaozhang.threadbase2ndedition.no5volatilekeyword;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈Volatile保持线程的可见性〉
 *
 * @author hanxinghua
 * @create 2021/11/3
 * @since 1.0.0
 */
public class No1HelloVolatile {
    private static volatile boolean running = true;

    public static void main(String[] args) throws InterruptedException {

        new Thread(() -> {
            System.out.println("t1 start");
            while (running) {

            }
            System.out.println("t1 end");
        }, "t1").start();

        TimeUnit.SECONDS.sleep(1);

        running = false;
        System.out.println("running set false over!");
    }

}
