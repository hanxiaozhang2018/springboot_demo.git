package com.hanxiaozhang.threadbase2ndedition.no5volatilekeyword;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈Volatile 修饰引用类型（包括数组）只能保证引用本身的可见性，
 *  不能保证内部字段的可见性〉
 *
 * @author hanxinghua
 * @create 2021/11/4
 * @since 1.0.0
 */
public class No2VolatileReference {

    private static class A {
        boolean running = true;

        void m() {
            System.out.println("m start");
            while (running) {

            }
            System.out.println("m end");
        }
    }

    private static volatile A a = new A();

    public static void main(String[] args) throws InterruptedException {
        new Thread(a::m, "t").start();
        TimeUnit.SECONDS.sleep(1);
        a.running = false;
    }
}
