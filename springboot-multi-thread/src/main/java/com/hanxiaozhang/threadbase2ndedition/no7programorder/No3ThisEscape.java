package com.hanxiaozhang.threadbase2ndedition.no7programorder;

import java.io.IOException;

/**
 * 〈一句话功能简述〉<br>
 * 〈this对象溢出〉
 *
 *  对象的半初始化
 *  重排序
 *  不要在构造法中启动线程
 *
 * @author hanxinghua
 * @create 2021/11/7
 * @since 1.0.0
 */
public class No3ThisEscape {

    private int num =8 ;

    public No3ThisEscape(){
        new Thread(()-> System.out.println(this.num)
        ).start();
    }

    public static void main(String[] args) throws IOException {
        new No3ThisEscape();
        System.in.read();
    }

}
