package com.hanxiaozhang.threadbase2ndedition.no7programorder;

/**
 * 〈一句话功能简述〉<br>
 * 〈可见性，有序性有问题的程序〉
 *   可见性问题，添加volatile关键字  private static boolean  volatile ready = false
 *   有序性问题，返回值可能是0  System.out.println(number);  -> 0
 *
 * @author hanxinghua
 * @create 2021/11/7
 * @since 1.0.0
 */
public class No2NoVisibilityAndDisorder {


    private static boolean ready = false;
    private static int number;

    private static class ReaderThread extends Thread {
        @Override
        public void run() {
            while (!ready) {
                Thread.yield();
            }
            System.out.println(number);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t = new ReaderThread();
        t.start();
        number = 42;
        ready = true;
        t.join();

    }

}
