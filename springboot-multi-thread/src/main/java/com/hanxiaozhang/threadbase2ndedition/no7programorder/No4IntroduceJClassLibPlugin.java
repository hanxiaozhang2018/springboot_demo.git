package com.hanxiaozhang.threadbase2ndedition.no7programorder;

/**
 * 〈一句话功能简述〉<br>
 * 〈介绍JClassLib插件的使用〉
 *
 * @author hanxinghua
 * @create 2021/11/7
 * @since 1.0.0
 */
public class No4IntroduceJClassLibPlugin {


    /**
     *
     * main方法中，通过JClassLib 查看汇编码
     * 0 new #2 <java/lang/Object>
     * 3 dup
     * 4 invokespecial #1 <java/lang/Object.<init> : ()V>
     * 7 astore_1
     * 8 return
     *
     *
     * @param args
     */
    public static void main(String[] args) {
        Object o = new Object();
    }

}
