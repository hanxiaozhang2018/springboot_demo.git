package com.hanxiaozhang.threadbase2ndedition.no4threadend;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈stop() 停止线程〉
 *
 * @author hanxinghua
 * @create 2021/11/3
 * @since 1.0.0
 */
public class No1Stop {

    public static void main(String[] args) throws InterruptedException {
        Thread t =new Thread(()->{
            while (true){
                System.out.println("go on");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        TimeUnit.SECONDS.sleep(5);
        t.stop();
    }
}
