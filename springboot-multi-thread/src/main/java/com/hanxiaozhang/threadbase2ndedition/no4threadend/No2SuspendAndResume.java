package com.hanxiaozhang.threadbase2ndedition.no4threadend;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈暂停(Suspend)和继续(Resume)线程〉
 *
 * @author hanxinghua
 * @create 2021/11/3
 * @since 1.0.0
 */
public class No2SuspendAndResume {
    public static void main(String[] args) throws InterruptedException {

        Thread t =new Thread(()->{
            while (true){
                System.out.println("go on");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        // 5秒钟之后暂停
        TimeUnit.SECONDS.sleep(5);
        // 暂停 [səˈspend]
        t.suspend();
        TimeUnit.SECONDS.sleep(3);
        // 继续  [rɪˈzuːm]
        t.resume();

    }
}
