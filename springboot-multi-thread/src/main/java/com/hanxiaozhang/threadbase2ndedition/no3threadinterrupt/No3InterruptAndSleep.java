package com.hanxiaozhang.threadbase2ndedition.no3threadinterrupt;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈Interrupt与Sleep会抛InterruptedException异常〉
 *
 *  线程中有sleep()，如果使用t.interrupt(), 线程会抛InterruptedException异常，用户自己去处理异常
 *  catch InterruptedException之后，会把标志位复位，即 false
 *
 * @author hanxinghua
 * @create 2021/11/3
 * @since 1.0.0
 */
public class No3InterruptAndSleep {

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                System.out.println("Thread is interrupted");
                System.out.println(Thread.currentThread().isInterrupted());
            }
        });

        t.start();
        TimeUnit.SECONDS.sleep(2);

        t.interrupt();

    }
}
