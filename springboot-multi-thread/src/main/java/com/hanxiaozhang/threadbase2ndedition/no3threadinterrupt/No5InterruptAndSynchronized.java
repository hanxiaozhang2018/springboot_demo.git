package com.hanxiaozhang.threadbase2ndedition.no3threadinterrupt;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈Synchronized同步锁竞争的过程，不可能被interrupt打断〉
 *
 * @author hanxinghua
 * @create 2021/11/3
 * @since 1.0.0
 */
public class No5InterruptAndSynchronized {

    private static Object o =new Object();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 =new Thread(()->{
            synchronized (o){
                try {
                    // 10s
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();

        TimeUnit.SECONDS.sleep(1);

        Thread t2 =new Thread(()->{
            synchronized (o){

            }
            System.out.println("t2 end");
        });

        t2.start();
        TimeUnit.SECONDS.sleep(1);
        t2.interrupt();
    }


}
