package com.hanxiaozhang.threadbase2ndedition.no3threadinterrupt;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 〈一句话功能简述〉<br>
 * 〈锁竞争的过程，想被interrupt打断，请使用ReentrantLock的lockInterruptibly〉
 *
 * @author hanxinghua
 * @create 2021/11/3
 * @since 1.0.0
 */
public class No7InterruptAndLockInterruptibly {

    private static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(() -> {
            lock.lock();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
            System.out.println("t1 end");
        });

        t1.start();

        TimeUnit.SECONDS.sleep(1);

        Thread t2 = new Thread(() -> {
            System.out.println("t2 start");

            try {
                lock.lockInterruptibly();
            } catch (InterruptedException e) {
                System.out.println("Thread is interrupted");
                System.out.println(Thread.currentThread().isInterrupted());
            } finally {
                lock.unlock();
            }
            System.out.println("t2 end");
        });
        t2.start();
        TimeUnit.SECONDS.sleep(1);
        t2.interrupt();
        
    }

}
