package com.hanxiaozhang.threadbase2ndedition.no3threadinterrupt;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈设置标志位 + 查询标志位〉
 *
 * @author hanxinghua
 * @create 2021/11/3
 * @since 1.0.0
 */
public class No1InterruptAndIsInterrupted {

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            for (; ; ) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("Thread is interrupted ");
                    System.out.println(Thread.currentThread().isInterrupted());
                }
            }
        });

        t.start();
        TimeUnit.SECONDS.sleep(2);

        t.interrupt();

    }

}
