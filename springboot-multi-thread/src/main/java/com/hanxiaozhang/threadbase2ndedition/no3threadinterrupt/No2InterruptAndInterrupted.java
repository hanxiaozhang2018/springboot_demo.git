package com.hanxiaozhang.threadbase2ndedition.no3threadinterrupt;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈设置标志位 + 静态查询标志位并重置标志位〉
 * <p>
 * interrupt() 打断某个线程（设置标志位）
 * isInterrupted() 查询某线程是否被打断过（查询标志位）
 * static interrupted() 查询当前线程是否被打断过，并重置打断标识(将true --> false)
 *
 * @author hanxinghua
 * @create 2021/11/3
 * @since 1.0.0
 */
public class No2InterruptAndInterrupted {

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            for (; ; ) {
                if (Thread.interrupted()) {
                    System.out.println("Thread is interrupted ");
                    System.out.println(Thread.interrupted());
                }
            }
        });

        t.start();
        TimeUnit.SECONDS.sleep(2);

        t.interrupt();

        // 这里interrupted()静态方法，是拿当前线程，即main线程
        // System.out.println("main is "+t.interrupted());

    }
}
