package com.hanxiaozhang.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/12/29
 * @since 1.0.0
 */
public class Test {

    private static ExecutorService executor = Executors.newFixedThreadPool(10);


    public static void main(String[] args) {

        new Test().method();
        System.out.println("--end");
        executor.shutdown();

    }

    public void method() {

        for (int i = 0; i < 10; i++) {
            Future<String> submit = executor.submit(() -> {
                        TimeUnit.SECONDS.sleep(1);
                        System.out.println(Thread.currentThread().getName() + " execute end");
                        return "OK";
                    }
            );
        }

        return;

    }

    public void method1() {

        List<Future> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Future<String> future = executor.submit(() -> {
                        TimeUnit.SECONDS.sleep(1);
                        System.out.println(Thread.currentThread().getName() + " execute end");
                        return "OK";
                    }
            );
            list.add(future);
        }

        for (Future future : list) {
            try {
                future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

}
