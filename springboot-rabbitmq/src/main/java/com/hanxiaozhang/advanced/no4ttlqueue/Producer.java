package com.hanxiaozhang.advanced.no4ttlqueue;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈TTL队列〉
 * 场景：
 * 下单，下单之后在一定的时间内，如果没有被处理 ，自动失效
 * Tips：
 * 设置队列过期时间：x-message-ttl，单位：ms，会对整个队列消息统一过期。
 * 设置消息过期时间：expiration。单位：ms，当该消息在队列头部时（消费时），会单独判断这一消息是否过期。
 * 如果两者都进行了设置，以时间短的为准。
 *
 * @author hanxinghua
 * @create 2022/9/24
 * @since 1.0.0
 */
public class Producer {
    private static final String QUEUE_NAME = "ttl-01";

    public static void main(String[] args) throws Exception {
        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置RabbitMQ所在主机ip或者主机名
        factory.setHost("localhost");
        // 创建一个连接
        Connection connection = factory.newConnection();
        // 创建一个频道
        Channel channel = connection.createChannel();

        // 给队列添加ttl属性 单位：毫秒
        // x-message-ttl：表示消息在队列中最多能存活多久
        Map<String, Object> map = new HashMap<>(4);
        map.put("x-message-ttl", 10000);
        channel.queueDeclare(QUEUE_NAME, false, false, false, map);

        // 给单个消息设置ttl属性 单位：毫秒
        AMQP.BasicProperties props = new AMQP.BasicProperties()
                .builder()
                .expiration("10000")
                .build();

        String message = "hello world!";
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        System.out.println("Sent '" + message + "'");


        channel.close();
        connection.close();
    }

}
