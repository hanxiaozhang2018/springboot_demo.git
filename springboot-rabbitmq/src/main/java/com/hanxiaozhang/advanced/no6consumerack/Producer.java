package com.hanxiaozhang.advanced.no6consumerack;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * 〈一句话功能简述〉<br>
 * 〈消费端的ack〉
 * 消费者端手动签收与消息的重回队列
 *
 * @author hanxinghua
 * @create 2022/9/24
 * @since 1.0.0
 */
public class Producer {

    private static final String QUEUE_NAME = "ack-01";

    public static void main(String[] args) throws Exception {

        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置RabbitMQ所在主机ip或者主机名
        factory.setHost("localhost");
        // 创建一个连接
        Connection connection = factory.newConnection();
        // 创建一个频道
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        String message = "hello world!";
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        System.out.println("Sent '" + message + "'");

        channel.close();
        connection.close();
    }

}
