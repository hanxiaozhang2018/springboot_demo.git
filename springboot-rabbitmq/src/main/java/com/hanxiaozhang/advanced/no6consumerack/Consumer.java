package com.hanxiaozhang.advanced.no6consumerack;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/24
 * @since 1.0.0
 */
public class Consumer {


    private static final String QUEUE_NAME = "ack-01";

    public static void main(String[] args) throws Exception {

        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置RabbitMQ所在主机ip或者主机名
        factory.setHost("localhost");
        // 创建一个连接
        Connection connection = factory.newConnection();
        // 创建一个频道
        Channel channel = connection.createChannel();
        // 声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println("Waiting for messages.");

        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("接受到的消息是:" + new String(body));

                // 签收 第一个参数：当前消息的标记 第二个参数：是否批量进行应答
                //channel.basicAck(envelope.getDeliveryTag(),false);

                // 拒绝签收  第三个参数：表示决绝签收之后这个消息是否要重回队列
                channel.basicNack(envelope.getDeliveryTag(), false, true);
            }
        };

        channel.basicConsume(QUEUE_NAME, false, defaultConsumer);
    }

}
