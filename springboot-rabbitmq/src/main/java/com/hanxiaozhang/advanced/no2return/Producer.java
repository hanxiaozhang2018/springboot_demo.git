package com.hanxiaozhang.advanced.no2return;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * 〈一句话功能简述〉<br>
 * 〈Return机制〉
 * 场景：
 * 发送消息时，指定的交换机或路由key不存在，生产者需要监听消息不可达时，使用Return机制
 * 前提：
 * 当前的队列必须要有消费者存在
 * channel.basicPublish(..) 第三个参数 mandatory： 设置为true，表示监听不可达的消息
 *
 * @author hanxinghua
 * @create 2022/9/24
 * @since 1.0.0
 */
public class Producer {

    private static final String EXCHANGE_NAME = "return_exchange";

    /**
     * 可以路由的key
     */
    private static final String ROUTING_KEY = "return.save";

    /**
     * 不可以路由的key
     */
    private static final String ROUTING_ERROR_KEY = "abc.save";

    public static void main(String[] args) throws Exception {

        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置RabbitMQ所在主机ip或者主机名
        factory.setHost("localhost");
        // 创建一个连接
        Connection connection = factory.newConnection();
        // 创建一个频道
        Channel channel = connection.createChannel();
        // 添加监听
        channel.addReturnListener(new ReturnListener() {

            /**
             *
             * @param replyCode 队列响应给浏览器的状态码
             * @param replyText 表示的是状态码对应的文本信息
             * @param exchange 交换机的名字
             * @param routingKey 表示的是路由的key
             * @param properties 表示的是消息的属性
             * @param body 消息体的内容
             * @throws IOException
             */
            @Override
            public void handleReturn(int replyCode, String replyText, String exchange,
                                     String routingKey, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("状态码：" + replyCode + " 文本信息:" + replyText + " 交换机名字:" + exchange + " 路由的key:" + routingKey);
            }
        });
        String message = "hello world!";
        // 第三个参数 mandatory： 设置为true，表示监听不可达的消息
        channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY, true, null, message.getBytes());
        System.out.println("Sent '" + message + "'");

    }

}
