package com.hanxiaozhang.advanced.no1confirm;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * 〈一句话功能简述〉<br>
 * 〈接受队列中的消息〉
 *
 * @author hanxinghua
 * @create 2022/9/24
 * @since 1.0.0
 */
public class Consumer {

    /**
     * 队列名称
     */
    private final static String QUEUE_NAME = "confirm-01";

    public static void main(String[] argv) throws Exception {

        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置RabbitMQ所在主机ip或者主机名
        factory.setHost("localhost");
        // 创建一个连接
        Connection connection = factory.newConnection();
        // 创建一个频道
        Channel channel = connection.createChannel();
        // 声明队列，主要为了防止消息接收者先运行此程序，队列还不存在时创建队列。
        channel.queueDeclare(QUEUE_NAME, false, false, true, null);
        System.out.println("Waiting for messages.");

        // 创建消费者
        com.rabbitmq.client.Consumer consumer = new DefaultConsumer(channel) {

            /**
             * 处理交付
             *
             * @param consumerTag 这个消息的唯一标记
             * @param envelope 信封(请求的消息属性的一个封装)
             * @param properties 前面队列带过来的值
             * @param body 接受到的消息
             * @throws IOException
             */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
                                       byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Received '" + message + "'");
            }
        };

        // 启动一个消费者，并返回服务端生成的消费者标识
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }

}
