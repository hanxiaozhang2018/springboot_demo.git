package com.hanxiaozhang.advanced.no3currentlimiting;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * 〈一句话功能简述〉<br>
 * 〈消费端的限流〉
 * <p>
 * 场景：
 * 消费者死了，队列里一瞬间积累了上万条数据，这时重新打开客户端，瞬间就有巨量的信息给推送过来，
 * 但是客户端是没有办法同时处理这么多数据，结果消费者又死了
 *
 * @author hanxinghua
 * @create 2022/9/24
 * @since 1.0.0
 */
public class Producer {


    private static final String QUEUE_NAME = "limit-01";

    public static void main(String[] args) throws Exception {
        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置RabbitMQ所在主机ip或者主机名
        factory.setHost("localhost");
        // 创建一个连接
        Connection connection = factory.newConnection();
        // 创建一个频道
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        for (int i = 0; i < 100; i++) {
            channel.basicPublish("", QUEUE_NAME, null, ("limit_" + i).getBytes());
        }
        System.out.println("Sent over!");
        channel.close();
        connection.close();
    }

}
