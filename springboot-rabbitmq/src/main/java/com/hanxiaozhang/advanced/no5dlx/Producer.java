package com.hanxiaozhang.advanced.no5dlx;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * 〈一句话功能简述〉<br>
 * 〈死信队列〉
 * <p>
 * 当消息死信之后、可以定义它重新push到另一个交换机，这个交换机也有对应的队列，这个队列就称为死信队列。
 * <p>
 * 死信出现的三种情况：（拒接、超时、超长）
 * i. 发送到队列中的消息被拒绝
 * ii. 消息设置ttl时间，过期的
 * iii. 队列达到了最大长度，再往里面放的信息
 * 在满足上面情况，我们可以定义一个队列（死信队列），专门处理。
 *
 *
 * @author hanxinghua
 * @create 2022/9/24
 * @since 1.0.0
 */
public class Producer {


    private static final String EXCHANGE_NAME = "dlx-right-exchange";

    private static final String ROUTING_KEY = "dlx.#";

    public static void main(String[] args) throws Exception {

        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置RabbitMQ所在主机ip或者主机名
        factory.setHost("localhost");
        // 创建一个连接
        Connection connection = factory.newConnection();
        // 创建一个频道
        Channel channel = connection.createChannel();

        String message = "hello world ";
        for (int i = 0; i < 5; i++) {
            channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY, false, null, (message + i).getBytes());
        }
        System.out.println("Sent");
    }

}
