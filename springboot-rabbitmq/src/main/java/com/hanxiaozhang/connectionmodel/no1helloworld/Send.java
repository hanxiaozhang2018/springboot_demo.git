package com.hanxiaozhang.connectionmodel.no1helloworld;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * 〈一句话功能简述〉<br>
 * 〈发送消息到队列中〉
 *
 * @author hanxinghua
 * @create 2022/9/23
 * @since 1.0.0
 */
public class Send {

    /**
     * 队列名称
     */
    private final static String QUEUE_NAME = "hello.mq";

    public static void main(String[] argv) throws Exception {

        // 创建链接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置RabbitMQ所在主机ip或者主机名
        factory.setHost("localhost");
        // 创建一个连接
        Connection connection = factory.newConnection();
        // 创建一个频道
        Channel channel = connection.createChannel();
        // 指定一个队列
        // 第一个参数：队列名称
        // 第二个参数：false：重启后，队列没有。true：持久化队列，重启后，队列依然存在
        // 第三个参数：声明一个独占队列，仅限于此连接，连接关闭，删除这个队列 true
        // 第四个参数：最后一个消费者退出去之后，这个队列是否自动删除
        // 第五个参数：队列的其他属性
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 往队列中发出一条消息
        String message = "hello world!";
        // 第一个参数: 交换机，不能为null，但是可以设置成 ""
        // 第二个参数：路由key，不能为null，但是可以设置成 ""
        // 第三个参数：设置的队列的属性
        // 第四个参数：消息值
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        System.out.println("Sent '" + message + "'");
        //关闭频道和连接
        channel.close();
        connection.close();
    }
}
