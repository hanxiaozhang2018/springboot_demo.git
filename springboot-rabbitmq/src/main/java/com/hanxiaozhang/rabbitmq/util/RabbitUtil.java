package com.hanxiaozhang.rabbitmq.util;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/21
 * @since 1.0.0
 */
public class RabbitUtil {

    /**
     * 组装消息体
     *
     * @param msg
     * @return
     */
    public static Map<String, Object> getMessage(String msg) {
        String msgId = UUID.randomUUID().toString().replace("-", "").substring(0, 32);
        Map<String, Object> map = new HashMap<>();
        map.put("msgId", msgId);
        map.put("sendTime", LocalDateTime.now().toString());
        map.put("msg", msg);
        return map;
    }

}
