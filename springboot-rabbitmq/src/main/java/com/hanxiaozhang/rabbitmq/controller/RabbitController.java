package com.hanxiaozhang.rabbitmq.controller;

import com.alibaba.fastjson.JSON;
import com.hanxiaozhang.rabbitmq.service.RabbitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/19
 * @since 1.0.0
 */

@RestController
@RequestMapping("/rabbit")
public class RabbitController {

    @Autowired
    @Qualifier("directRabbitService")
    private RabbitService directRabbitService;

    @Autowired
    @Qualifier("fanoutRabbitService")
    private RabbitService fanoutRabbitService;

    @Autowired
    @Qualifier("topicRabbitService")
    private RabbitService topicRabbitService;

    @Autowired
    @Qualifier("headersRabbitService")
    private RabbitService headersRabbitService;


    @GetMapping("/sendMsgOnDirect")
    public String sendMsgOnDirect(@RequestParam(name = "msg") String msg) throws Exception {
        return directRabbitService.sendMsg(msg);
    }

    @GetMapping("/sendMsgOnFanout")
    public String sendMsgOnFanout(@RequestParam(name = "msg") String msg) throws Exception {
        return fanoutRabbitService.sendMsg(msg);
    }


    @GetMapping("/sendMsgOnTopic")
    public String sendMsgOnTopic(@RequestParam(name = "msg") String msg, @RequestParam(name = "routingKey") String routingKey) throws Exception {
        return topicRabbitService.sendMsg(msg, routingKey);
    }

    @GetMapping("/sendMsgOnHeaders")
    public String sendMsgOnHeaders(@RequestParam(name = "msg") String msg, @RequestParam(name = "json") String json) throws Exception {

        Map<String, Object> map = JSON.parseObject(json, HashMap.class);
        return headersRabbitService.sendMsg(msg, map);
    }


}
