package com.hanxiaozhang.rabbitmq.service;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/19
 * @since 1.0.0
 */
public interface RabbitService {

    default String sendMsg(String msg) throws Exception {
        throw new RuntimeException("不支持次方法!");
    }

    default String sendMsg(String msg, String routingKey) throws Exception {
        throw new RuntimeException("不支持次方法!");
    }

    default String sendMsg(String msg, Map<String, Object> map) throws Exception{
        throw new RuntimeException("不支持次方法!");
    }

}
