package com.hanxiaozhang.rabbitmq.service.impl;

import com.hanxiaozhang.constant.RabbitConstant;
import com.hanxiaozhang.rabbitmq.service.RabbitService;
import com.hanxiaozhang.rabbitmq.util.RabbitUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/21
 * @since 1.0.0
 */
@Service("topicRabbitService")
public class TopicRabbitServiceImpl implements RabbitService {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Override
    public String sendMsg(String msg, String routingKey) throws Exception {
        Map<String, Object> message = RabbitUtil.getMessage(msg);
        try {
            //发送消息
            rabbitTemplate.convertAndSend(RabbitConstant.TOPIC_EXCHANGE, routingKey, message);
            return "ok";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

}
