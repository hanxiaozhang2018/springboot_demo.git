package com.hanxiaozhang.rabbitmq.service.impl;

import com.hanxiaozhang.constant.RabbitConstant;
import com.hanxiaozhang.rabbitmq.service.RabbitService;
import com.hanxiaozhang.rabbitmq.util.RabbitUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/21
 * @since 1.0.0
 */
@Service("fanoutRabbitService")
public class FanoutRabbitServiceImpl implements RabbitService {


    @Resource
    private RabbitTemplate rabbitTemplate;

    @Override
    public String sendMsg(String msg) throws Exception {
        Map<String, Object> message = RabbitUtil.getMessage(msg);
        try {
            rabbitTemplate.convertAndSend(RabbitConstant.FANOUT_EXCHANGE, null, message);
            return "ok";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

}
