package com.hanxiaozhang.rabbitmq.consumer;

import com.hanxiaozhang.constant.RabbitConstant;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/21
 * @since 1.0.0
 */

@Component
public class HeadersRabbitConsumerB {

    @RabbitHandler
    @RabbitListener(queuesToDeclare = @Queue(RabbitConstant.HEADERS_QUEUE_B))
    public void process(Message message) throws Exception {
        MessageProperties messageProperties = message.getMessageProperties();
        String contentType = messageProperties.getContentType();
        System.out.println("队列[" + RabbitConstant.HEADERS_QUEUE_B + "]收到消息：" + new String(message.getBody(), contentType));
    }
}
