package com.hanxiaozhang.rabbitmq.consumer;

import com.hanxiaozhang.constant.RabbitConstant;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/21
 * @since 1.0.0
 */
@Component
public class FanoutRabbitConsumerAB {

    @RabbitHandler
    @RabbitListener(queuesToDeclare = @Queue(RabbitConstant.FANOUT_TOPIC_A))
    public void processA(Map<String, Object> map) {
        System.out.println("队列A收到消息：" + map.toString());
    }

    @RabbitHandler
    @RabbitListener(queuesToDeclare = @Queue(RabbitConstant.FANOUT_TOPIC_B))
    public void processB(Map<String, Object> map) {
        System.out.println("队列B收到消息：" + map.toString());
    }

}
