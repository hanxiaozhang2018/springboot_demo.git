package com.hanxiaozhang.rabbitmq.consumer;


import com.hanxiaozhang.constant.RabbitConstant;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/21
 * @since 1.0.0
 */
@Component
public class TopicRabbitConsumerC {

    @RabbitHandler
    @RabbitListener(queuesToDeclare = @Queue(RabbitConstant.TOPIC_QUEUE_C))
    public void process(Map<String, Object> map) {
        System.out.println("队列[" + RabbitConstant.TOPIC_QUEUE_C + "]收到消息：" + map.toString());
    }
}
