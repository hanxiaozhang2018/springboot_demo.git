package com.hanxiaozhang.constant;

/**
 * 〈一句话功能简述〉<br>
 * 〈RabbitMQ常量〉
 *
 * @author hanxinghua
 * @create 2022/9/19
 * @since 1.0.0
 */
public class RabbitConstant {

    // ---- directExchange ----
    /**
     * 队列主题名称
     */
    public static final String DIRECT_TOPIC = "direct.topic";

    /**
     * DIRECT交换机名称
     */
    public static final String DIRECT_EXCHANGE = "direct.exchange";

    /**
     * DIRECT交换机和队列绑定的匹配键 DirectRouting
     */
    public static final String DIRECT_ROUTING = "direct.routing";


    // ---- fanoutExchange ----
    /**
     * FANOUT交换机队列A的名称
     */
    public static final String FANOUT_TOPIC_A = "fanout.topic.a";

    /**
     * FANOUT交换机队列B的名称
     */
    public static final String FANOUT_TOPIC_B = "fanout.topic.b";

    /**
     * FANOUT交换机名称
     */
    public static final String FANOUT_EXCHANGE = "fanout.exchange";


    // ---- topicExchange ----
    /**
     * TOPIC交换机的队列A的名称
     */
    public static final String TOPIC_QUEUE_A = "topic.queue.a";

    /**
     * TOPIC交换机的队列B的名称
     */
    public static final String TOPIC_QUEUE_B = "topic.queue.b";

    /**
     * TOPIC交换机的队列C的名称
     */
    public static final String TOPIC_QUEUE_C = "topic.queue.c";

    /**
     * TOPIC交换机名称
     */
    public static final String TOPIC_EXCHANGE = "topic.exchange";


    // ---- headersExchange ----
    /**
     * HEADERS交换机名称
     */
    public static final String HEADERS_EXCHANGE = "headers.exchange";

    /**
     * HEADERS交换机的队列A的名称
     */
    public static final String HEADERS_QUEUE_A = "headers.queue.a";

    /**
     * HEADERS交换机的队列B的名称
     */
    public static final String HEADERS_QUEUE_B = "headers.queue.b";
}
