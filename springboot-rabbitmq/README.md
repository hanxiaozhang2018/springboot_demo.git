# 工程简介

## 交换机的使用：
+ DirectExchange
+ FanoutExchange
+ TopicExchange
+ HeadersExchange

## connectionmodel 通讯模型：
+ helloworld模型：一个生产者发送消息，一个接收者接收消息。
![Image HelloWord模型](/images/HelloWord模型.png)
+ work模型：多个消费者消费的数据之和才是原来队列中的所有数据，适用于流量的消峰。
![Image Work模型](/images/Work模型.png)
+ pubsub模型：发布订阅模式，使用Fanout交换机
![Image 发布订阅模型](/images/发布订阅模型.png)
+ router模型：路由模型，相当于是分布订阅的升级版，根据路由的key（routing key）来判断是否路由到哪一个队列里面去
，使用Direct交换机
![Image 路由模型](/images/路由模型.png)
+ topic模型：相当于是对路由模式的一个升级，在匹配的规则上可以实现模糊匹配
![Image Topic模型](/images/Topic模型.png)
+ rpc模型
![Image RPC模型](/images/RPC模型.png)


## advanced 高级属性：
+ no1confirm Confirm机制，消息成功进入Queue，队列就会给生产者进行反馈
+ no2return Return机制，发送消息时，指定的交换机或路由key不存在，生产者需要监听消息不可达时，使用Return机制
+ no3currentlimiting 消费端的限流
+ no4ttlqueue TTL队列
+ no5dlx 死信交换机（死信队列）
+ no6consumerack 消费者端手动签收与消息的重回队列
+ 还差一个延迟队列，但是RabbitMQ没有直接提供，可以使用ttl+死信交换机一起实现，或者引入差价

