# springboot-algorithm

## 目录介绍
### bitoperation 位运算
+ Xor 异或运算
  + topic1 如何不用额外变量的情况下交互两个数（前提是两个数不指向同一个内存地址）
  + topic2 一个数组中只有一种出现了奇数次，其他数都出现了偶数次，怎么找到奇数此数，并打印这种数
  + topic3 怎么把一个int类型的数，在二进制的情形下，提取出最右侧的1来
  + topic4 一个数组中只有两种出现了奇数次，其他数都出现了偶数次，怎么找到两种奇数次，并打印这种数：设两种奇数次，分别为 x ，y
  + topic5 给出一个数，数二进制含有1的个数：


### dynamicprogramming 动态规划（这块的题，需要有时间看一看）
+ Coffee 清洗咖啡杯问题
+ CoinsWay 每个值代表一种面值的货币，每种面值的货币可以使用任意张，再给定一个整数aim，代表要找的钱数，求组成 aim 的方法数
+ PalindromeSubsequence 两个字符串的最长公共子序列问题
+ RobotWalk 机器人来回走问题
+ StickersToSpellWord 贴纸问题

### graph 图
+ Graph 自定的图结构
+ Node 自定图的节点
+ Edge 自定图的边
+ GraphGenerator 举例一种图结构转换成自定义的图结构
+ BFS 宽度(广度)优先遍历
+ DFS 深度优先遍历
+ Dijkstra 迪杰斯特拉算法 -->最短路径算法
+ Kruskal 克鲁斯卡尔算法 --> 最小生成树
+ Prim 普里姆算法 --> 最小生成树
+ TopologySort 拓扑排序

### greedy 贪心算法
+ BestArrange 最优会议安排
+ Light 居民点放等问题
+ LowestString 拼接字符串，返回最小字典序
+ MaxProfit 最大利润
+ SplitGold 切割黄金

### heap
+ DynamicHeap 动态修改堆
+ JdkHeap Jdk自己实现堆结构
+ MaxHeap 自己实现大根堆

### link 链表
+ singlyLink 单链表
  + 空链表
  + addByNullHead 头为空节点插入
  + addByHead 头节点插入
  + addByMiddle 中间节点插入(插入在 head 与 next 之间)
  + whileNode 循环
  + removeByHead 头结点删除
  + removeByMiddle 中间结点删除(删除在 head 与 next 之间)
  + reverse 翻转
+ DoubleLink 双链表
  + addOnAfter head节点之后插入
  + addOnBefore head.next节点之前插入
  + whileNode 循环
  + remove 删除
  + reverse 翻转
+ CopyListWithRandom rand指针是单链表节点结构中新增的指针
+ FindFirstIntersectNode 给定两个可能有环也可能无环的单链表，头节点head1和head2。请实现一个函数，如果两个链表相交，请返回相交的 第一个节点。如果不相交，返回null
+ IsPalindromeList 给定一个单链表的头节点head，请判断该链表是否为回文结构
+ fastslowpointer 快慢指针 
  + FastSlowPointerNode  快慢指针知识
    + midOrUpMidNode 输入链表头节点，奇数长度返回中点，偶数长度返回上中点
    + midOrDownMidNode 输入链表头节点，奇数长度返回中点，偶数长度返回下中点
    + midOrUpMidPreNode 输入链表头节点，奇数长度返回中点前一个，偶数长度返回上中点前一个
    + midOrDownMidPreNode 输入链表头节点，奇数长度返回中点前一个，偶数长度返回下中点前一个
  + FastSlowPointerTopic 快慢指针相关算法题
    + isRingNode 是否有环
    + RingNodeLength 环的长度
    + getConnectNode 获取连接点
    + 
+ RemoveSomeNode 能不能不给单链表的头节点，只给想要删除的节点，就能做到在链表上把这个点删掉？
+ SmallerEqualBigger 将单向链表按某值划分成左边小、中间相等、右边大的形式


### recursion 递归
+ Recursion
  + getMax1 求数组arr[L ... R]最大值，怎么用递归方法实现。
  + getMax2 求数组arr[L ... R]最大值，怎么用递归方法实现。
#### violentrecursion 暴利递归
+ CardsInLine 玩家发牌问题
+ ConvertToLetterString 规定1和A对应、2和B对应、3和C对应... 26和Z对应 ... 转换结果问题
+ Fibonacci 斐波那契数列
+ Hanoi 汉诺塔问题
+ Knapsack 背包问题
+ NQueens N皇后问题
+ PrintAllPermutations 打印一个字符串的全部排列 和 打印一个字符串的全部排列，要求不要出现重复的排列
+ PrintAllSubsquences 打印一个字符串的全部子序列 和 打印一个字符串的全部子序列，要求不要出现重复字面值的子序列
+ ReverseStackUsingRecursive 给你一个栈，请你逆序这个栈，不能申请额外的数据结构，只能使用递归函数?

### search  查找算法
+ DichotomicSearch 二分法查找
  + dichotomic1 二分算法一
  + dichotomic2 二分算法二
  + dichotomic3 二分算法变形：在一个有序数组中，找出>=某个数最左侧的位置
  + dichotomic4 二分算法变形：在一个有序数组中，找出<=某个数最右侧的位置
  + dichotomic5 二分算法变形：无序一个数组，相邻两个元素不等，找出任意一个局部最小
  
+ FibonacciSearch 斐波那契查找

### sort 排序
+ BubbleSort 冒泡排序
+ BucketSort 桶排序
  + countSort 计数排序
  + radixSort 基数排序
+ FastSort 快速排序
+ FastTopic 快速排序，相关算法题
  + partition_1 Partition问题
  + partition_2 Partition问题
  + hollandFlag 荷兰国旗问题
+ HeapSort 堆排序
+ HeapTopic 堆排序，相关算法题
  + topic1 已知一个几乎有序的数组。几乎有序是指，如果把数组排好顺序的话，每个元素移动的距离一定不超过k，并且k相对于数组长度来说是比较小的。
+ InsertSort 插入排序 
+ MergeSort 归并排序
+ MergeTopic 归并排序，相关算法题
  + topic1 在一个数组中，一个数左边比它小的数的总和，叫数的小和，所有数的小和累加起来，叫数组小和。求数组小和
  + topic2 求数组当前顺序中的升序对：举例：[3,1,7,0,2]：(3,7) (1,7) (1,2) (0,2)
  + topic3 求数组当前顺序中的降序对：举例：[3,1,7,0,2]：(3,1) (3,0) (3,2) (1,0)  (7,0) (7,2)
+ SelectSort 选择排序

### stackandqueue 栈和队列
+ QueueWhileArray 使用数组实现循环队列
+ StackArray 使用数组实现栈
+ StackQueueLinked 使用链表实现栈和队列
+ StackQueueTopic 栈和队列相关算法题
  + peekAndPop 补充知识点：Stack中peek与Pop的区别
  + topic1 实现一个特殊的栈，在基本功能的基础上，再实现返回栈中最小元素的功能
  + topic2 如何使用栈结构实现队列结构？
  + topic3 如何使用队列结构实现栈结构？
 
### tree 树
+ binarytreerecursion 二叉树递归套路
  + IsBalanced 给定一棵二叉树的头节点head，返回这颗二叉树是不是平衡二叉树
  + IsBST 给定一棵二叉树的头节点head，返回这颗二叉树是不是搜索二叉树
  + IsCBT 给定一棵二叉树的头节点head，返回这颗二叉树中是不是完全二叉树
  + IsFull 给定一棵二叉树的头节点head，返回这颗二叉树是不是满二叉树
  + lowestAncestor 给定一棵二叉树的头节点head，和另外两个节点a和b。返回a和b的最低公共祖先
  + MaxDistance 给定一棵二叉树的头节点head，任何两个节点之间都存在距离，返回整棵二叉树的最大距离
  + MaxHappy 派对的最大快乐值
  + MaxSubBSTHead 给定一棵二叉树的头节点head，返回这颗二叉树中最大的二叉搜索子树的头节点
  + MaxSubBSTSize 给定一棵二叉树的头节点head，返回这颗二叉树中最大的二叉搜索子树的大小
+ PrefixTree1 前缀树
+ PrefixTree2 前缀树
+ BinaryTreeBase 二叉树
  + pre 递归先序
  + mid 递归中序
  + pos 递归后序
  + recursion 递归序解释，先中后序
  + preStack 栈先序
  + midStack 栈中序
  + posStack 栈后序
  + posStack1 栈后序1
  + layerFor 按层遍历
  + serializationByPre 先序方式序列化
  + deserializationByPre 先序方式反序列化
  + serializationByLayer 按层序列化
  + deserializationByLayer 按层反序列化
+ BinaryTreeTopic 二叉树算法题
  + topic1 求最大层节点数用Map
  + topic2 求最大层节点数不用Map
+ PaperFolding 折纸条问题
+ SuccessorNode 定义出一个新的结构，给你二叉树中的某个节点，返回该节点的后继节点  

### unionfindset 并查集
+ UnionFindSet 并查集
+ MergeUser 合并用户实体

### util 工具类
+ AlgorithmUtil 算法工具类
  + swap_1 交换1
  + swap_2 交换2
  + generateRandomArray 数组随机生成器
  + generateRandomSinglyLinked 随机生成单链表
  + generateRandomDoubleList 随机生成双链表







