package com.hanxiaozhang.tree;

import java.util.HashMap;

/**
 * 〈一句话功能简述〉<br>
 * 〈前缀树2版本〉
 *
 * @author hanxinghua
 * @create 2021/9/15
 * @since 1.0.0
 */
public class PrefixTree2 {


    public static void main(String[] args) {

        Tree tree = new Tree();
        tree.insert("abc");
        tree.insert("abcf");
        tree.insert("abca");
        tree.insert("abcc");
        tree.insert("add");
        tree.insert("abed");
        tree.insert("abc");
        System.out.println("abc search number is "+tree.search("abc"));
        System.out.println("abc prefixNumber is "+tree.prefixNumber("abc"));
        tree.delete("abc");
        System.out.println("abc search number is "+tree.search("abc"));
        System.out.println("abc prefixNumber is "+tree.prefixNumber("abc"));


    }

    /**
     * 使用HashMap存储路，
     * 可以存储更多种（类别PrefixTree1）
     *
     */
    public static class Node {
        public int pass;
        public int end;
        public HashMap<Integer, Node> nexts;

        public Node() {
            pass = 0;
            end = 0;
            nexts = new HashMap<>();
        }
    }

    public static class Tree {
        private Node root;

        public Tree() {
            root = new Node();
        }

        public void insert(String word) {
            if (word == null) {
                return;
            }
            char[] chs = word.toCharArray();
            Node node = root;
            node.pass++;
            int index = 0;
            for (int i = 0; i < chs.length; i++) {
                index = (int) chs[i];
                if (!node.nexts.containsKey(index)) {
                    node.nexts.put(index, new Node());
                }
                node = node.nexts.get(index);
                node.pass++;
            }
            node.end++;
        }

        public void delete(String word) {
            if (search(word) != 0) {
                char[] chs = word.toCharArray();
                Node node = root;
                node.pass--;
                int index = 0;
                for (int i = 0; i < chs.length; i++) {
                    index = (int) chs[i];
                    if (--node.nexts.get(index).pass == 0) {
                        node.nexts.remove(index);
                        return;
                    }
                    node = node.nexts.get(index);
                }
                node.end--;
            }
        }

        // word这个单词之前加入过几次
        public int search(String word) {
            if (word == null) {
                return 0;
            }
            char[] chs = word.toCharArray();
            Node node = root;
            int index = 0;
            for (int i = 0; i < chs.length; i++) {
                index = (int) chs[i];
                if (!node.nexts.containsKey(index)) {
                    return 0;
                }
                node = node.nexts.get(index);
            }
            return node.end;
        }

        // 所有加入的字符串中，有几个是以pre这个字符串作为前缀的
        public int prefixNumber(String pre) {
            if (pre == null) {
                return 0;
            }
            char[] chs = pre.toCharArray();
            Node node = root;
            int index = 0;
            for (int i = 0; i < chs.length; i++) {
                index = (int) chs[i];
                if (!node.nexts.containsKey(index)) {
                    return 0;
                }
                node = node.nexts.get(index);
            }
            return node.pass;
        }
    }
}
