package com.hanxiaozhang.tree.binarytreerecursion;

import com.hanxiaozhang.tree.BinaryTreeUtil;
import com.hanxiaozhang.tree.Node;

import java.util.ArrayList;

/**
 * 〈一句话功能简述〉<br>
 * 〈给定一棵二叉树的头节点head，返回这颗二叉树是不是搜索二叉树〉
 * <p>
 * BST -> Binary Search Tree
 * <p>
 * 重点看一下，isBST1() 简单容易理解 2024-04-10
 *
 * @author hanxinghua
 * @create 2021/10/30
 * @since 1.0.0
 */
public class IsBST {

    public static void main(String[] args) {
        int maxLevel = 4;
        int maxValue = 100;
        int testTimes = 1000000;
        for (int i = 0; i < testTimes; i++) {
            Node head = generateRandomBST(maxLevel, maxValue);
            if (isBST1(head) != isBST2(head)) {
                System.out.println("Oops!");
            }
        }
        System.out.println("finish!");
    }


    /**
     * 方法1
     * 重要推论：二叉搜索树的中序遍历必定是升序的。
     * 根据该推论，将二叉树中序遍历结果存入数组，判断数组中元素是否为升序
     *
     * @param head
     * @return
     */
    public static boolean isBST1(Node head) {
        if (head == null) {
            return true;
        }
        ArrayList<Node> arr = new ArrayList<>();
        in(head, arr);
        for (int i = 1; i < arr.size(); i++) {
            if (arr.get(i).value <= arr.get(i - 1).value) {
                return false;
            }
        }
        return true;
    }

    public static void in(Node head, ArrayList<Node> arr) {
        if (head == null) {
            return;
        }
        in(head.left, arr);
        arr.add(head);
        in(head.right, arr);
    }

    public static boolean isBST2(Node head) {
        if (head == null) {
            return true;
        }
        return process(head).isBST;
    }

    /**
     * 递归
     *
     * @param head
     * @return
     */
    public static Info process(Node head) {
        // base case
        if (head == null) {
            return null;
        }
        Info leftInfo = process(head.left);
        Info rightInfo = process(head.right);
        int min = head.value;
        int max = head.value;
        // 左树不为空
        if (leftInfo != null) {
            min = Math.min(min, leftInfo.min);
            max = Math.max(max, leftInfo.max);
        }
        // 右树不为空
        if (rightInfo != null) {
            min = Math.min(min, rightInfo.min);
            max = Math.max(max, rightInfo.max);
        }
        boolean isBST = false;
        // leftInfo == null  -> 左子树是空一定是搜索二叉树
        // eftInfo ! = null && leftInfo.isBST  && leftInfo.max < head.value
        // rightInfo == null
        // rightInfo ! = null &&rightInfo.isBST && rightInfo.min > head.value
        if ((leftInfo == null ? true : (leftInfo.isBST && leftInfo.max < head.value))
                &&
                (rightInfo == null ? true : (rightInfo.isBST && rightInfo.min > head.value))
        ) {
            isBST = true;
        }
        return new Info(isBST, min, max);
    }

    /**
     * 实体
     */
    public static class Info {
        // 是不是搜索二叉树
        boolean isBST;
        // 最小值
        public int max;
        // 最大值
        public int min;

        public Info(boolean is, int mi, int ma) {
            isBST = is;
            min = mi;
            max = ma;
        }
    }

    // for test
    public static Node generateRandomBST(int maxLevel, int maxValue) {
        return BinaryTreeUtil.generate(1, maxLevel, maxValue);
    }


}
