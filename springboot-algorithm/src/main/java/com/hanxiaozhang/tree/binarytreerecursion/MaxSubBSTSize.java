package com.hanxiaozhang.tree.binarytreerecursion;

import com.hanxiaozhang.tree.BinaryTreeUtil;
import com.hanxiaozhang.tree.Node;

import java.util.ArrayList;

/**
 * 〈一句话功能简述〉<br>
 * 〈给定一棵二叉树的头节点head，返回这颗二叉树中最大的二叉搜索子树的大小〉
 *
 * @author hanxinghua
 * @create 2021/10/28
 * @since 1.0.0
 */
public class MaxSubBSTSize {


    public static void main(String[] args) {
        int maxLevel = 4;
        int maxValue = 100;
        int testTimes = 1000000;
        for (int i = 0; i < testTimes; i++) {
            Node head = generateRandomBST(maxLevel, maxValue);
            if (maxSubBSTSize1(head) != maxSubBSTSize2(head)) {
                System.out.println("Oops!");
            }
        }
        System.out.println("finish!");
    }

    public static int getBSTSize(Node head) {
        if (head == null) {
            return 0;
        }
        ArrayList<Node> arr = new ArrayList<>();
        in(head, arr);
        for (int i = 1; i < arr.size(); i++) {
            if (arr.get(i).value <= arr.get(i - 1).value) {
                return 0;
            }
        }
        return arr.size();
    }

    public static void in(Node head, ArrayList<Node> arr) {
        if (head == null) {
            return;
        }
        in(head.left, arr);
        arr.add(head);
        in(head.right, arr);
    }

    public static int maxSubBSTSize1(Node head) {
        if (head == null) {
            return 0;
        }
        int h = getBSTSize(head);
        if (h != 0) {
            return h;
        }
        return Math.max(maxSubBSTSize1(head.left), maxSubBSTSize1(head.right));
    }

    public static int maxSubBSTSize2(Node head) {
        if (head == null) {
            return 0;
        }
        return process(head).maxSubBSTSize;
    }

    /**
     * 递归
     *
     * @param head
     * @return
     */
    public static Info process(Node head) {
        if (head == null) {
            return null;
        }
        Info leftInfo = process(head.left);
        Info rightInfo = process(head.right);

        int min = head.value;
        int max = head.value;
        // 左子树不空
        if (leftInfo != null) {
            // 比较
            min = Math.min(min, leftInfo.min);
            max = Math.max(max, leftInfo.max);
        }
        // 右子树不空
        if (rightInfo != null) {
            // 比较
            min = Math.min(min, rightInfo.min);
            max = Math.max(max, rightInfo.max);
        }
        // ---- 可能性1：搜索树不包含头结点的情况，即该整颗树不是搜索二叉树
        int maxSubBSTSize = 0;
        boolean isBST = false;
        // 左子树不空
        if (leftInfo != null) {
            // 比较
            maxSubBSTSize = Math.max(maxSubBSTSize, leftInfo.maxSubBSTSize);
        }
        // 右子树不空
        if (rightInfo != null) {
            // 比较
            maxSubBSTSize = Math.max(maxSubBSTSize, rightInfo.maxSubBSTSize);
        }

        // ---- 可能性2：搜索树包含头结点的情况，即该整颗树搜索二叉树
        // 左子树整体是搜索二叉树 ：
        // leftInfo == null  -> 左子树是空一定是搜索二叉树
        // eftInfo ! = null && leftInfo.isBST  && leftInfo.max < head.value
        // rightInfo == null
        // rightInfo ! = null &&rightInfo.isBST && rightInfo.min > head.value
        if ((leftInfo == null ? true : (leftInfo.isBST && leftInfo.max < head.value))
                && (rightInfo == null ? true : (rightInfo.isBST && rightInfo.min > head.value))) {
            isBST = true;
            // 如果子树整体是搜索二叉树，所以maxSubBSTSize就是子树的大小
            maxSubBSTSize = (leftInfo == null ? 0 : leftInfo.maxSubBSTSize)
                    + (rightInfo == null ? 0 : rightInfo.maxSubBSTSize) + 1;
        }
        return new Info(isBST, maxSubBSTSize, min, max);
    }

    // for test
    public static Node generateRandomBST(int maxLevel, int maxValue) {
        return BinaryTreeUtil.generate(1, maxLevel, maxValue);
    }

    private static class Info {

        /**
         * 是不是搜索二叉树
         */
        public boolean isBST;

        /**
         * 最大二叉子树的大小
         */
        public int maxSubBSTSize;

        /**
         * 最小值
         */
        public int min;

        /**
         * 最大值
         */
        public int max;

        public Info(boolean is, int size, int mi, int ma) {
            isBST = is;
            maxSubBSTSize = size;
            min = mi;
            max = ma;
        }
    }

}
