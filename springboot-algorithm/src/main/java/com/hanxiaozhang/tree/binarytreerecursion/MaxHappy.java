package com.hanxiaozhang.tree.binarytreerecursion;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈派对的最大快乐值〉
 *
 * @author hanxinghua
 * @create 2021/10/28
 * @since 1.0.0
 */
public class MaxHappy {


    public static void main(String[] args) {
        int maxLevel = 4;
        int maxNexts = 7;
        int maxHappy = 100;
        int testTimes = 100000;
        for (int i = 0; i < testTimes; i++) {
            Employee boss = genarateBoss(maxLevel, maxNexts, maxHappy);
            if (maxHappy1(boss) != maxHappy2(boss)) {
                System.out.println("Oops!");
            }
        }
        System.out.println("finish!");
    }

    public static int maxHappy1(Employee boss) {
        if (boss == null) {
            return 0;
        }
        return process1(boss, false);
    }

    public static int process1(Employee cur, boolean up) {
        if (up) {
            int ans = 0;
            for (Employee next : cur.nexts) {
                ans += process1(next, false);
            }
            return ans;
        } else {
            int p1 = cur.happy;
            int p2 = 0;
            for (Employee next : cur.nexts) {
                p1 += process1(next, true);
                p2 += process1(next, false);
            }
            return Math.max(p1, p2);
        }
    }

    public static int maxHappy2(Employee boss) {
        if (boss == null) {
            return 0;
        }
        Info all = process2(boss);
        return Math.max(all.yes, all.no);
    }


    /**
     * 递归2
     *
     * @param x
     * @return
     */
    public static Info process2(Employee x) {
        // 如果没有下级，返回自己的情况
        if (x.nexts.isEmpty()) {
            return new Info(x.happy, 0);
        }
        // 自己来
        int yes = x.happy;
        // 自己不来
        int no = 0;
        for (Employee next : x.nexts) {
            // 获取自己下级员工的信息
            Info nextInfo = process2(next);
            // 处理yes
            yes += nextInfo.no;
            //  处理no
            no += Math.max(nextInfo.yes, nextInfo.no);
        }
        return new Info(yes, no);
    }

    // for test
    public static Employee genarateBoss(int maxLevel, int maxNexts, int maxHappy) {
        if (Math.random() < 0.02) {
            return null;
        }
        Employee boss = new Employee((int) (Math.random() * (maxHappy + 1)));
        genarateNexts(boss, 1, maxLevel, maxNexts, maxHappy);
        return boss;
    }

    // for test
    public static void genarateNexts(Employee e, int level, int maxLevel, int maxNexts, int maxHappy) {
        if (level > maxLevel) {
            return;
        }
        int nextsSize = (int) (Math.random() * (maxNexts + 1));
        for (int i = 0; i < nextsSize; i++) {
            Employee next = new Employee((int) (Math.random() * (maxHappy + 1)));
            e.nexts.add(next);
            genarateNexts(next, level + 1, maxLevel, maxNexts, maxHappy);
        }
    }

    /**
     * 来不来实体
     */
    public static class Info {
        /**
         * 来的快乐值
         */
        public int yes;
        /**
         * 不来的快乐值
         */
        public int no;

        public Info(int y, int n) {
            yes = y;
            no = n;
        }
    }

    /**
     * 员工
     */
    private static class Employee {
        /**
         * 快乐值
         */
        public int happy;
        /**
         * 下级
         */
        public List<Employee> nexts;

        public Employee(int h) {
            happy = h;
            nexts = new ArrayList<>();
        }

    }


}
