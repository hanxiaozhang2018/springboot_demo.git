package com.hanxiaozhang.tree;

/**
 * 〈一句话功能简述〉<br>
 * 〈定义出一个新的结构，给你二叉树中的某个节点，返回该节点的后继节点 〉
 *
 *  Tips:后续节点是指在中序遍历过程中，一个节点的后一个节点
 *
 *
 *
 * @author hanxinghua
 * @create 2021/10/26
 * @since 1.0.0
 */
public class SuccessorNode {


    public static void main(String[] args) {
        Node head = new Node(6);
        head.parent = null;
        head.left = new Node(3);
        head.left.parent = head;
        head.left.left = new Node(1);
        head.left.left.parent = head.left;
        head.left.left.right = new Node(2);
        head.left.left.right.parent = head.left.left;
        head.left.right = new Node(4);
        head.left.right.parent = head.left;
        head.left.right.right = new Node(5);
        head.left.right.right.parent = head.left.right;
        head.right = new Node(9);
        head.right.parent = head;
        head.right.left = new Node(8);
        head.right.left.parent = head.right;
        head.right.left.left = new Node(7);
        head.right.left.left.parent = head.right.left;
        head.right.right = new Node(10);
        head.right.right.parent = head.right;

        Node test = head.left.left;
        System.out.println(test.value + " next: " + getNextNode(test).value);
        test = head.left.left.right;
        System.out.println(test.value + " next: " + getNextNode(test).value);
        test = head.left;
        System.out.println(test.value + " next: " + getNextNode(test).value);
        test = head.left.right;
        System.out.println(test.value + " next: " + getNextNode(test).value);
        test = head.left.right.right;
        System.out.println(test.value + " next: " + getNextNode(test).value);
        test = head;
        System.out.println(test.value + " next: " + getNextNode(test).value);
        test = head.right.left.left;
        System.out.println(test.value + " next: " + getNextNode(test).value);
        test = head.right.left;
        System.out.println(test.value + " next: " + getNextNode(test).value);
        test = head.right;
        System.out.println(test.value + " next: " + getNextNode(test).value);
        test = head.right.right; // 10's next is null
        System.out.println(test.value + " next: " + getNextNode(test));
    }


    /**
     * 寻找后继节点
     * 
     * @param node
     * @return
     */
    public static Node getNextNode(Node node) {
       // 节点为空，返回空
        if (node == null) {
            return null;
        }
        // 节点的右子树不为空，找到右子树的最左孩子
        if (node.right != null) {
            return getLeftMost(node.right);
        } else { // 节点的右子树为空，往上找，找到我是我父亲的左孩子时，返回父亲
            Node parent = node.parent;
            // parent != null && parent.right == node -> 有父亲，并且当前节点是其父亲节点右孩子
            while (parent != null && parent.right == node) {
                node = parent;
                parent = node.parent;
            }
            return parent;
        }
    }



    /**
     * 获取最左孩子
     *
     * @param node
     * @return
     */
    private static Node getLeftMost(Node node) {
        if (node == null) {
            return null;
        }
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }


    private static class Node {
        public int value;
        public Node left;
        public Node right;
        public Node parent;

        public Node(int data) {
            this.value = data;
        }
    }


}
