package com.hanxiaozhang.tree;

/**
 * 〈一句话功能简述〉<br>
 * 〈树节点〉
 *
 * @author hanxinghua
 * @create 2021/9/16
 * @since 1.0.0
 */
public class Node {

    public int value;
    public Node left;
    public Node right;

    public Node(int value) {
        this.value = value;
    }


}
