package com.hanxiaozhang.link;

/**
 * 〈一句话功能简述〉<br>
 * 〈能不能不给单链表的头节点，只给想要删除的节点，就能做到在链表上把这个点删掉？〉
 *
 * @author hanxinghua
 * @create 2021/10/25
 * @since 1.0.0
 */
public class RemoveSomeNode {

    public static void main(String[] args) {
        Node head = new Node(1);
        Node node2 = new Node(2);
        head.next = node2;
        Node node3 = new Node(3);
        node2.next = node3;
        Node node4 = new Node(4);
        node3.next = node4;
        Node node5 = new Node(5);
        node4.next = node5;


        printNode(head);
        // 不给头结点，删除node3。抖机灵的做法，前提node3不是尾节点，这种做法有问题的
        while (node3.next.next != null) {
            node3.value = node3.next.value;
            node3 = node3.next;
        }
        node3.value = node3.next.value;
        node3.next = null;
        printNode(head);

        Node head11 = new Node(11);
        Node node12 = new Node(12);
        head11.next = node12;
        Node node13 = new Node(13);
        node12.next = node13;
        Node node14 = new Node(14);
        node13.next = node14;
        Node node15 = new Node(15);
        node14.next = node15;
        printNode(head11);
        node13 = null;
        printNode(head11);
    }


    private static class Node {
        public int value;
        public Node next;

        public Node(int data) {
            this.value = data;
        }
    }

    private static void printNode(Node head) {
        while (head != null) {
            System.out.print(head.value);
            System.out.print(" ");
            head = head.next;
        }
        System.out.println();
    }


}




