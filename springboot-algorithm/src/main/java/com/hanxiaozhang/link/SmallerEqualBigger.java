package com.hanxiaozhang.link;

/**
 * 〈一句话功能简述〉<br>
 * 〈将单向链表按某值划分成左边小、中间相等、右边大的形式〉
 *
 * @author hanxinghua
 * @create 2021/10/24
 * @since 1.0.0
 */
public class SmallerEqualBigger {


    public static void main(String[] args) {
        Node head1 = new Node(7);
        head1.next = new Node(9);
        head1.next.next = new Node(1);
        head1.next.next.next = new Node(8);
        head1.next.next.next.next = new Node(5);
        head1.next.next.next.next.next = new Node(2);
        head1.next.next.next.next.next.next = new Node(5);
        printLinkedList(head1);
        // head1 = listPartition1(head1, 4);
        head1 = listPartition2(head1, 5);
        printLinkedList(head1);

    }

    /**
     * 方法1
     * <p>
     * 1) 将链表放入数组中
     * 2) 数组进行荷兰国旗划分
     * 3) 将数组再转换成链表
     *
     * @param head
     * @param pivot
     * @return
     */
    public static Node listPartition1(Node head, int pivot) {
        if (head == null) {
            return head;
        }
        Node cur = head;
        int i = 0;
        while (cur != null) {
            i++;
            cur = cur.next;
        }
        Node[] nodeArr = new Node[i];
        i = 0;
        cur = head;
        for (i = 0; i != nodeArr.length; i++) {
            nodeArr[i] = cur;
            cur = cur.next;
        }
        arrPartition(nodeArr, pivot);
        for (i = 1; i != nodeArr.length; i++) {
            nodeArr[i - 1].next = nodeArr[i];
        }
        nodeArr[i - 1].next = null;
        return nodeArr[0];
    }


    /**
     * 荷兰国旗任务
     *
     * @param nodeArr
     * @param pivot
     */
    private static void arrPartition(Node[] nodeArr, int pivot) {
        int small = -1;
        int big = nodeArr.length;
        int index = 0;
        while (index != big) {
            if (nodeArr[index].value < pivot) {
                swap(nodeArr, ++small, index++);
            } else if (nodeArr[index].value == pivot) {
                index++;
            } else {
                swap(nodeArr, --big, index);
            }
        }
    }

    /**
     * 交换
     *
     * @param nodeArr
     * @param a
     * @param b
     */
    private static void swap(Node[] nodeArr, int a, int b) {
        Node tmp = nodeArr[a];
        nodeArr[a] = nodeArr[b];
        nodeArr[b] = tmp;
    }


    /**
     * 方法2
     * 分成小、中、大三部分，再把各个部分之间串起来
     *
     * @param head
     * @param pivot
     * @return
     */
    public static Node listPartition2(Node head, int pivot) {
        // 小链表的head
        Node sH = null;
        // 小链表的tail
        Node sT = null;
        // 等于链表的head
        Node eH = null;
        // 等于链表的tail
        Node eT = null;
        // 大链表的head
        Node gH = null;
        // 大链表的tail
        Node gT = null;
        Node next = null;
        // 循环
        while (head != null) {
            // 获取下一个节点
            next = head.next;
            // 把每个节点断开
            head.next = null;
            // 进行三个判断
            if (head.value < pivot) {
                if (sH == null) {
                    sH = head;
                    sT = head;
                } else {
                    sT.next = head;
                    sT = head;
                }
            } else if (head.value == pivot) {
                if (eH == null) {
                    eH = head;
                    eT = head;
                } else {
                    eT.next = head;
                    eT = head;
                }
            } else {
                if (gH == null) {
                    gH = head;
                    gT = head;
                } else {
                    gT.next = head;
                    gT = head;
                }
            }
            head = next;
        }
        // ----做法：小于区域的尾巴，连等于区域的头，等于区域的尾巴，连接大于区域的头
        // 如果有小于区域
        if (sT != null) {
            // 小于区域的尾巴，连等于区域的头
            sT.next = eH;
            // 判断等于区域的尾巴，如果没有等于为空，sT就是变成eT，反之...
            eT = (eT == null) ? sT : eT;
        }
        // 如果有等于区域
        if (eT != null) {
            // 等于区域的尾巴，连接大于区域的头
            eT.next = gH;
        }
        // sH != null 从小于区域返回。
        // sH == null && eH != null 从等于区域返回。
        // sH == null && eH == null 从大于区域返回。
        return sH != null ? sH : (eH != null ? eH : gH);
    }


    private static class Node {
        public int value;
        public Node next;

        public Node(int data) {
            this.value = data;
        }
    }

    private static void printLinkedList(Node node) {
        System.out.print("Linked List: ");
        while (node != null) {
            System.out.print(node.value + " ");
            node = node.next;
        }
        System.out.println();
    }

}
