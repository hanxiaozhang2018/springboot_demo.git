package com.hanxiaozhang.link.singlylink;

/**
 * 〈一句话功能简述〉<br>
 * 〈单节点〉
 *
 * @author hanxinghua
 * @create 2021/9/5
 * @since 1.0.0
 */
public class Node<T> {

    public T data;
    public Node<T> next;


    public Node(T data, Node next) {
        this.data = data;
        this.next = next;
    }

    public Node(T data) {
        this.data = data;
    }

}
