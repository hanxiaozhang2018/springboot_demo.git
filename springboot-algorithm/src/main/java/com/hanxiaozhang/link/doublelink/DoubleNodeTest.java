package com.hanxiaozhang.link.doublelink;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/25
 * @since 1.0.0
 */
public class DoubleNodeTest {

    public static void main(String[] args) {

        DoubleNode<Integer> head = new DoubleNode<>(1);
        DoubleNode node2 = new DoubleNode<>(2, head);
        head.next = node2;
        DoubleNode node3 = new DoubleNode<>(3, node2);
        node2.next = node3;
        DoubleNode node4 = new DoubleNode<>(4, node3);
        node3.next = node4;

        printLog(head);
        DoubleNode pre = reserve(head);
        printLog(pre);

    }


    private static void printLog(DoubleNode<Integer> head) {
        System.out.println("---- ---- ---- ----");
        while (head != null) {
            System.out.println(head.data);
            head = head.next;
        }
    }



    private static DoubleNode reserve(DoubleNode<Integer> head) {
        DoubleNode pre = null;
        DoubleNode next = null;
        while (head != null) {
            next = head.next;
            head.pre = next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

}
