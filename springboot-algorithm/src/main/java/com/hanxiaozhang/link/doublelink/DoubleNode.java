package com.hanxiaozhang.link.doublelink;

/**
 * 〈一句话功能简述〉<br>
 * 〈双节点〉
 *
 * @author hanxinghua
 * @create 2021/9/5
 * @since 1.0.0
 */
public class DoubleNode<T>{

    public T data;
    public DoubleNode<T> pre;
    public DoubleNode<T> next;


    public DoubleNode(T data) {
        this.data = data;
    }


    public DoubleNode(T data, DoubleNode<T> pre, DoubleNode<T> next) {
        this.data = data;
        this.pre = pre;
        this.next = next;
    }
    public DoubleNode(T data, DoubleNode<T> pre) {
        this.data = data;
        this.pre = pre;
    }
}
