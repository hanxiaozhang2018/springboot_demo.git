package com.hanxiaozhang.search;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/24
 * @since 1.0.0
 */
public class Test {

    public static void main(String[] args) {

        int[] arr = {1, 2, 4, 6, 8, 10};

        System.out.println(test(arr,4));


    }

    /**
     * 问题 2022-12-23
     *  right = arr.length  ->   right = arr.length-1
     *  right = mid  ->   right = mid-1;
     *  left = mid  ->   left = mid-1;
     *
     * @param arr
     * @param target
     * @return
     */
    public static int test(int[] arr, int target) {

        int left = 0, right = arr.length-1, mid = 0;
        while (left <= right) {
            mid = (right - left) >> 1;
            if (arr[mid] == target) {
                return mid;
            } else if (arr[mid] > target) {
                right = mid-1;
            } else {
                left = mid-1;
            }
        }
        return -1;
    }


}
