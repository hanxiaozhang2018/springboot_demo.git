package com.hanxiaozhang.heap;

import java.util.PriorityQueue;

/**
 * 〈一句话功能简述〉<br>
 * 〈jdk中实现的堆〉
 *
 * @author hanxinghua
 * @create 2021/9/14
 * @since 1.0.0
 */
public class JdkHeap {

    public static void main(String[] args) {

        // 默认是小根堆
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        heap.add(2);
        heap.add(5);
        heap.add(1);
        heap.add(42);
        heap.add(66);
        heap.add(4);
        while (!heap.isEmpty()){
            System.out.println(heap.poll());
        }

    }

}
