package com.hanxiaozhang.util;

import com.hanxiaozhang.link.doublelink.DoubleNode;
import com.hanxiaozhang.link.singlylink.Node;

import java.util.Arrays;

/**
 * 〈一句话功能简述〉<br>
 * 〈算法工具类〉
 *
 * @author hanxinghua
 * @create 2021/9/2
 * @since 1.0.0
 */
public class AlgorithmUtil {

    /**
     * 交换1
     *
     * @param arr
     * @param i
     * @param j
     */
    public static void swap_1(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    /**
     * 交换2
     *
     * @param arr
     * @param i
     * @param j
     */
    public static void swap_2(int[] arr, int i, int j) {
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }


    /**
     * 数组随机生成器
     * <p>
     * Math.random()   [0,1)
     * (int)(Math.random() * N)  [0, N-1]
     *
     * @param maxSize
     * @param maxValue
     * @return
     */
    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            // [-? , +?]
            arr[i] = (int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random());
        }
        return arr;
    }


    /**
     * 随机生成单链表
     *
     * @param len
     * @param value
     * @return
     */
    public static Node generateRandomSinglyLinked(int len, int value) {

        // 创建一个头结点
        Node head = new Node((int) (Math.random() * (value + 1)));
        Node pre = head;
        len--;
        // 创建其他节点
        while (len != 0) {
            Node cur = new Node((int) (Math.random() * (value + 1)));
            pre.next = cur;
            pre = cur;
            len--;
        }
        return head;
    }


    /**
     * 随机生成双链表
     *
     * @param len
     * @param value
     * @return
     */
    public static DoubleNode generateRandomDoubleLinked(int len, int value) {
        // 创建一个头结点
        DoubleNode head = new DoubleNode((int) (Math.random() * (value + 1)));
        DoubleNode pre = head;
        len--;
        // 创建其他节点
        while (len != 0) {
            DoubleNode node = new DoubleNode((int) (Math.random() * (value + 1)), pre, pre.next);
            pre.next = node;
            pre = node;
            len--;
        }
        return head;
    }


    public static void main(String[] args) {
        int[] arr = {1, 2};
        swap_2(arr, 1, 1);
        System.out.println(Arrays.toString(arr));

    }
}
