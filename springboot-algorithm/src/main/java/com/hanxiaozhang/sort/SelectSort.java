package com.hanxiaozhang.sort;

import com.hanxiaozhang.util.AlgorithmUtil;

import java.util.Arrays;

/**
 * 〈一句话功能简述〉<br>
 * 〈选择排序法〉
 *
 * @author hanxinghua
 * @create 2021/9/2
 * @since 1.0.0
 */
public class SelectSort {

    /**
     * 在冒泡排序上做了优化，减少了交换次数。
     * 思路：第一次从待排序的数据元素中选出最小（或最大）的一个元素，存放在序列的起始位置，
     * 然后再从剩余的未排序元素中寻找到最小（大）元素，然后放到已排序的序列的末尾。
     * 以此类推，直到全部待排序的数据元素的个数为零。
     * 选择排序是不稳定的排序方法。
     *
     * @param args
     */
    public static void main(String[] args) {
        // int[] arr = {1, 4, 5, 79, 22, 10, 3, 66, 77};
        int[] arr = {9, 3, 21, 1, 66};
        System.out.println(Arrays.toString(selectSort(arr)));
    }


    private static int[] selectSort(int[] arr) {
        // 模拟执行的次数
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            AlgorithmUtil.swap_1(arr, i, minIndex);
        }
        return arr;
    }


}
