package com.hanxiaozhang.sort;

import com.hanxiaozhang.util.AlgorithmUtil;

import java.util.Arrays;

/**
 * 〈一句话功能简述〉<br>
 * 〈冒泡排序〉
 *
 * @author hanxinghua
 * @create 2021/9/2
 * @since 1.0.0
 */
public class BubbleSort {

    /**
     * 冒泡排序（Bubble Sort），是一种计算机科学领域的较简单的排序算法。
     * 它重复地走访过要排序的元素列，依次比较两个相邻的元素，如果顺序（如从大到小、首字母从Z到A）错误就把他们交换过来
     *
     * @param args
     */
    public static void main(String[] args) {
        int[] arr = {9, 3, 21, 1, 66};
        System.out.println(Arrays.toString(train1(arr)));
        System.out.println(Arrays.toString(bubbleSort(arr)));
    }


    private static int[] bubbleSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    AlgorithmUtil.swap_2(arr, j, j + 1);
                }
            }
        }
        return arr;
    }

    /**
     * 训练1
     *
     * @param arr
     * @return
     */
    public static int[] train1(int[] arr) {
        if (arr == null || arr.length == 0 || arr.length == 1) {
            return arr;
        }

        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    AlgorithmUtil.swap_1(arr, j, j + 1);
                }
            }
        }

        return arr;
    }

}
