package com.hanxiaozhang.sort;

import java.util.Arrays;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/27
 * @since 1.0.0
 */
public class SortTest {

    public static void main(String[] args) {

        int[] arr = new int[]{66, 1, 3, 100};

        selectSort(arr);
    }


    /**
     * 选择排序
     *
     * 升序时，选择最小值的代码简洁，好理解，选择最大值代码写起来容易错。
     * 2022-12-27
     *
     * @param arr
     */
    private static void selectSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int maxIndex = 0;
            for (int j = 0; j < arr.length - i; j++) {
                if (arr[j] > arr[maxIndex]) {
                    maxIndex = j;
                }
            }
            swap(maxIndex, arr.length - 1 - i, arr);
        }
        System.out.println(Arrays.toString(arr));
    }

    public static void swap(int i, int j, int[] arr) {
        if (i == j) {
            return;
        }
        arr[i] = arr[i] ^ arr[j];
        arr[j] = arr[i] ^ arr[j];
        arr[i] = arr[i] ^ arr[j];
    }
}
