package com.hanxiaozhang.recursion.violentrecursion;

import java.util.Stack;

/**
 * 〈一句话功能简述〉<br>
 * 〈给你一个栈，请你逆序这个栈，不能申请额外的数据结构，
 *  只能使用递归函数。如何实现?〉
 *
 * @author hanxinghua
 * @create 2021/10/7
 * @since 1.0.0
 */
public class ReverseStackUsingRecursive {


    public static void main(String[] args) {
        Stack<Integer> test = new Stack<Integer>();
        test.push(1);
        test.push(2);
        test.push(3);
        test.push(4);
        test.push(5);
        reverse(test);
        while (!test.isEmpty()) {
            System.out.println(test.pop());
        }

    }

    /**
     * 翻转
     *
     * @param stack
     */
    public static void reverse(Stack<Integer> stack) {
        if (stack.isEmpty()) {
            return;
        }
        // 弹出栈底元素
        int i = f(stack);
        // 递归调用
        reverse(stack);
        // 插入栈中
        stack.push(i);
    }

    /**
     * f函数
     * 作用：返回栈底元素
     *
     *
     * @param stack
     * @return
     */
    public static int f(Stack<Integer> stack) {
        // 弹出一个元素
        int result = stack.pop();
        // 如果栈为空，返回弹出元素
        if (stack.isEmpty()) {
            return result;
        } else {
            // 如果栈不为空，继续调用f函数
            int last = f(stack);
            // 将弹出元素添加回栈
            stack.push(result);
            // 返回 last
            return last;
        }
    }

}
