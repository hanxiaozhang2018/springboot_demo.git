package com.hanxiaozhang.recursion.violentrecursion;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈打印一个字符串的全部排列
 *   打印一个字符串的全部排列，要求不要出现重复的排列
 * 〉
 *
 * @author hanxinghua
 * @create 2021/10/12
 * @since 1.0.0
 */
public class PrintAllPermutations {


    public static void main(String[] args) {
        String s = "aac";
        List<String> ans1 = permutation(s);
        for (String str : ans1) {
            System.out.println(str);
        }
        System.out.println("=======");
        List<String> ans2 = permutationNoRepeat(s);
        for (String str : ans2) {
            System.out.println(str);
        }

    }

    /**
     * 打印一个字符串的全部排列
     *
     * @param str
     * @return
     */
    public static ArrayList<String> permutation(String str) {
        ArrayList<String> result = new ArrayList<>();
        if (str == null || str.length() == 0) {
            return result;
        }
        char[] chs = str.toCharArray();
        process(chs, 0, result);
        return result;
    }


    /**
     * 递归
     * 思路，
     * 第一大步，确定数组0位置可能出现的情况，其他位置与0位置交换，通过循环完成
     * 第二大步，确定数组1位置可能出现的情况，其他位置与1位置交换，通过循环完成
     * ... ...
     * 最后，索引位置等于字符数组长度时，添加结果，停止递归
     *
     *
     * @param chs
     * @param i
     * @param result
     */
    public static void process(char[] chs, int i, ArrayList<String> result) {
        // 索引位置等于字符数组长度时，添加结果，停止递归
        if (i == chs.length) {
            result.add(String.valueOf(chs));
            return;
        }
        // 循环
        for (int j = i; j < chs.length; j++) {
            // 交换位置
            swap(chs, i, j);
            // 继续调用
            process(chs, i + 1, result);
            // 恢复现场
            swap(chs, i, j);
        }
    }


    /**
     * 打印一个字符串的全部排列，要求不要出现重复的排列
     *
     * @param str
     * @return
     */
    public static ArrayList<String> permutationNoRepeat(String str) {
        ArrayList<String> result = new ArrayList<>();
        if (str == null || str.length() == 0) {
            return result;
        }
        char[] chs = str.toCharArray();
        process2(chs, 0, result);
        return result;
    }

    /**
     * 递归
     * 新增加了一个布尔数组，如果一次大循环中，需要交换位置的元素，
     * 没有与布尔数组重复，则可以交换
     *
     * @param str
     * @param i
     * @param result
     */
    public static void process2(char[] str, int i, ArrayList<String> result) {
        if (i == str.length) {
            result.add(String.valueOf(str));
        }
        // visit[0 1 .. 25]
        boolean[] visit = new boolean[26];
        for (int j = i; j < str.length; j++) {
            if (!visit[str[j] - 'a']) {
                visit[str[j] - 'a'] = true;
                swap(str, i, j);
                process2(str, i + 1, result);
                swap(str, i, j);
            }
        }
    }

    /**
     * 交换
     *
     * @param chs
     * @param i
     * @param j
     */
    public static void swap(char[] chs, int i, int j) {
        char tmp = chs[i];
        chs[i] = chs[j];
        chs[j] = tmp;
    }


}
