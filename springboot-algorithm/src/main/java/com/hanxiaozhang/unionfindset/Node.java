package com.hanxiaozhang.unionfindset;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/9/24
 * @since 1.0.0
 */
public class Node<V> {

    V value;

    public Node(V v) {
        value = v;
    }
}
