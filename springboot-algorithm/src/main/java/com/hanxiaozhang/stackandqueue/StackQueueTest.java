package com.hanxiaozhang.stackandqueue;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/24
 * @since 1.0.0
 */
public class StackQueueTest {

    public static void main(String[] args) {

        Stack myStackArray = new Stack(4);
        myStackArray.push(1);
        myStackArray.push(2);
        myStackArray.push(3);
        myStackArray.push(4);

    }


    public static class LoopQueue {

        private int capacity;

        private int pushIndex;

        private int popIndex;

        private Integer[] array;

        public LoopQueue(int capacity) {
            this.array = new Integer[capacity];
            this.capacity = capacity;
            this.popIndex = 0;
            this.pushIndex = 0;
        }


        /**
         * todo
         *
         * 获取下标
         *
         * @param i
         * @return
         */
        private int nextIndex(int i) {
            return i < capacity - 1 ? i + 1 : 0;
        }

    }

    public static class Stack {

        private Integer index;

        private Integer[] array;

        private int capacity;

        public Stack(int capacity) {
            this.index = 0;
            this.array = new Integer[capacity];
            this.capacity = capacity;
        }

        public Integer pop() {
            if (isEmpty()) {
                return null;
            }
            return array[index--];
        }

        public Boolean push(Integer i) {

            if (isFull()) {
                return false;
            }
            array[index++] = i;
            return true;
        }


        public Boolean isFull() {
            return index == capacity;
        }

        /**
         * 这里有bug，index==0，不能代表索引为0的位置，没有元素
         *
         * @return
         */
        public Boolean isEmpty() {
            return index == 0;
        }
    }

}
