package com.hanxiaozhang.windowandmonotonestack;

import java.util.LinkedList;

/**
 * 功能描述: <br>
 * 〈滑动窗口，求每一次滑出状况的最大值〉
 * <p>
 * 假设一个固定大小为W的窗口，依次划过arr，返回每一次滑出状况的最大值。
 * 例如，arr = [4,3,5,4,3,3,6,7], W = 3
 * 返回：[5,5,5,4,6,7]
 *
 * @Author: hanxinghua
 * @Date: 2024/1/1
 */
public class No1SlidingWindowMaxArray {


    public static void main(String[] args) {
        int testTime = 100000;
        int maxSize = 100;
        int maxValue = 100;
        System.out.println("test begin");
        for (int i = 0; i < testTime; i++) {
            int[] arr = generateRandomArray(maxSize, maxValue);
            int w = (int) (Math.random() * (arr.length + 1));
            int[] ans1 = method1(arr, w);
            int[] ans2 = methodControl(arr, w);
            if (!isEqual(ans1, ans2)) {
                System.out.println("Oops!");
            }
        }
        System.out.println("test finish");
    }

    /**
     * 使用单调双端队列滑动窗口方式
     *
     * @param arr
     * @param w
     * @return
     */
    public static int[] method1(int[] arr, int w) {
        if (arr == null || w < 1 || arr.length < w) {
            return null;
        }
        int[] res = new int[arr.length - w + 1];
        int index = 0;

        // 双端队列中放的是位置（索引位置），arr[位置]  -> 记住存储的位置
        LinkedList<Integer> qmax = new LinkedList<>();
        // 当前让 i -> [i] 进窗口 ，i就是r
        for (int i = 0; i < arr.length; i++) {
            // 循环，条件：qmax不为空  &&  arr[队列尾位置] <= arr[当前要插入位置] 时，弹出队列尾部元素
            while (!qmax.isEmpty() && arr[qmax.peekLast()] <= arr[i]) {
                qmax.pollLast();
            }
            // 尾部插入当前位置索引
            qmax.addLast(i);
            // 控制滑动窗口左指针。 队列头部元素 = 当前元素位置 - 窗口大小 时，弹出头部元素
            if (qmax.peekFirst() == i - w) {
                qmax.pollFirst();
            }
            // 当前位置 >= 滑动窗口 -1，其作用：形成w大小的滑动窗口。
            if (i >= w - 1) {
                res[index++] = arr[qmax.peekFirst()];
            }
        }
        return res;
    }

    /**
     * 对照组方法
     * 笨方法
     *
     * @param arr
     * @param w
     * @return
     */
    public static int[] methodControl(int[] arr, int w) {
        if (arr == null || w < 1 || arr.length < w) {
            return null;
        }
        int[] res = new int[arr.length - w + 1];
        int index = 0;
        // 初始化窗口
        int L = 0, R = w - 1;
        // 遍历数组
        while (R < arr.length) {
            int max = arr[L];
            // 寻找窗口中，最大值
            for (int i = L + 1; i <= R; i++) {
                max = Math.max(max, arr[i]);
            }
            // 结果数据
            res[index++] = max;
            // 移动窗口
            L++;
            R++;
        }
        return res;
    }

    /**
     * 生成数组
     *
     * @param maxSize
     * @param maxValue
     * @return
     */
    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * (maxValue + 1));
        }
        return arr;
    }

    /**
     * 判断两个数组是否相等
     *
     * @param arr1
     * @param arr2
     * @return
     */
    public static boolean isEqual(int[] arr1, int[] arr2) {
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)) {
            return false;
        }
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }


}
