package com.hanxiaozhang.test;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/4/23
 * @since 1.0.0
 */
public class Test1 {


    public static void main(String[] args) {

        int num1 = 10;
        System.out.println(Integer.toBinaryString(num1));
        System.out.println("偶数判断：" + ((num1 & 1) == 0));
        System.out.println("奇数判断：" + ((num1 & 1) == 1));

        int num2 = 11;
        System.out.println(Integer.toBinaryString(num2));
        System.out.println("偶数判断：" + ((num2 & 1) == 0));
        System.out.println("奇数判断：" + ((num2 & 1) == 1));


    }

}
