package com.hanxiaozhang.graph;

import java.util.HashMap;
import java.util.HashSet;

/**
 * 功能描述: <br>
 * 〈图〉
 *
 * @Author:hanxinghua
 * @Date: 2021/9/25
 */
public class Graph {

	/**
	 * 点集 key--> 值 value -->实际点
	 */
	public HashMap<Integer, Node> nodes;

	/**
	 * 边集
	 */
	public HashSet<Edge> edges;

	public Graph() {
		nodes = new HashMap<>();
		edges = new HashSet<>();
	}
}
