package com.hanxiaozhang.graph;

/**
 * 功能描述: <br>
 * 〈边〉
 *
 * @Author:hanxinghua
 * @Date: 2021/9/25
 */
public class Edge {
	/**
	 * 权重
	 */
	public int weight;

	/**
	 * 边出发的节点
	 */
	public Node from;

	/**
	 * 边到达的节点
	 */
	public Node to;

	public Edge(int weight, Node from, Node to) {
		this.weight = weight;
		this.from = from;
		this.to = to;
	}

}
