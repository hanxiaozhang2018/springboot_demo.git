package com.hanxiaozhang.graph;

import java.util.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈K算法实现最小生成树〉
 * 克鲁斯卡尔算法
 *
 * @author hanxinghua
 * @create 2021/9/27
 * @since 1.0.0
 */
public class Kruskal {


    public static Set<Edge> kruskalMST(Graph graph) {

        // 创建并查集，把所有点，放入并查集
        UnionFind unionFind = new UnionFind();
        unionFind.makeSets(graph.nodes.values());
        // 使用最小堆的优先队列，把图中所有边存入队列
        PriorityQueue<Edge> priorityQueue = new PriorityQueue<>(new EdgeComparator());
        // M 条边
        for (Edge edge : graph.edges) {
            // O(logM)
            priorityQueue.add(edge);
        }
        // 结果队列
        Set<Edge> result = new HashSet<>();
        // M 条边
        // 不为空循环队列
        while (!priorityQueue.isEmpty()) {
            // O(logM)
            // 弹出队列
            Edge edge = priorityQueue.poll();
            // O(1)
            // 如果并查集不包含边，添加，并且合并
            if (!unionFind.isSameSet(edge.from, edge.to)) {
                result.add(edge);
                unionFind.union(edge.from, edge.to);
            }
        }
        return result;
    }



    /**
     * 实现一个Set，完成最小生成树？？
     * 没有用
     */
    public static class MySets{
        public HashMap<Node, List<Node>>  setMap;
        public MySets(List<Node> nodes) {
            for(Node cur : nodes) {
                List<Node> set = new ArrayList<Node>();
                set.add(cur);
                setMap.put(cur, set);
            }
        }


        public boolean isSameSet(Node from, Node to) {
            List<Node> fromSet  = setMap.get(from);
            List<Node> toSet = setMap.get(to);
            return fromSet == toSet;
        }


        public void union(Node from, Node to) {
            List<Node> fromSet  = setMap.get(from);
            List<Node> toSet = setMap.get(to);
            for(Node toNode : toSet) {
                fromSet.add(toNode);
                setMap.put(toNode, fromSet);
            }
        }
    }


    /**
     * 并查集
     */
    public static class UnionFind {
        // key 某一个节点， value key节点往上的节点
        private HashMap<Node, Node> fatherMap;
        // key 某一个集合的代表节点, value key所在集合的节点个数
        private HashMap<Node, Integer> sizeMap;

        public UnionFind() {
            fatherMap = new HashMap<Node, Node>();
            sizeMap = new HashMap<Node, Integer>();
        }

        public void makeSets(Collection<Node> nodes) {
            fatherMap.clear();
            sizeMap.clear();
            for (Node node : nodes) {
                fatherMap.put(node, node);
                sizeMap.put(node, 1);
            }
        }

        private Node findFather(Node n) {
            Stack<Node> path = new Stack<>();
            while(n != fatherMap.get(n)) {
                path.add(n);
                n = fatherMap.get(n);
            }
            while(!path.isEmpty()) {
                fatherMap.put(path.pop(), n);
            }
            return n;
        }

        public boolean isSameSet(Node a, Node b) {
            return findFather(a) == findFather(b);
        }

        public void union(Node a, Node b) {
            if (a == null || b == null) {
                return;
            }
            Node aDai = findFather(a);
            Node bDai = findFather(b);
            if (aDai != bDai) {
                int aSetSize = sizeMap.get(aDai);
                int bSetSize = sizeMap.get(bDai);
                if (aSetSize <= bSetSize) {
                    fatherMap.put(aDai, bDai);
                    sizeMap.put(bDai, aSetSize + bSetSize);
                    sizeMap.remove(aDai);
                } else {
                    fatherMap.put(bDai, aDai);
                    sizeMap.put(aDai, aSetSize + bSetSize);
                    sizeMap.remove(bDai);
                }
            }
        }
    }


    /**
     * 比较器
     */
    public static class EdgeComparator implements Comparator<Edge> {

        @Override
        public int compare(Edge o1, Edge o2) {
            return o1.weight - o2.weight;
        }

    }


}
