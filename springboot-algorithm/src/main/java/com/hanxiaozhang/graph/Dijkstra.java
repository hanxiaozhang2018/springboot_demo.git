package com.hanxiaozhang.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈迪杰斯特拉算法〉
 *
 * @author hanxinghua
 * @create 2021/10/6
 * @since 1.0.0
 */
public class Dijkstra {

    /**
     * 算法实现1
     *
     * @param from
     * @return
     */
    public static HashMap<Node, Integer> dijkstra1(Node from) {
        // 从from出发到所有点的最小距离的Map  key : 从from出发到达key  value : 从from出发到达key的最小距离
        // 如果在表中，没有T的记录，含义是从from出发到T这个点的距离为正无穷
        HashMap<Node, Integer> distanceMap = new HashMap<>(16);
        distanceMap.put(from, 0);
        // 已经求过距离的节点，存在selectedNodes中，以后再也不碰
        HashSet<Node> selectedNodes = new HashSet<>();
        Node minNode = getMinDistanceAndUnselectedNode(distanceMap, selectedNodes);
        // minNode为空，证明没有到最后一个节点
        while (minNode != null) {
            int distance = distanceMap.get(minNode);
            for (Edge edge : minNode.edges) {
                Node toNode = edge.to;
                if (!distanceMap.containsKey(toNode)) {
                    distanceMap.put(toNode, distance + edge.weight);
                } else {
                    distanceMap.put(edge.to, Math.min(distanceMap.get(toNode), distance + edge.weight));
                }
            }
            selectedNodes.add(minNode);
            minNode = getMinDistanceAndUnselectedNode(distanceMap, selectedNodes);
        }
        return distanceMap;
    }

    /**
     * 获得距离最小的并且没有选择过的节点
     *
     * @param distanceMap
     * @param touchedNodes
     * @return
     */
    public static Node getMinDistanceAndUnselectedNode(HashMap<Node, Integer> distanceMap, HashSet<Node> touchedNodes) {
        Node minNode = null;
        int minDistance = Integer.MAX_VALUE;
        for (Map.Entry<Node, Integer> entry : distanceMap.entrySet()) {
            Node node = entry.getKey();
            int distance = entry.getValue();
            if (!touchedNodes.contains(node) && distance < minDistance) {
                minNode = node;
                minDistance = distance;
            }
        }
        return minNode;
    }



    /**
     * 改进后的dijkstra算法
     *
     * 从from出发，所有head能到达的节点，生成到达每个节点的最小路径记录并返回
     *
     * @param from
     * @param size
     * @return
     */
    public static HashMap<Node, Integer> dijkstra2(Node from, int size) {
        NodeHeap nodeHeap = new NodeHeap(size);
        nodeHeap.addOrUpdateOrIgnore(from, 0);
        HashMap<Node, Integer> result = new HashMap<>();
        while (!nodeHeap.isEmpty()) {
            NodeRecord record = nodeHeap.pop();
            Node cur = record.node;
            int distance = record.distance;
            for (Edge edge : cur.edges) {
                nodeHeap.addOrUpdateOrIgnore(edge.to, edge.weight + distance);
            }
            result.put(cur, distance);
        }
        return result;
    }

    /**
     * 小根堆
     *
     */
    public static class NodeHeap {
        // 实际的堆结构
        private Node[] nodes;
        // key 某一个node， value 上面数组中的位置
        private HashMap<Node, Integer> heapIndexMap;
        // key 某一个节点， value 从源节点出发到该节点的目前最小距离
        private HashMap<Node, Integer> distanceMap;
        // 堆上有多少个点
        private int size;

        public NodeHeap(int size) {
            nodes = new Node[size];
            heapIndexMap = new HashMap<>();
            distanceMap = new HashMap<>();
            size = 0;
        }

        /**
         * 判断是否为空
         * @return
         */
        public boolean isEmpty() {
            return size == 0;
        }


        /**
         * 有一个点叫node，现在发现了一个从源节点出发到达node的距离为distance
         * 判断要不要更新，如果需要的话，就更新
         *
         * @param node
         * @param distance
         */
        public void addOrUpdateOrIgnore(Node node, int distance) {
            // 节点在堆上
            if (inHeap(node)) {
                // 添加节点最小距离
                distanceMap.put(node, Math.min(distanceMap.get(node), distance));
                // 插入堆，调整堆的结构
                heapInsert(node, heapIndexMap.get(node));
            }
            // 节点没有进入过堆，添加到堆中
            if (!isEntered(node)) {
                nodes[size] = node;
                heapIndexMap.put(node, size);
                distanceMap.put(node, distance);
                heapInsert(node, size++);
            }
            // 其他，忽略
        }

        /**
         * 弹出
         *
         * @return
         */
        public NodeRecord pop() {
            NodeRecord nodeRecord = new NodeRecord(nodes[0], distanceMap.get(nodes[0]));
            swap(0, size - 1);
            heapIndexMap.put(nodes[size - 1], -1);
            distanceMap.remove(nodes[size - 1]);
            // free C++同学还要把原本堆顶节点析构，对java同学不必
            nodes[size - 1] = null;
            heapify(0, --size);
            return nodeRecord;
        }

        /**
         * 插入
         *
         * @param node
         * @param index
         */
        private void heapInsert(Node node, int index) {
            while (distanceMap.get(nodes[index]) < distanceMap.get(nodes[(index - 1) / 2])) {
                swap(index, (index - 1) / 2);
                index = (index - 1) / 2;
            }
        }

        private void heapify(int index, int size) {
            int left = index * 2 + 1;
            while (left < size) {
                int smallest = left + 1 < size && distanceMap.get(nodes[left + 1]) < distanceMap.get(nodes[left])
                        ? left + 1
                        : left;
                smallest = distanceMap.get(nodes[smallest]) < distanceMap.get(nodes[index]) ? smallest : index;
                if (smallest == index) {
                    break;
                }
                swap(smallest, index);
                index = smallest;
                left = index * 2 + 1;
            }
        }


        /**
         * 节点是否进来过
         *
         * @param node
         * @return
         */
        private boolean isEntered(Node node) {
            return heapIndexMap.containsKey(node);
        }


        /**
         * 节点是否在堆上
         *
         * @param node
         * @return
         */
        private boolean inHeap(Node node) {
            return isEntered(node) && heapIndexMap.get(node) != -1;
        }

        /**
         * 交换
         *
         * @param index1
         * @param index2
         */
        private void swap(int index1, int index2) {
            heapIndexMap.put(nodes[index1], index2);
            heapIndexMap.put(nodes[index2], index1);
            Node tmp = nodes[index1];
            nodes[index1] = nodes[index2];
            nodes[index2] = tmp;
        }
    }

    /**
     * 节点记录
     */
    public static class NodeRecord {
        public Node node;
        public int distance;

        public NodeRecord(Node node, int distance) {
            this.node = node;
            this.distance = distance;
        }
    }


}
