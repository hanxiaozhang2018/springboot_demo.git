package com.hanxiaozhang.graph;

import java.util.ArrayList;

/**
 * 功能描述: <br>
 * 〈节点〉
 *
 * @Author:hanxinghua
 * @Date: 2021/9/25
 */
public class Node {

	/**
	 * 值
	 */
	public int value;

	/**
	 * 入度，进入该节点的个数
	 */
	public int in;

	/**
	 * 出度，出发到节点个数
	 */
	public int out;

	/**
	 * 直接邻居,出度连接的邻居
	 */
	public ArrayList<Node> nexts;

	/**
	 * 边的描述
	 */
	public ArrayList<Edge> edges;

	public Node(int value) {
		this.value = value;
		in = 0;
		out = 0;
		nexts = new ArrayList<>();
		edges = new ArrayList<>();
	}
}
