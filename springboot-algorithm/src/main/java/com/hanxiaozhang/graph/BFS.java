package com.hanxiaozhang.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 〈一句话功能简述〉<br>
 * 〈宽度(广度)优先遍历〉
 *
 * @author hanxinghua
 * @create 2021/9/26
 * @since 1.0.0
 */
public class BFS {

    /**
     * 宽度（广度）优先遍历
     *
     * 队列和Set
     *
     * @param node
     */
    public static void bfs(Node node) {
        if (node == null) {
            return;
        }
        Queue<Node> queue = new LinkedList<>();
        HashSet<Node> set = new HashSet<>();
        queue.add(node);
        set.add(node);
        // 队列不为空，循环
        while (!queue.isEmpty()) {
            // 弹出当前节点，并且打印
            Node cur = queue.poll();
            System.out.println(cur.value);
            // 循环弹出当前节点的所有邻接节点，
            // 如果set中不存在，分别添加到set和queue中
            for (Node next : cur.nexts) {
                if (!set.contains(next)) {
                    set.add(next);
                    queue.add(next);
                }
            }
        }
    }
}
