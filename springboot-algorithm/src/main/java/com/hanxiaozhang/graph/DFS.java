package com.hanxiaozhang.graph;

import java.util.HashSet;
import java.util.Stack;

/**
 * 〈一句话功能简述〉<br>
 * 〈深度优先遍历〉
 *
 * @author hanxinghua
 * @create 2021/9/26
 * @since 1.0.0
 */
public class DFS {


    /**
     * 深度优先遍历
     *
     * 栈和Set
     *
     * @param node
     */
    public static void dfs(Node node) {
        if (node == null) {
            return;
        }
        Stack<Node> stack = new Stack<>();
        HashSet<Node> set = new HashSet<>();
        stack.push(node);
        set.add(node);
        // 插入打印，这里可以替换成你的业务
        System.out.println(node.value);
        // 栈不为空，循环
        while (!stack.isEmpty()) {
            // 弹出当前节点
            Node cur = stack.pop();
            // 循环弹出当前节点的所有邻接节点，
            // 如果set中不存在，当弹出前节点和邻接节点添加到stack，
            // 弹出节点添加添加到set，并打印，并跳出循环
            for (Node next : cur.nexts) {
                if (!set.contains(next)) {
                    stack.push(cur);
                    stack.push(next);
                    set.add(next);
                    System.out.println(next.value);
                    break;
                }
            }
        }
    }

}
