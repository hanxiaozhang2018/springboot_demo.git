package com.hanxiaozhang.no3javaruntimedataarea;

/**
 * 〈一句话功能简述〉<br>
 * 〈Object对象字节码〉
 *
 * @author hanxinghua
 * @create 2022/6/15
 * @since 1.0.0
 */
public class ObjectByteCode {

    public static void main(String[] args) {
        Object obj = new Object();
    }
}
