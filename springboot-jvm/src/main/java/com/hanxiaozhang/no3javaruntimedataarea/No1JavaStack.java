package com.hanxiaozhang.no3javaruntimedataarea;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/12/28
 * @since 1.0.0
 */
public class No1JavaStack {

    private static int memberVariable1 = 10;
    private int memberVariable2 = 10;

    public static void main(String[] args) {
        int localVariable1 = 10;
        System.out.println(memberVariable1);
    }


    public void method() {
        int localVariable2 = memberVariable2;
    }


}
