package com.hanxiaozhang.no4Instructionset;

/**
 * 〈一句话功能简述〉<br>
 * 〈指令集1〉
 *
 * @author hanxinghua
 * @create 2021/12/16
 * @since 1.0.0
 */
public class No2Instruction1 {


    public static void main(String[] args) {

    }

    /**
     * 0 bipush 100
     * 2 istore_1
     * 3 return
     */
    public void method1() {
        int i = 100;
    }

    /**
     * 0 bipush 200
     * 2 istore_1
     * 3 return
     */
    public void method2() {
        int i = 200;
    }

    /**
     * 0 sipush 300
     * 3 istore_1
     * 4 return
     * <p>
     * LocalVariableTable中this变量，如果有静态就不会有
     */
    public void method3(int k) {
        int i = 300;
    }


    /**
     * 0 aconst_null
     * 1 astore_1
     * 2 return
     */
    public void method4() {
        Object o = null;
    }

    /**
     * 初始化对象
     * <p>
     * 0 new #2 <java/lang/Object>
     * 3 dup
     * 4 invokespecial #1 <java/lang/Object.<init>>
     * 7 astore_1
     * 8 return
     */
    public void method5() {
        Object o = new Object();
    }

    /**
     * 初始化对象
     * <p>
     * 0 new #2 <java/lang/Object>
     * 3 dup
     * 4 invokespecial #1 <java/lang/Object.<init>>
     * 7 pop
     * 8 return
     */
    public void method6() {
        new Object();
    }


    /**
     * 相加
     * <p>
     * 0 iload_1
     * 1 iload_2
     * 2 iadd
     * 3 istore_3
     * 4 return
     * <p>
     * LocalVariableTable中有this变量，静态方法不会有
     *
     * @param a
     * @param b
     */
    public void method7(int a, int b) {

        int c = a + b;
    }

    /**
     * 相减
     * <p>
     * 0 iload_0
     * 1 iload_1
     * 2 isub
     * 3 istore_2
     * 4 return
     * <p>
     * LocalVariableTable中没有this变量，静态方法不会有
     *
     * @param a
     * @param b
     */
    public static void method8(int a, int b) {
        int c = a - b;
    }


    /**
     * 0 new #5 <com/hanxiaozhang/no3javaruntimedataarea/No2Instruction1>
     * 3 dup
     * 4 invokespecial #6 <com/hanxiaozhang/no3javaruntimedataarea/No2Instruction1.<init>>
     * 7 pop
     * 8 return
     */
    public void method9() {

        new No2Instruction1();
    }

    /**
     * 0 new #5 <com/hanxiaozhang/no3javaruntimedataarea/No2Instruction1>
     * 3 dup
     * 4 invokespecial #6 <com/hanxiaozhang/no3javaruntimedataarea/No2Instruction1.<init>>
     * 7 astore_1
     * 8 aload_1
     * 9 invokevirtual #7 <com/hanxiaozhang/no3javaruntimedataarea/No2Instruction1.method2>
     * 12 return
     */
    public void method10() {
        No2Instruction1 no2Instruction1 = new No2Instruction1();
        no2Instruction1.method2();

    }

}
