package com.hanxiaozhang.no4Instructionset;

/**
 * 〈一句话功能简述〉<br>
 * 〈指令集2-返回值〉
 *
 * @author hanxinghua
 * @create 2021/12/17
 * @since 1.0.0
 */
public class No3Instruction2 {

    public static void main(String[] args) {

    }

    /**
     * 0 new #2 <com/hanxiaozhang/no3javaruntimedataarea/No3Instruction2>
     * 3 dup
     * 4 invokespecial #3 <com/hanxiaozhang/no3javaruntimedataarea/No3Instruction2.<init>>
     * 7 astore_1
     * 8 aload_1
     * 9 invokevirtual #4 <com/hanxiaozhang/no3javaruntimedataarea/No3Instruction2.m1>
     * 12 return
     */
    public void method1() {
        No3Instruction2 instruction = new No3Instruction2();
        instruction.m1();
    }

    /**
     * 0 new #2 <com/hanxiaozhang/no3javaruntimedataarea/No3Instruction2>
     * 3 dup
     * 4 invokespecial #3 <com/hanxiaozhang/no3javaruntimedataarea/No3Instruction2.<init>>
     * 7 astore_1
     * 8 aload_1
     * 9 invokevirtual #5 <com/hanxiaozhang/no3javaruntimedataarea/No3Instruction2.m2>
     * 12 pop
     * 13 return
     */
    public void method2() {
        No3Instruction2 instruction = new No3Instruction2();
        instruction.m2();
    }

    /**
     * 0 new #2 <com/hanxiaozhang/no3javaruntimedataarea/No3Instruction2>
     * 3 dup
     * 4 invokespecial #3 <com/hanxiaozhang/no3javaruntimedataarea/No3Instruction2.<init>>
     * 7 astore_1
     * 8 aload_1
     * 9 invokevirtual #5 <com/hanxiaozhang/no3javaruntimedataarea/No3Instruction2.m2>
     * 12 istore_2
     * 13 return
     */
    public void method3() {
        No3Instruction2 instruction = new No3Instruction2();
        int i = instruction.m2();
    }


    /**
     * 0 return
     */
    public void m1() {

    }

    /**
     * 0 bipush 100
     * 2 ireturn
     *
     * @return
     */
    public int m2() {
        return 100;
    }

    /**
     * 0 bipush 10
     * 2 invokestatic #6 <java/lang/Integer.valueOf>
     * 5 areturn
     * @return
     */
    public Integer m3() {
        return 10;
    }

    /**
     * 0 sipush 10000
     * 3 invokestatic #6 <java/lang/Integer.valueOf>
     * 6 areturn
     *
     * @return
     */
    public Integer m4() {
        return 10000;
    }

}
