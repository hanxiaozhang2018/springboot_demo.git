package com.hanxiaozhang.no4Instructionset;

/**
 * 〈一句话功能简述〉<br>
 * 〈i++〉
 *
 * @author hanxinghua
 * @create 2021/12/14
 * @since 1.0.0
 */
public class No1IPulsPuls {

    public static void main(String[] args) {
        int i = 8;
        i = i++;
        // i = ++i;
        System.out.println(i);
    }
}
