package com.hanxiaozhang.no4Instructionset;

/**
 * 〈一句话功能简述〉<br>
 * 〈指令集3-递归〉
 *
 * @author hanxinghua
 * @create 2021/12/17
 * @since 1.0.0
 */
public class No4Instruction3 {

    public static void main(String[] args) {

        No4Instruction3 instruction = new No4Instruction3();
        int i = instruction.method(3);

    }

    /**
     * 0 iload_1
     * 1 iconst_1
     * 2 if_icmpne 7 (+5)
     * 5 iconst_1
     * 6 ireturn
     * 7 iload_1
     * 8 aload_0
     * 9 iload_1
     * 10 iconst_1
     * 11 isub
     * 12 invokevirtual #4 <com/hanxiaozhang/no3javaruntimedataarea/No4Instruction3.method>
     * 15 imul
     * 16 ireturn
     *
     * @param n
     * @return
     */
    public int method(int n) {
        if (n == 1) {
            return 1;
        }
        return n * method(n - 1);
    }

}
