package com.hanxiaozhang.no5gc;

import java.util.LinkedList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * 0. 编译java文件：
 * javac  -encoding utf-8   No2HelloGC.java
 * <p>
 * 1. 区分概念：内存泄漏memory leak，内存溢出out of memory
 * <p>
 * 2. 查看程序使用的默认JVM参数：
 * Linux：java -XX:+PrintCommandLineFlags No2HelloGC
 * Windows：-Dfile.encoding=utf-8 -XX:+PrintCommandLineFlags
 * <p>
 * 3. Xmn：新生代大小 Xms：最小堆 Xmx：最大堆  打印GC：-XX:+PrintGC
 * Linux：java -Xmn10M -Xms40M -Xmx60M -XX:+PrintCommandLineFlags -XX:+PrintGCDetails  No2HelloGC
 * Windows：-Dfile.encoding=utf-8 -Xmn10M -Xms40M -Xmx60M -XX:+PrintCommandLineFlags -XX:+PrintGCDetails
 * <p>
 * 4. 常用的GC参数
 * PrintGCDetails：打印GC详情  PrintGCTimeStamps：打印GC发生的时间戳，以JVM启动时间为基准
 * PrintGCDateStamps：输出GC的时间戳，以日期的形式 PrintHeapAtGC：在GC的前后打印出堆的信息
 * PrintGCCauses：打印GC原因
 * <p>
 * 5. 使用SMC
 * Linux：java -XX:+UseConcMarkSweepGC -XX:+PrintCommandLineFlags -XX:+PrintGCDetails No2HelloGC
 * Windows：-Dfile.encoding=utf-8 -XX:+UseConcMarkSweepGC -XX:+PrintCommandLineFlags -XX:+PrintGCDetails
 * <p>
 * 6. 查询JVM参数
 * -XX:+PrintFlagsInitial 默认参数值
 * -XX:+PrintFlagsFinal 最终参数值
 * -XX:+PrintFlagsFinal | grep xxx 找到对应的参数
 * -XX:+PrintFlagsFinal > flags.txt  写入txt
 * -XX:+PrintFlagsFinal -version |grep GC
 *
 * @author hanxinghua
 * @create 2021/12/23
 * @since 1.0.0
 */
public class No2HelloGC {

    public static void main(String[] args) {
        System.out.println("No2HelloGC!");
        List<Byte[]> list = new LinkedList<Byte[]>();
        for (; ; ) {
            Byte[] b = new Byte[1024 * 1024];
            list.add(b);
        }
    }
}
