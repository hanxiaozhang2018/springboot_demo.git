package com.hanxiaozhang.no5gc.jvmparam;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈GCApplicationStoppedTime参数〉
 * <p>
 * -Xms10m -Xmx30m  -XX:+PrintGCApplicationStoppedTime
 * 打印STW的时间
 *
 * @author hanxinghua
 * @create 2023/6/4
 * @since 1.0.0
 */
public class No3GCApplicationStoppedTime {


    /*
    打印日志实例：
    Total time for which application threads were stopped: 0.0022431 seconds, Stopping threads took: 0.0000268 seconds

     */

    public static void main(String[] args) {

        List list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add(String.valueOf(i));
        }
    }
}
