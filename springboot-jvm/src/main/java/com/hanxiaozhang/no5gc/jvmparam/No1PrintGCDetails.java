package com.hanxiaozhang.no5gc.jvmparam;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈PrintGCDetails参数与PrintGCDateStamps〉
 *
 *  -Xms10m -Xmx30m  -XX:+PrintGCDetails -XX:+PrintGCDateStamps
 *   开启打印GC参数 打印GC发生的时间戳
 *
 *
 * @author hanxinghua
 * @create 2023/6/4
 * @since 1.0.0
 */
public class No1PrintGCDetails {

    /*
    打印日志实例:
    2023-06-04T16:27:17.318+0800: [Full GC (Allocation Failure) [PSYoungGen: 4096K->4096K(7168K)] [ParOldGen: 20479K->20479K(20480K)] 24575K->24575K(27648K), [Metaspace: 3971K->3971K(1056768K)], 0.1558414 secs] [Times: user=0.19 sys=0.00, real=0.16 secs]
    2023-06-04T16:27:17.474+0800: [Full GC (Ergonomics) [PSYoungGen: 4096K->4096K(7168K)] [ParOldGen: 20479K->20479K(20480K)] 24575K->24575K(27648K), [Metaspace: 3971K->3971K(1056768K)], 0.1299323 secs] [Times: user=0.27 sys=0.00, real=0.13 secs]
    2023-06-04T16:27:17.604+0800: [Full GC (Allocation Failure) [PSYoungGen: 4096K->4096K(7168K)] [ParOldGen: 20479K->20479K(20480K)] 24575K->24575K(27648K), [Metaspace: 3971K->3971K(1056768K)], 0.1303078 secs] [Times: user=0.25 sys=0.00, real=0.13 secs]
    2023-06-04T16:27:17.736+0800: [Full GC (Ergonomics) [PSYoungGen: 4096K->0K(7168K)] [ParOldGen: 20479K->1139K(14336K)] 24575K->1139K(21504K), [Metaspace: 3971K->3971K(1056768K)], 0.0236298 secs] [Times: user=0.00 sys=0.00, real=0.02 secs]
    Heap
     PSYoungGen      total 7168K, used 108K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
      eden space 4096K, 2% used [0x00000000ff600000,0x00000000ff61b0b0,0x00000000ffa00000)
      from space 3072K, 0% used [0x00000000ffd00000,0x00000000ffd00000,0x0000000100000000)
      to   space 3072K, 0% used [0x00000000ffa00000,0x00000000ffa00000,0x00000000ffd00000)
     ParOldGen       total 14336K, used 1139K [0x00000000fe200000, 0x00000000ff000000, 0x00000000ff600000)
      object space 14336K, 7% used [0x00000000fe200000,0x00000000fe31cd10,0x00000000ff000000)
     Metaspace       used 4002K, capacity 4568K, committed 4864K, reserved 1056768K
      class space    used 441K, capacity 460K, committed 512K, reserved 1048576K


     */

    public static void main(String[] args) {

        List list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add(String.valueOf(i));
        }
    }
}
