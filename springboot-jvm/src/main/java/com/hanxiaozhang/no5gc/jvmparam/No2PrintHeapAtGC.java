package com.hanxiaozhang.no5gc.jvmparam;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈PrintHeapAtGC参数〉
 * -Xms10m -Xmx30m  -XX:+PrintHeapAtGC
 *
 * @author hanxinghua
 * @create 2023/6/6
 * @since 1.0.0
 */
public class No2PrintHeapAtGC {

    /*
    日志实例：
    {Heap before GC invocations=1 (full 0):
     PSYoungGen      total 2560K, used 2048K [0x00000000ff600000, 0x00000000ff900000, 0x0000000100000000)
        eden space 2048K, 100% used [0x00000000ff600000,0x00000000ff800000,0x00000000ff800000)
        from space 512K, 0% used [0x00000000ff880000,0x00000000ff880000,0x00000000ff900000)
        to   space 512K, 0% used [0x00000000ff800000,0x00000000ff800000,0x00000000ff880000)
     ParOldGen       total 7168K, used 0K [0x00000000fe200000, 0x00000000fe900000, 0x00000000ff600000)
        object space 7168K, 0% used [0x00000000fe200000,0x00000000fe200000,0x00000000fe900000)
     Metaspace       used 2538K, capacity 4480K, committed 4480K, reserved 1056768K
        class space    used 275K, capacity 384K, committed 384K, reserved 1048576K
    Heap after GC invocations=1 (full 0):
     PSYoungGen      total 2560K, used 504K [0x00000000ff600000, 0x00000000ff900000, 0x0000000100000000)
        eden space 2048K, 0% used [0x00000000ff600000,0x00000000ff600000,0x00000000ff800000)
        from space 512K, 98% used [0x00000000ff800000,0x00000000ff87e010,0x00000000ff880000)
        to   space 512K, 0% used [0x00000000ff880000,0x00000000ff880000,0x00000000ff900000)
     ParOldGen       total 7168K, used 475K [0x00000000fe200000, 0x00000000fe900000, 0x00000000ff600000)
        object space 7168K, 6% used [0x00000000fe200000,0x00000000fe276c48,0x00000000fe900000)
     Metaspace       used 2538K, capacity 4480K, committed 4480K, reserved 1056768K
        class space    used 275K, capacity 384K, committed 384K, reserved 1048576K
    }

     */


    public static void main(String[] args) {

        List list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add(String.valueOf(i));
        }
    }
}
