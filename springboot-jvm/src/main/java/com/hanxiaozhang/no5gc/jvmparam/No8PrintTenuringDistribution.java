package com.hanxiaozhang.no5gc.jvmparam;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈PrintTenuringDistrution参数〉
 *
 *  -Xms10m -Xmx30m  -XX:+PrintTenuringDistribution
 *
 *  打印对象分布情况-为了分析GC时晋升情况和晋升导致的高暂停
 *
 *
 * @author hanxinghua
 * @create 2023/6/5
 * @since 1.0.0
 */
public class No8PrintTenuringDistribution {

    /*
    打印日志实例：
    Desired survivor size 524288 bytes, new threshold 7 (max 15)
    Desired survivor size 524288 bytes, new threshold 7 (max 15)
     */

    public static void main(String[] args) {

        List list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add(String.valueOf(i));
        }
    }
}
