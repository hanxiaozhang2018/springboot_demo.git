package com.hanxiaozhang.no5gc.jvmparam;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈保存Dump文件〉
 * <p>
 * -Xms10m -Xmx30m -XX:+HeapDumpOnOutOfMemoryError  -XX:HeapDumpPath=d:/dump
 * <p>
 * <p>
 * 保存Full GC前后的dump
 * -XX:+HeapDumpBeforeFullGC
 * -XX:+HeapDumpAfterFullGC
 * -XX:HeapDumpPath=e:\dump
 * <p>
 * 保存OutOfMemoryError的dump
 * -XX:+HeapDumpOnOutOfMemoryError
 * -XX:HeapDumpPath=e:\dump
 *
 * @author hanxinghua
 * @create 2023/6/6
 * @since 1.0.0
 */
public class No5SaveHeapDump {

    /*
    保存dump日志：
    java.lang.OutOfMemoryError: GC overhead limit exceeded
    Dumping heap to d:/dump ...
    Heap dump file created [32030602 bytes in 0.811 secs]
     */

    public static void main(String[] args) {

        List list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add(String.valueOf(i));
        }
    }
}
