package com.hanxiaozhang.no5gc.jvmparam;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈保存打印GC日志〉
 *
 * -Xms10m -Xmx30m  -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintHeapAtGC -Xloggc:d:/logs/gc-%t.log -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=10k
 *
 *
 *  GC日志输出的文件路径
 * -Xloggc:/path/to/gc-%t.log
 *  开启日志文件分割
 * -XX:+UseGCLogFileRotation
 *  最多分割几个文件，超过之后从头文件开始写
 * -XX:NumberOfGCLogFiles=14
 *  每个文件上限大小，超过就触发分割
 * -XX:GCLogFileSize=100M
 *
 *
 * @author hanxinghua
 * @create 2023/6/6
 * @since 1.0.0
 */
public class No4PrintGcLogFile {


    public static void main(String[] args) {

        List list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add(String.valueOf(i));
        }
    }
}
