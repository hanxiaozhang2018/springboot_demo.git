package com.hanxiaozhang.no5gc.jvmparam;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * -Xms10m -Xmx30m -XX:+PrintSafepointStatistics -XX:PrintSafepointStatisticsCount=1
 *
 *  会定时采集，但是采集的时候会触发所有线程进入Safepoint，在调试节点才打卡
 *
 *
 *
 * @author hanxinghua
 * @create 2023/6/5
 * @since 1.0.0
 */
public class No7PrintSafepointStatistics {


    /*
    日志实例：
             vmop                    [threads: total initially_running wait_to_block]    [time: spin block sync cleanup vmop] page_trap_count
    0.680: ParallelGCFailedAllocation       [      11          0              0    ]      [     0     0     0     0    70    ]  0
             vmop                    [threads: total initially_running wait_to_block]    [time: spin block sync cleanup vmop] page_trap_count
    0.775: no vm operation                  [      10          0              1    ]      [     0     0     0     0   491    ]  0

    Polling page always armed
    ParallelGCFailedAllocation         1
        0 VM operations coalesced during safepoint
    Maximum sync time      0 ms
    Maximum vm operation time (except for Exit VM operation)     70 ms

     */

    public static void main(String[] args) {

        List list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add(String.valueOf(i));
        }
    }
}
