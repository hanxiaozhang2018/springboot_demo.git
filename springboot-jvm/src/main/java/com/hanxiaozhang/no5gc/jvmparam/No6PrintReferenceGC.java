package com.hanxiaozhang.no5gc.jvmparam;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈PrintReferenceGC参数〉
 * <p>
 * -Xms10m -Xmx30m  -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintReferenceGC
 * 打印对象引用关系处理信息  Tips：必须加PrintGCDetails参数才可以打印对象引用关系
 *
 * @author hanxinghua
 * @create 2023/6/5
 * @since 1.0.0
 */
public class No6PrintReferenceGC {


    /*
    日志实例：
    2023-06-06T09:41:16.410+0800: [GC (Allocation Failure)
    2023-06-06T09:41:16.411+0800: [SoftReference, 0 refs, 0.0000288 secs]
    2023-06-06T09:41:16.411+0800: [WeakReference, 5 refs, 0.0000306 secs]
    2023-06-06T09:41:16.411+0800: [FinalReference, 0 refs, 0.0000154 secs]
    2023-06-06T09:41:16.411+0800: [PhantomReference, 0 refs, 0 refs, 0.0000149 secs]
    2023-06-06T09:41:16.411+0800: [JNI Weak Reference, 0.0000110 secs]
    [PSYoungGen: 2048K->504K(2560K)] 2048K->992K(9728K), 0.0014706 secs]
    [Times: user=0.00 sys=0.00, real=0.00 secs]

     */

    public static void main(String[] args) {

        List<Object> list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {

            String strong = String.valueOf(i);
            SoftReference<Integer> soft = new SoftReference(i);
            WeakReference<Integer> weak = new WeakReference(i);

            list.add(strong);
            list.add(soft);
            list.add(weak);
        }
    }
}
