package com.hanxiaozhang.no5gc;

//-XX:-DoEscapeAnalysis -XX:-EliminateAllocations -XX:-UseTLAB -Xlog:c5_gc*
// 逃逸分析 标量替换 线程专有对象分配

/**
 * 功能描述: <br>
 * 〈TLAB〉
 *
 * @Author:hanxinghua
 * @Date: 2021/12/16
 */
public class No1TestTLAB {

    public static void main(String[] args) {
        No1TestTLAB t = new No1TestTLAB();
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000_0000; i++) {
            t.alloc(i);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    public void alloc(int i) {
        new User(i, "name " + i);
    }

    private class User {
        int id;
        String name;

        public User(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }

}
