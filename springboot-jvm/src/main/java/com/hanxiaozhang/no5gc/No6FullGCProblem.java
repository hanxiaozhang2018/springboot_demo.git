package com.hanxiaozhang.no5gc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 〈FGC问题〉
 *
 * @Author:hanxinghua
 * @Date: 2021/12/27
 */
public class No6FullGCProblem {

    private static ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(50,
            new ThreadPoolExecutor.DiscardOldestPolicy());

    public static void main(String[] args) throws Exception {
        executor.setMaximumPoolSize(50);

        for (; ; ) {
            modelFit();
            Thread.sleep(100);
        }
    }

    /**
     * 模拟数据数据计算
     */
    private static void modelFit() {
        List<CardInfo> taskList = getAllCardInfo();
        taskList.forEach(info -> {
            // do something
            executor.scheduleWithFixedDelay(() -> {
                //do sth with info
                info.method();

            }, 2, 3, TimeUnit.SECONDS);
        });
    }

    /**
     * 获取所有卡片信息，模拟每次获取100个
     *
     * @return
     */
    private static List<CardInfo> getAllCardInfo() {
        List<CardInfo> taskList = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            CardInfo ci = new CardInfo();
            taskList.add(ci);
        }

        return taskList;
    }


    /**
     * 卡片信息类
     */
    private static class CardInfo {

        private BigDecimal price = new BigDecimal(0.0);
        private String name = "张三";
        private int age = 5;
        private Date birthdate = new Date();

        /**
         * 模拟处理计算
         */
        public void method() {

        }
    }
}
