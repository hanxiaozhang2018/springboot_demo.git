package com.hanxiaozhang.no5gc;

/**
 * 功能描述: <br>
 * 〈Lambda表达式，会动态产生类〉
 *
 *  -XX:MaxMetaspaceSize=9M -XX:+PrintGCDetails
 *
 * @Author:hanxinghua
 * @Date: 2021/12/28
 */
public class No7LambdaOOM {

    public static void main(String[] args) {
        for (; ; ) {
            I i = C::n;
        }
    }

    public static interface I {
        void m();
    }

    public static class C {
        static void n() {
            System.out.println("hello");
        }
    }
}
