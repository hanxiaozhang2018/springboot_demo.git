package com.hanxiaozhang.no2jmm;

import org.openjdk.jol.info.ClassLayout;

/**
 * 〈一句话功能简述〉<br>
 * 〈查看对象使用内存〉
 * <p>
 * 关闭类型指针压缩（对象头中指针） -XX:-UseCompressedClassPointers
 * 关闭普通类型指针压缩 -XX:-UseCompressedOops
 *
 * @author hanxinghua
 * @create 2021/12/13
 * @since 1.0.0
 */
public class No1UseMemory {

    public static void main(String[] args) {
        Object object = new Object();
        Object[] objectArray0 = new Object[0];
        Object[] objectArray1 = new Object[1];
        Object[] objectArray2 = new Object[2];
        int[] ints = new int[]{};
        System.out.println("---- Object对象占用的内存大小:");
        System.out.println(ClassLayout.parseInstance(object).toPrintable());
        System.out.println("---- Object[0]占用的内存大小:");
        System.out.println(ClassLayout.parseInstance(objectArray0).toPrintable());
        System.out.println("---- Object[1]占用的内存大小:");
        System.out.println(ClassLayout.parseInstance(objectArray1).toPrintable());
        System.out.println("---- Object[2]占用的内存大小:");
        System.out.println(ClassLayout.parseInstance(objectArray2).toPrintable());
        System.out.println("---- int[]占用的内存大小:");
        System.out.println(ClassLayout.parseInstance(ints).toPrintable());
        System.out.println("---- CustomObject占用的内存大小:");
        System.out.println(ClassLayout.parseInstance(new CustomObject()).toPrintable());
    }

    private static class CustomObject {
        // markword  8
        // oop指针  4
        int id;         //4
        String name;    //4
        int age;        //4

        byte b1;        //1
        byte b2;        //1

        Object o;       //4
        byte b3;        //1

    }

}
