package com.hanxiaozhang.no1classloader;

/**
 * 〈一句话功能简述〉<br>
 * 〈面试题〉
 *
 * @author hanxinghua
 * @create 2021/12/11
 * @since 1.0.0
 */
public class No11InterviewTopic {


    public static void main(String[] args) {
        System.out.println("T1 count is " + T1.count);
        System.out.println("T2 count is " + T2.count);
    }

}

/**
 * 此时执行顺序
 * 1 -> 3 -> 2
 *
 */
class T1 {
    // 1
    public static T1 t1 = new T1();

    // 2
    public static int count = 2;

    // 3
    public T1() {
        System.out.println("T1 into count  is " + count);
        count++;
        System.out.println("T1 count++  is " + count);
    }
}

/**
 *  此时执行顺序
 *  1 -> 2 -> 3
 *
 */
class T2 {

    // 1
    public static int count = 2;

    // 2
    public static T2 t2 = new T2();

    // 3
    public T2() {
        System.out.println("T2 into count  is " + count);
        count++;
        System.out.println("T2 count++  is " + count);
    }

}

