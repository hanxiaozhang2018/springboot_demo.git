package com.hanxiaozhang.no1classloader.staticshare.no1sameprocess;


import java.lang.reflect.Field;

/**
 * 〈一句话功能简述〉<br>
 * 〈进程2〉
 *
 * @author hanxinghua
 * @create 2023/1/7
 * @since 1.0.0
 */
public class Process2 {

    public static void main(String[] args) throws Exception {

        Class<?> clazz = Process1.getDefaultClassLoader().loadClass("com.hanxiaozhang.no1classloader.staticshare.no1sameprocess.StaticVariable");
        Field field = clazz.getField("NUM");
        System.out.println("Process2 get num is " + field.getInt(clazz));
    }

}
