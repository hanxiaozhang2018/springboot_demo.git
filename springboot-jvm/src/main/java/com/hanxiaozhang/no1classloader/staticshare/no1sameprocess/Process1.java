package com.hanxiaozhang.no1classloader.staticshare.no1sameprocess;


import java.lang.reflect.Field;

/**
 * 〈一句话功能简述〉<br>
 * 〈进程1〉
 *  如果同一个类在同一个JVM进程中，并且两个程序通过相同的类加载器加载该类，则静态成员将被共享。
 *  没有模拟出来
 *
 *
 * @author hanxinghua
 * @create 2023/1/7
 * @since 1.0.0
 */
public class Process1 {

    public static void main(String[] args) throws Exception {
        ClassLoader classLoader = getDefaultClassLoader();
        Class<?> clazz = classLoader.loadClass("com.hanxiaozhang.no1classloader.staticshare.no1sameprocess.StaticVariable");
        Field field = clazz.getField("NUM");
        field.set(null, 200);
        System.out.println("Process1 modify num is " + field.getInt(clazz));
        Class<?> clazz1 = classLoader.loadClass("com.hanxiaozhang.no1classloader.staticshare.no1sameprocess.StaticVariable");
        System.out.println("Process1 modify num is " + field.getInt(clazz1));

//        while (true) {
//
//        }
    }

    public static ClassLoader getDefaultClassLoader() {
        return Process1.class.getClassLoader();
    }


}
