package com.hanxiaozhang.no1classloader.staticshare.no1sameprocess;

/**
 * 〈一句话功能简述〉<br>
 * 〈静态变量〉
 *
 * @author hanxinghua
 * @create 2023/1/7
 * @since 1.0.0
 */
public class StaticVariable {

    public static int NUM = 10;

}
