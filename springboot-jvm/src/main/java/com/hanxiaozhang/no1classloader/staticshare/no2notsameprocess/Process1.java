package com.hanxiaozhang.no1classloader.staticshare.no2notsameprocess;

/**
 * 〈一句话功能简述〉<br>
 * 〈进程1〉
 *
 * 如果不在同一个JVM进程中，则不能共享，因为每个进程都有自己的内存空间
 *
 * @author hanxinghua
 * @create 2023/1/7
 * @since 1.0.0
 */
public class Process1 {


    public static void main(String[] args) {

        System.out.println("Process1 modify name before is " + StaticVariable.NAME);
        StaticVariable.NAME = "小韩";
        System.out.println("Process1 modify name after is " + StaticVariable.NAME);
        while (true) {

        }
    }

}
