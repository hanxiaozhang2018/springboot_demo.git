package com.hanxiaozhang.no1classloader.staticshare.no2notsameprocess;


/**
 * 〈一句话功能简述〉<br>
 * 〈进程2〉
 *
 * @author hanxinghua
 * @create 2023/1/7
 * @since 1.0.0
 */
public class Process2 {

    public static void main(String[] args) {
        System.out.println("Process2 get name is "+StaticVariable.NAME);
    }
}
