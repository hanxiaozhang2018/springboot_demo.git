package com.hanxiaozhang.no1classloader;

/**
 * 功能描述: <br>
 * 〈JVM执行模式〉
 * <p>
 * JVM执行模式：
 * - 解释器：
 * - JIT：
 * - 混合模式：混合使用解释器+热点代码编译。初始阶段使用解释执行，后期使用计数器记录，把代码进行编译
 * <p>
 * JVM默认使用混合模式
 * 配置使用：
 * -Xmixed 混合模式 -Xint 编译模式，启动很快，执行稍慢 -Xcomp 使用纯编译模式，执行很快，启动很慢
 *
 * @Author:hanxinghua
 * @Date: 2021/12/11
 */
public class No6WayToRun {

    public static void main(String[] args) {
        for (int i = 0; i < 10_0000; i++) {
            method();
        }

        long start = System.currentTimeMillis();
        for (int i = 0; i < 10_0000; i++) {
            method();
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    public static void method() {
        for (long i = 0; i < 10_0000L; i++) {
            long j = i % 3;
        }
    }
}
