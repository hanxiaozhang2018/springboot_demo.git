package com.hanxiaozhang.no1classloader;

/**
 * 功能描述: <br>
 * 〈懒加载〉
 * <p>
 * 严格讲应该叫lazy initialzing，因为java虚拟机规范并没有严格规定什么时候必须loading，
 * 但严格规定了什么时候initialzing
 *
 * @Author:hanxinghua
 * @Date: 2021/12/11
 */
public class No5LazyLoading {

    public static void main(String[] args) throws Exception {
        // 什么也不会加载
        // P p;

        // 加载 X类 P类  打印 P X
        // X x = new X();

        // 什么也不会加载
        // System.out.println(P.i);

        // 加载 P类  打印 P 9
        // System.out.println(P.j);

        // 加载 X类 P类  打印 P X 10
        //System.out.println(X.z);

        // 加载 P类  打印  P
        // Class.forName("com.hanxiaozhang.no1classloader.No5LazyLoading$P");

        // 加载 X类 P类  打印 P X
        Class.forName("com.hanxiaozhang.no1classloader.No5LazyLoading$X");

    }

    public static class P {
        final static int i = 8;
        static int j = 9;

        static {
            System.out.println("P");
        }
    }

    public static class X extends P {

        static int z = 10;

        static {
            System.out.println("X");
        }
    }
}
