package com.hanxiaozhang.no1classloader;

/**
 * 功能描述: <br>
 * 〈类加载器的使用〉
 * <p>
 * 使用场景：
 * 利用类加载器加载资源
 *
 * @Author:hanxinghua
 * @Date: 2021/12/9
 */
public class No3LoadClassUse {

    public static void main(String[] args) throws ClassNotFoundException {

        // 点进去看一下loadClass源码，可以了解一下双亲委派的过程
        Class clazz = No3LoadClassUse.class.getClassLoader().loadClass("com.hanxiaozhang.no1classloader.No3LoadClassUse");
        System.out.println(clazz.getName());

    }
}
