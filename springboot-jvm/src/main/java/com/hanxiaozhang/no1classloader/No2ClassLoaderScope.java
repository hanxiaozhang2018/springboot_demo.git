package com.hanxiaozhang.no1classloader;

/**
 * 功能描述: <br>
 * 〈类加载器的范围〉
 *
 * @Author:hanxinghua
 * @Date: 2021/12/8
 */
public class No2ClassLoaderScope {

    public static void main(String[] args) {

        System.out.println("---- BootStrap类加载器的加载范围");
        // BootStrap类加载器的加载路径
        String pathBoot = System.getProperty("sun.boot.class.path");
        System.out.println(pathBoot.replaceAll(";", System.lineSeparator()));

        System.out.println("---- Extension类加载器的加载范围");
        // Extension类加载器的加载路径
        String pathExt = System.getProperty("java.ext.dirs");
        System.out.println(pathExt.replaceAll(";", System.lineSeparator()));

        System.out.println("---- Application类加载器的加载范围");
        // Application类加载器的加载路径
        String pathApp = System.getProperty("java.class.path");
        System.out.println(pathApp.replaceAll(";", System.lineSeparator()));
    }
}
