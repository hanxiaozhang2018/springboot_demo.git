package com.hanxiaozhang.no1classloader;

/**
 * 功能描述: <br>
 * 〈类加载器〉
 *
 * @Author:hanxinghua
 * @Date: 2021/12/8
 */
public class No1ClassLoader {

    public static void main(String[] args) {

        // BootStrap加载，返回null
        System.out.println("String的加载器: " + String.class.getClassLoader());
        // BootStrap加载，返回null
        System.out.println("HKSCS的加载器: " + sun.awt.HKSCS.class.getClassLoader());
        // Extension加载
        System.out.println("DNSNameService的加载器: " + sun.net.spi.nameservice.dns.DNSNameService.class.getClassLoader());
        // Application加载
        System.out.println("No1ClassLoader的加载器: " + No1ClassLoader.class.getClassLoader());

        // 加载器的加载器是BootStrap
        System.out.println("---- 加载器的加载器是BootStrap");
        System.out.println("DNSNameService的加载器的加载器: " + sun.net.spi.nameservice.dns.DNSNameService.class.getClassLoader().getClass().getClassLoader());
        System.out.println("No1ClassLoader加载器的加载器: " + No1ClassLoader.class.getClassLoader().getClass().getClassLoader());

        System.out.println("---- 加载器的父加载器");
        // Extension的父加载是BootStrap
        System.out.println("DNSNameService的加载器的父加载器: " + sun.net.spi.nameservice.dns.DNSNameService.class.getClassLoader().getParent());
        // Application的父加载是Extension
        System.out.println("No1ClassLoader加载器的父加载器: " + No1ClassLoader.class.getClassLoader().getParent());

        // 返回委派的系统类加载器，这是新"类加载器"实例的默认委托父级，通常是用于启动应用程序的类加载器
        System.out.println(ClassLoader.getSystemClassLoader());
    }
}
