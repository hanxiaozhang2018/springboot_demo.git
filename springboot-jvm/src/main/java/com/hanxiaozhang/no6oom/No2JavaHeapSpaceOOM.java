package com.hanxiaozhang.no6oom;

import java.util.Random;
import java.util.concurrent.TimeUnit;


/**
 * 功能描述: <br>
 * 〈堆空间溢出〉
 * <p>
 * JVM参数:-Xms20m -Xmx20m
 * <p>
 * 如下例子说明：
 * 1. 展示堆溢出的情况
 * 2. 一个线程堆溢出了，不影响另一个线程运行。 解释：一个线程不会分配整个堆内存；一个线程堆溢出后，会释放占用的堆空间
 *
 * @Author: hanxinghua
 * @Date: 2023/11/30
 */
public class No2JavaHeapSpaceOOM {

    public static void main(String[] args) {

        // 堆溢出线程
        new Thread(() -> {
            String str = "happy day";
            while (true) {
                str += str + new Random().toString();
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        // 正常线程
        new Thread(() -> {
            int num = 0;
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(num);
                num++;
            }
        }).start();
    }
}
