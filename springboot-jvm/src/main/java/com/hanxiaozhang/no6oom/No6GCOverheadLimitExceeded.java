package com.hanxiaozhang.no6oom;

import java.util.ArrayList;
import java.util.List;


/**
 * 功能描述: <br>
 * 〈GC开销超过极限〉
 *
 * <p>
 * JVM参数
 * -Xms20m -Xmx20m -XX:+PrintGCDetails
 * <p>
 * 故障现象：
 * Exception in thread "main" java.lang.OutOfMemoryError: GC overhead limit exceeded
 * <p>
 * 出现场景：
 * 如果JVM花费超过98%的总时间进行GC，并且在GC之后仅回收不到2%的堆，则JVM被配置为抛出此错误
 * <p>
 * 假如不抛出GC overhead limit exceeded错误会发生什么情况呢？
 * GC清理的内存很快会再次填满，迫使GC再次执行，形成了恶性循环，CPU使用率一直100%，而GC却没有任何成果。
 *
 * @Author: hanxinghua
 * @Date: 2023/11/30
 */
public class No6GCOverheadLimitExceeded {


    public static void main(String[] args) {
        int i = 0;
        List<String> list = new ArrayList<>();
        try {
            while (true) {
                list.add(String.valueOf(++i).intern());
            }
        } catch (Throwable e) {
            System.out.println("*******i:" + i);
            e.printStackTrace();
            throw e;
        }
    }
}
