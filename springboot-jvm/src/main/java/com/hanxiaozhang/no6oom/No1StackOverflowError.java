package com.hanxiaozhang.no6oom;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * 功能描述: <br>
 * 〈堆栈溢出错误模拟〉
 * <p>
 * Xss（-XX:ThreadStackSize）：线程的栈空间大小，Xss=0代表使用默认值。
 * 在64位系统上，Linux/x64（64 位）的默认值为1024KB，即1M。
 * <p>
 * 如下例子说明：
 * 1. 展示栈溢出的情况
 * 2. 一个线程栈溢出了，不影响另一个线程运行
 *
 * @Author: hanxinghua
 * @Date: 2023/11/30
 */
public class No1StackOverflowError {


    public static void main(String[] args) throws InterruptedException {

        // 栈溢出线程
        new Thread(() -> {
            try {
                stackOverflowError();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

        // 正常线程
        new Thread(() -> {
            int num = 0;
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(num);
                num++;
            }
        }).start();

        // 阻塞
        new CountDownLatch(1).await();
    }

    private static void stackOverflowError() {
        try {
            TimeUnit.NANOSECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        stackOverflowError();
    }
}
