package com.hanxiaozhang.io.test;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;

/**
 * 〈一句话功能简述〉<br>
 * 〈Nio服务端〉
 *
 * @author hanxinghua
 * @create 2023/5/30
 * @since 1.0.0
 */
public class NioServerTest {

    public static void main(String[] args) throws Exception {

        LinkedList<SocketChannel> clients = new LinkedList<>();

        // 服务端开启监听
        ServerSocketChannel server = ServerSocketChannel.open();
        server.bind(new InetSocketAddress(9090));
        // 不阻塞
        server.configureBlocking(false);


        while (true) {
            // 第一步：接受客户端的连接
            SocketChannel client = server.accept();

            // 服务端listen的Socket，在连接请求三次握手后，通过accept得到连接的socket。
            if (client != null) {
                client.configureBlocking(false);
                System.out.println("client connect success,client port is " + client.socket().getPort());
                clients.add(client);
            }

            // 第二步：遍历已经链接进来的客户端能不能读写数据
            for (SocketChannel c : clients) {
                ByteBuffer buffer = ByteBuffer.allocateDirect(4096);
                // > 0  -1  0   //  不会阻塞
                int num = c.read(buffer);
                if (num > 0) {
                    // 反转缓存区，切换读写模式，将position给limit，然后将position置为0
                    buffer.flip();
                    // 创建一个limit大小的数据
                    byte[] bytes = new byte[buffer.limit()];
                    // 获取缓冲区数据到数组
                    buffer.get(bytes);
                    System.out.println(c.socket().getPort() + " : " + new String(bytes));
                    buffer.clear();
                }
            }
        }

    }

}
