package com.hanxiaozhang.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

/**
 * 功能描述: <br>
 * 〈多路复用器多线程版--主要说写事件〉
 *  前置学习代码：SocketMultiplexingSingleThreadv1_1
 *
 * @Author:hanxinghua
 * @Date: 2021/8/19
 */
public class SocketMultiplexingSingleThreadv2 {

    private ServerSocketChannel server = null;
    /**
     * java的selector是linux中的多路复用器(select、poll、epoll)或 nginx的event{}
     */
    private Selector selector = null;
    int port = 9090;

    public void initServer() {
        try {
            server = ServerSocketChannel.open();
            server.configureBlocking(false);
            server.bind(new InetSocketAddress(port));
            // select、poll、epoll，优先选择：epoll，但是可以-Djava参数修正。
            // 如果在epoll模型下，open  -->  epoll_create -> fd3
            selector = Selector.open();
            server.register(selector, SelectionKey.OP_ACCEPT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 启动第一版
     * 问题：read抛给一个线程去处理，但是因为时间差原因，read没有被完整读，会一直循环触发
     *
     */
    public void start_1() {
        initServer();
        System.out.println("服务器启动了。。。。。");
        try {
            while (true) {
                while (selector.select(50) > 0) {
                    Set<SelectionKey> selectionKeys = selector.selectedKeys();
                    Iterator<SelectionKey> iter = selectionKeys.iterator();
                    while (iter.hasNext()) {
                        SelectionKey key = iter.next();
                        iter.remove();
                        if (key.isAcceptable()) {
                            acceptHandler(key);
                        } else if (key.isReadable()) {
                            // 当前代码readHandler还是阻塞的嘛？ --> 不是，抛出了线程去读取。
                            // 抛出了线程去读取，在时间差里，这个key的read事件会被重复触发。
                            readHandler(key);
                        } else if (key.isWritable()) {
                            // 写事件 <--  send-queue只要是空的，就一定会给你返回可以写的事件，就会回调写方法
                            // 你真的要明白：什么时候写？不是依赖send-queue是不是有空间.
                            // 1.第一步，你准备好要写什么了；
                            // 2.第二步，你才关心send-queue是否有空间；
                            // 3.所以，读read一开始就要注册，但是write依赖以上关系，write什么时候用什么时候注册
                            // 4.如果一开始就注册了write的事件，进入死循环，一直调起。
                            writeHandler(key);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 启动第二版
     * 问题：
     *
     */
    public void start_2() {
        initServer();
        System.out.println("服务器启动了。。。。。");
        try {
            while (true) {
                while (selector.select(50) > 0) {
                    Set<SelectionKey> selectionKeys = selector.selectedKeys();
                    Iterator<SelectionKey> iter = selectionKeys.iterator();
                    while (iter.hasNext()) {
                        SelectionKey key = iter.next();
                        iter.remove();
                        if (key.isAcceptable()) {
                            acceptHandler(key);
                        } else if (key.isReadable()) {
                            // 在多路复用器里把key取消了，不会被重复触发
                            key.cancel();
                            // 当前代码readHandler还是阻塞的嘛？ --> 不是，抛出了线程去读取。
                            // 抛出了线程去读取，在时间差里，这个key的read事件会被重复触发。
                            readHandler(key);
                        } else if (key.isWritable()) {
                            // 写事件 <--  send-queue只要是空的，就一定会给你返回可以写的事件，就会回调写方法
                            // 你真的要明白：你想什么时候写？不是依赖send-queue是不是有空间。（多路复用器能不能写是参考send-queue是否有空间）
                            // 1.第一步，你准备好要写什么了；
                            // 2.第二步，你才关心send-queue是否有空间；
                            // 3.所以，读read一开始就要注册，但是write依赖以上关系，write什么时候用什么时候注册
                            // 4.如果一开始就注册了write的事件，进入死循环，一直调起。
                           // key.cancel();
                            writeHandler(key);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 写
     *
     * @param key
     */
    private void writeHandler(SelectionKey key) {
        new Thread(() -> {
            System.out.println("write handler...");
            SocketChannel client = (SocketChannel) key.channel();
            ByteBuffer buffer = (ByteBuffer) key.attachment();
            buffer.flip();
            while (buffer.hasRemaining()) {
                try {
                    client.write(buffer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            buffer.clear();
            key.cancel();
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * 接收
     *
     * @param key
     */
    public void acceptHandler(SelectionKey key) {
        try {
            ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
            SocketChannel client = ssc.accept();
            client.configureBlocking(false);
            ByteBuffer buffer = ByteBuffer.allocate(8192);
            client.register(selector, SelectionKey.OP_READ, buffer);
            System.out.println("-------------------------------------------");
            System.out.println("新客户端：" + client.getRemoteAddress());
            System.out.println("-------------------------------------------");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 读
     *
     * @param key
     */
    public void readHandler(SelectionKey key) {
        new Thread(() -> {
            System.out.println("read handler.....");
            SocketChannel client = (SocketChannel) key.channel();
            ByteBuffer buffer = (ByteBuffer) key.attachment();
            buffer.clear();
            int read = 0;
            try {
                while (true) {
                    read = client.read(buffer);
                    if (read > 0) {
                        // 客户端注册写事件，这里只关心，OP_WRITE --> 其实就是关心send-queue是不是有空间
                        client.register(key.selector(),SelectionKey.OP_WRITE,buffer);
                    } else if (read == 0) {
                        break;
                    } else {
                        client.close();
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public static void main(String[] args) {
        SocketMultiplexingSingleThreadv2 service = new SocketMultiplexingSingleThreadv2();
        service.start_2();
    }
}
