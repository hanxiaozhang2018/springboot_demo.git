package com.hanxiaozhang.io.makereactor_1;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

/**
 * 〈一句话功能简述〉<br>
 * 〈多路复用器线程〉
 * 作用：每个线程对应一个selector。在多线程情况下，
 * 该程序的并发客户端被分配到多个selector上。
 * 注意：每个客服端，只绑定其中一个selector。所以，不会有交互问题。
 *
 * @author hanxinghua
 * @create 2021/8/22
 * @since 1.0.0
 */
public class SelectorThread implements Runnable {

    public Selector selector = null;

    SelectorThread() {
        try {
            selector = Selector.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {

        // 死循环 Loop
        while (true) {
            try {
                // 1.select   它是阻塞的 --> selector.wakeup()：用于唤醒阻塞在select方法上的线程
                System.out.println(Thread.currentThread().getName() + "   :  before select...." + selector.keys().size());
                // selector.select(long timeout) 可以设置超时时间
                int num = selector.select();
                // Thread.sleep(1000);  /* 这绝对不是解决方案，我只是给你演示*/
                System.out.println(Thread.currentThread().getName() + "   :  after select...." + selector.keys().size());

                // 2.处理selectKeys
                if (num > 0) {
                    Set<SelectionKey> keys = selector.selectedKeys();
                    Iterator<SelectionKey> iterator = keys.iterator();
                    // 线性处理过程
                    while (iterator.hasNext()) {
                        SelectionKey key = iterator.next();
                        iterator.remove();
                        // 接受客户端过程，比较复杂（接受之后，要注册，新的客户端，注册到哪里？）
                        if (key.isAcceptable()) {
                            acceptHandler(key);
                        } else if (key.isReadable()) {
                            readHandler(key);
                        } else if (key.isWritable()) {

                        }
                    }

                }
                // 3.处理一些task


            } catch (IOException e) {
                e.printStackTrace();
            }
//            catch (InterruptedException e) {
//                e.printStackTrace();
//            }

        }

    }

    private void readHandler(SelectionKey key) {
        ByteBuffer buffer = (ByteBuffer) key.attachment();
        SocketChannel client = (SocketChannel) key.channel();
        buffer.clear();
        while (true) {
            try {
                int num = client.read(buffer);
                if (num > 0) {
                    // 将读到内容翻转，然后直接写出
                    buffer.flip();
                    while (buffer.hasRemaining()) {
                        client.write(buffer);
                    }
                    buffer.clear();
                } else if (num == 0) {
                    break;
                } else if (num < 0) {
                    // 客户端断开
                    System.out.println("client： " + client.getRemoteAddress() + " closed!");
                    client.close();
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private void acceptHandler(SelectionKey key) {

        ServerSocketChannel server = (ServerSocketChannel) key.channel();
        try {
            SocketChannel client = server.accept();
            client.configureBlocking(false);
            // 需要选择一个多路复用器，并注册

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
