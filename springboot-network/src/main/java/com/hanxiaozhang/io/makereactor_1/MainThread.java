package com.hanxiaozhang.io.makereactor_1;

/**
 * 〈一句话功能简述〉<br>
 * 〈主线程〉
 *  makereactor_1: serverSocketChannel.register(st.selector, SelectionKey.OP_ACCEPT); 发生阻塞了
 *
 * @author hanxinghua
 * @create 2021/8/22
 * @since 1.0.0
 */
public class MainThread {

    public static void main(String[] args) {
        // 这里不做关于 IO 和 业务的事情

        // 1.创建 IO Thread (一个或多个)
        SelectorThreadGroup selectorThreadGroup = new SelectorThreadGroup(1);

        // 2.把监听的server注册到某一个selector上
        selectorThreadGroup.bind(9999);



    }
}
