package com.hanxiaozhang.io.makereactor_4;

/**
 * 〈一句话功能简述〉<br>
 * 〈主线程--主从模式〉
 *  makereactor_1: serverSocketChannel.register(st.selector, SelectionKey.OP_ACCEPT); 发生阻塞了
 *  makereactor_2: 新增队列解决makereactor_1问题，SelectorThread(SelectorThreadGroup selectorThreadGroup)增加入参
 *  makereactor_3：混杂模式 --> 说的是listen  read 事件混用线程
 *  makereactor_4：主从模式 --> 修改取模算法，保证listen只在0线程上，read在其他线程上
 *
 * @author hanxinghua
 * @create 2021/8/25
 * @since 1.0.0
 */
public class MainThread {

    public static void main(String[] args) {
        // 这里不做关于 IO 和 业务的事情

        // 1.创建 IO Thread (一个或多个)  混杂模式
        SelectorThreadGroup selectorThreadGroup = new SelectorThreadGroup(3);

        // 2.把监听的server注册到某一个selector上
        selectorThreadGroup.bind(9999);



    }
}
