package com.hanxiaozhang.io.makereactor_5;

/**
 * 〈一句话功能简述〉<br>
 * 〈主线程--绑定多个端口〉
 *  makereactor_1: serverSocketChannel.register(st.selector, SelectionKey.OP_ACCEPT); 发生阻塞了
 *  makereactor_2: 新增队列解决makereactor_1问题，SelectorThread(SelectorThreadGroup selectorThreadGroup)增加入参
 *  makereactor_3：混杂模式 --> 说的是listen  read 事件混用线程
 *  makereactor_4：主从模式 --> 修改取模算法，保证listen只在0线程上，read在其他线程上
 *  makereactor_5：绑定多个端口
 *
 * @author hanxinghua
 * @create 2021/8/25
 * @since 1.0.0
 */
public class MainThread {

    public static void main(String[] args) {
        // 这里不做关于 IO 和 业务的事情

        // 1.创建 IO Thread (一个或多个)  混杂模式
        //  boss有自己的线程组
        SelectorThreadGroup boss = new SelectorThreadGroup(3);

        // worker有自己的线程组
        SelectorThreadGroup worker = new SelectorThreadGroup(3);

        // boss持有worker的引用
        boss.setWorker(worker);
        /**
         * boss里选一个线程注册listen，触发bind，从而，这个不选中的线程得持有workerGroup(worker组)的引用
         * 因为未来listen一旦accept，得到client后得去worker中next出一个线程分配
         */

        boss.bind(9999);
        boss.bind(8888);
        boss.bind(6666);
        boss.bind(7777);

    }
}
