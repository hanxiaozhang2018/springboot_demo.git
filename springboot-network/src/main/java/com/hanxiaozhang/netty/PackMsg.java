package com.hanxiaozhang.netty;

/**
 * 功能描述: <br>
 * 〈〉
 *
 * @Author:hanxinghua
 * @Date: 2021/8/28
 */
public class PackMsg {

    MyHeader header;
    MyContent content;

    public MyHeader getHeader() {
        return header;
    }

    public void setHeader(MyHeader header) {
        this.header = header;
    }

    public MyContent getContent() {
        return content;
    }

    public void setContent(MyContent content) {
        this.content = content;
    }

    public PackMsg(MyHeader header, MyContent content) {
        this.header = header;
        this.content = content;
    }
}
