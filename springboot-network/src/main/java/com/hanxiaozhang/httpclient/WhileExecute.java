package com.hanxiaozhang.httpclient;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/2/29
 * @since 1.0.0
 */
public class WhileExecute {


    private static final String FILE_PATH = "D:\\test.txt";

    public static final String URL = "http://127.0.0.1:8080/test18?id=";

    private static final Logger logger = LoggerFactory.getLogger(WhileExecute.class);


    private static ThreadPoolExecutor THREAD_POOL = new ThreadPoolExecutor(4, 16, 1, TimeUnit.MINUTES,
            new LinkedBlockingQueue(), Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy());


    public static void main(String[] args) {

        logger.info("eeee");
        //去除http请求debug日志
        // 设置默认工厂类
        System.setProperty("org.apache.commons.logging.LogFactory", "org.apache.commons.logging.impl.LogFactoryImpl");
        // 设置日志打印类
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
        //设置默认日志级别
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.simplelog.defaultlog", "error");


        List<String> list = readDataByFile(FILE_PATH);


        if (CollectionUtils.isNotEmpty(list)) {

            list.forEach(x -> {
                THREAD_POOL.execute(() -> {
                            try {
                                String url = URL + x;
                                String result = HttpClientUtil.get(url);
                                System.out.println("url is " + url + " result is " + result);
                            } catch (Exception e) {
                                logger.error("", e);
                            }
                        }
                );
            });
        }

        // 所有线程返回后，关闭线程池
        THREAD_POOL.shutdown();

    }

    /**
     * 通过文件读取数据
     *
     * @param filePath
     * @return
     */

    private static List<String> readDataByFile(String filePath) {
        List<String> list = new ArrayList<>();

        File file = new File(filePath);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String s = null;
            // 使用readLine方法，一次读一行
            while (StringUtils.isNotEmpty(s = br.readLine())) {
                list.add(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return list;
    }

}
