# 零、目录结构：
|
|-- base :模拟TCP粘包拆包导致功能的异常
|-- delimiterbasedframedecoder :使用DelimiterBasedFrameDecoder解决TCP粘包拆包
|-- fixedlengthframedecoder :使用FixedLengthBasedFrameDecoder解决TCP粘包拆包
|-- heartbeat：心跳检测
|-- http: http协议
|-- linebasedframedecoder :使用LineBasedFrameDecoder解码器解决TCP粘包拆包
|-- messagepack 使用MessagePack作为编解码器的开发
|-- nio: 使用传统nio实现通信
|-- privateprotocol：私有协议的开发
|-- test：一些简单的测试
|-- websocket Netty Websocket协议开发


# 一、TCP：
TCP是个"流"协议，所谓流，就没有界限的一串数据。
## 1. TCP粘包/拆包：
### 概念：
TCP底层并不了解上层业务数据的具体含义，它会根据TCP缓冲区的实际情况进行包的划分，所以在业务上认为，
一个完整的包可能会被TCP拆成多个包进行发送，也有可能把多个小的包封装成一个大的数据包发送，这就是TCP粘包和拆包。
### 原因：
i.应用程序write写入的字节大小大于套接字接口发送缓冲区大小；
ii.进行MSS（最大报文段长度）大小的TCP分段；
iii.以太网帧的payload（有效载荷）大于MTU(最大传输单元)进行IP分片
![TCP粘包_拆包问题原因.png]
### 解决：
i.消息定长，例如每个报文的大小固定长度200字节，如果不够用空格补齐。
ii.在包尾增加回车换行符进行分割，例如FTP协议；
iii.将消息分为消息头和消息体，消息头中包含消息总长度（或消息体长度）的字段，
通常设计思路为消息头的第一个字段使用int32来表示消息的总长度；
iv.更复杂的应用层协议。

## 2. TCP上层的应用协议进行区分消息的4种方式：
+ 消息长度固定，累计读取到长度总和为定长LEN的报文后，就认为读取到一个完整的消息：将计数器位置，重新开始读取下一个数据报。
+ 将回车换行符作为消息的结束符，例如FTP协议，这种方式在文本协议中应用比较广泛；
+ 将特殊的分隔符作为消息的结束标志，回车换行符就是一种特殊的结束分隔符；
+ 通过在消息头中定义长度字段来标识消息的总长度。
** Tips：Netty对上面4种应用做了统一的抽象，提供了4种解码器来解决对应的问题，使用起来非常方便。**


# 二、解码器（用来解决TCP粘包拆包）：
## 1. LineBasedFrameDecoder + StringDecoder -> com.hanxiaozhang.linebasedframedecoder
+ 它依次遍历ByteBuf中的可读字节，判断看是否有"\n"或者"\r\n"，如果有，就以此位置为结束位置，
从可读索引到结束位置区间的字节就组成了一行。它是以换行符为结束标识的解码器，支持携带结束符
或者不带结束符两种解码方式，同时支持配置单行的最大长度。
+ StringDecoder将接收到对象转换成字符串，然后继续调用后边的Handler。

## 2. DelimiterBasedFrameDecoder -> com.hanxiaozhang.delimiterbasedframedecoder
它可以自动完成以分割符作为码流结束标识的消息进行解码。

## 3. FixedLengthBasedFrameDecoder -> com.hanxiaozhang.fixedlengthframedecoder
它可以按照固定长度解码器，它能够按照指定的长度对消息进行解码。如果是半包消息，
FixedLengthBasedFrameDecoder会缓存半包消息并等待下一个包到达后进行拼包。


# 三、编解码技术：
## 1. 概念：
编码和解码是char和byte两个数据类型之间转换中产生的：
+ 编码是将字符串转换为byte数组；
+ 解码是将byte数组转换为字符串。

## 2. Java序列化
### 作用：
网络传输和对象持久化
### 缺点：
无法跨语言、序列化后码流太大、序列化性能太低

## 3. 业界主流的编解码框架
### Protobuf（Google Protocol Buffers）：
+ 它将数据结构以.Proto文件进行描述，通过代码生成工具可以生成对应数据结构的POJO对象和Protobuf相关的方法和属性。
+ 特点：
- 结构化数据存储格式（XML、JSON等）；高效的编解码性能；
- 言语无关、平台无关、拓展性好；官方支持Java、C++和Python三种语言。

### Facebook的Thrift  -> 跨多语言之间的转换
解决Facebook各系统间大数据量的传输通信以及系统之间语言环境不同需要跨平台的特性。
因此Thrift支持多种程序语言，如C++、C#、Cocoa、Erlang、Haskell、Java、Ocami、Prel、
PHP、Python、Ruby和Smalltalk。
它主要由5部分构成：
+ 语音系统以及IDL编译器：负责由用户给定的IDL文件生成相应的语言接口代码
+ TProtocol：RPC的协议层,可以选择多种不同对象序列化方式，如JSON和Binary
+ TTransport：RPC的传输层，同样可以选择不同的传输层实现、如socket、NIO、MemoryBuffer
+ TProcessor：作为协议层和用户提供服务实现之间的纽带，负责调用服务实现的接口
+ TServer：聚合TProtocol、TTransport、TProcessor对象


### JBoss Marshalling
它是java对象序列化API包，修正了JDK自带的序列化包的很多问题。
相比传统的Java序列化对象机制，它的优点：
+ 可插拔的类解释器，提供更加便捷的类加载定制策略，通过一个接口即可实现定制
+ 可插拔的对象替换技术，不需要通过继承方式
+ 可插拔的预定义类缓存表，可以减少序列化的字节数组长度，提升常用类型的对象序列化性能
+ 无须实现java.io.Serizable接口
+ 通过缓存提升对象的序列化性能

### MessagePack编解码（重要）-> com.hanxiaozhang.messagepack
+ 它是一个高效的二进制序列化框架，它像JSON一样支持不同语言间的数据交换，
   它的性能更快，序列化之后码流也更小。
+ 特点：
  - 编解码高效，性能高；
  - 序列化之后的码流小；
  - 支持跨语言。
+ Netty的CodeC框架新增对MessagePack的支持


# 四、Netty多协议开发与应用：
## 1. HTTP协议：-> com.hanxiaozhang.http
### 弊端：
+ HTTP协议为半双工协议；
+ HTTP消息冗长而繁琐；
+ 针对服务器推送的黑客攻击，例如长时间轮询。

## 2. WebSocket协议： -> com.hanxiaozhang.websocket
### 概念：
它是HTML5 开始提供的一种浏览器与服务器间进行全双工通信的网络技术，
在WebSocket API中，浏览器和服务器只需要做一个握手的动作，然后，浏览器和服务器
之间就形成了一条快速通道，两者就可以直接相互传送数据了。
### 特点：
+ 单一的TCP链接，采用全双工模式通信；
+ 对代理、防火墙和路由器透明；
+ 无头部信息、Cookie和身份验证；
+ 无安全开销；
+ 通过"ping/pong"帧保持链路激活；
+ 服务器可以主动传递消息给客户端，不需要客户端轮询。
### Tips：
有时间系统学习一下，它可能是未来实时Web应用的首先方案。

## 3. 私有协议栈：
+ 私有协议比较灵活，他会在某个公司内部或组织内使用，可以按需定制。
+ 私有协议具有垄断性的特征。
+ 绝大多数的私有协议传输层都基于TCP/IP，所以利用Netty的NIO TCP 协议栈可以非常方便地进行私有协议的定制和开发。
### Tips：
通过本章，可以了解Netty协议栈功能的设计，如果以后想要设计私有协议栈，可以再看本章。

## 4. 服务端创建：
![Netty服务端创建时序图.png]
### 相关源码：
P263 - P275

## 5. 客户端创建：
![Netty客户端创建时序图.png]
### 相关源码：
P276 - P285

# 五、Netty功能介绍与源码分析（源码分析大部分没有时间看2022-11-20）：
## 1. ByteBuf：
### ByteBuf源码分析：
+ AbstractByteBuf源码分析 P310
+ AbstractReferenceCountedByteBuf源码分析 P3l9
+ UnpooledHeapByteBuf源码分折 P321
+ PooledByteBuf内存池原理分析 P326
+ PooledDirectByteBuf源码分析 P329

## 2. Channel：
### Channel源码分析：
+ Channel的主要继承关系类图 P343
+ AbstractChannel源码分析 P344
+ AbstractNioChannel源码分析 P347
+ AbstractNioByteChannel源码分析 P350
+ AbstractNioMessageChannel源码分析 P353
+ AbstractNioMessageServerChannel源码分析 P354
+ NioServerSocketChannel源码分折 P355
+ NioSocketChannel源码分析 P356

## 3. Unsafe：
### Unsafe源码分析：
+ Unsafe继承关系类图 P365
+ AbstractUnsafe源码分析 P366
+ AbstractNioUnsafe源码分析 P375
+ NioByteUnsafe源码分析 P379

## 4. ChannelPipeline：
### ChannelPipeline源码分析：
+ ChannelPipeline的类继承关系图 P393
+ ChannelPipeline对ChannelHandler的管理 P393
+ ChannelPipeline的inbound事件 P396
+ ChannelPipeline的outbound事件 P397

## 5. ChannelHandler
### ChannelHandler源码分折：
+ ChannelHandler的类继承关系图 P405
+ ByteToMessageDecoder源码分析 P407
+ MessageToMessageDecoder源码分析 P409
+ LengthFieldBasedFrameDecoder源码分析 P 411
+ MessageToByteEncoder源码分折 P415
+ MessageToMessageEncoder源码分析 P416
+ LengthFieldPrepender源码分析 P417

## 6. NioEventLoop
### NioEventLoop源码分析：
+ NioEventLoop设计原理 P425
+ NioEventLoop继承关系类图 P426
+ NioEventLoop P427  时间重点看一下                                                                                                                                                                                         

## 7. ChannelFuture
### ChannelFuture源码分析：
+ ChannelFuture源码分析 P443

## 8. Promise
### Promise源码分析：
+ Promise继承关系图 P447
+ DefaultPromise P447

