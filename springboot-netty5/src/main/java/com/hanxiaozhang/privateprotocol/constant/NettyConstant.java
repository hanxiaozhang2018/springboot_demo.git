package com.hanxiaozhang.privateprotocol.constant;

/**
 * 功能描述: <br>
 * 〈netty常量〉
 *
 * @Author:hanxinghua
 * @Date: 2020/10/23
 */
public final class NettyConstant {

    /**
     * 远程Ip
     */
    public static final String REMOTE_IP = "127.0.0.1";

    /**
     * 本地Ip
     */
    public static final String LOCAL_IP = "127.0.0.1";

    /**
     * 端口
     */
    public static final int PORT = 8080;

    /**
     * 本地端口
     */
    public static final int LOCAL_PORT = 12088;

}
