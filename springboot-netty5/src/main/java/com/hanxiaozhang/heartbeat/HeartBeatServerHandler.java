package com.hanxiaozhang.heartbeat;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * 〈一句话功能简述〉<br>
 * 〈服务端的心跳Handler〉
 *
 * @author hanxinghua
 * @create 2022/11/27
 * @since 1.0.0
 */
public class HeartBeatServerHandler extends SimpleChannelInboundHandler<String> {

    /**
     * 记录读空闲的心跳次数
     */
    private int readIdleTimes = 0;


    @Override
    protected void messageReceived(ChannelHandlerContext ctx, String msg) throws Exception {
        System.out.println("[服务端] 接收到消息 : " + msg);
        if ("I am alive".equals(msg)) {
            ctx.channel().writeAndFlush("copy that");
        } else {
            System.out.println("其他信息处理 ... ");
        }
    }

    /**
     * 作用：
     * 处理IdleStateEvent（心跳事件），如果读空闲超过3次，主动关闭链接
     *
     * @param ctx
     * @param evt
     * @throws Exception
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        IdleStateEvent event = (IdleStateEvent) evt;

        String eventType = null;
        switch (event.state()) {
            case READER_IDLE:
                eventType = "读空闲";
                // 读空闲的计数加1
                readIdleTimes++;
                break;
            case WRITER_IDLE:
                eventType = "写空闲";
                // 不处理
                break;
            case ALL_IDLE:
                eventType = "读写空闲";
                // 不处理
                break;
        }
        System.out.println(ctx.channel().remoteAddress() + " 超时事件：" + eventType);

        if (readIdleTimes > 3) {
            System.out.println("[服务端] 读空闲超过3次，关闭连接");
            ctx.channel().writeAndFlush("你必须退出了");
            ctx.channel().close();
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.err.println("=== " + ctx.channel().remoteAddress() + " TCP链接建立成功 ===");
    }


}
