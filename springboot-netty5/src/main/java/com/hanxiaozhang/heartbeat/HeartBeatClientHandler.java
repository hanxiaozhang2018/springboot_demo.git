package com.hanxiaozhang.heartbeat;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 〈一句话功能简述〉<br>
 * 〈客户端心跳检测Handler〉
 *
 * @author hanxinghua
 * @create 2022/11/27
 * @since 1.0.0
 */
public class HeartBeatClientHandler extends SimpleChannelInboundHandler<String> {

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, String msg) throws Exception {
        System.out.println("[客户端] 接收到消息 :" + msg);
        if (msg != null && msg.equals("你必须退出了")) {
            System.out.println("服务器关闭连接，所以客户端也会关闭");
            ctx.channel().closeFuture();
        }
    }
}
