package com.hanxiaozhang.enums;


/**
 * 功能描述: <br>
 * 〈敏感信息类型〉
 *
 * @Author:hanxinghua
 * @Date: 2020/1/29
 */
public enum SensitiveType {
    /**
     * 身份证
     */
    ID_CARD,
    /**
     * 银行卡
     */
    BANK_CARD,
    /**
     * 电话号码
     */
    PHONE,
    /**
     * 真实姓名
     */
    REAL_NAME;
}
