package com.hanxiaozhang.testdictcode.service;


import com.hanxiaozhang.testdictcode.domain.TestDictDO;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2019/12/28
 * @since 1.0.0
 */
public interface TestDictService {


    TestDictDO get(Long id);


}
