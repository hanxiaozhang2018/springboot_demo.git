package com.hanxiaozhang.constant;

/**
 * 〈一句话功能简述〉<br>
 * 〈合同常量〉
 *
 * @author hanxinghua
 * @create 2020/2/17
 * @since 1.0.0
 */
public class ContractConstant {

    /**
     * 私有构造函数，避免被继承和扩展
     */
    private ContractConstant(){}

    public final static String DOCX = ".docx";

    public final static String DOC = ".doc";

}
