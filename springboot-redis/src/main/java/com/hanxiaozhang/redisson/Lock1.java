package com.hanxiaozhang.redisson;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 * 〈一句话功能简述〉<br>
 * 〈Redisson分布式锁〉
 *
 * @author hanxinghua
 * @create 2022/9/12
 * @since 1.0.0
 */
public class Lock1 {

    public static void main(String[] args) {

        Config config = new Config();
        config.useSingleServer().setAddress("http://127.0.0.1:6379").setDatabase(0);
        RedissonClient client = Redisson.create(config);
        RLock lock = client.getFairLock("test");
        lock.lock();

    }

}
