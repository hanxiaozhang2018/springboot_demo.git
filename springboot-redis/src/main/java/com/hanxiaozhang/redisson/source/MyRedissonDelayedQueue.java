package com.hanxiaozhang.redisson.source;

import io.netty.util.internal.PlatformDependent;
import org.redisson.RedissonTopic;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RFuture;
import org.redisson.api.RTopic;
import org.redisson.client.codec.Codec;
import org.redisson.client.codec.LongCodec;
import org.redisson.client.protocol.RedisCommands;
import org.redisson.command.CommandAsyncExecutor;
import org.redisson.misc.RedissonPromise;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 〈延迟队列〉
 *
 * @Author: hanxinghua
 * @Date: 2022/9/17
 */
public class MyRedissonDelayedQueue<V> extends MyRedissonExpirable implements RDelayedQueue<V> {

    private final MyQueueTransferService queueTransferService;
    private final String channelName;
    private final String queueName;
    private final String timeoutSetName;

    protected MyRedissonDelayedQueue(MyQueueTransferService queueTransferService, Codec codec, final CommandAsyncExecutor commandExecutor, String name) {
        super(codec, commandExecutor, name);
        // 设置频道名称
        channelName = prefixName("redisson_delay_queue_channel", getName());
        // 总队列名
        queueName = prefixName("redisson_delay_queue", getName());
        // 延迟队列名
        timeoutSetName = prefixName("redisson_delay_queue_timeout", getName());
        // 设置一个任务，作用：将到期的任务推送阻塞队列
        MyQueueTransferTask task = new MyQueueTransferTask(commandExecutor.getConnectionManager()) {
            @Override
            protected RFuture<Long> pushTaskAsync() {
                return commandExecutor.evalWriteAsync(getName(), LongCodec.INSTANCE, RedisCommands.EVAL_LONG,
                        // timeoutSet队列，根据分值区间[ 0-System.currentTimeMillis()]，分页[ limit 0 100 ]获取成员。
                        //  zrangebyscore redisson_delay_queue_timeout:{name}  0  System.currentTimeMillis() limit 0 100
                        "local expiredValues = redis.call('zrangebyscore', KEYS[2], 0, ARGV[1], 'limit', 0, ARGV[2]); "
                                // 如果个数大于0
                                + "if #expiredValues > 0 then "
                                // 循环遍历数组
                                + "for i, v in ipairs(expiredValues) do "
                                // 解析数据，当前数据组成是：'dLc0' + randomId + string.len(数据) + 数据
                                + "local randomId, value = struct.unpack('dLc0', v);"
                                // 添加到阻塞队列中
                                // rpush  name  value
                                + "redis.call('rpush', KEYS[1], value);"
                                // 从 queue 队列中删除
                                // lrem  redisson_delay_queue:{name} 1 v
                                + "redis.call('lrem', KEYS[3], 1, v);"
                                + "end; "
                                // 从 timeoutSet 删除
                                // zrem  redisson_delay_queue_timeout:{name}  ..
                                + "redis.call('zrem', KEYS[2], unpack(expiredValues));"
                                + "end; "
                                // 获取 timeoutSet 队头数据，即获取下一个要过期的数据的时间戳返回
                                // zrange  redisson_delay_queue_timeout:{name}  0 0  WITHSCORES
                                + "local v = redis.call('zrange', KEYS[2], 0, 0, 'WITHSCORES'); "
                                // 如果成员不为空，则返回该成员的分值，作为下一次要过期的时间戳
                                + "if v[1] ~= nil then "
                                + "return v[2]; "
                                + "end "
                                + "return nil;",
                        Arrays.<Object>asList(getName(), timeoutSetName, queueName),
                        System.currentTimeMillis(), 100);
            }

            @Override
            protected RTopic<Long> getTopic() {
                // 消息主题，向channelName发布消息，订阅该主题的客户端接收对应消息
                return new RedissonTopic<Long>(LongCodec.INSTANCE, commandExecutor, channelName);
            }
        };
        // 启动任务，添加监听
        queueTransferService.schedule(queueName, task);

        this.queueTransferService = queueTransferService;
    }

    /**
     * 推送消息
     *
     * @param e
     * @param delay
     * @param timeUnit
     */
    @Override
    public void offer(V e, long delay, TimeUnit timeUnit) {
        get(offerAsync(e, delay, timeUnit));
    }

    /**
     * 推送消息异步执行
     *
     * @param e
     * @param delay
     * @param timeUnit
     * @return
     */
    @Override
    public RFuture<Void> offerAsync(V e, long delay, TimeUnit timeUnit) {
        long delayInMs = timeUnit.toMillis(delay);
        long timeout = System.currentTimeMillis() + delayInMs;

        long randomId = PlatformDependent.threadLocalRandom().nextLong();
        return commandExecutor.evalWriteAsync(getName(), codec, RedisCommands.EVAL_VOID,
                // struct.pack(...) ：将v1,v2等参数的值进行一层包装，返回字符串
                "local value = struct.pack('dLc0', tonumber(ARGV[2]), string.len(ARGV[3]), ARGV[3]);"
                        // 向 timeoutSet 延迟队列添加数据
                        // zadd  redisson_delay_queue_timeout:{name}  timeout  value
                        + "redis.call('zadd', KEYS[2], ARGV[1], value);"
                        // 向 queue 队列添加数据
                        // rpush redisson_delay_queue:{name}  value
                        + "redis.call('rpush', KEYS[3], value);"
                        // 获取 timeoutSet 延迟队列的队头数据
                        + "local v = redis.call('zrange', KEYS[2], 0, 0); "
                        // 队头数据等于刚加入的数据，则向频道发送消息
                        + "if v[1] == value then "
                        // publish channelName timeout
                        + "redis.call('publish', KEYS[4], ARGV[1]); "
                        + "end;"
                ,
                Arrays.<Object>asList(getName(), timeoutSetName, queueName, channelName),
                timeout, randomId, encode(e));
    }

    @Override
    public boolean add(V e) {
        throw new UnsupportedOperationException("Use 'offer' method with timeout param");
    }

    @Override
    public boolean offer(V e) {
        throw new UnsupportedOperationException("Use 'offer' method with timeout param");
    }

    @Override
    public V remove() {
        V value = poll();
        if (value == null) {
            throw new NoSuchElementException();
        }
        return value;
    }

    @Override
    public V poll() {
        return get(pollAsync());
    }

    @Override
    public V element() {
        V value = peek();
        if (value == null) {
            throw new NoSuchElementException();
        }
        return value;
    }

    @Override
    public V peek() {
        return get(peekAsync());
    }

    @Override
    public int size() {
        return get(sizeAsync());
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return get(containsAsync(o));
    }

    V getValue(int index) {
        return (V) get(commandExecutor.evalReadAsync(getName(), codec, RedisCommands.EVAL_OBJECT,
                "local v = redis.call('lindex', KEYS[1], ARGV[1]); "
                        + "if v ~= false then "
                        + "local randomId, value = struct.unpack('dLc0', v);"
                        + "return value; "
                        + "end "
                        + "return nil;",
                Arrays.<Object>asList(queueName), index));
    }

    void remove(int index) {
        get(commandExecutor.evalWriteAsync(getName(), null, RedisCommands.EVAL_VOID,
                "local v = redis.call('lindex', KEYS[1], ARGV[1]);" +
                        "if v ~= false then " +
                        "local randomId, value = struct.unpack('dLc0', v);" +
                        "redis.call('lrem', KEYS[1], 1, v);" +
                        "redis.call('zrem', KEYS[2], v);" +
                        "end; ",
                Arrays.<Object>asList(queueName, timeoutSetName), index));
    }

    @Override
    public Iterator<V> iterator() {
        return new Iterator<V>() {

            private V nextCurrentValue;
            private V currentValueHasRead;
            private int currentIndex = -1;
            private boolean hasBeenModified = true;

            @Override
            public boolean hasNext() {
                V val = MyRedissonDelayedQueue.this.getValue(currentIndex + 1);
                if (val != null) {
                    nextCurrentValue = val;
                }
                return val != null;
            }

            @Override
            public V next() {
                if (nextCurrentValue == null && !hasNext()) {
                    throw new NoSuchElementException("No such element at index " + currentIndex);
                }
                currentIndex++;
                currentValueHasRead = nextCurrentValue;
                nextCurrentValue = null;
                hasBeenModified = false;
                return currentValueHasRead;
            }

            @Override
            public void remove() {
                if (currentValueHasRead == null) {
                    throw new IllegalStateException("Neither next nor previous have been called");
                }
                if (hasBeenModified) {
                    throw new IllegalStateException("Element been already deleted");
                }
                MyRedissonDelayedQueue.this.remove(currentIndex);
                currentIndex--;
                hasBeenModified = true;
                currentValueHasRead = null;
            }

        };
    }

    @Override
    public Object[] toArray() {
        List<V> list = readAll();
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        List<V> list = readAll();
        return list.toArray(a);
    }

    @Override
    public List<V> readAll() {
        return get(readAllAsync());
    }

    @Override
    public RFuture<List<V>> readAllAsync() {
        return commandExecutor.evalReadAsync(getName(), codec, RedisCommands.EVAL_LIST,
                "local result = {}; " +
                        "local items = redis.call('lrange', KEYS[1], 0, -1); "
                        + "for i, v in ipairs(items) do "
                        + "local randomId, value = struct.unpack('dLc0', v); "
                        + "table.insert(result, value);"
                        + "end; "
                        + "return result; ",
                Collections.<Object>singletonList(queueName));
    }

    @Override
    public boolean remove(Object o) {
        return get(removeAsync(o));
    }

    @Override
    public RFuture<Boolean> removeAsync(Object o) {
        return removeAsync(o, 1);
    }

    protected RFuture<Boolean> removeAsync(Object o, int count) {
        return commandExecutor.evalWriteAsync(getName(), codec, RedisCommands.EVAL_BOOLEAN,
                "local s = redis.call('llen', KEYS[1]);" +
                        "for i = 0, s-1, 1 do "
                        + "local v = redis.call('lindex', KEYS[1], i);"
                        + "local randomId, value = struct.unpack('dLc0', v);"
                        + "if ARGV[1] == value then "
                        + "redis.call('zrem', KEYS[2], v);"
                        + "redis.call('lrem', KEYS[1], 1, v);"
                        + "return 1;"
                        + "end; "
                        + "end;" +
                        "return 0;",
                Arrays.<Object>asList(queueName, timeoutSetName), encode(o));
    }

    @Override
    public RFuture<Boolean> containsAllAsync(Collection<?> c) {
        if (c.isEmpty()) {
            return RedissonPromise.newSucceededFuture(true);
        }

        return commandExecutor.evalReadAsync(getName(), codec, RedisCommands.EVAL_BOOLEAN,
                "local s = redis.call('llen', KEYS[1]);" +
                        "for i = 0, s-1, 1 do "
                        + "local v = redis.call('lindex', KEYS[1], i);"
                        + "local randomId, value = struct.unpack('dLc0', v);"

                        + "for j = 1, #ARGV, 1 do "
                        + "if value == ARGV[j] then "
                        + "table.remove(ARGV, j) "
                        + "end; "
                        + "end; "
                        + "end;" +
                        "return #ARGV == 0 and 1 or 0;",
                Collections.<Object>singletonList(queueName), encode(c).toArray());
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return get(containsAllAsync(c));
    }

    @Override
    public boolean addAll(Collection<? extends V> c) {
        throw new UnsupportedOperationException("Use 'offer' method with timeout param");
    }

    @Override
    public RFuture<Boolean> removeAllAsync(Collection<?> c) {
        if (c.isEmpty()) {
            return RedissonPromise.newSucceededFuture(false);
        }

        return commandExecutor.evalWriteAsync(getName(), codec, RedisCommands.EVAL_BOOLEAN,
                "local result = 0;" +
                        "local s = redis.call('llen', KEYS[1]);" +
                        "local i = 0;" +
                        "while i < s do "
                        + "local v = redis.call('lindex', KEYS[1], i);"
                        + "local randomId, value = struct.unpack('dLc0', v);"

                        + "for j = 1, #ARGV, 1 do "
                        + "if value == ARGV[j] then "
                        + "result = 1; "
                        + "i = i - 1; "
                        + "s = s - 1; "
                        + "redis.call('zrem', KEYS[2], v);"
                        + "redis.call('lrem', KEYS[1], 0, v); "
                        + "break; "
                        + "end; "
                        + "end; "
                        + "i = i + 1;"
                        + "end; "
                        + "return result;",
                Arrays.<Object>asList(queueName, timeoutSetName), encode(c).toArray());
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return get(removeAllAsync(c));
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return get(retainAllAsync(c));
    }

    @Override
    public RFuture<Boolean> retainAllAsync(Collection<?> c) {
        if (c.isEmpty()) {
            return deleteAsync();
        }

        return commandExecutor.evalWriteAsync(getName(), codec, RedisCommands.EVAL_BOOLEAN,
                "local changed = 0; " +
                        "local items = redis.call('lrange', KEYS[1], 0, -1); "
                        + "local i = 1; "
                        + "while i <= #items do "
                        + "local randomId, element = struct.unpack('dLc0', items[i]); "
                        + "local isInAgrs = false; "
                        + "for j = 1, #ARGV, 1 do "
                        + "if ARGV[j] == element then "
                        + "isInAgrs = true; "
                        + "break; "
                        + "end; "
                        + "end; "
                        + "if isInAgrs == false then "
                        + "redis.call('LREM', KEYS[1], 0, items[i]) "
                        + "changed = 1; "
                        + "end; "
                        + "i = i + 1; "
                        + "end; "
                        + "return changed; ",
                Collections.<Object>singletonList(queueName), encode(c).toArray());
    }

    @Override
    public void clear() {
        delete();
    }

    @Override
    public RFuture<Boolean> deleteAsync() {
        return commandExecutor.writeAsync(getName(), RedisCommands.DEL_OBJECTS, queueName, timeoutSetName);
    }

    @Override
    public RFuture<Boolean> expireAsync(long timeToLive, TimeUnit timeUnit) {
        return commandExecutor.evalWriteAsync(getName(), LongCodec.INSTANCE, RedisCommands.EVAL_BOOLEAN,
                "redis.call('pexpire', KEYS[1], ARGV[1]); " +
                        "return redis.call('pexpire', KEYS[2], ARGV[1]); ",
                Arrays.<Object>asList(queueName, timeoutSetName),
                timeUnit.toMillis(timeToLive));
    }

    @Override
    public RFuture<Boolean> expireAtAsync(long timestamp) {
        return commandExecutor.evalWriteAsync(getName(), LongCodec.INSTANCE, RedisCommands.EVAL_BOOLEAN,
                "redis.call('pexpireat', KEYS[1], ARGV[1]); " +
                        "return redis.call('pexpireat', KEYS[2], ARGV[1]); ",
                Arrays.<Object>asList(queueName, timeoutSetName),
                timestamp);
    }

    @Override
    public RFuture<Boolean> clearExpireAsync() {
        return commandExecutor.evalWriteAsync(getName(), LongCodec.INSTANCE, RedisCommands.EVAL_BOOLEAN,
                "redis.call('persist', KEYS[1]); " +
                        "return redis.call('persist', KEYS[2]); ",
                Arrays.<Object>asList(queueName, timeoutSetName));
    }


    @Override
    public RFuture<V> peekAsync() {
        return commandExecutor.evalReadAsync(getName(), codec, RedisCommands.EVAL_OBJECT,
                "local v = redis.call('lindex', KEYS[1], 0); "
                        + "if v ~= false then "
                        + "local randomId, value = struct.unpack('dLc0', v);"
                        + "return value; "
                        + "end "
                        + "return nil;",
                Arrays.<Object>asList(queueName));
    }

    @Override
    public RFuture<V> pollAsync() {
        return commandExecutor.evalWriteAsync(getName(), codec, RedisCommands.EVAL_OBJECT,
                "local v = redis.call('lpop', KEYS[1]); "
                        + "if v ~= false then "
                        + "redis.call('zrem', KEYS[2], v); "
                        + "local randomId, value = struct.unpack('dLc0', v);"
                        + "return value; "
                        + "end "
                        + "return nil;",
                Arrays.<Object>asList(queueName, timeoutSetName));
    }

    @Override
    public RFuture<Boolean> offerAsync(V e) {
        throw new UnsupportedOperationException("Use 'offer' method with timeout param");
    }

    @Override
    public RFuture<V> pollLastAndOfferFirstToAsync(String queueName) {
        return commandExecutor.evalWriteAsync(getName(), codec, RedisCommands.EVAL_OBJECT,
                "local v = redis.call('rpop', KEYS[1]); "
                        + "if v ~= false then "
                        + "redis.call('zrem', KEYS[2], v); "
                        + "local randomId, value = struct.unpack('dLc0', v);"
                        + "redis.call('lpush', KEYS[3], value); "
                        + "return value; "
                        + "end "
                        + "return nil;",
                Arrays.<Object>asList(this.queueName, timeoutSetName, queueName));
    }

    @Override
    public RFuture<Boolean> containsAsync(Object o) {
        return commandExecutor.evalReadAsync(getName(), codec, RedisCommands.EVAL_BOOLEAN,
                "local s = redis.call('llen', KEYS[1]);" +
                        "for i = 0, s-1, 1 do "
                        + "local v = redis.call('lindex', KEYS[1], i);"
                        + "local randomId, value = struct.unpack('dLc0', v);"
                        + "if ARGV[1] == value then "
                        + "return 1;"
                        + "end; "
                        + "end;" +
                        "return 0;",
                Collections.<Object>singletonList(queueName), encode(o));
    }

    @Override
    public RFuture<Integer> sizeAsync() {
        return commandExecutor.readAsync(getName(), codec, RedisCommands.LLEN_INT, queueName);
    }

    @Override
    public RFuture<Boolean> addAsync(V e) {
        throw new UnsupportedOperationException("Use 'offer' method with timeout param");
    }

    @Override
    public RFuture<Boolean> addAllAsync(Collection<? extends V> c) {
        throw new UnsupportedOperationException("Use 'offer' method with timeout param");
    }

    @Override
    public V pollLastAndOfferFirstTo(String dequeName) {
        return get(pollLastAndOfferFirstToAsync(dequeName));
    }

    @Override
    public void destroy() {
        queueTransferService.remove(queueName);
    }

}
