package com.hanxiaozhang.redisson;

import org.redisson.Redisson;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈延迟队列的简单使用〉
 *
 * @author hanxinghua
 * @create 2022/9/15
 * @since 1.0.0
 */
public class DelayedQueue {

    public static void main(String[] args) {

        Config config = new Config();
        config.useSingleServer().setAddress("http://127.0.0.1:6379").setDatabase(0);
        RedissonClient client = Redisson.create(config);
        RBlockingQueue blockingQueue  = client.getBlockingQueue("queueName");
        RDelayedQueue delayedQueue = client.getDelayedQueue(blockingQueue);


        // 1.启动一个消费者消费
        new Thread(() -> {
            while (true) {
                try {
                    System.err.println("\n\n curTime=" + LocalDateTime.now() + " receive message : " + blockingQueue.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();


        // 2.发送延时消息
        for (int i = 0; i < 5; i++) {
            delayedQueue.offer("msg-" + i, 100, TimeUnit.SECONDS);
            System.err.println("\n\n curTime=" + LocalDateTime.now() + " send message : " + i);
        }

    }

}
