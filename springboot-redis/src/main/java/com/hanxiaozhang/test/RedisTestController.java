package com.hanxiaozhang.test;

import com.hanxiaozhang.dictcode.domain.DictDO;
import com.hanxiaozhang.utils.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/4/24
 * @since 1.0.0
 */
@RestController
@RequestMapping("/redisTest")
public class RedisTestController {

    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("/test1")
    public String test1(Integer num) {

        if (num == 1) {
            redisTemplate.opsForHash().put("test", "1", "1");
            Object value = redisTemplate.opsForHash().get("test", "1");
            System.out.println(value);
        }

        if (num == 2) {
            redisTemplate.opsForHash().put("test", "2", "你好");
            Object value = redisTemplate.opsForHash().get("test", "2");
            System.out.println(value);
        }
        // 复现问题
        if(num ==3){
            Long l1 = redisTemplate.opsForHash().increment("test", "3", 1);
            System.out.println(l1);
            Object l2 = redisTemplate.opsForHash().get("test", "3");
            System.out.println(l2);
        }

        if (num ==4){
            Long l1 = redisTemplate.opsForValue().increment("test1");
            System.out.println(l1);
            Object l2 = redisTemplate.opsForValue().get("test1");
            System.out.println(l2);
        }

        return "ok";
    }



}
