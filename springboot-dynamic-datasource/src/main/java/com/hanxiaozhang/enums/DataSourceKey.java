package com.hanxiaozhang.enums;

/**
 * 〈一句话功能简述〉<br>
 * 〈数据源名称枚举类〉
 *
 * @author hanxinghua
 * @create 2019/12/28
 * @since 1.0.0
 */
public enum DataSourceKey {

    /* 主数据库源 */
    DB_MASTER,
    /* 从数据库源1 */
    DB_SLAVE1,
    /* 从数据库源2 */
    DB_SLAVE2,
    /* 其他数据库源 */
    DB_OTHER

}

