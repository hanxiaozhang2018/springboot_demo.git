package com.hanxiaozhang.testcode.service;


import com.hanxiaozhang.testcode.domain.DictDO;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2019/12/28
 * @since 1.0.0
 */
public interface TestService {


    void distributedTrans(DictDO dictDO);


}
