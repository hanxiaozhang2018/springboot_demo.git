package com.hanxiaozhang.buffer;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/6/25
 * @since 1.0.0
 */
public class No1ByteBuffer {


    public static void main(String[] args) {


        /*
        slice()：创建新的字节缓冲区，其内容是老缓冲区内容的共享。新缓冲区的内容将从老缓冲区的当前位置开始。
                老缓冲区的内容的更改在新缓冲区中是可见的，但是这两个缓冲区的位置、限制、标记是相互独立的。
                新缓冲区的位置将为0，其容量和限制将为此缓冲区中剩余的字节数，其标记是不确定的。
                当且仅当此缓冲区为直接缓冲区时，新缓冲区才为直接缓冲区。
                当且仅当此缓冲区为只读缓冲区，新缓冲区才为只读缓冲区。
         */

        sliceNode();

    }

    private static void sliceNode() {
        byte[] array = {1, 2, 3, 4, 5, 6, 7, 8};
        ByteBuffer buffer = ByteBuffer.wrap(array);
        buffer.position(5);

        ByteBuffer slice = buffer.slice();
        System.out.println("buffer capacity = " + buffer.capacity() +
                " limit=" + buffer.limit() + " position=" + buffer.position());
        System.out.println("slice capacity = " + slice.capacity() +
                " limit=" + slice.limit() + " position=" + slice.position());

        System.out.println("buffer content is " + Arrays.toString(buffer.array()));
        System.out.println("slice content is " + Arrays.toString(slice.array()));

        buffer.put(5, (byte) 'a');

        System.out.println("buffer update content is " +Arrays.toString(buffer.array()));
        System.out.println("buffer update content is " +Arrays.toString(slice.array()));


        // 在使用slice()后，在调用arrayOffset方法会出现返回的值为非0的情况
        System.out.println("---- ---- ---- ----");
        byte[] array1 = {1,2,3,4,5,6,7,8};
        ByteBuffer buffer1 = ByteBuffer.wrap(array1);
        buffer1.position(5);
        ByteBuffer buffer2 = buffer1.slice();
        System.out.println(""+buffer2.arrayOffset());
    }


}
