package com.hanxiaozhang.utils.constant;

/**
 * 〈一句话功能简述〉<br>
 * 〈全局常量类〉
 *
 * @author hanxinghua
 * @create 2020/9/9
 * @since 1.0.0
 */
public class Constant {

    private Constant(){}

    /**
     * 停止计划任务
     */
    public static String STATUS_RUNNING_STOP = "stop";

    /**
     * 开启计划任务
     */
    public static String STATUS_RUNNING_START = "start";
}
