package com.hanxiaozhang.pipeline.config;

import com.hanxiaozhang.core.Pipe;
import com.hanxiaozhang.core.Pipeline;
import com.hanxiaozhang.core.pipe.IfPipe;
import com.hanxiaozhang.core.pipe.TryCatchFinallyPipe;
import com.hanxiaozhang.core.pipeline.PipelineImpl;
import com.hanxiaozhang.pipeline.pipe.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/8/4
 * @since 1.0.0
 */
@Configuration
public class PipelineConfig {

    @Autowired
    private OnePipe onePipe;

    @Autowired
    private TwoPipe twoPipe;

    @Autowired
    private MalePipe malePipe;

    @Autowired
    private FemalePipe femalePipe;

    @Autowired
    private SexCondition sexCondition;

    @Bean("testPipeline")
    public Pipeline testPipeline() {
        PipelineImpl pipeline = new PipelineImpl();
        List<Pipe> pipes = new ArrayList<>();
        pipes.add(onePipe);
        pipes.add(twoPipe);
        pipes.add(ifPipe());
        pipeline.setPipes(pipes);
        return pipeline;
    }


    @Bean
    public Pipe ifPipe() {
        IfPipe<Pipe, Pipe> ifPipe = new IfPipe<>();
        ifPipe.setIfPipe(malePipe);
        ifPipe.setElsePipe(femalePipe);
        ifPipe.setCondition(sexCondition);
        return ifPipe;
    }


    // @Bean
    public Pipe tryCatchFinallyPipe() {
        TryCatchFinallyPipe tryCatchFinallyPipe = new TryCatchFinallyPipe();
        return tryCatchFinallyPipe;
    }

}
