package com.hanxiaozhang.pipeline.pipe;

import com.hanxiaozhang.core.AbstractPipe;
import com.hanxiaozhang.core.PipelineContext;
import com.hanxiaozhang.core.PipelineExecuteResult;
import com.hanxiaozhang.core.support.PipelineExecuteResultSupport;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/8/4
 * @since 1.0.0
 */
@Slf4j
@Component
public class OnePipe extends AbstractPipe {

    @Override
    public PipelineExecuteResult execute(PipelineContext pipelineContext) {
        log.info("OnePipe");
        return PipelineExecuteResultSupport.createContinuePipelineExecuteResult();
    }

}
