package com.hanxiaozhang.pipeline.pipe;

import com.hanxiaozhang.core.Condition;
import com.hanxiaozhang.core.PipelineContext;
import com.hanxiaozhang.pipeline.util.PipelineContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/8/6
 * @since 1.0.0
 */
@Slf4j
@Component
public class SexCondition implements Condition {

    @Override
    public boolean isSatisfied(PipelineContext pipelineContext) {

        Object sex = pipelineContext.getAttribute(PipelineContextUtil.SEX_KEY);
        if (sex != null && sex instanceof Boolean) {
            return (Boolean) sex;
        }
        return false;
    }

}
