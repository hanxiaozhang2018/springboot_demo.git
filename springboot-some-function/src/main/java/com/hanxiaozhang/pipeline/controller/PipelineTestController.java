package com.hanxiaozhang.pipeline.controller;

import com.hanxiaozhang.core.Pipeline;
import com.hanxiaozhang.core.PipelineContext;
import com.hanxiaozhang.core.PipelineExceptionHandler;
import com.hanxiaozhang.core.support.PipelineContextSupport;
import com.hanxiaozhang.pipeline.util.PipelineContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/8/5
 * @since 1.0.0
 */
@Slf4j
@Controller
@RequestMapping("/pipe")
public class PipelineTestController {

    @Autowired
    @Qualifier("testPipeline")
    private Pipeline testPipeline;

    @GetMapping("/test")
    @ResponseBody
    public String test() {

        Map<String, Object> attributes = new HashMap<>(4);
        attributes.put(PipelineContextUtil.SEX_KEY, true);

        PipelineContext pipelineContext = PipelineContextSupport.createPipelineContext(testPipeline, attributes, new PipelineExceptionHandler() {
            @Override
            public void handlerException(PipelineContext pipelineContext, Throwable throwable) {
                log.error("this pipeline error is [{}]", throwable);
            }
        });

        testPipeline.execute(pipelineContext);
        return "ok";
    }

}
