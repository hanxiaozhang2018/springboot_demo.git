package com.hanxiaozhang.systemparam;

/**
 * 〈一句话功能简述〉<br>
 * 〈jvm参数〉
 * https://blog.csdn.net/weixin_37646636/article/details/120526673
 *
 * @author hanxinghua
 * @create 2023/6/15
 * @since 1.0.0
 */
public class JvmParam {


    public static void main(String[] args) {
        // 单位字节
        float freeMemory = (float) Runtime.getRuntime().freeMemory();
        float totalMemory = (float) Runtime.getRuntime().totalMemory();
        float usedMemory = (totalMemory - freeMemory);
        float memPercent = Math.round(freeMemory / totalMemory * 100);
        String os = System.getProperty("os.name");
        String javaVersion = System.getProperty("java.version");


        System.out.println("totalMemory: " + totalMemory / 1024 / 1024);
        System.out.println("usedMemory: " + usedMemory / 1024 / 1024);
        System.out.println("memPercent(%): " + memPercent);
        System.out.println("os: " + os);
        System.out.println("javaVersion: " + javaVersion);

    }

}
