package com.hanxiaozhang.delayoperation.timewheel;


/**
 * 功能描述: <br>
 * 〈定时器接口〉
 *
 * 参照Kafka来的
 *
 * @Author: hanxinghua
 * @Date: 2024/2/26
 */
public interface Timer {

    /**
     * 将新任务添加到此执行器
     * <p>
     * Add a new task to this executor. It will be executed after the task's delay
     * (beginning parseFrom the time of submission)
     *
     * @param timerTask the task to add
     */
    void add(TimerTask timerTask);

    /**
     * 时间轮表针的推进
     * <p>
     * Advance the internal clock, executing any tasks whose expiration has been
     * reached within the duration of the passed timeout.
     *
     * @param timeoutMs  延迟队列弹出元素的超时时间
     * @return whether or not any tasks were executed
     */
    Boolean advanceClock(Long timeoutMs);

    /**
     * 获取挂起执行的任务数，同时对到期的TimerTaskList中的任务进行处理。
     * <p>
     * Get the number of tasks pending execution
     *
     * @return the number of tasks
     */
    Integer size();

    /**
     * 关闭计时器服务，使挂起的任务不执行
     * <p>
     * Shutdown the timer service, leaving pending tasks unexecuted
     */
    void shutdown();
}
