package com.hanxiaozhang.delayoperation.timewheel.use;

import com.hanxiaozhang.delayoperation.timewheel.ShutdownThread;
import com.hanxiaozhang.delayoperation.timewheel.SystemTimer;
import com.hanxiaozhang.delayoperation.timewheel.Timer;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/2/26
 * @since 1.0.0
 */
public class DelayedTest {

    public static void main(String[] args) {

        long multiple = 1000 * 2L;


        // 创建定时器，即时间轮
        Timer timeoutTimer = new SystemTimer("DelayedTest-Timer", 1 * multiple, 20, null);

        // 添加延迟操作
        timeoutTimer.add(new DelayedOperation(2 * multiple, "1"));
        timeoutTimer.add(new DelayedOperation(12 * multiple, "1"));
        timeoutTimer.add(new DelayedOperation(25 * multiple, "1"));
        timeoutTimer.add(new DelayedOperation(23 * multiple, "1"));

        // 推动时间轮的指针转动
        new ExpiredOperationReaper(timeoutTimer, multiple).start();

        while (true) {

        }
    }


    /**
     * 推动时间轮线程
     */
    private static class ExpiredOperationReaper extends ShutdownThread {

        private Timer timeoutTimer;

        private Long multiple;

        ExpiredOperationReaper(Timer timeoutTimer, Long multiple) {
            super("ExpirationReaper", false);
            this.timeoutTimer = timeoutTimer;
            this.multiple = multiple;
        }

        @Override
        public void doWork() {
            timeoutTimer.advanceClock(2 * multiple);
        }
    }
}
