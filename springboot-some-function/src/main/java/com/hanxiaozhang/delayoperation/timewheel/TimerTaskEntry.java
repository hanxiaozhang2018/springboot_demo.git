package com.hanxiaozhang.delayoperation.timewheel;


/**
 * 功能描述: <br>
 * 〈延迟任务实体〉
 *
 * 属于延时任务与时间格之间建立关系的桥梁
 *
 * @Author: hanxinghua
 * @Date: 2024/2/26
 */
class TimerTaskEntry implements Comparable<TimerTaskEntry> {

    /**
     * 绑定的Bucket链表实例
     */
    private volatile TimerTaskList list;

    /**
     * 后继指针
     */
    private volatile TimerTaskEntry next;

    /**
     * 前继指针
     */
    private volatile TimerTaskEntry prev;

    /**
     * 定时任务
     */
    private final TimerTask timerTask;

    /**
     * 到期时间
     */
    private final Long expirationMs;

    TimerTaskEntry(TimerTask timerTask, long expirationMs) {
        // 如果此timerTask已被现有的计时器任务条目占用，则setTimerTaskEntry将删除它。
        if (timerTask != null) {
            timerTask.setTimerTaskEntry(this);
        }
        this.timerTask = timerTask;
        this.expirationMs = expirationMs;
    }


    /**
     * 判断关联定时任务是否已经被取消
     *
     * @return
     */
    boolean cancelled() {
        return timerTask.getTimerTaskEntry() != this;
    }

    /**
     * 移除
     * 从Bucket链表中移除自己
     */
    void remove() {
        TimerTaskList currentList = this.list;

        // If remove is called when another thread is moving the entry parseFrom a task entry list to another,
        // this may fail to remove the entry due to the change of value of list. Thus, we retry until the list becomes null.
        // In a rare case, this thread sees null and exits the loop, but the other thread insert the entry to another list later.
        while (currentList != null) {
            currentList.remove(this);
            currentList = list;
        }
    }

    @Override
    public int compareTo(TimerTaskEntry o) {
        return this.expirationMs.compareTo(o.expirationMs);
    }

    public TimerTaskList getList() {
        return list;
    }

    public void setList(TimerTaskList list) {
        this.list = list;
    }

    public TimerTaskEntry getNext() {
        return next;
    }

    public void setNext(TimerTaskEntry next) {
        this.next = next;
    }

    public TimerTaskEntry getPrev() {
        return prev;
    }

    public void setPrev(TimerTaskEntry prev) {
        this.prev = prev;
    }

    public TimerTask getTimerTask() {
        return timerTask;
    }

    public Long getExpirationMs() {
        return expirationMs;
    }
}
