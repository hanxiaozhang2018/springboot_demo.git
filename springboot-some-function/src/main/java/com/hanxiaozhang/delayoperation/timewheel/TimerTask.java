package com.hanxiaozhang.delayoperation.timewheel;


/**
 * 功能描述: <br>
 * 〈延迟任务〉
 *
 * @Author: hanxinghua
 * @Date: 2024/2/26
 */
public abstract class TimerTask implements Runnable {

    /**
     * 延迟时间
     * 时间戳（毫秒）
     */
    private final Long delayMs;

    /**
     * 延迟任务实体，属于延时任务与时间格之间建立关系的桥梁
     */
    private TimerTaskEntry timerTaskEntry;


    public TimerTask(Long delayMs) {
        this.delayMs = delayMs;
    }

    /**
     * 取消
     *
     * 原理：将关联的timerTaskEntry置空
     */
    public void cancel() {
        synchronized (this) {
            if (timerTaskEntry != null) {
                timerTaskEntry.remove();
            }
            timerTaskEntry = null;
        }
    }

    /**
     * 关联timerTaskEntry
     *
     * @param entry
     */
    void setTimerTaskEntry(TimerTaskEntry entry) {
        // 加锁
        synchronized (this) {
            // 删除历史持有的timerTaskEntry
            if (timerTaskEntry != null && timerTaskEntry != entry) {
                timerTaskEntry.remove();
            }

            timerTaskEntry = entry;
        }
    }

    /**
     * 获取关联的timerTaskEntry实例
     *
     * @return
     */
    TimerTaskEntry getTimerTaskEntry() {
        return timerTaskEntry;
    }

    Long getDelayMs() {
        return this.delayMs;
    }
}
