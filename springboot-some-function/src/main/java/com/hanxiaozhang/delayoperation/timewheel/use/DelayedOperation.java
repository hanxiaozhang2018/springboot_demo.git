package com.hanxiaozhang.delayoperation.timewheel.use;

import com.hanxiaozhang.delayoperation.timewheel.TimerTask;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * 〈一句话功能简述〉<br>
 * 〈延迟操作〉
 *
 * @author hanxinghua
 * @create 2024/2/26
 * @since 1.0.0
 */
@Slf4j
public class DelayedOperation extends TimerTask {

    private String operationName;

    private String setDate;

    public DelayedOperation(Long delayMs,String operationName) {
        super(delayMs);
        this.operationName = operationName;
        this.setDate = LocalDateTime.now().toString();
    }

    @Override
    public void run() {
        log.info(operationName+ " starter , setDate is "+setDate+" now is" +LocalDateTime.now());
    }
}
