package com.hanxiaozhang.delayoperation.timewheel;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 功能描述: <br>
 * 〈可手动结束的线程〉
 *
 * @Author: hanxinghua
 * @Date: 2024/2/29
 */
@Slf4j
public abstract class ShutdownThread extends Thread {

    private final Boolean isInterruptible;

    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    private final CountDownLatch shutdownLatch = new CountDownLatch(1);

    public ShutdownThread(String name) {
        this(name, true);
    }

    public ShutdownThread(String name, boolean isInterruptible) {
        super(name);
        this.isInterruptible = isInterruptible;
        this.setDaemon(false);
    }

    public void shutdown() {
        this.initiateShutdown();
        this.awaitShutdown();
    }

    private boolean initiateShutdown() {
        if (isRunning.compareAndSet(true, false)) {
            log.info("Shutting down");
            isRunning.set(false);
            if (isInterruptible) {
                interrupt();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * After calling initiateShutdown(), use this API to wait until the shutdown is complete
     */
    void awaitShutdown() {
        try {
            shutdownLatch.await();
            log.info("Shutdown completed");
        } catch (Throwable t) {
            log.error("Shutdown error", t);
        }
    }

    /**
     * This method is repeatedly invoked until the thread shuts down or this method throws an errors
     */
    protected abstract void doWork();


    @Override
    public void run() {
        log.info("Starting ");
        try {
            while (isRunning.get()) {
                doWork();
            }
        } catch (Throwable e) {
            if (isRunning.get()) {
                log.error("Error due to ", e);
            }
        }
        shutdownLatch.countDown();
        log.info("Stopped ");
    }
}