package com.hanxiaozhang.delayoperation.delayqueue.no1;

import lombok.Data;

import java.util.Date;

/**
 * 〈一句话功能简述〉<br>
 * 〈订单实体〉
 *
 * @author hanxinghua
 * @create 2022/9/11
 * @since 1.0.0
 */
@Data
public class OrderEntry {

    private Long id;


    private Date createDate;
}
