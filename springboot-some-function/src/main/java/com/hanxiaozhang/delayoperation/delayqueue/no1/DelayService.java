package com.hanxiaozhang.delayoperation.delayqueue.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈延迟队列服务接口〉
 *
 * @author hanxinghua
 * @create 2022/9/11
 * @since 1.0.0
 */
public interface DelayService<T> {

    /**
     * 添加延迟对象到延时队列
     *
     * @param itemDelayed 延迟对象
     * @return boolean
     */
    boolean addToDelayQueue(ItemDelayed<T> itemDelayed);

    /**
     * 添加延迟对象到延时队列
     *
     * @param data 数据对象
     * @return boolean
     */
    boolean addToDelayQueue(T data);

    /**
     * 移除指定的延迟对象从延时队列中
     *
     * @param data 数据对象
     */
    void removeToDelayQueue(T data);

}
