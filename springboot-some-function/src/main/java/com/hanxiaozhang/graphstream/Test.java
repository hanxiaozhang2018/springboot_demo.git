package com.hanxiaozhang.graphstream;

import org.graphstream.graph.Edge;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/10/28
 * @since 1.0.0
 */
public class Test {

    public static void main(String[] args) {


        Graph graph = new SingleGraph("流程图");

        Node startNode = graph.addNode("开始节点");
        startNode.addAttribute("ui.label", "开始");

        Node middleNode = graph.addNode("中间节点");
        middleNode.addAttribute("ui.label", "中间");

        Node endNode = graph.addNode("结束节点");
        endNode.addAttribute("ui.label", "结束");

        Edge edge = graph.addEdge("开始到中间", startNode, middleNode, true);
        edge.addAttribute("ui.label", "连接线");

        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");

        Viewer viewer = graph.display();
        viewer.disableAutoLayout();
        viewer.enableAutoLayout();
        viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.CLOSE_VIEWER);

    }
}
