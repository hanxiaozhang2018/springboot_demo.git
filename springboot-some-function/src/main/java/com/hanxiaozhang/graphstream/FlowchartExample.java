package com.hanxiaozhang.graphstream;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FlowchartExample extends JFrame {
    public FlowchartExample() {
        super("Flowchart Example");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // 流程图的起点和终点坐标
        int startX = 50;
        int startY = 50;
        int endX = 350;
        int endY = 250;

        // 流程图线条粗细
        Stroke thickStroke = new BasicStroke(3.0f);

        // 设置线条粗细
        g2d.setStroke(thickStroke);

        // 绘制流程图线条
        g2d.drawLine(startX, startY, 100, startY); // 水平线
        g2d.drawLine(100, startY, 100, 120);       // 垂直线
        g2d.drawLine(100, 120, 150, 120);          // 斜线（条件判断）
        g2d.drawLine(150, 120, 150, endY);         // 水平线
        g2d.drawLine(150, endY, endX, endY);       // 斜线（结束）

        // 绘制流程图图形
        g2d.fillOval(95, 95, 10, 10); // 圆形（开始）
        g2d.fillRect(145, endY - 20, 60, 20); // 矩形（结束）

        // 绘制流程图文字
        g2d.setFont(new Font("Arial", Font.BOLD, 14));
        g2d.drawString("Start", 60, 70);
        g2d.drawString("End", 200, endY - 5);
        g2d.drawString("Condition", 120, 90);
    }

    public static void main(String[] args) {
        FlowchartExample frame = new FlowchartExample();
        frame.setVisible(true);
    }
}