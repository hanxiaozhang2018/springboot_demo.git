package com.hanxiaozhang.common.validation;

/**
 * 〈一句话功能简述〉<br>
 * 〈更新校验器分组〉
 *  Tips：接口中不需要定义任何方法，仅是对不同的校验规则进行分组
 *
 * @author hanxinghua
 * @create 2020/7/10
 * @since 1.0.0
 */
public interface UpdateGroupValidation {
}
