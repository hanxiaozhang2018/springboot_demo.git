package com.hanxiaozhang.smallfunction.customannotation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/11/4
 * @since 1.0.0
 */
@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class Bean {

    public Bean(Long id,String name, Integer age, String address) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
    }

    @BeanToMap(mapKeyName = "T_NO")
    private Long id;

    @BeanToMap(mapKeyName = "T_NO", notNull = false)
    private Long no;

    @BeanToMap(mapKeyName = "T_NAME")
    private String name;

    @BeanToMap(mapKeyName = "T_AGE")
    private Integer age;

    @BeanToMap(mapKeyName = "T_ADDRESS", notNull = false)
    private String address;

    @BeanToMap(mapKeyName = "T_SEX")
    private Long sex=1L;


}
