package com.hanxiaozhang.smallfunction.customannotation;

import java.lang.annotation.*;


/**
 * 功能描述: <br>
 * 〈Bean与Map〉
 *
 * @Author: hanxinghua
 * @Date: 2022/11/4
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface BeanToMap {

    /**
     * map的key
     *
     * @return
     */
    String mapKeyName();

    /**
     * 不为空
     *
     * @return
     */
    boolean notNull() default true;

}
