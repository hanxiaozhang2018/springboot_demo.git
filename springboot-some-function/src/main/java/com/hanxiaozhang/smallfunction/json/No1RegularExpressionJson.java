package com.hanxiaozhang.smallfunction.json;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 〈一句话功能简述〉<br>
 * 〈正则表达式例子〉
 *
 * @author hanxinghua
 * @create 2023/6/7
 * @since 1.0.0
 */
public class No1RegularExpressionJson {


    public static void main(String[] args) {

        String src1 = "入参;[{\"PayType\":\"SUCCESS\",\"orderId\":20230607,\"orderStatus\":\"CANCEL\"}] ---- 结果:[{\"exception\":null,\"remarks\":null,\"resStatus\":\"PASS\",\"target\":{},\"valFailMsg\":{}}] ---- 校验:[{\"result\":false}]";

        System.out.println(regularJsonSimple(src1, "PayType"));
        System.out.println(regularJsonComplex(src1, "exception", 3));
        System.out.println(regularJsonArray(src1, "PayType"));
        System.out.println(regularJsonArray(src1, "exception"));

    }


    //  https://blog.csdn.net/diezhunkao7362/article/details/101894201

    
    
    
    /**
     * 简单JSON串匹配
     *
     * @param src        源字符串
     * @param firstsSign 首次出现标识
     * @return
     */
    private static String regularJsonSimple(String src, String firstsSign) {

        // \{ : 字符串转义
        // [^}] ： 匹配除了}以外的任意字符  -> Tips：反义的知识点
        // * ：重复零次或更多次  + ： 重复一次或更多次  -> Tips：重复的知识dian
        String target = "\\{\"" + firstsSign + "\":[^\\}]*\\}";
        Matcher m = Pattern.compile(target).matcher(src);
        if (m.find()) {
            return m.group();
        }
        return null;
    }


    /**
     * 复杂JSON串匹配
     *
     * @param src          源字符串
     * @param firstsSign   首次出现标识
     * @param lastBraceNum 最后出现}的次数
     * @return
     */
    private static String regularJsonComplex(String src, String firstsSign, Integer lastBraceNum) {
        // . : 匹配除换行符以外的任意字符 -> Tips：元字符
        String target = "(\\{\"" + firstsSign + "\":.*)";
        Matcher m = Pattern.compile(target).matcher(src);
        if (m.find()) {
            String newSrc = m.group();
            int count = 0;
            String target1 = "\\}";
            Matcher m1 = Pattern.compile(target1).matcher(newSrc);
            while (m1.find()) {
                if (++count == lastBraceNum) {
                    return newSrc.substring(0, m1.end());
                }
            }
        }
        return null;
    }


    /**
     * JSON数组匹配
     *
     * @param src        源字符串
     * @param firstsSign 首次出现标识
     * @return
     */
    private static String regularJsonArray(String src, String firstsSign) {

        // \{ : 字符串转义
        // [^}] ： 匹配除了}以外的任意字符  -> Tips：反义的知识点
        // * ：重复零次或更多次  + ： 重复一次或更多次  -> Tips：重复的知识dian
        String target = "\\[\\{\"" + firstsSign + "\":[^\\]]*\\]";
        Matcher m = Pattern.compile(target).matcher(src);
        if (m.find()) {
            return m.group();
        }
        return null;
    }

}
