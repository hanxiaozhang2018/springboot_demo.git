package com.hanxiaozhang.smallfunction.callback;

/**
 * 〈一句话功能简述〉<br>
 * 〈回调接口〉
 *
 * @author hanxinghua
 * @create 2024/5/13
 * @since 1.0.0
 */
public interface Callback {


    /**
     * 学生思考完毕告诉老师答案
     *
     * @param answer
     */
    void tellAnswer(String answer);

}
