package com.hanxiaozhang.smallfunction.callback;

/**
 * 〈一句话功能简述〉<br>
 * 〈回调测试类〉
 * <p>
 * 回调是一种思想、是一种机制。
 * 回调的核心就是回调方将本身（即this）传递给调用方，调用方就可以在调用完毕之后，告诉回调方它想要知道的信息。
 *
 * @author hanxinghua
 * @create 2024/5/13
 * @since 1.0.0
 */
public class Test {

    public static void main(String[] args) {

        Student student = new Ricky();
        Teacher teacher = new Teacher(student);
        teacher.askQuestion();
    }

}
