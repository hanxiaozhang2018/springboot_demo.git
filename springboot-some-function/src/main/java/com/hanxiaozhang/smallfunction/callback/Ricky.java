package com.hanxiaozhang.smallfunction.callback;

/**
 * 〈一句话功能简述〉<br>
 * 〈学生Ricky解决老师提出的问题〉
 *
 * @author hanxinghua
 * @create 2024/5/13
 * @since 1.0.0
 */
public class Ricky implements Student {

    @Override
    public void resolveQuestion(Callback callback) {

        System.out.println("学生开始思考");

        // 模拟解决问题
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {

        }
        // 回调，告诉老师作业写了多久
        System.out.println("学生思考结束，准备告诉老师答案");
        callback.tellAnswer("2");
    }
}
