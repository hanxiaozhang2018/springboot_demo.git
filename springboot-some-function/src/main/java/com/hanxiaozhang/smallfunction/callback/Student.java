package com.hanxiaozhang.smallfunction.callback;

/**
 * 〈一句话功能简述〉<br>
 * 〈学生接口〉
 *
 * @author hanxinghua
 * @create 2024/5/13
 * @since 1.0.0
 */
public interface Student {

    /**
     * 解决问题
     *
     * @param callback
     */
    void resolveQuestion(Callback callback);
}
