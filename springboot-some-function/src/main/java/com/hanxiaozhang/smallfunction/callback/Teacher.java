package com.hanxiaozhang.smallfunction.callback;

/**
 * 〈一句话功能简述〉<br>
 * 〈老师对象〉
 *
 * @author hanxinghua
 * @create 2024/5/13
 * @since 1.0.0
 */
public class Teacher implements Callback {

    private Student student;

    public Teacher(Student student) {
        this.student = student;
    }

    public void askQuestion() {
        System.out.println("老师提问：1 + 1 = ?");
        student.resolveQuestion(this);
    }

    @Override
    public void tellAnswer(String answer) {
        System.out.println("知道了，你的答案是" + answer);
    }

}
