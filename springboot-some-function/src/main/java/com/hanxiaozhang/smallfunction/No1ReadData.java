package com.hanxiaozhang.smallfunction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈读取数据〉
 * <p>
 * Window下编译和运行方法(多个jar包之间使用分号;)：
 * javac  -encoding UTF-8  -cp .;F:/test/mysql-connector-java-8.0.20.jar  ./No1ReadData.java
 * java  -Dfile.encoding=utf8  -cp .;F:/test/mysql-connector-java-8.0.20.jar No1ReadData
 *
 * Linux下编译和运行方法(多个jar包之间使用分号:)：
 * javac  -encoding UTF-8  -cp .:/data/test/mysql-connector-java-8.0.20.jar  ./No1ReadData.java
 * java  -Dfile.encoding=utf8  -cp .:/data/test/mysql-connector-java-8.0.20.jar No1ReadData
 *
 * @author hanxinghua
 * @create 2021/12/20
 * @since 1.0.0
 */
public class No1ReadData {


    private static final String FILE_PATH = "D:\\shiti.txt";

    private static final String DB_URL = "jdbc:mysql://localhost:3306/test?useSSL=false&useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&serverTimezone=GMT%2B8&allowPublicKeyRetrieval=true";

    private static final String DB_USERNAME = "root";

    public static final String DB_PASSWORD = "root";

    public static final String SQL = "SELECT * FROM test";


    public static void main(String[] args) {

        List<Map<String, String>> list1 = readDataByFile(FILE_PATH);
        System.out.println(list1.size());

//        List<Map<String, String>> list2 = readDateByMySQL();
//        System.out.println(list2.size());


    }


    /**
     * 通过文件读取数据
     *
     * @param filePath
     * @return
     */
    private static List<Map<String, String>> readDataByFile(String filePath) {
        List<Map<String, String>> list = new ArrayList<>();

        File file = new File(filePath);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String s = null;
            // 使用readLine方法，一次读一行
            while ((s = br.readLine()) != null) {
                String[] array = s.split("#");
                list.add(new HashMap<String, String>(4) {{
                    put("name", array[1]);
                    put("age", array[2]);
                }});
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return list;
    }


    /**
     * 通过MySQL读取数据
     *
     * @return
     */
    private static List<Map<String, String>> readDateByMySQL() {
        List<Map<String, String>> list = new ArrayList<>();
        Connection connection = null;
        try {
            // 注册驱动
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            // 创建连接
            connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            // 执行语句
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL);
            // 获取元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            HashMap<String, String> map = null;
            // 获取每一行数据
            while (resultSet.next()) {
                map = new HashMap<>(8);
                for (int i = 0; i < columnCount; i++) {
                    // 获取列名
                    String colName = metaData.getColumnName(i + 1);
                    // 获取列对应的值
                    String value = resultSet.getString(colName);
                    map.put(colName, value);
                }
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭连接
            try {
                connection.close();
            } catch (Exception ex) {
            }
        }
        return list;
    }


}
