package com.hanxiaozhang.smallfunction.judgecustomobject;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈判断自定义对象〉
 *
 * @author hanxinghua
 * @create 2023/7/20
 * @since 1.0.0
 */
public class No1JudgeCustomObject {


    public static void main(String... args) {

        System.out.println(isJavaOriginalObject(Integer.class));
        System.out.println(isJavaOriginalObject(Map.class));
        System.out.println(isJavaOriginalObject(No1JudgeCustomObject.class));
    }

    /**
     * 判断一个类是否为Java原生对象
     *
     * @param clz
     * @return
     */
    public static boolean isJavaOriginalObject(Class<?> clz) {
        return clz != null && clz.getClassLoader() == null;
    }




}
