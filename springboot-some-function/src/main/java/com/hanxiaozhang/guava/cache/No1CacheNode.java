package com.hanxiaozhang.guava.cache;

import com.google.common.cache.*;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/11/28
 * @since 1.0.0
 */
public class No1CacheNode {


    // CacheBuilder中主要方法：
    // 1. 设置最大存储：maximumSize(2)
    // 2. 设置过期时间：
    // + 指定对象被写入到缓存后多久过期：expireAfterWrite(2, TimeUnit.SECONDS)
    // + 指定对象多久没有被访问后过期：expireAfterAccess(2, TimeUnit.SECONDS)
    // 3. 弱引用使用：
    // + key的弱引用：weakKeys()
    // + value的弱引用：weakValues()
    // 4. 移除监听器：removalListener(new RemovalListener<String, String>() ...
    // 5. 统计信息：recordStats()  -> Cache的命中率、加载数据时间等信息进行统计。
    // 6. 并发度：concurrencyLevel(5)
    // 7. 还车加载器(CacheLoader)：在build()中，new  CacheLoader<String, String>() { ... public String load(String key)
    // 8. 设置定时刷新：refreshAfterWrite(10, TimeUnit.MINUTES) ->  这里的定时并不是真正意义上的定时。
    // Guava cache的刷新需要依靠用户请求线程，让该线程去进行load方法的调用，
    // 所以如果一直没有用户尝试获取该缓存值，则该缓存也并不会刷新。
    // 9. 设置异步刷新： refreshAfterWrite(10, TimeUnit.MINUTES)  +  在build()中，new CacheLoader<String, String>() {...
    // public ListenableFuture<Object> reload( ...  -> 当缓存的key很多时，
    // 高并发条件下大量线程同时，获取不同key对应的缓存，此时依然会造成大量线程阻塞，并且给数据库带来很大压力。
    // 这个问题的解决办法，将刷新缓存值的任务交给后台线程，所有的用户请求线程均返回旧的缓存值，这样不会有用户线程被阻塞。
    // 10.  ... ...


    // Cache使用方法：
    // 1. getIfPresent(Object key)    存在时，返回值；不存在时，返回空
    // 2. get(K key, Callable<? extends V> loader)   存在时，返回值；不存在时，创建值后返回
    // 3. getAllPresent(Iterable<? extends Object> keys) 返回查询的所有存在的值
    // 4. put(K key, V value)
    // 5. putAll(Map<? extends K, ? extends V> m)
    // 6. invalidate(@CompatibleWith("K") Object key) 清除个别
    // 7. invalidateAll(Iterable<? extends Object> keys)
    // 8. invalidateAll()
    // 9. size()
    // 10. CacheStats stats()
    // 11. ConcurrentMap<K, V> asMap()
    // 12. cleanUp()

    private static Cache<String, String> CACHE = CacheBuilder
            .newBuilder()
            // 缓存的个数
            .maximumSize(2)
            // 移除缓存时的监听事件
            .removalListener(new RemovalListener<String, String>() {
                @Override
                public void onRemoval(RemovalNotification<String, String> notification) {
                    System.err.println("remove key:" + notification.getKey());
                }
            })
            // 指定对象被写入到缓存后多久过期
            .expireAfterWrite(2, TimeUnit.SECONDS)
            .build();


    public static void main(String[] args) throws Exception {

        CACHE.put("a", "A");
        System.out.println("获取缓存中a的值: " + CACHE.getIfPresent("a"));
        // 睡眠3秒
        Thread.sleep(3000);
        System.out.println("获取缓存中a的值: " + CACHE.getIfPresent("a"));
    }


}
