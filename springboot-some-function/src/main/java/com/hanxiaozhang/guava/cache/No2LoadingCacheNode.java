package com.hanxiaozhang.guava.cache;

import com.google.common.cache.*;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/11/28
 * @since 1.0.0
 */
public class No2LoadingCacheNode {


    //  LoadingCache的特点：当从LoadingCache中读取一个指定key的记录时，如果该记录不存在，则LoadingCache可以自动执行加载数据到缓存的操作。

    // LoadingCache使用方法(LoadingCache继承Cache，Cache的方法均可以使用，下面说明特有的方法)：
    // get(K key)：获取值，必要时首先加载该值。
    // getUnchecked(K key) ：获取值，必要时首先加载该值。
    // getAll(Iterable<? extends K> keys)
    // refresh(K key)：可能异步加载的新值
    // asMap()

    // ---- ----
    // apply(K key)：该方法不是LoadingCache接口中的方法，而是Function接口中的方法。LoadingCache接口继承了Function接口，因此LoadingCache实现类需要实现apply(K key) 。
    //              apply(K key)方法的作用是，当LoadingCache中不存在指定的key时，调用CacheLoader的load(K key)方法来加载key对应的值，并将其存储到缓存中。
    //              如果CacheLoader抛出异常，则apply(K key)方法也会抛出异常。
    //  Tips:AbstractLoadingCache中实现该方法。


    // get()与getUnchecked() 的区别：
    // 当CacheLoader抛出受检异常时，get()会抛出ExecutionException异常，而getUnchecked()则会抛出UncheckedExecutionException异常。
    // 如果CacheLoader不会抛出受检异常，那么建议使用getUnchecked()方法。
    // getUnchecked()的好处是，它不会抛出受检异常，因此只能用于CacheLoader不会抛出受检异常的情况。这样可以避免在代码中处理受检异常的繁琐操作，使代码更加简洁易读


    private static LoadingCache<String, String> loadingCache = CacheBuilder
            .newBuilder()
            // 缓存的个数
            .maximumSize(2)
            // 移除缓存时的监听事件
            .removalListener(new RemovalListener<String, String>() {
                @Override
                public void onRemoval(RemovalNotification<String, String> notification) {
                    System.err.println("remove key:" + notification.getKey());
                }
            })
            // 指定对象被写入到缓存后多久过期
            .expireAfterWrite(2, TimeUnit.SECONDS)
            // 不存在时，创建值
            .build(new CacheLoader<String, String>() {
                @Override
                public String load(String key) {
                    return key.toUpperCase(Locale.ROOT);
                }
            });

    public static void main(String[] args) throws Exception {
        System.out.println("从缓存中获取a值：" + loadingCache.get("a"));
        System.out.println("从缓存中获取b值：" + loadingCache.get("b"));
        loadingCache.put("c", "C");
        System.out.println("从缓存中获取c值：" + loadingCache.get("c"));
    }

}
