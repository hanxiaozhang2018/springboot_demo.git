package com.hanxiaozhang.test;

import com.hanxiaozhang.common.domain.DictDO;
import com.hanxiaozhang.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈测试控制器〉
 *
 * @author hanxinghua
 * @create 2022/11/11
 * @since 1.0.0
 */
@Slf4j
@RequestMapping("/test")
@Controller
public class TestController {


    /*
    test1、test2 验证不同Content-type
    test1:application/x-www-form-urlencoded -> null会处理成"null"，空会处理成""
    test2:application/json -> null处理成null，空会处理成空
     */

    @PostMapping("/test1")
    @ResponseBody
    public String test1(@RequestParam HashMap<String, String> map) {
        if (!map.isEmpty()) {
            map.forEach((k, v) -> {
                log.info("k:{},v:{}", k, v);
            });
        }
        return "ok";
    }

    @PostMapping("/test2")
    @ResponseBody
    public String test2(@RequestBody HashMap<String, String> map) {
        if (!map.isEmpty()) {
            map.forEach((k, v) -> {
                log.info("k:{},v:{}", k, v);
            });
        }
        return "ok";
    }



    @PostMapping("/test3")
    @ResponseBody
    public String test3(DictDO dictDO) {

        dictDO.setFlag("3");

        log.info(JsonUtil.beanToJson(dictDO));
        log.info(dictDO.toString());

        return "ok";
    }



}
