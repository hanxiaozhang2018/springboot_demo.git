package com.hanxiaozhang.distributedlock;

import com.hanxiaozhang.DistributedLock;
import com.hanxiaozhang.DistributedLockFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/14
 * @since 1.0.0
 */
@Slf4j
@RestController
@RequestMapping("/lock")
public class  DistributedLockTestController {

    @Autowired
    private DistributedLockFactory distributedLockFactory;
    @Autowired
    private DistributedLockTestService distributedLockTestService;

    @GetMapping("/test")
    public String test() {
        DistributedLock lock = distributedLockFactory.getRedisLock("test");
        lock.lock();

        return "ok";
    }

    @GetMapping("/test2")
    public String test2() {

        OrderEntity order = new OrderEntity();
        order.setId(1L);
        distributedLockTestService.lockAnnotationTest1(order);
        return "ok";
    }

    @GetMapping("/test3")
    public String test3() {

        distributedLockTestService.lockAnnotationTest2(2L);
        return "ok";
    }


}
