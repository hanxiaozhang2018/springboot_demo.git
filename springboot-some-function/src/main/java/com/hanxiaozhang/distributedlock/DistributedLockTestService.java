package com.hanxiaozhang.distributedlock;

import com.hanxiaozhang.annotation.Lock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/7/20
 * @since 1.0.0
 */
@Slf4j
@Service
public class DistributedLockTestService {

    @Lock(keyPrefix = "test_lock", keyEl = "#order.getId()")
    public void lockAnnotationTest1(OrderEntity order) {
        log.info("测试一下1");
    }

    @Lock(keyPrefix = "test_lock", keyEl = "#id")
    public void lockAnnotationTest2(Long id) {
        log.info("测试一下2");
    }


}
