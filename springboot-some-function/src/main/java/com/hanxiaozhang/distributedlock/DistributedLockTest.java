package com.hanxiaozhang.distributedlock;

import com.hanxiaozhang.DistributedLock;
import com.hanxiaozhang.DistributedLockFactory;
import com.hanxiaozhang.DistributedLockFactoryBuilder;

/**
 * 〈一句话功能简述〉<br>
 * 〈分布式锁测试〉
 *
 * @author hanxinghua
 * @create 2022/9/4
 * @since 1.0.0
 */
public class DistributedLockTest {

    public static void main(String[] args) {

        DistributedLockFactory factory = new DistributedLockFactoryBuilder().build("hanxiaozhang.yml", "hanxiaozhang");
//        DistributedLock lock = factory.getRedisLock("lock");
//        lock.lock();
        DistributedLock lock = factory.getZkLock("lock");
        lock.lock();


        System.out.println();


    }
}
