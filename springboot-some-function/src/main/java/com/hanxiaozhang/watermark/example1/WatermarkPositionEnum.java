package com.hanxiaozhang.watermark.example1;

/**
 * 〈一句话功能简述〉<br>
 * 〈水印位置类型〉
 *
 * @author hanxinghua
 * @create 2024/7/13
 * @since 1.0.0
 */
public enum WatermarkPositionEnum {


    CENTER(1, "居中"),
    LEFT_SIDE(2, "左侧"),
    RIGHT_SIDE(3, "右侧"),
    DIRECTLY_ABOVE(4, "正上方"),
    DIRECTLY_BELOW(5, "正下方"),
    UPPER_LEFT(6, "左上角"),
    LOWER_LEFT(7, "左下角"),
    UPPER_RIGHT(8, "右上方"),
    LOWER_RIGHT(9, "右下方"),
    TILE(10, "平铺"),
    TILT_TILE(11, "倾斜平铺");


    private final Integer code;
    private final String msg;


    WatermarkPositionEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public Integer getCode() {
        return code;
    }
}
