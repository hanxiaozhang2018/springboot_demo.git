package com.hanxiaozhang.watermark.example1;

/**
 * 〈一句话功能简述〉<br>
 * 〈水印类型〉
 *
 * @author hanxinghua
 * @create 2024/7/13
 * @since 1.0.0
 */
public enum WatermarkTypeEnum {


    TEXT_WATERMARK(1, "文字水印"),
    IMAGE_WATERMARK(2, "图片水印");

    private Integer code;
    private String msg;

    WatermarkTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }
}
