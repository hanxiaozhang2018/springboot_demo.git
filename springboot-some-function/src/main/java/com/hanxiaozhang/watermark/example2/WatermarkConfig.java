package com.hanxiaozhang.watermark.example2;

import com.hanxiaozhang.watermark.example1.WatermarkPositionEnum;
import com.hanxiaozhang.watermark.example1.WatermarkTypeEnum;

import java.awt.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈水印配置信息〉
 *
 * @author hanxinghua
 * @create 2024/7/14
 * @since 1.0.0
 */
public class WatermarkConfig {

    /**
     * 白色
     */
    private final static Color DEFAULT_WHITE_COLOR = new Color(255, 255, 255, 128);

    /**
     * 字体
     */
    private final static Font DEFAULT_FONT = new Font("微软雅黑", Font.BOLD, 24);

    /**
     * 水印字体
     */
    private Font watermarkFont = DEFAULT_FONT;

    /**
     * 水印颜色
     */
    private Color watermarkColor = DEFAULT_WHITE_COLOR;

    /**
     * 水印类型
     */
    private WatermarkTypeEnum watermarkType;

    /**
     * 水印位置
     */
    private WatermarkPositionEnum watermarkPosition;

    /**
     * 水印倾斜平铺角度
     */
    private Double watermarkTiltTileAngle;

    /**
     * 水印平铺间隔倍数
     */
    private Integer watermarkTileSplitMultiple;

    /**
     * 水印宽度
     */
    private Integer watermarkWidth;

    /**
     * 水印高度
     */
    private Integer watermarkHeight;


    /**
     * 多行水印文本位置
     */
    private WatermarkTextPositionEnum watermarkTextPosition = WatermarkTextPositionEnum.CENTER;


    /**
     * 构建默认文本类型水印
     *
     * @param watermarkPosition
     * @return
     */
    public WatermarkConfig buildDefaultText(WatermarkPositionEnum watermarkPosition) {

        setWatermarkType(WatermarkTypeEnum.TEXT_WATERMARK)
                .setWatermarkTileSplitMultiple(2)
                .setWatermarkTiltTileAngle(0.2)
                .setWatermarkPosition(watermarkPosition);
        return this;
    }


    /**
     * 构建默认图片类型水印
     *
     * @param watermarkPosition
     * @return
     */
    public WatermarkConfig buildDefaultImage(WatermarkPositionEnum watermarkPosition) {

        setWatermarkType(WatermarkTypeEnum.IMAGE_WATERMARK);
        return this;
    }


    public Font getWatermarkFont() {
        return watermarkFont;
    }

    public WatermarkConfig setWatermarkFont(Font watermarkFont) {
        this.watermarkFont = watermarkFont;
        return this;
    }

    public Color getWatermarkColor() {
        return watermarkColor;
    }

    public WatermarkConfig setWatermarkColor(Color watermarkColor) {
        this.watermarkColor = watermarkColor;
        return this;
    }

    public WatermarkTypeEnum getWatermarkType() {
        return watermarkType;
    }

    public WatermarkConfig setWatermarkType(WatermarkTypeEnum watermarkType) {
        this.watermarkType = watermarkType;
        return this;
    }

    public WatermarkPositionEnum getWatermarkPosition() {
        return watermarkPosition;
    }

    public WatermarkConfig setWatermarkPosition(WatermarkPositionEnum watermarkPosition) {
        this.watermarkPosition = watermarkPosition;
        return this;
    }

    public Double getWatermarkTiltTileAngle() {
        return watermarkTiltTileAngle;
    }

    public WatermarkConfig setWatermarkTiltTileAngle(Double watermarkTiltTileAngle) {
        this.watermarkTiltTileAngle = watermarkTiltTileAngle;
        return this;
    }

    public Integer getWatermarkTileSplitMultiple() {
        return watermarkTileSplitMultiple;
    }

    public WatermarkConfig setWatermarkTileSplitMultiple(Integer watermarkTileSplitMultiple) {
        this.watermarkTileSplitMultiple = watermarkTileSplitMultiple;
        return this;
    }

    public Integer getWatermarkWidth() {
        return watermarkWidth;
    }

    public WatermarkConfig setWatermarkWidth(Integer watermarkWidth) {
        this.watermarkWidth = watermarkWidth;
        return this;
    }

    public Integer getWatermarkHeight() {
        return watermarkHeight;
    }

    public WatermarkConfig setWatermarkHeight(Integer watermarkHeight) {
        this.watermarkHeight = watermarkHeight;
        return this;
    }

    public WatermarkTextPositionEnum getWatermarkTextPosition() {
        return watermarkTextPosition;
    }

    public WatermarkConfig setWatermarkTextPosition(WatermarkTextPositionEnum watermarkTextPosition) {
        this.watermarkTextPosition = watermarkTextPosition;
        return this;
    }
}
