package com.hanxiaozhang.watermark.example2;

/**
 * 〈一句话功能简述〉<br>
 * 〈水印文本位置类型〉
 *
 * @author hanxinghua
 * @create 2024/7/14
 * @since 1.0.0
 */
public enum WatermarkTextPositionEnum {


    CENTER(1, "居中"),
    LEFT_SIDE(2, "左侧"),
    // RIGHT_SIDE(3, "右侧")
    ;


    private final Integer code;
    private final String msg;


    WatermarkTextPositionEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public Integer getCode() {
        return code;
    }
}
