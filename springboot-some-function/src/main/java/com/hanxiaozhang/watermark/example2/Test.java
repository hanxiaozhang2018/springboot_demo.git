package com.hanxiaozhang.watermark.example2;

import com.hanxiaozhang.watermark.example1.WatermarkPositionEnum;
import com.hanxiaozhang.watermark.example2.WatermarkConfig;
import com.hanxiaozhang.watermark.example2.WatermarkUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/7/14
 * @since 1.0.0
 */
public class Test {

    /**
     * 文字类型测试
     *
     * @param args
     */
    public static void main(String[] args) {

        // 要处理的图片路径：
        String localPath = "C:\\Users\\han\\Desktop\\水印\\水印测试1.png";

        // 定义存储的地址
        String tarImgPath = "C:\\Users\\han\\Desktop\\水印\\加水印图片\\";


        BufferedImage bufferedImage = WatermarkUtil.readLocalPicture(localPath);

        WatermarkConfig config = new WatermarkConfig()
                .buildDefaultText(WatermarkPositionEnum.TILT_TILE)
                .setWatermarkColor(Color.RED);

        // 创建一个DateTimeFormatter对象，指明日期和时间的格式
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");

        // 使用formatter格式化LocalDateTime对象
        String now = LocalDateTime.now().format(formatter);






        List<String> watermarks = new ArrayList<>();
        watermarks.add(now);
        watermarks.add("北京市通州区东关地区镇万达广场1单元2层120");

        BufferedImage test = WatermarkUtil.addWatermark(bufferedImage, watermarks, null, config);
        WatermarkUtil.writeImage(test, tarImgPath + "watermark—" + now + ".jpg");

        System.out.println("done");

    }


    /**
     * 图片水印测试
     *
     * @param args
     */
//    public static void main(String[] args) {
//
//        // 要处理的图片路径：
//        String localPath = "https://vrar-obs-production.obs.cn-north-4.myhuaweicloud.com/survey/watermark/image.jpg";
//
//        // 定义存储的地址
//        String tarImgPath = "/Users/wangjing/Desktop/watermark/image/";
//
//        // 图片水印内容
//        String textWatermark = "https://vrar-obs-production.obs.cn-north-4.myhuaweicloud" +
//                ".com/survey/watermark/watermark.png";
//
//
//        for (int i = 1; i < 12; i++) {
//            BufferedImage bufferedImage = readPicture(localPath);
//            BufferedImage test = addWatermark(bufferedImage, null, null, WatermarkTypeEnum.IMAGE_WATERMARK.getCode(), i,
//                    textWatermark);
//            writeImage(test, tarImgPath + "watermark_image_" + i + ".jpg");
//        }
//    }

}
