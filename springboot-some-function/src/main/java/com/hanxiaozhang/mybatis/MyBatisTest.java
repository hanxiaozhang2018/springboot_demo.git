package com.hanxiaozhang.mybatis;

import com.hanxiaozhang.common.dao.DictDao;
import com.hanxiaozhang.common.domain.DictDO;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

/**
 * 〈一句话功能简述〉<br>
 * 〈MyBatis测试〉
 *
 * @author hanxinghua
 * @create 2019/11/12
 * @since 1.0.0
 */
public class MyBatisTest {


    public static void main(String[] args){
        SqlSession session=null;
        try {
            String path = "config/mybatis-config.xml";
            //获取相关路径下的资源
            InputStream inputStream = Resources.getResourceAsStream(path);
            //创建SqlSessionFactory工厂
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
            //创建SqlSession
            session = sqlSessionFactory.openSession();
            //获取接口的实例，反射代理获取实例
            DictDao dictDao = session.getMapper(DictDao.class);
            //调用方法，原理：MapperProxy.invoke()方法，反射代理获取相关方法
            DictDO dict = dictDao.get(1L);
            System.out.println(dict);
            DictDO dict1 = dictDao.get(1L);
            System.out.println(dict1);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
