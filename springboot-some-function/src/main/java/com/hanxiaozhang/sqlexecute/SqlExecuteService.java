package com.hanxiaozhang.sqlexecute;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/6/14
 * @since 1.0.0
 */
public interface SqlExecuteService {

    String execute(String sql);

}
