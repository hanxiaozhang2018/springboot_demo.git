package com.hanxiaozhang.sqlexecute;

import com.hanxiaozhang.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/6/14
 * @since 1.0.0
 */
@Controller
@RequestMapping("/sql")
public class SqlExecuteController {

    @Autowired
    private SqlExecuteService sqlExecuteService;

    @PostMapping("/execute")
    @ResponseBody
    public R execute(String sql) {
        String result = null;
        try {
            result = sqlExecuteService.execute(sql);
        } catch (Exception e) {
            return R.error(e.getMessage());
        }
        return R.ok(result);
    }
}
