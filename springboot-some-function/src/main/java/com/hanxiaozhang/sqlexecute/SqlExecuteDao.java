package com.hanxiaozhang.sqlexecute;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/6/14
 * @since 1.0.0
 */
@Mapper
public interface SqlExecuteDao {


    List<String> getTables();

    @MapKey("sql")
    List<Map<String, Object>> select(@Param("sql") String sql);

    int update(@Param("sql") String sql);

    int delete(@Param("sql") String sql);

    int insert(@Param("sql") String sql);

}
