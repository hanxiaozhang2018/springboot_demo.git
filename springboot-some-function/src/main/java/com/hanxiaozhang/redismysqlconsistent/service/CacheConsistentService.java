package com.hanxiaozhang.redismysqlconsistent.service;

import com.hanxiaozhang.common.domain.DictDO;

/**
 * 〈一句话功能简述〉<br>
 * 〈缓存一致性〉
 *
 * @author hanxinghua
 * @create 2022/10/12
 * @since 1.0.0
 */
public interface CacheConsistentService {

    /**
     * 查询
     * (先查缓存，没有查库)
     *
     * @param id
     * @return
     */
    DictDO query(Long id);

    /**
     * 更新
     * (先淘汰缓存，再更新数据库，最后再延迟淘汰缓存，即延迟双删)
     *
     * @param dict
     */
    void update(DictDO dict);

}
