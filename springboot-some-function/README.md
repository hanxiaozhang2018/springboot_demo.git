### 功能介绍：
+ common 通用功能
  + dict 数据字典功能
  + file 文件上传下载功能
  + task 定时任务功能（基于quartz）
+ config 常用配置信息配置方法
+ delayoperation 延迟操作
  + delay-queue 延迟队列
  + time-wheel 时间轮
+ design-pattern 设计模式组合应用
  + strategy 策略模式
+ distributed-lock 分布式锁
+ expiring-map 过期时间Map
+ guava Guava【锅哇】是一组来自Google的核心Java库
 + 主要功能包括：新的集合类型（如 multimap 和 multiset）、不可变集合、
   图形库以及用于并发、I/O、散列、缓存、原语、字符串等的实用程序。
+ idempotence 幂等性

+ id-generator id生成器
+ mybatis 
+ pipeline 责任链模式应用
+ plan-remind 定时提醒
+ redis-mysql-consistent Redis缓存与MySQL数据一致性问题
+ sql-execute 使用接口执行SQL
+ system-param 系统参数
+ test 测试类

###  validation校验框架中：@NotNull、@NotEmpty、@NotBlank的区别？
1) @NotNull:    
任何对象的value不能为null
2) @NotEmpty:     
集合对象的元素不为0，即集合不为空，也可以用于字符串不为null
3) @NotBlank:   
只能用于字符串不为null，并且字符串trim()以后length要大于0