package com.hanxiaozhang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootNetty4Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootNetty4Application.class, args);
    }

}
