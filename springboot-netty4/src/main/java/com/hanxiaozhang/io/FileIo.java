package com.hanxiaozhang.io;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDate;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/6/14
 * @since 1.0.0
 */
public class FileIo {

    public static void main(String[] args) throws Exception {
        System.out.println(LocalDate.of(2022, 7, 4).getDayOfYear());
        File file = new File("");
        FileInputStream fileInputStream = new FileInputStream(file);

        int read = fileInputStream.read();
        System.out.println((char) read);
    }

}
