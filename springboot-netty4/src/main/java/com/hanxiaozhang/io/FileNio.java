package com.hanxiaozhang.io;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 〈一句话功能简述〉<br>
 * 〈文件读写的Nio〉
 *
 * @author hanxinghua
 * @create 2022/6/15
 * @since 1.0.0
 */
public class FileNio {

    public static void main(String[] args) throws Exception {

        File file = new File("");
        FileInputStream fileInputStream = new FileInputStream(file);
        // 构建通道
        FileChannel channel = fileInputStream.getChannel();
        // 申请缓冲区
        ByteBuffer buffer = ByteBuffer.allocate(1);
        // 开始读
        int read = channel.read(buffer);
        // 缓冲区读写切换
        buffer.flip();
        System.out.println((char) buffer.get());

    }

}
