package com.hanxiaozhang.beanlifecycle;

import lombok.Data;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2020/3/16
 * @since 1.0.0
 */
@Data
public class Bean {

    private  String name="小张";
}
