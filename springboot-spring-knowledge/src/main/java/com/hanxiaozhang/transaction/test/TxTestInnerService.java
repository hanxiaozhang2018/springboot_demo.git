package com.hanxiaozhang.transaction.test;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/1/26
 * @since 1.0.0
 */
public interface TxTestInnerService {


    void test();

    // ==== ====  ==== ==== ====

    void required1();

    void requiredNew1();

    void support1();

    void notSupport1();

    void mandatory1();

    void never1();

    void nested1();

}
