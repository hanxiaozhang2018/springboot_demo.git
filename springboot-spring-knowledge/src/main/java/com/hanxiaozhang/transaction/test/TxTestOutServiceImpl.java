package com.hanxiaozhang.transaction.test;

import com.hanxiaozhang.common.DictTwoDO;
import com.hanxiaozhang.common.DictTwoDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/1/26
 * @since 1.0.0
 */
@Slf4j
@Service
public class TxTestOutServiceImpl implements TxTestOutService {

    @Autowired
    public TxTestInnerService txTestInnerService;

    @Resource
    private DictTwoDao dictTwoDao;

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void required1() {

        dictTwoDao.save(setDict("out is required"));
        txTestInnerService.required1();
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void requiredNew1() {

        dictTwoDao.save(setDict("out is requiredNew"));
        txTestInnerService.nested1();
        throw new RuntimeException();
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.NESTED)
    public void nested1() {

        dictTwoDao.save(setDict("out is nested"));
        txTestInnerService.nested1();
        throw new RuntimeException();
    }


    private DictTwoDO setDict(String name) {
        return DictTwoDO.builder()
                .name(name)
                .createDate(new Date())
                .build();
    }


}
