package com.hanxiaozhang.transaction.onlineissue;

import com.hanxiaozhang.common.DictTwoDO;
import com.hanxiaozhang.common.DictTwoDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/10/25
 * @since 1.0.0
 */
@Slf4j
@Service
public class TxOnlineService1 {

    @Resource
    private DictTwoDao dictTwoDao;

    @Autowired
    private TxOnlineService2 txOnlineService2;

    @Transactional(rollbackFor = Exception.class)
    public void method() {

        DictTwoDO twoDO = DictTwoDO.builder().id(2023L).name("哈哈").build();
        dictTwoDao.save(twoDO);

        try {
            txOnlineService2.method();
        } catch (Exception e) {
            log.info("捕获异常");
        }

        log.info("继续往下执行");
    }


}
