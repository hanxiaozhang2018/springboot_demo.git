package com.hanxiaozhang.transaction.onlineissue;

import com.hanxiaozhang.common.DictTwoDO;
import com.hanxiaozhang.common.DictTwoDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/10/25
 * @since 1.0.0
 */
@Slf4j
@Service
public class TxOnlineService2 {


    @Transactional(rollbackFor = Exception.class)
    public void method() {
        throw new NullPointerException("空指针");
    }


}
