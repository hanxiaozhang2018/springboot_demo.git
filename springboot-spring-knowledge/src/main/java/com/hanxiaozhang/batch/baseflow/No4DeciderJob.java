package com.hanxiaozhang.batch.baseflow;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

/**
 * 〈一句话功能简述〉<br>
 * 〈自定义任务决策器执行〉
 *
 * @author hanxinghua
 * @create 2023/1/17
 * @since 1.0.0
 */
//@Component
//@EnableBatchProcessing
public class No4DeciderJob {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private No4MyDecider myDecider;

    @Bean
    public Job deciderJob() {
        // 首先执行step1，然后指定自定义决策器，如果决策器返回weekend，那么执行step2，
        // 如果决策器返回workingDay，那么执行step3。
        // 如果执行step3，那么无论step3的结果是什么，都将执行step4。
        return jobBuilderFactory.get("deciderJob")
                .start(step1())
                .next(myDecider)
                .from(myDecider).on("weekend").to(step2())
                .from(myDecider).on("workingDay").to(step3())
                .from(step3()).on("*").to(step4())
                .end()
                .build();
    }

    private Step step1() {
        return stepBuilderFactory.get("step1")
                .tasklet((stepContribution, chunkContext) -> {
                    System.out.println("DeciderJob，执行步骤一操作...");
                    return RepeatStatus.FINISHED;
                }).build();
    }

    private Step step2() {
        return stepBuilderFactory.get("step2")
                .tasklet((stepContribution, chunkContext) -> {
                    System.out.println("DeciderJob，执行步骤二操作...");
                    return RepeatStatus.FINISHED;
                }).build();
    }

    private Step step3() {
        return stepBuilderFactory.get("step3")
                .tasklet((stepContribution, chunkContext) -> {
                    System.out.println("DeciderJob，执行步骤三操作...");
                    return RepeatStatus.FINISHED;
                }).build();
    }


    private Step step4() {
        return stepBuilderFactory.get("step4")
                .tasklet((stepContribution, chunkContext) -> {
                    System.out.println("DeciderJob，执行步骤四操作...");
                    return RepeatStatus.FINISHED;
                }).build();
    }
}
