package com.hanxiaozhang.batch.baseflow;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈一个任务执行〉
 *
 * @author hanxinghua
 * @create 2023/1/16
 * @since 1.0.0
 */
@Component
@EnableBatchProcessing
public class No1FirstJob {


    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job firstJob() {
        // get()：用于创建一个指定名称的任务
        Job job = jobBuilderFactory.get("firstJob")
                .incrementer(new RunIdIncrementer())
                // start()：指定任务的开始步骤
                .start(step())
                .build();

        return job;
    }

    private Step step() {
        return stepBuilderFactory.get("step")
                .tasklet((contribution, chunkContext) -> {
                    System.out.println("FirstJob，执行步骤...");
                    return RepeatStatus.FINISHED;
                }).build();
    }
}