package com.hanxiaozhang.batch.baseflow;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.JobStepBuilder;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;


/**
 * 〈一句话功能简述〉<br>
 * 〈任务嵌套执行〉
 *
 * @author hanxinghua
 * @create 2023/1/17
 * @since 1.0.0
 */
//@Component
//@EnableBatchProcessing
public class No5NestedJob {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private JobLauncher jobLauncher;
    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private PlatformTransactionManager platformTransactionManager;

    /**
     * 父任务
     *
     * @return
     */
    @Bean
    public Job parentJob() {
        return jobBuilderFactory.get("parentJob")
                .start(parentStep1())
                .next(parentStep2())
                .build();
    }


    /**
     * 父任务的步骤1
     *
     * @return
     */
    private Step parentStep1() {
        return new JobStepBuilder(new StepBuilder("parentStep1"))
                .job(childJob1())
                .launcher(jobLauncher)
                .repository(jobRepository)
                .transactionManager(platformTransactionManager)
                .build();
    }

    /**
     * 父任务的步骤2
     *
     * @return
     */
    private Step parentStep2() {
        return new JobStepBuilder(new StepBuilder("parentStep2"))
                .job(childJob2())
                .launcher(jobLauncher)
                .repository(jobRepository)
                .transactionManager(platformTransactionManager)
                .build();
    }

    /**
     * 子任务1
     *
     * @return
     */
    private Job childJob1() {
        return jobBuilderFactory.get("childJob1")
                .start(step1())
                .next(step2())
                .build();
    }


    private Step step1() {
        return stepBuilderFactory.get("childJob1-step1")
                .tasklet((stepContribution, chunkContext) -> {
                    System.out.println("childJob1，执行步骤一操作...");
                    return RepeatStatus.FINISHED;
                }).build();
    }

    private Step step2() {
        return stepBuilderFactory.get("childJob1-step2")
                .tasklet((stepContribution, chunkContext) -> {
                    System.out.println("childJob1，执行步骤二操作...");
                    return RepeatStatus.FINISHED;
                }).build();
    }


    /**
     * 子任务2
     *
     * @return
     */
    private Job childJob2() {
        return jobBuilderFactory.get("childJob2")
                .start(
                        stepBuilderFactory.get("childJob2-step2")
                                .tasklet((stepContribution, chunkContext) -> {
                                    System.out.println("childJob2，子任务一执行步骤。。。");
                                    return RepeatStatus.FINISHED;
                                }).build()
                ).build();
    }
}
