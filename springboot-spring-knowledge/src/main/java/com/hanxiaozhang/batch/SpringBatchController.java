package com.hanxiaozhang.batch;

import com.hanxiaozhang.batch.baseflow.No1FirstJob;
import lombok.SneakyThrows;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/1/17
 * @since 1.0.0
 */
@RestController
@RequestMapping("/batch")
public class SpringBatchController {

     /*
    概念：
    1. Spring Batch里最基本的单元就是任务Job，一个Job由若干个步骤Step组成。
    2. Job处理任务又可以分为三大类：
        数据读取 Item Reader
        数据中间处理 Item Processor
        数据输出 Item Writer。
    3. 任务存储仓库可以是关系型数据库MySQL，非关系型数据库MongoDB 或者直接存储在内存中。

    重要组件描述：
    Job：封装整个批处理过程。
    Step：封装了批处理作业的一个独立的阶段。
    Job Repository（任务存储仓库）：存储着Job的执行状态，参数和日志等信息。
    Job Launcher（任务启动器）：负责运行Job。
    JobInstance、JobParameters、JobExecution、StepExecution、ExecutionContext
    见  https://blog.csdn.net/topdeveloperr/article/details/84337956

    Springboot下的相关配置：
    # 启动时要执行的job，默认执行全部job
    spring.batch.job.name=job1,job2
    # 是否自动执行定义的job，默认是
    spring.batch.job.enabled=true
    # 是否初始化Spring Batch的数据库，默认 是
    spring.batch.initializer.enabled=true
    spring.batch.schema=
    # 设置Spring Batch的数据库表的前缀
    spring.batch.table-prefix=

    如何默认不启动job？
    在application.properties中添加如下属性：spring.batch.job.enabled=false

     */

    @Autowired
    @Qualifier("firstJob")
    private Job firstJob;

    @Autowired
    private JobLauncher jobLauncher;


    @SneakyThrows
    @GetMapping("/test")
    public String test() {

        JobParameters jobParameters =  new JobParametersBuilder()
                .addDate("date", new Date())
                .toJobParameters();
        jobLauncher.run(firstJob, jobParameters);
        return "ok";
    }

}
