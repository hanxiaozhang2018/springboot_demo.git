package com.hanxiaozhang.circulardependence.example2;

/**
 * 〈一句话功能简述〉<br>
 * 〈模拟循环依赖，导致的栈溢出〉
 * java.lang.StackOverflowError
 * <p>
 * 设置栈空间的大小：-Xss 1M
 *
 * @author hanxinghua
 * @create 2023/2/10
 * @since 1.0.0
 */
public class AService {

    private BService bService = new BService();

    public static void main(String[] args) {
        new AService();
    }
}
