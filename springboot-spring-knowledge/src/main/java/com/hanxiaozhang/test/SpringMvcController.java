package com.hanxiaozhang.test;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈SpringMvc测试〉
 *
 * @author hanxinghua
 * @create 2023/4/3
 * @since 1.0.0
 */
@Slf4j
@Controller
public class SpringMvcController {

    @RequestMapping("/test")
    public void test(HttpServletRequest request, HttpServletResponse response) {

        Map<String, String[]> requestMap = request.getParameterMap();

        try {
            log.info("post body is [{}]", getBody(request));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (!requestMap.isEmpty()) {
            requestMap.forEach((k, v) -> {
                log.info("get params,k is [{}],v is [{}]", k, Arrays.toString(v));
            });
        }

    }


    String getBody(HttpServletRequest request) throws IOException {
        BufferedReader br = request.getReader();
        String str, wholeStr = "";
        while ((str = br.readLine()) != null) {
            wholeStr += str;
        }
        return wholeStr;
    }


    /**
     * 测试401 跳转登录问题
     *
     * @param num
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @GetMapping("/test1")
    public ResponseEntity test1(Integer num, HttpServletRequest request, HttpServletResponse response) throws IOException {

        if (num == 1 && StringUtils.isBlank(request.getHeader(HttpHeaders.AUTHORIZATION))) {
            response.setHeader(HttpHeaders.WWW_AUTHENTICATE, "Basic realm='.'");
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        log.info(request.getHeader(HttpHeaders.AUTHORIZATION));

        return new ResponseEntity(HttpStatus.OK);
    }

}
