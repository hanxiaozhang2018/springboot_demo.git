package com.hanxiaozhang.retry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/29
 * @since 1.0.0
 */
@Controller
@RequestMapping("/retry")
public class SpringRetryController {

    @Autowired
    private SpringRetryService retryService;


    @GetMapping("/test1")
    @ResponseBody
    public String test1() {
        retryService.retry1();
        return "ok";
    }

    @GetMapping("/test2")
    @ResponseBody
    public String test2() {
        retryService.retry2("retry2");
        return "ok";
    }
}
