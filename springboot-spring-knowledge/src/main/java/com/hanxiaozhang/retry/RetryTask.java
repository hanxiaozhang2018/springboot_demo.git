package com.hanxiaozhang.retry;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.remoting.RemoteAccessException;

/**
 * 〈一句话功能简述〉<br>
 * 〈重试任务〉
 *
 * @author hanxinghua
 * @create 2022/12/29
 * @since 1.0.0
 */
@Slf4j
public class RetryTask {

    /**
     * 具体任务
     *
     * @param param
     * @return
     */
    public static Boolean task(String param) {

        int num = RandomUtils.nextInt(0, 9);
        log.info("请求参数:[{}],生成随机数:[{}]", param, num);
        if (num == 0) {
            log.info("模拟抛出参数异常");
            throw new IllegalArgumentException("参数异常");
        } else if (num == 1) {
            log.info("模拟执行成功");
            return true;
        } else if (num == 2) {
            log.info("模拟执行失败");
            return false;
        } else {
            log.info("模拟抛出特定异常");
            throw new RemoteAccessException("抛出远程访问异常");
        }
    }

}
