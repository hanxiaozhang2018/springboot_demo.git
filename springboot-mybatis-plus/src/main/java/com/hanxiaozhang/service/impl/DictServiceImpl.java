package com.hanxiaozhang.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hanxiaozhang.dao.DictMapper;
import com.hanxiaozhang.entity.DictEntity;
import com.hanxiaozhang.service.DictService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/8/18
 * @since 1.0.0
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, DictEntity> implements DictService {

    @Resource
    private DictMapper dictMapper;



}
