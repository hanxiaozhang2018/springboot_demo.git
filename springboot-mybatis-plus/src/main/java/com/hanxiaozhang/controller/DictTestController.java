package com.hanxiaozhang.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hanxiaozhang.dao.DictMapper;
import com.hanxiaozhang.entity.DictEntity;
import com.hanxiaozhang.service.DictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/8/18
 * @since 1.0.0
 */
@Slf4j
@RestController
public class DictTestController {

    @Autowired
    private DictService dictService;

    @Resource
    private DictMapper dictMapper;

    @GetMapping("/test")
    public String test(@RequestParam( value = "id" ,required = false) Long id) {

        List<Long> ids =new ArrayList<>();
        ids.add(1L);
        ids.add(10L);
        ids.add(12L);
        List<DictEntity> entities = dictMapper.multiSqlTest1(ids);

        System.out.println(entities.size());

        List<DictEntity> entities2 = dictMapper.multiSqlTest2(ids);

        System.out.println(entities.size());


        Page<DictEntity> page1 = new Page<>(1, 5);
        IPage<DictEntity> dictEntityIPage1 = dictMapper.selectPage(page1, 0);



        // 关闭分页
        Page<DictEntity> page2 = new Page<>(-1, -1);
        IPage<DictEntity> dictEntityIPage2 = dictMapper.selectPage(page2, 0);


        // 只查总数，不查数据
        Page<DictEntity> page3 = new Page<>();
        page3.setSize(0);
        page3.setSearchCount(true);
        IPage<DictEntity> dictEntityIPage3 = dictMapper.selectPage(page3, 0);


        return "OK";
    }




    @PostMapping("/test2")
    public DictEntity test2(@RequestBody DictEntity dictEntity){

        return dictMapper.selectById(dictEntity.getId());
    }
}
