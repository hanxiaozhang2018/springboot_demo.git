package com.hanxiaozhang.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hanxiaozhang.entity.DictEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/8/18
 * @since 1.0.0
 */
@Mapper
public interface DictMapper extends BaseMapper<DictEntity> {

    @Select("SELECT * FROM sys_dict WHERE del_flag = #{delFlag}")
    IPage<DictEntity> selectPage(Page<?> page, @Param("delFlag") Integer delFlag);


    List<DictEntity> multiSqlTest1(@Param("list") List<Long> ids);

    List<DictEntity> multiSqlTest2(@Param("list") List<Long> ids);
}
