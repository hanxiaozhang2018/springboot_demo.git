package com.hanxiaozhang.mp.vo;

import lombok.Data;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2025/1/20
 * @since 1.0.0
 */
@Data
public class WxJsApiConfigVO {

    private String url;
}
