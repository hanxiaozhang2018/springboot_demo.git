package com.hanxiaozhang.mp.controller;

import com.hanxiaozhang.mp.vo.WxJsApiConfigVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.web.bind.annotation.*;

/**
 * 功能描述: <br>
 * 〈config 接口〉
 *
 * @Author:hanxinghua
 * @Date: 2025/1/18
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/wx/jsapi/{appid}")
public class WxJsapiController {
    private final WxMpService wxService;

    @GetMapping("/getJsapiTicket")
    public String getJsapiTicket(@PathVariable String appid) throws WxErrorException {
        final WxJsapiSignature jsapiSignature = this.wxService.switchoverTo(appid).createJsapiSignature("http://page.free.hanxiaozhang.xyz");
        System.out.println(jsapiSignature);
        return this.wxService.getJsapiTicket(true);
    }

    @PostMapping("config")
    public WxJsapiSignature config(@PathVariable String appid, @RequestBody WxJsApiConfigVO configVO) throws WxErrorException {
        final WxJsapiSignature jsapiSignature = this.wxService.switchoverTo(appid).createJsapiSignature(configVO.getUrl());
        log.info("WxJsapiSignature is {}", jsapiSignature);
        return jsapiSignature;
    }
}
