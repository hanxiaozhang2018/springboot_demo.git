package com.hanxiaozhang.es.controller;

import com.hanxiaozhang.es.dao.ArticleRepository;
import com.hanxiaozhang.es.entity.ArticleEntity;
import com.hanxiaozhang.es.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.data.elasticsearch.core.query.Field;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/8/31
 * @since 1.0.0
 */
@Controller
@RequestMapping("/article")
public class ArticleController {


    @Autowired
    private ArticleService articleService;
    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @GetMapping("/get/{id}")
    @ResponseBody
    public ArticleEntity getBookById(@PathVariable Long id) {
        Optional<ArticleEntity> opt = articleService.findById(id);
        ArticleEntity article = opt.get();
        return article;
    }


    @GetMapping("/test")
    @ResponseBody
    public void test() {

        PageRequest pageRequest = PageRequest.of(0, 5000,
                Sort.by(Sort.Order.asc("id")));
        CriteriaQuery query = new CriteriaQuery(new Criteria(), pageRequest);
        query.addCriteria(Criteria.where("delFlag").is(0));
        query.addCriteria(Criteria.where("num").is(2));

        List<Criteria> criteriaChain = query.getCriteria().getCriteriaChain();
        Iterator<Criteria> iterator = criteriaChain.iterator();

        query.getCriteria().getCriteriaChain();

        while (iterator.hasNext()) {
            Criteria next = iterator.next();
            if (next.getField().getName().equals("num")) {
                // 不能清除 Collections.unmodifiableSet()
                next.getQueryCriteriaEntries().clear();
            }
        }


        // Page<ArticleEntity> articleEntities = elasticsearchTemplate.queryForPage(query, ArticleEntity.class);

        System.out.println(query);

        // articleRepository.deleteAll();
//        System.out.println(articleRepository.count());
//        System.out.println(articleService.count());
    }

    @GetMapping("/save")
    @ResponseBody
    public void save() {
        ArticleEntity article = new ArticleEntity(1L, "测试", "hanxiaozhang", "2021-08-14", 2, 0);
        articleService.save(article);
    }

    @RequestMapping("/batchSave")
    @ResponseBody
    public void batchSave() {
        Random random = new Random();
        int count = 0;
        List<ArticleEntity> list = new ArrayList<>();
        for (long i = 0; i < 10000; i++) {

            ArticleEntity article = new ArticleEntity(i , "测试" + i, "hanxiaozhang" + i, "2021-08-14", random.nextInt(10) + 1, 0);
            list.add(article);
            count++;
            if (count == 2000) {
                articleService.batchSave(list);
                list.clear();
                count = 0;
            }
        }
    }

}
