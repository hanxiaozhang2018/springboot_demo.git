package com.hanxiaozhang.es.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * 〈一句话功能简述〉<br>
 * 〈文章〉
 *
 *  @Document参数：
 *  indexName：对应索引库名称 ==> 数据库名称
 *  type：对应在索引库中的类型  ==> 表名称
 *  shards：分片数
 *  replicas：副本数
 *
 *
 *  @Field常用参数：
 *  type：字段的类型，取值是枚举，FieldType；
 *  index：是否索引，布尔值类型，默认是true；
 *  store：是否存储，布尔值类型，默认值是false；
 *  analyzer：分词器名称
 *
 * @author hanxinghua
 * @create 2021/8/31
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Document(indexName = "han", type = "article")
public class ArticleEntity {

    /**
     * @Id ： 标记一个字段为id主键；一般id字段或是域，不需要存储也不需要分词，用于hash后进行分片；
     *
     */
    @Id
    private Long id;

    /**
     * 标题
     */
    @Field
    private String title;

    /**
     * 作者
     */
    private String author;

    /**
     * 内容
     */
    private String content;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 删除标准
     */
    private Integer delFlag;

}
