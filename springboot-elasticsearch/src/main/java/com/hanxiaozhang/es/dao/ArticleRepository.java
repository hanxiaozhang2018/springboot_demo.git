package com.hanxiaozhang.es.dao;

import com.hanxiaozhang.es.entity.ArticleEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/8/31
 * @since 1.0.0
 */
@Repository
public interface ArticleRepository extends ElasticsearchRepository<ArticleEntity, Long> {

    /**
     * 通过作者获取文章
     *
     *
     * @param author
     * @return
     */
    List<ArticleEntity> findByAuthor(String author);

}