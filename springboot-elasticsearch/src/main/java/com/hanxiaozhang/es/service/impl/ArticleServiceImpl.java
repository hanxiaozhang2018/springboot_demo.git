package com.hanxiaozhang.es.service.impl;

import com.hanxiaozhang.es.dao.ArticleRepository;
import com.hanxiaozhang.es.entity.ArticleEntity;
import com.hanxiaozhang.es.service.ArticleService;
import com.hanxiaozhang.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/8/31
 * @since 1.0.0
 */
@Slf4j
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;


    @Override
    public Optional<ArticleEntity> findById(Long id) {
        return articleRepository.findById(id);
    }

    @Override
    public List<ArticleEntity> list(Map<String, Object> params) {

        CriteriaQuery query = setBaseQuery(params);
        return elasticsearchTemplate.queryForList(query, ArticleEntity.class);
    }


    @Override
    public Long
    count(Map<String, Object> params) {

        CriteriaQuery query = setBaseQuery(params);
        return elasticsearchTemplate.count(query, ArticleEntity.class);
    }

    /**
     * 封装基础查询
     *
     * @param params
     * @return
     */
    private CriteriaQuery setBaseQuery(Map<String, Object> params) {


        PageRequest pageRequest = PageRequest.of((int) params.get("current") - 1, (int) params.get("pageSize"),
                Sort.by(Sort.Order.asc("id")));
        CriteriaQuery query = new CriteriaQuery(new Criteria(), pageRequest);
        query.addCriteria(Criteria.where("delFlag").is(0));

        if (params.get("id") != null) {
            query.addCriteria(Criteria.where("id").is(params.get("id")));
        }
        if (params.get("num") != null) {
            query.addCriteria(Criteria.where("num").is(params.get("num")));
        }
        if (params.get("author") != null) {
            query.addCriteria(Criteria.where("author").is(params.get("author")));
        }
        if (params.get("title") != null) {
            query.addCriteria(Criteria.where("title").is(params.get("title")));
        }
        // todo 待测试
        if (params.get("content") != null) {
            query.addCriteria(Criteria.where("content").contains(String.valueOf(params.get("content"))));
        }
        log.info("Query is [{}]", query.toString());
        return query;
    }

    @Override
    public ArticleEntity save(ArticleEntity article) {
        return articleRepository.save(article);
    }

    @Override
    public List<ArticleEntity> batchSave(List<ArticleEntity> article) {
        return (List<ArticleEntity>) articleRepository.saveAll(article);
    }

    @Override
    public void delete(ArticleEntity article) {
        articleRepository.delete(article);
    }

    @Override
    public List<ArticleEntity> findAll() {
        return (List<ArticleEntity>) articleRepository.findAll();
    }

    @Override
    public List<ArticleEntity> findByAuthor(String author) {
        return articleRepository.findByAuthor(author);
    }

}
