package com.hanxiaozhang.es.service;

import com.hanxiaozhang.es.entity.ArticleEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/8/31
 * @since 1.0.0
 */
public interface ArticleService {


    Optional<ArticleEntity> findById(Long id);

    List<ArticleEntity> list(Map<String,Object> params);

    Long count(Map<String,Object> params);

    ArticleEntity save(ArticleEntity book);

    List<ArticleEntity> batchSave(List<ArticleEntity> article);

    void delete(ArticleEntity article);

    List<ArticleEntity> findAll();

    List<ArticleEntity> findByAuthor(String author);

}
