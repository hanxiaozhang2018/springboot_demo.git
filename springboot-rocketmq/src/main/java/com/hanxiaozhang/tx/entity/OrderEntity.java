package com.hanxiaozhang.tx.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * RocketMQ事务订单表
 * 
 * @author hanxiaozhang
 * @email hanxiaozhang2018@sina.com
 * @date 2022-10-09
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class OrderEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
   /**
	* id
	*/
	private Integer id;

   /**
	* 订单id
	*/
	private String orderId;

   /**
	* 购买数量
	*/
	private Integer buyNum;

   /**
	* 商品ID
	*/
	private Integer goodId;

   /**
	* 用户ID
	*/
	private Integer userId;

   /**
	* 支付状态，0：没有支付，1：已经支付
	*/
	private Integer payStatus;


}
