package com.hanxiaozhang.tx.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * RocketMQ事务库存表
 * 
 * @author hanxiaozhang
 * @email hanxiaozhang2018@sina.com
 * @date 2022-10-09
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class RepoEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
   /**
	* id
	*/
	private Integer id;

   /**
	* 商品名称
	*/
	private String goodName;

   /**
	* 库存数量
	*/
	private Integer num;



}
