package com.hanxiaozhang.tx.service;

import com.hanxiaozhang.tx.entity.OrderEntity;

import java.util.List;
import java.util.Map;

/**
 * RocketMQ事务订单表
 * 
 * @author hanxiaozhang
 * @email hanxiaozhang2018@sina.com
 * @date 2022-10-09 14:13:50
 */
public interface OrderService {


	/**
	 * 通过订单id查询
	 *
	 * @param orderId
	 * @return
	 */
	OrderEntity getByOrderId(String orderId);

	/**
	 * 检查订单是否存在并且状态是支付完成
	 *
	 * @param orderId
	 * @return
	 */
	boolean checkOrderPaySuccess(String orderId);

	/**
	 * 更新订单是为支付完成
	 *
	 * @param orderId
	 */
	void updatePayStatusByOrderId(String orderId);

	/**
	 * 生成订单，状态默认是未支付
	 *
	 * @param orderId
	 * @param num
	 * @param goodId
	 * @param userId
	 */
	void save(String orderId, int num, int goodId, int userId);

}
