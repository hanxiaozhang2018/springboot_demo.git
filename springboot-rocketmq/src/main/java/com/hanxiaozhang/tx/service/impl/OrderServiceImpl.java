package com.hanxiaozhang.tx.service.impl;

import com.hanxiaozhang.constant.PayStatusEnum;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;
import java.util.Map;

import com.hanxiaozhang.tx.dao.OrderDao;
import com.hanxiaozhang.tx.entity.OrderEntity;
import com.hanxiaozhang.tx.service.OrderService;
import org.springframework.transaction.annotation.Transactional;


/**
 * RocketMQ事务订单表
 *
 * @author hanxiaozhang
 * @email hanxiaozhang2018@sina.com
 * @date 2022-10-09
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDao orderDao;


    @Override
    public OrderEntity getByOrderId(String orderId){
    	return orderDao.getByOrderId(orderId);
	}

    @Override
    public boolean checkOrderPaySuccess(String orderId) {

        OrderEntity orderEntity = orderDao.getByOrderId(orderId);
        if (orderEntity != null) {
            if (orderEntity.getPayStatus() == 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void updatePayStatusByOrderId(String orderId) {

        orderDao.updatePayStatusByOrderId(orderId, PayStatusEnum.PAY_FINISH.getCode());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(String orderId, int num, int goodId, int userId) {

        OrderEntity order = OrderEntity.builder()
                .orderId(orderId)
                .buyNum(num)
                .goodId(goodId)
                .userId(userId)
                .payStatus(PayStatusEnum.NO_PAY.getCode())
                .build();
        orderDao.save(order);
    }

}
