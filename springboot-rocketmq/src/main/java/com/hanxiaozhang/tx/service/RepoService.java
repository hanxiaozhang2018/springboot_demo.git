package com.hanxiaozhang.tx.service;

import com.hanxiaozhang.tx.entity.RepoEntity;

import java.util.List;
import java.util.Map;

/**
 * RocketMQ事务库存表
 * 
 * @author hanxiaozhang
 * @email hanxiaozhang2018@sina.com
 * @date 2022-10-09
 */
public interface RepoService {

	/**
	 * 扣减数量
	 *
	 * @param buyNum
	 * @param goodId
	 * @return
	 */
	int reduce(Integer buyNum, Integer goodId);

}
