package com.hanxiaozhang.tx.controller;

import java.util.UUID;


import com.hanxiaozhang.config.RedisUtil;
import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.tx.dao.OrderDao;
import com.hanxiaozhang.tx.dao.RepoDao;
import com.hanxiaozhang.tx.entity.RepoEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hanxiaozhang.tx.entity.OrderEntity;
import com.hanxiaozhang.tx.service.OrderService;

import javax.annotation.Resource;


/**
 * RocketMQ事务订单表
 *
 * @author hanxiaozhang
 * @email hanxiaozhang2018@sina.com
 * @date 2022-10-09
 */
@Slf4j
@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Resource
    private OrderDao orderDao;
    @Resource
    private RepoDao repoDao;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 生成订单
     *
     * @param num
     * @param goodId
     * @return
     */
    @ResponseBody
    @GetMapping("/createOrder")
    public String createOrder(@RequestParam("num") int num,
                              @RequestParam("goodId") int goodId) {

        String orderId = UUID.randomUUID().toString();
        orderService.save(orderId, num, goodId, 1);
        return orderId;
    }


    /**
     * 支付成功调用
     *
     * @param orderId
     * @return
     */
    @ResponseBody
    @GetMapping("/pay")
    public String pay(@RequestParam("orderId") String orderId) {
        OrderEntity orderEntity = orderDao.getByOrderId(orderId);
        if (orderEntity == null) {
            log.info("不存在该订单,orderId:{}", orderId);
            return "不存在该订单";
        }
        String transactionId = UUID.randomUUID().toString();
        log.info("transactionId is {}", transactionId);
        Message<OrderEntity> mqMessage = MessageBuilder.
                withPayload(orderEntity)
                // 事务id
                .setHeader(RocketMQHeaders.TRANSACTION_ID, transactionId)
                // orderId，生产者执行本地事务和消息回查时使用
                .setHeader("orderId", orderId)
                // keys，做幂等性处理
                .setHeader(RocketMQHeaders.KEYS, transactionId)
                .build();

        rocketMQTemplate.sendMessageInTransaction(RocketConstant.TX_ORDER_REPO_TOPIC, mqMessage, null);
        return "ok";
    }

    /**
     * 创建库存
     *
     * @param num
     * @param goodName
     * @return
     */
    @ResponseBody
    @GetMapping("/createRepo")
    public Integer createRepo(@RequestParam("num") int num,
                              @RequestParam("goodName") String goodName) {
        RepoEntity repoEntity = RepoEntity
                .builder()
                .num(num)
                .goodName(goodName)
                .build();
        repoDao.save(repoEntity);
        return repoEntity.getId();
    }

    @ResponseBody
    @GetMapping("/test")
    public Boolean test(){
        return  redisUtil.hasConsume("text","1",60*100);
    }

}
