package com.hanxiaozhang.tx.dao;

import com.hanxiaozhang.tx.entity.OrderEntity;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * RocketMQ事务订单表
 *
 * @author hanxiaozhang
 * @email hanxiaozhang2018@sina.com
 * @date 2022-10-09
 */
@Mapper
public interface OrderDao {

	OrderEntity get(Long id);
	
	List<OrderEntity> list(Map<String, Object> map);

	int save(OrderEntity order);
	
	int update(OrderEntity order);
	
	int remove(Integer id);

	@Update("update rocket_order set pay_status=#{payStatus}  where `order_id` = #{orderId}")
	void updatePayStatusByOrderId(String orderId, int payStatus);

	@Select("select * from rocket_order where `order_id` = #{orderId}")
	OrderEntity getByOrderId(String orderId);
}
