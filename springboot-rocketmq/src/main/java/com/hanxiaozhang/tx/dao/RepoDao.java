package com.hanxiaozhang.tx.dao;

import com.hanxiaozhang.tx.entity.RepoEntity;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * RocketMQ事务库存表
 *
 * @author hanxiaozhang
 * @email hanxiaozhang2018@sina.com
 * @date 2022-10-09
 */
@Mapper
public interface RepoDao {

	RepoEntity get(Integer id);
	
	List<RepoEntity> list(Map<String, Object> map);
	
	int save(RepoEntity repo);
	
	int update(RepoEntity repo);
	
	int remove(Integer id);

	@Update("update `rocket_repo` set `num` =#{reduceNum} where `num` = #{curNum} and `id` = #{goodId}")
	int updateGoodNum(int reduceNum, Integer curNum, Integer goodId);
}
