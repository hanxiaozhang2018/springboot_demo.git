package com.hanxiaozhang.config;

import org.apache.rocketmq.spring.annotation.ExtRocketMQTemplateConfiguration;
import org.apache.rocketmq.spring.core.RocketMQTemplate;

/**
 * 〈一句话功能简述〉<br>
 * 〈其他数据源〉
 *
 * @author hanxinghua
 * @create 2022/9/29
 * @since 1.0.0
 */
@ExtRocketMQTemplateConfiguration(nameServer="127.0.0.1:9876")
public class OtherRocketMQTemplate extends RocketMQTemplate {
}
