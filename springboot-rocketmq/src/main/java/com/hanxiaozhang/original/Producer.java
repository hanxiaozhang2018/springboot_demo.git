package com.hanxiaozhang.original;

import com.hanxiaozhang.constant.RocketConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.io.UnsupportedEncodingException;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/30
 * @since 1.0.0
 */
@Slf4j
public class Producer {


    public static void main(String[] args) throws Exception {

        // 实例化消息生产者
        DefaultMQProducer producer = new DefaultMQProducer(RocketConstant.ORIGINAL_PRODUCER_GROUP);
        // 设置NameServer的地址
        producer.setNamesrvAddr(RocketConstant.NAME_SERVER_ADDR);
        // 启动Producer实例
        producer.start();
        // 创建消息，设置 Topic、Tag、keys、flag、消息体等
        Message message = new Message(RocketConstant.ORIGINAL_TOPIC, ("Hello RocketMQ").getBytes(RemotingHelper.DEFAULT_CHARSET));
        // 发送消息到一个Broker
        SendResult sendResult = producer.send(message);
        // 通过sendResult返回消息是否成功送达
        log.info("消息发送成功:{}", sendResult);
        // 如果不再发送消息，关闭Producer实例。
        producer.shutdown();
    }

}
