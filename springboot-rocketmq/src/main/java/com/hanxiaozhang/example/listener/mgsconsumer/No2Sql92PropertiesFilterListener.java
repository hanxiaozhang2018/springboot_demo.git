package com.hanxiaozhang.example.listener.mgsconsumer;

import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈使用SQL92过滤自定义消息属性的消息消费〉
 *
 * @author hanxinghua
 * @create 2022/9/30
 * @since 1.0.0
 */
@Slf4j
@Component
@RocketMQMessageListener(
        topic = RocketConstant.SQL92_PROPERTIES_TOPIC,
        selectorType = SelectorType.SQL92,
        selectorExpression = "(a is not null and a between 0 and 3)",
        consumerGroup = RocketConstant.SQL92_PROPERTIES_CONSUMER_GROUP)
public class No2Sql92PropertiesFilterListener implements RocketMQListener<RocketMessage> {
    @Override
    public void onMessage(RocketMessage message) {
        log.info("{}收到消息：{}", this.getClass().getSimpleName(), message);
    }
}