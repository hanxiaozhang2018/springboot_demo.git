package com.hanxiaozhang.example.listener.mgssend;

import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 〈一句话功能简述〉<br>
 * 〈延迟消息〉
 *
 * @author hanxinghua
 * @create 2022/9/27
 * @since 1.0.0
 */
@Slf4j
@Component
@RocketMQMessageListener(
        topic = RocketConstant.DELAY_TOPIC,
        consumerGroup = RocketConstant.DELAY_CONSUMER_GROUP)
public class No5DelayListener implements RocketMQListener<RocketMessage> {

    @Override
    public void onMessage(RocketMessage message) {
        log.info("{}在{}收到消息：{}", this.getClass().getSimpleName(), LocalDateTime.now(), message);
    }

}
