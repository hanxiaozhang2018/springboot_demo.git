package com.hanxiaozhang.example.listener.mgssend;

import com.hanxiaozhang.constant.RocketConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/10/8
 * @since 1.0.0
 */
@Slf4j
@Component
@RocketMQMessageListener(
        topic = RocketConstant.TX_TOPIC,
        consumerGroup = RocketConstant.TX_CONSUMER_GROUP)
public class No6TxConsumerListener implements RocketMQListener<String>{

    @Override
    public void onMessage(String message) {
        log.info("{}收到消息：{}", this.getClass().getSimpleName(), message);
    }
}