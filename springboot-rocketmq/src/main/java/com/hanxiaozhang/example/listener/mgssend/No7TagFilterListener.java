package com.hanxiaozhang.example.listener.mgssend;

import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈使用Tag过滤带Tag的消息消费〉
 *
 * @author hanxinghua
 * @create 2022/9/28
 * @since 1.0.0
 */
@Slf4j
@Component
@RocketMQMessageListener(
        topic = RocketConstant.TAG_TOPIC,
        selectorType = SelectorType.TAG,
        selectorExpression = RocketConstant.TAG_EXPRESSION, // 支持写法 ：1. "xx"：订阅具体的tag  2. "*":订阅Topic的下所有消息  3. "TagA||TagB":订阅两个tag
        consumerGroup = RocketConstant.TAG_CONSUMER_TOPIC)
public class No7TagFilterListener implements RocketMQListener<RocketMessage> {
    @Override
    public void onMessage(RocketMessage message) {
        log.info("{}收到消息：{}", this.getClass().getSimpleName(), message);
    }
}
