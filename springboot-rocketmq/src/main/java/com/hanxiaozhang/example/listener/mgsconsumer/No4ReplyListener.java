package com.hanxiaozhang.example.listener.mgsconsumer;

import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQReplyListener;
import org.springframework.stereotype.Service;

/**
 * 〈一句话功能简述〉<br>
 * 〈可接受回复的消息〉
 *
 * @author hanxinghua
 * @create 2022/9/30
 * @since 1.0.0
 */
@Slf4j
@Service
@RocketMQMessageListener(
        topic = RocketConstant.REPLY_TOPIC,
        consumerGroup = RocketConstant.REPLY_CONSUMER_GROUP)
public class No4ReplyListener implements RocketMQReplyListener<RocketMessage, RocketMessage> {

    @Override
    public RocketMessage onMessage(RocketMessage message) {
        log.info("{}收到消息：{}", this.getClass().getSimpleName(), message);
        return RocketMessage.builder().name("回复响应消息").build();
    }
}
