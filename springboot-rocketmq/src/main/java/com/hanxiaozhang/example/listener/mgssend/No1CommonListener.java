package com.hanxiaozhang.example.listener.mgssend;

import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈基本消息〉
 *
 *  Tips：
 *  消费者组的消费者实例必须订阅完全相同的Topic
 *
 * @author hanxinghua
 * @create 2022/9/26
 * @since 1.0.0
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = RocketConstant.COMMON_TOPIC, consumerGroup = RocketConstant.COMMON_CONSUMER_GROUP)
public class No1CommonListener implements RocketMQListener<RocketMessage> {

    @Override
    public void onMessage(RocketMessage message) {
        log.info("{}收到消息：{}", this.getClass().getSimpleName(), message);
    }
}
