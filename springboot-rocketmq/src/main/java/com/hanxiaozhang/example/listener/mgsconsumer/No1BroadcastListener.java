package com.hanxiaozhang.example.listener.mgsconsumer;


import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈广播消费〉
 *
 * @author hanxinghua
 * @create 2022/9/28
 * @since 1.0.0
 */
@Slf4j
@Component
@RocketMQMessageListener(
        topic = RocketConstant.BROADCAST_TOPIC,
        messageModel = MessageModel.BROADCASTING,
        consumerGroup = RocketConstant.BROADCAST_CONSUMER_GROUP)
public class No1BroadcastListener implements RocketMQListener<RocketMessage> {
    @Override
    public void onMessage(RocketMessage message) {
        log.info("{}收到消息：{}", this.getClass().getSimpleName(), message);
    }
}
