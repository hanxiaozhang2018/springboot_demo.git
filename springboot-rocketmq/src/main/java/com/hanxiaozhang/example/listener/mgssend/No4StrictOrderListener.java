package com.hanxiaozhang.example.listener.mgssend;

import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈严格顺序消费〉
 *
 * @author hanxinghua
 * @create 2022/9/27
 * @since 1.0.0
 */
@Slf4j
@Component
@RocketMQMessageListener(
        topic = RocketConstant.STRICT_ORDER_TOPIC,
        consumeMode = ConsumeMode.ORDERLY,
        consumerGroup = RocketConstant.STRICT_ORDER_CONSUMER_GROUP)
public class No4StrictOrderListener implements RocketMQListener<RocketMessage> {

    @Override
    public void onMessage(RocketMessage message) {
        //发送消息是顺序发送的0,1,2,3,消费的顺序也是顺序的
        log.info("{}收到消息：{}", this.getClass().getSimpleName(), message);
    }

}
