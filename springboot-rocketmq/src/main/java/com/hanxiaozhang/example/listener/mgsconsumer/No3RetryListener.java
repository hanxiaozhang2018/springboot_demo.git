package com.hanxiaozhang.example.listener.mgsconsumer;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/9/29
 * @since 1.0.0
 */

import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.stereotype.Service;


/**
 * 功能描述: <br>
 * 〈消息重试〉
 * <p>
 * todo  怎么模拟消息失败
 *
 * @Author: hanxinghua
 * @Date: 2022/9/29
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = RocketConstant.RETRY_TOPIC, consumerGroup = RocketConstant.RETRY_CONSUMER_GROUP)
public class No3RetryListener implements RocketMQListener<RocketMessage>, RocketMQPushConsumerLifecycleListener {

    private int count = 0;

    @Override
    public void onMessage(RocketMessage message) {
        System.out.println(++count);
        log.info("{}收到消息：{}", this.getClass().getSimpleName(), message);
        // 会重试吗？ 抛异常会触发重试
        if (count != 5) {
            throw new RuntimeException("");
        }
    }

    /**
     * 对消费者客户端的一些配置
     *
     * @param consumer
     */
    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {

        // 设置最大重试次数
        consumer.setMaxReconsumeTimes(10);
        // 设置重试间隔
        consumer.setSuspendCurrentQueueTimeMillis(1000);
    }

}
