package com.hanxiaozhang.example.listener.mgssend;

import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈单向发送消息的消费〉
 *
 * @author hanxinghua
 * @create 2022/9/26
 * @since 1.0.0
 */
@Slf4j
@Component
@RocketMQMessageListener(
        topic = RocketConstant.ONE_WAY_TOPIC,
        consumerGroup = RocketConstant.ONE_WAY_CONSUMER_GROUP)
public class No3OneWayListener implements RocketMQListener<RocketMessage> {
    @Override
    public void onMessage(RocketMessage message) {
        log.info("{}收到消息：{}", this.getClass().getSimpleName(), message);
    }
}