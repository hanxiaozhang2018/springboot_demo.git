package com.hanxiaozhang.test;

import com.hanxiaozhang.constant.RocketConstant;
import com.hanxiaozhang.example.entity.RocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;

/**
 * 〈一句话功能简述〉<br>
 * 〈测试Controller〉
 *
 * @author hanxinghua
 * @create 2023/8/14
 * @since 1.0.0
 */
@Slf4j
@RequestMapping("/test")
@Controller
public class TestController {


    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @GetMapping("/test1")
    @ResponseBody
    public Object test1(){
        RocketMessage message = RocketMessage.builder().name("测试消息" + LocalDateTime.now()).build();
        SendResult sendResult = rocketMQTemplate.syncSend(RocketConstant.TEST_1_TOPIC, message);
        return sendResult;
    }


}
