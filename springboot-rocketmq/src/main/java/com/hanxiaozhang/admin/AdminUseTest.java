package com.hanxiaozhang.admin;

import org.apache.rocketmq.common.protocol.body.TopicList;
import org.junit.jupiter.api.Test;

import org.apache.rocketmq.tools.admin.DefaultMQAdminExt;

import java.util.Set;

/**
 * 〈一句话功能简述〉<br>
 * 〈Admin后台管理接口使用〉
 *
 * @author hanxinghua
 * @create 2023/5/25
 * @since 1.0.0
 */
public class AdminUseTest {


    @Test
    public void admin() throws Exception {

        DefaultMQAdminExt admin = new DefaultMQAdminExt();
        admin.setNamesrvAddr("");
        admin.start();

        TopicList topicList = admin.fetchAllTopicList();
        Set<String> topicList1 = topicList.getTopicList();

    }

}
