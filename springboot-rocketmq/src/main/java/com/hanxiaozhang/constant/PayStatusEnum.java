package com.hanxiaozhang.constant;

/**
 * 〈一句话功能简述〉<br>
 * 〈支付状态〉
 *
 * @author hanxinghua
 * @create 2022/10/9
 * @since 1.0.0
 */
public enum PayStatusEnum {

    NO_PAY(0, "没有支付"),
    PAY_FINISH(1, "支付完成");

    private Integer code;

    private String name;

    PayStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }


    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
