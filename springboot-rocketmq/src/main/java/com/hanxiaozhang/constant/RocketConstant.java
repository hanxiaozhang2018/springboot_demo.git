package com.hanxiaozhang.constant;

/**
 * 〈一句话功能简述〉<br>
 * 〈Rocket常量〉
 *
 * @author hanxinghua
 * @create 2022/9/26
 * @since 1.0.0
 */
public class RocketConstant {

    public static final String NAME_SERVER_ADDR = "localhost:9876";

    public static final String COMMON_TOPIC = "common_topic";
    public static final String COMMON_CONSUMER_GROUP = "common_consumer_group";

    public static final String ASYNC_TOPIC = "async_topic";
    public static final String ASYNC_CONSUMER_GROUP = "async_consumer_group";

    public static final String ONE_WAY_TOPIC = "one_way_topic";
    public static final String ONE_WAY_CONSUMER_GROUP = "one_way_consumer_group";

    public static final String STRICT_ORDER_TOPIC = "strict_order_topic";
    public static final String STRICT_ORDER_CONSUMER_GROUP = "strict_order_consumer_group";

    public static final String DELAY_TOPIC = "delay_topic";
    public static final String DELAY_CONSUMER_GROUP = "delay_consumer_group";

    public static final String TX_TOPIC = "tx_topic";
    public static final String TX_CONSUMER_GROUP = "tx_consumer_group";
    public static final String TX_PRODUCER_GROUP = "tx_producer_group";

    public static final String TAG_TOPIC = "tag_topic";
    public static final String TAG_CONSUMER_TOPIC = "tag_consumer_topic";
    /**
     * 支持写法 ：1. "xx"：订阅具体的tag  2. "*":订阅Topic的下所有消息  3. "TagA||TagB":订阅两个tag
     */
    public static final String TAG_EXPRESSION = "tag";

    public static final String BROADCAST_TOPIC = "broadcast_topic";
    public static final String BROADCAST_CONSUMER_GROUP = "broadcast_consumer_group";

    public static final String SQL92_TOPIC = "sql92_topic";
    public static final String SQL92_CONSUMER_GROUP = "sql92_consumer_group";
    public static final String SQL92_TAG_EXPRESSION = "sql91 or sql92";

    public static final String SQL92_PROPERTIES_TOPIC = "sql92_properties_topic";
    public static final String SQL92_PROPERTIES_CONSUMER_GROUP = "sql92_properties_consumer_group";

    public static final String RETRY_TOPIC = "retry_topic";
    public static final String RETRY_CONSUMER_GROUP = "retry_consumer_group";

    public static final String REPLY_TOPIC = "reply_topic";
    public static final String REPLY_CONSUMER_GROUP = "reply_consumer_group";

    public static final String PULL_ORIGINAL_TOPIC = "pull_original_topic";
    public static final String PULL_ORIGINAL_CONSUMER_GROUP = "pull_original_consumer_group";

    public static final String LITE_PULL_SUBSCRIBE_TOPIC = "lite_pull_subscribe_topic";
    public static final String LITE_PULL_SUBSCRIBE_CONSUMER_GROUP = "lite_pull_subscribe_consumer_group";

    public static final String LITE_PULL_ASSIGN_TOPIC = "lite_pull_assign_topic";
    public static final String LITE_PULL_ASSIGN_CONSUMER_GROUP = "lite_pull_assign_consumer_group";

    public static final String LITE_PULL_TEMPLATE_TOPIC = "lite_pull_template_topic";
    public static final String LITE_PULL_TEMPLATE_CONSUMER_GROUP = "lite_pull_template_consumer_group";

    public static final String CONSUME_POINT_TOPIC = "consume_point_topic";
    public static final String CONSUME_POINT_CONSUMER_GROUP = "consume_point_consumer_group";

    public static final String MANUAL_CONFIRM_TOPIC = "manual_confirm_topic";
    public static final String MANUAL_CONFIRM_CONSUMER_GROUP = "manual_confirm_consumer_group";

    public static final String ORIGINAL_TOPIC = "original_topic";
    public static final String ORIGINAL_CONSUMER_GROUP = "original_consumer_group";
    public static final String ORIGINAL_PRODUCER_GROUP = "original_producer_group";


    public static final String TX_ORDER_REPO_TOPIC = "tx_order_repo_topic";
    public static final String TX_ORDER_REPO_CONSUMER_GROUP = "tx_order_repo_consumer_group";


    public static final String TEST_1_TOPIC = "test_1_topic";
    public static final String TEST_1_CONSUMER_GROUP = "test_1_consumer_group";

}
