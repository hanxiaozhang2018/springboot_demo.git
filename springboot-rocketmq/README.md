# 工程简介


## 目录介绍：
### original
原始写法

### example
RocketMQ各种例子
Tips:
com.hanxiaozhang.example.controller.RocketController 记录一些知识点

### tx 
模拟分布式环境下，完成订单支付成功后，库存的扣减。
+ 1.使用RocketMQ事务，保证生产者成功更新订单支付状态和发送扣减库存MQ消息。
+ 2.使用分布式锁，保证高并发下，防止库存被扣成负数。
+ 3.使用Redis进行消费者幂等性处理，并且使用Lua脚本，保证Redis操作原子。



## 知识点：
RocketMQ有两种消费模式：集群消费 和 广播消费
集群消费模式下，相同Consumer Group的每个Consumer实例平均分摊消息。
广播消费模式下，相同Consumer Group的每个Consumer实例都接收全量的消息。