package com.hanxiaozhang.strategy.simple3;

import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈一个简单的bean〉
 *
 * @author hanxinghua
 * @create 2020/5/11
 * @since 1.0.0
 */
@Component
public class Bean {

    String mgs="success";
}
