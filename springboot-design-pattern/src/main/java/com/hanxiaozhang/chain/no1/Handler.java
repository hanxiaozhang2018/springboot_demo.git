package com.hanxiaozhang.chain.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/8/7
 * @since 1.0.0
 */
public abstract class Handler {


    private Handler successor;

    /**
     * 处理方法，调用此方法处理请求
     */
    public abstract void handleRequest();

    public Handler getSuccessor() {
        return successor;
    }

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

}
