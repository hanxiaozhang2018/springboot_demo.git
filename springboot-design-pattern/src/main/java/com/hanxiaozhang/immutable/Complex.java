package com.hanxiaozhang.immutable;

/**
 * 〈一句话功能简述〉<br>
 * 〈复数〉
 *
 * @author hanxinghua
 * @create 2023/3/16
 * @since 1.0.0
 */
public final class Complex extends Number implements java.io.Serializable, Cloneable,
        Comparable {

    /**
     * 虚数单位
     */
    static final public Complex i = new Complex(0.0, 1.0);
    /**
     * 复数实部
     */
    private double re;
    /**
     * 复数虚部
     */
    private double im;

    public Complex(Complex z) {
        re = z.re;
        im = z.im;
    }

    public Complex(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public Complex(double re) {
        this.re = re;
        this.im = 0;
    }


    /**
     * 默认构造函数，构造一个为0的复数
     */
    public Complex() {
        re = 0;
        im = 0;
    }


    public boolean equals(Complex z) {
        return (re == z.re && im == z.im);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (obj instanceof Complex) {
            return equals((Complex) obj);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        long re_bits = Double.doubleToLongBits(re);
        long im_bits = Double.doubleToLongBits(im);
        return (int) ((re_bits ^ im_bits) ^ ((re_bits ^ im_bits) >> 32));
    }

    /**
     * 返回实部
     *
     * @return
     */
    public double real() {
        return re;
    }

    /**
     * 返回虚部
     *
     * @return
     */
    public double imag() {
        return im;
    }


    /**
     * 返回实部
     *
     * @param z
     * @return
     */
    public static double real(Complex z) {
        return z.re;
    }


    /**
     * 返回虚部
     *
     * @param z
     * @return
     */
    public double imag(Complex z) {
        return z.im;
    }


    @Override
    @Deprecated
    public int compareTo(Object arg0) {
        // todo
        return 0;
    }


    @Override
    @Deprecated
    public double doubleValue() {
        // todo
        return 0;
    }


    @Override
    @Deprecated
    public float floatValue() {
        // todo
        return 0;
    }


    @Override
    @Deprecated
    public int intValue() {
        // todo
        return 0;
    }


    @Override
    @Deprecated
    public long longValue() {
        // todo
        return 0;
    }

    /**
     * 乘法
     *
     * @param c1
     * @param c2
     * @return
     */
    public static Complex multiply(Complex c1, Complex c2) {
        return new Complex(c1.re * c2.re - c1.im * c2.im, c1.re * c2.im + c1.im
                * c2.re);
    }
}