package com.hanxiaozhang.immutable;

/**
 * 〈一句话功能简述〉<br>
 * 〈复数测试〉
 *
 * @author hanxinghua
 * @create 2023/3/16
 * @since 1.0.0
 */
public class Client {

    public static void main(String[] args) {

        Complex c1 = new Complex(10, 20);
        Complex c2 = new Complex(0, 1);

        Complex resComplex = Complex.multiply(c1, c2);
        System.out.println("实部= " + resComplex.real());
        System.out.println("虚部= " + resComplex.imag());

    }
}
