package com.hanxiaozhang.bridge.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/10/15
 * @since 1.0.0
 */
public class LinuxImp implements ImageImp {

    @Override
    public void doPaint() {
        System.out.println("这是在Linux系统的展示");
    }
}
