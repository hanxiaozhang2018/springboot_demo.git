package com.hanxiaozhang.bridge.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/10/15
 * @since 1.0.0
 */
public abstract class Image {

    protected ImageImp imageImp;

    public void setImp(ImageImp imageImp) {
        this.imageImp = imageImp;
    }

    public abstract void op();

}


