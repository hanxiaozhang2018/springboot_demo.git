package com.hanxiaozhang.bridge.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/10/15
 * @since 1.0.0
 */
public interface ImageImp {

    void doPaint();

}
