package com.hanxiaozhang.bridge.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/10/15
 * @since 1.0.0
 */
public class Client {

    public static void main(String[] args) {

        String str ="33";
        System.out.println(str.split("-"));

        // 测试在Linux上的BPM格式
        ImageImp imageImp1 = new LinuxImp();
        Image image1 = new BMP();
        image1.setImp(imageImp1);
        image1.op();

        // 测试WWindows上的GIF格式
        ImageImp imageImp2 = new WinImp();
        Image image2 = new GIF();
        image2.setImp(imageImp2);
        image2.op();

    }
}
