package com.hanxiaozhang.observer.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/6/30
 * @since 1.0.0
 */
public interface Subject {

    /**
     * 添加观察者
     *
     * @param observer
     */
    void add(Observer observer);


    /**
     * 删除观察者
     *
     * @param observer
     */
    void remove(Observer observer);

    /**
     * 通知所有的观察者
     */
    void notifyObservers();

}
