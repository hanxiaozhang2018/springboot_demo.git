package com.hanxiaozhang.observer.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/6/30
 * @since 1.0.0
 */
public class Test {


    public static void main(String[] args) {

        ConcreteObserver observer1 = new ConcreteObserver();
        ConcreteObserver observer2 = new ConcreteObserver();

        ConcreteSubject concreteSubject = new ConcreteSubject();
        concreteSubject.add(observer1);
        concreteSubject.add(observer2);

        concreteSubject.notifyObservers();

    }

}
