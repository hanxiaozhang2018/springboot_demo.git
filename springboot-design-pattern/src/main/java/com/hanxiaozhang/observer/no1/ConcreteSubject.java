package com.hanxiaozhang.observer.no1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/6/30
 * @since 1.0.0
 */
public class ConcreteSubject implements Subject {

    private List<Observer> observers = new ArrayList();

    @Override
    public void add(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void remove(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    /**
     * 拷贝当前集合
     *
     * @return
     */
    public List<Observer> getObservers() {
        return observers.stream().collect(Collectors.toList());
    }

}
