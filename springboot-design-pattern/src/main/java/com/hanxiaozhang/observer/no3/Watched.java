package com.hanxiaozhang.observer.no3;

import java.util.Observable;

/**
 * 〈一句话功能简述〉<br>
 * 〈被观察对象，被监视对象，即主题〉
 *
 * @author hanxinghua
 * @create 2022/7/2
 * @since 1.0.0
 */
public class Watched extends Observable {

    private String data = "";

    /**
     * 获取值
     *
     * @return
     */
    public String retrieveData() {

        return data;
    }


    /**
     * 改变值
     *
     * @param data
     */
    public void changeData(String data) {

        if (!this.data.equals(data)) {
            this.data = data;
            setChanged();
        }
        notifyObservers();
    }

}
