package com.hanxiaozhang.observer.no3;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/7/2
 * @since 1.0.0
 */
public class Test {


    public static void main(String[] args) {

        // 创建被观察者对象，被监视对象
        Watched watched = new Watched();

        // 创建观察者对象，并将被观察者对象登记
        Watcher watcher = new Watcher(watched);

        // 给被观察者对象的状态赋值
        watched.changeData("1");
        watched.changeData("2");
        watched.changeData("3");



    }
}
