package com.hanxiaozhang.observer.no3;

import java.util.Observable;
import java.util.Observer;

/**
 * 〈一句话功能简述〉<br>
 * 〈观察者〉
 *
 * @author hanxinghua
 * @create 2022/7/2
 * @since 1.0.0
 */
public class Watcher implements Observer {

    public Watcher(Watched w) {
        w.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Data has been changed to: " + ((Watched) o).retrieveData() + "");
    }
}
