package com.hanxiaozhang.observer.no2;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/6/30
 * @since 1.0.0
 */
public class Test {

    public static void main(String[] args) {
        // 创建主题对象
        ConcreteSubject subject = new ConcreteSubject();
        // 创建观察者对象
        ConcreteObserver observer = new ConcreteObserver();
        // 将观察者对象登记到主题对象上
        subject.add(observer);
        // 改变主题对象
        subject.change("更新状态");

    }
}
