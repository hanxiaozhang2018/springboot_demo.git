package com.hanxiaozhang.observer.no2;

/**
 * 〈一句话功能简述〉<br>
 * 〈观察者〉
 *
 * @author hanxinghua
 * @create 2022/6/30
 * @since 1.0.0
 */
public interface Observer {

    void update();

}
