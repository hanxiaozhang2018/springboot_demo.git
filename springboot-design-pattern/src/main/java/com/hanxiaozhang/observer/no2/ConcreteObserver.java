package com.hanxiaozhang.observer.no2;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/6/30
 * @since 1.0.0
 */
public class ConcreteObserver implements Observer {

    /**
     * 调用这个方法会更新自己
     */
    @Override
    public void update() {
        System.out.println("触发一次更新!");
    }
}
