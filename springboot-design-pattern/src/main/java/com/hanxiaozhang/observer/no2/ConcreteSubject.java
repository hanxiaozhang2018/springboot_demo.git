package com.hanxiaozhang.observer.no2;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/6/30
 * @since 1.0.0
 */
public class ConcreteSubject extends Subject {


    private String state;

    public void change(String state) {
        this.state = state;
        this.notifyObservers();
    }
}
