package com.hanxiaozhang.observer.no2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 〈一句话功能简述〉<br>
 * 〈主题〉
 *
 * @author hanxinghua
 * @create 2022/6/30
 * @since 1.0.0
 */
public abstract class Subject {

    private List<Observer> observers = new ArrayList<>();

    public void add(Observer observer) {

        observers.add(observer);
        System.out.println("添加一个观察者!");
    }


    public void remove(Observer observer) {
        observers.remove(observer);
        System.out.println("删除一个观察者!");
    }


    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }


    public List<Observer> getObservers() {
        return observers.stream().collect(Collectors.toList());
    }

}
