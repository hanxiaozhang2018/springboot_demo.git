package com.hanxiaozhang.nondesign.callback.no1;

import com.hanxiaozhang.nondesign.callback.Callback;

/**
 * 〈一句话功能简述〉<br>
 * 〈回调主题〉
 *
 * @author hanxinghua
 * @create 2023/5/23
 * @since 1.0.0
 */
public class CallbackSubject {

    private Callback callback;

    /**
     * 注册回调
     *
     * @param callback
     */
    public void register(Callback callback) {
        this.callback = callback;
    }


    /**
     * 执行回调
     */
    public void execute() {
        callback.doExecute();
    }

}
