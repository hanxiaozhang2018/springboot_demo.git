package com.hanxiaozhang.nondesign.callback.no2;

import com.hanxiaozhang.nondesign.callback.Callback;

/**
 * 〈一句话功能简述〉<br>
 * 〈使用回调，计算方法执行时间〉
 *
 * @author hanxinghua
 * @create 2023/5/23
 * @since 1.0.0
 */
public class Tools {

    public static void main(String[] args) {

        Callback callback = new Callback() {
            @Override
            public void doExecute() {
                TestObject.testMethod();
            }
        };

        Tools tools = new Tools();
        tools.printTime(callback);
    }


    /**
     * 打印时间方法
     *
     * @param callback
     */
    public void printTime(Callback callback) {
        long begin = System.currentTimeMillis();
        callback.doExecute();
        long end = System.currentTimeMillis();
        System.out.println("[use time]:" + (end - begin));
    }

}
