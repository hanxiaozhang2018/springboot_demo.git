package com.hanxiaozhang.nondesign.callback.no1;

import com.hanxiaozhang.nondesign.callback.Callback;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/5/23
 * @since 1.0.0
 */
public class Test {
    public static void main(String[] args) {

        CallbackSubject subject = new CallbackSubject();
        Callback callback = new Callback() {
            @Override
            public void doExecute() {
                System.out.println("call execute ...");
            }
        };
        subject.register(callback);
        subject.execute();
    }

}
