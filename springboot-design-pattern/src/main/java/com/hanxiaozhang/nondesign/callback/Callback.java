package com.hanxiaozhang.nondesign.callback;

/**
 * 〈一句话功能简述〉<br>
 * 〈回调函数〉
 *
 * @author hanxinghua
 * @create 2023/5/23
 * @since 1.0.0
 */
public interface Callback {

    /**
     * 回调
     */
    void doExecute();
}
