package com.hanxiaozhang.singleton;

/**
 * 〈一句话功能简述〉<br>
 * 〈单例模式1 - 饿汉模式〉
 *
 * @author hanxinghua
 * @create 2022/3/25
 * @since 1.0.0
 */
public class Singleton1 {

    private static Singleton1 singleton1 = new Singleton1();

    private Singleton1() {

    }

    public static Singleton1 getSingleton() {
        return singleton1;
    }
}
