package com.hanxiaozhang.singleton;

/**
 * 〈一句话功能简述〉<br>
 * 〈单例模式2 - 懒汉模式〉
 *
 * @author hanxinghua
 * @create 2022/3/25
 * @since 1.0.0
 */
public class Singleton2 {

    private static Singleton2 singleton2;

    private Singleton2() {

    }

    public static Singleton2 getSingleton() {
        if (singleton2 == null) {
            singleton2 = new Singleton2();
        }
        return singleton2;
    }
}
