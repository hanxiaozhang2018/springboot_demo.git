package com.hanxiaozhang.singleton;

/**
 * 〈一句话功能简述〉<br>
 * 〈单例模式4 - 枚举〉
 *
 * @author hanxinghua
 * @create 2022/3/25
 * @since 1.0.0
 */
public enum Singleton5 {
    INSTANCE;
}
