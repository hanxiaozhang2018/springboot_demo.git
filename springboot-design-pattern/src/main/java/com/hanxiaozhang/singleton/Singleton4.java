package com.hanxiaozhang.singleton;

/**
 * 〈一句话功能简述〉<br>
 * 〈单例模式4 - 静态内部类〉
 *
 * @author hanxinghua
 * @create 2022/3/25
 * @since 1.0.0
 */
public class Singleton4 {

    private static class Holder {
        private static final Singleton4 INSTANCE = new Singleton4();
    }

    private Singleton4() {
    }

    public static Singleton4 getSingleton() {
        return Holder.INSTANCE;
    }
}
