package com.hanxiaozhang.singleton;

/**
 * 〈一句话功能简述〉<br>
 * 〈单例模式3 - DCL + Synch+ volatile〉
 *
 * @author hanxinghua
 * @create 2022/3/25
 * @since 1.0.0
 */
public class Singleton3 {

    private static volatile Singleton3 singleton3;

    private Singleton3() {

    }

    public static Singleton3 getSingleton() {
        if (singleton3 == null) {
            synchronized (Singleton3.class) {
                if (singleton3 == null) {
                    singleton3 = new Singleton3();
                }
            }
        }
        return singleton3;
    }
}
