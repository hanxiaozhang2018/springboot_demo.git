package com.hanxiaozhang.decorator.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈咖啡装饰器〉
 * <p>
 * Decorator（装饰器）：继承自Component，同时持有一个指向Component的引用，
 * 这样可以通过组合的方式来对被装饰者进行包装。
 *
 * @author hanxinghua
 * @create 2024/10/18
 * @since 1.0.0
 */
public class CoffeeDecorator implements Coffee {

    protected Coffee decoratedCoffee;

    public CoffeeDecorator(Coffee coffee) {
        this.decoratedCoffee = coffee;
    }

    @Override
    public double cost() {
        return decoratedCoffee.cost();
    }
}
