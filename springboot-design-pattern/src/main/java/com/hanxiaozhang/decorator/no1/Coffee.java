package com.hanxiaozhang.decorator.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈咖啡〉
 * <p>
 * Component（组件）：定义一个对象接口，可以给这些对象动态地添加职责。
 * 在装饰器模式中，通常是一个抽象类或接口，它定义了被装饰者和装饰者的公共接口。
 *
 * @author hanxinghua
 * @create 2024/10/18
 * @since 1.0.0
 */
public interface Coffee {

    /**
     * 花费
     *
     * @return
     */
    double cost();
}
