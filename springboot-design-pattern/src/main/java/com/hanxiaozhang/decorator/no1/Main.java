package com.hanxiaozhang.decorator.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/10/18
 * @since 1.0.0
 */
public class Main {

    public static void main(String[] args) {

        Coffee simpleCoffee = new SimpleCoffee();
        System.out.println("Cost of simple coffee: " + simpleCoffee.cost());

        Coffee coffeeWithMilk = new WithMilk(simpleCoffee);
        System.out.println("Cost of coffee with milk: " + coffeeWithMilk.cost());

        Coffee coffeeWithMilkAndSugar = new WithSugar(coffeeWithMilk);
        System.out.println("Cost of coffee with milk and sugar: " + coffeeWithMilkAndSugar.cost());
    }
}
