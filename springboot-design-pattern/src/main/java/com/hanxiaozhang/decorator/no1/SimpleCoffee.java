package com.hanxiaozhang.decorator.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈普通咖啡〉
 * <p>
 * ConcreteComponent（具体组件）：实现了Component接口的具体对象，也就是被装饰的对象。
 *
 * @author hanxinghua
 * @create 2024/10/18
 * @since 1.0.0
 */
public class SimpleCoffee implements Coffee {

    @Override
    public double cost() {
        return 5;
    }
}
