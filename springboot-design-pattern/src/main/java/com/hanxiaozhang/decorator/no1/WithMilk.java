package com.hanxiaozhang.decorator.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈加奶咖啡〉
 * <p>
 * ConcreteDecorator（具体装饰器）：实现了Decorator接口的具体装饰器对象，
 * 它向被装饰的对象添加新的功能或行为。
 *
 * @author hanxinghua
 * @create 2024/10/18
 * @since 1.0.0
 */
public class WithMilk extends CoffeeDecorator {

    public WithMilk(Coffee coffee) {
        super(coffee);
    }

    @Override
    public double cost() {
        return super.cost() + 2;
    }
}
