# 目录：
## bridge 桥接模式
## chain 责任链模式
## decorator 装饰模式
## memento 备忘录模式
## observer 观察者模式
## singleton 单例模式
## strategy 策略模式
## ---- ----
## nondesign 非设计模式
### callback 回调
