package com.hanxiaozhang.facade;

/**
 * 〈一句话功能简述〉<br>
 * 〈灯〉
 *
 * @author hanxinghua
 * @create 2022/11/28
 * @since 1.0.0
 */
public class Light {

    /**
     * 打开灯
     */
    public void turnOff() {
        System.out.println("Turning on the light.");
    }

    /**
     * 关闭灯
     */
    public void turnOn() {
        System.out.println("Turning off the light.");
    }

    /**
     * 换灯泡
     */
    public void changeBulb() {
        System.out.println("changing the light-bulb.");
    }
}
