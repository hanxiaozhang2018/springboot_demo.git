package com.hanxiaozhang.facade;

/**
 * 〈一句话功能简述〉<br>
 * 〈客户端〉
 *
 * @author hanxinghua
 * @create 2022/11/28
 * @since 1.0.0
 */
public class Client {

    public static void main(String[] args) {
        SecurityFacade.activateAll();
    }

}
