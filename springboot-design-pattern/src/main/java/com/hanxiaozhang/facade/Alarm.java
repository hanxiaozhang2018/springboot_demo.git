package com.hanxiaozhang.facade;

/**
 * 〈一句话功能简述〉<br>
 * 〈报警器〉
 *
 * @author hanxinghua
 * @create 2022/11/28
 * @since 1.0.0
 */
public class Alarm {

    /**
     * 启动报警器
     */
    public void activate() {
        System.out.println("Activating the alarm.");
    }

    /**
     * 关闭报警器
     */
    public void deactivate() {
        System.out.println("Deactivating the alarm.");
    }

    /**
     * 拉响报警器
     */
    public void ring() {
        System.out.println("Ringing the alarm.");
    }

    /**
     * 停掉报警器
     */
    public void stopRing() {
        System.out.println("Stop the alarm.");
    }
}
