package com.hanxiaozhang.facade;

/**
 * 〈一句话功能简述〉<br>
 * 〈安保系统门面〉
 *
 * @author hanxinghua
 * @create 2022/11/28
 * @since 1.0.0
 */
public class SecurityFacade {

    private static Camera camera1, camera2;
    private static Light light1, light2, light3;
    private static Sensor sensor;
    private static Alarm alarm;

    static {
        camera1 = new Camera();
        camera2 = new Camera();
        light1 = new Light();
        light2 = new Light();
        light3 = new Light();
        sensor = new Sensor();
        alarm = new Alarm();
    }

    /**
     * 开启
     */
    public static void activateAll() {
        camera1.turnOn();
        camera2.turnOn();
        light1.turnOn();
        light2.turnOn();
        light3.turnOn();
        sensor.activate();
        alarm.activate();
    }

    /**
     * 关闭
     */
    public static void deactivateAll() {
        camera1.turnOff();
        camera2.turnOff();
        light1.turnOff();
        light2.turnOff();
        light3.turnOff();
        sensor.deactivate();
        alarm.deactivate();
    }
}
