package com.hanxiaozhang.facade;

/**
 * 〈一句话功能简述〉<br>
 * 〈相机〉
 *
 * @author hanxinghua
 * @create 2022/11/28
 * @since 1.0.0
 */
public class Camera {

    /**
     * 打开相机
     */
    public void turnOn() {
        System.out.println("Turning on the camera.");
    }

    /**
     * 关闭相机
     */
    public void turnOff() {
        System.out.println("Turning off the camera.");
    }

    /**
     * 转动相机
     *
     * @param degrees
     */
    public void rotate(int degrees) {
        System.out.println("Rotating the camera by {0} degrees." + degrees);
    }
}