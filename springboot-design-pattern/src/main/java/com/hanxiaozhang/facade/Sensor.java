package com.hanxiaozhang.facade;

/**
 * 〈一句话功能简述〉<br>
 * 〈感应器〉
 *
 * @author hanxinghua
 * @create 2022/11/28
 * @since 1.0.0
 */
public class Sensor {

    /**
     * 启动感应器
     */
    public void activate() {
        System.out.println("Activating the sensor.");
    }

    /**
     * 关闭感应器
     */
    public void deactivate() {
        System.out.println("Deactivating the sensor.");
    }

    /**
     * 触发感应器
     */
    public void trigger() {
        System.out.println("The sensor has triggered.");
    }
}
