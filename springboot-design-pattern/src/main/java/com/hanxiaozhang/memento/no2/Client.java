package com.hanxiaozhang.memento.no2;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public class Client {

    public static void main(String[] args) {
        // 发起人
        Originator originator = new Originator();
        // 负责人
        Caretaker caretaker = new Caretaker();
        // 设置发起人对象的状态
        originator.setState("On");
        System.out.println("初始状态:" + originator.getState());
        // 创建备忘录对象，并将发起人对象的状态存储起来
        caretaker.saveMemento(originator.creatMemento());
        // 改变发起人对象的状态
        originator.setState("Off");
        System.out.println("改变后状态:" + originator.getState());
        // 恢复发起人状态
        originator.restoreMemento(caretaker.retrieveMemento());
        System.out.println("恢复后状态:" + originator.getState());
    }

}
