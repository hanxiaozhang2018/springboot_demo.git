package com.hanxiaozhang.memento.no2;

/**
 * 〈一句话功能简述〉<br>
 * 〈窄接口，这是一个标识接口，因为它没有定义出任何方法〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public interface MementoIF {
}
