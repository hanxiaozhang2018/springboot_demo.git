package com.hanxiaozhang.memento.no2;

/**
 * 〈一句话功能简述〉<br>
 * 〈负责人角色〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public class Caretaker {


    private MementoIF memento;

    /**
     * 备忘录的取值方法
     */
    public MementoIF retrieveMemento() {
        return this.memento;
    }

    /**
     * 备忘录的赋值方法
     */
    public void saveMemento(MementoIF memento) {
        this.memento = memento;
    }

}
