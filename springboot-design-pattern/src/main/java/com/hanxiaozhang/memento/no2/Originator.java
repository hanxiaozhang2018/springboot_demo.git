package com.hanxiaozhang.memento.no2;

/**
 * 〈一句话功能简述〉<br>
 * 〈发起人角色〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public class Originator {

    private String state;

    public Originator() {

    }

    /**
     * 工厂方法，返回一个新的备忘录
     *
     * @return
     */
    public Memento creatMemento() {
        return new Memento(this.state);
    }


    /**
     * 将发起人恢复到备忘录对象所记载的状态
     */
    public void restoreMemento(MementoIF momIf) {
        this.setState(((Memento) momIf).getState());
        System.out.println("----黑箱恢复备忘录，存储的状态是：" + state);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    /**
     * 内部成员类，备忘录
     */
    private class Memento implements MementoIF {
        private String state;

        public Memento(String state) {
            this.state = state;
            System.out.println("----黑箱备忘录当前保存的状态是：" + state);
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }

}
