package com.hanxiaozhang.memento.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈负责人角色〉
 *
 * @author hanxinghua
 * @create 2022/7/10
 * @since 1.0.0
 */
public class Caretaker {

    private Memento memento;

    /**
     * 获取备忘录
     *
     * @return
     */
    public Memento getMemento() {
        return memento;
    }

    /**
     * 赋值备忘录
     *
     * @param memento
     */
    public void setMemento(Memento memento) {
        this.memento = memento;
    }

}
