package com.hanxiaozhang.memento.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈发起人〉
 *
 * @author hanxinghua
 * @create 2022/7/10
 * @since 1.0.0
 */
public class Originator {

    private String state = "";

    /**
     * 工厂方法，返回一个新的备忘录对象
     *
     * @return
     */
    public Memento createMemento() {
        return new Memento(this.state);
    }

    /**
     * 将发起人恢复到备忘录对象所记载的状态
     *
     * @param memento
     */
    public void restoreMemento(Memento memento) {
        this.setState(memento.getState());
    }

    /**
     * 获取状态
     *
     * @return
     */
    public String getState() {
        return state;
    }

    /**
     * 设置状态
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

}
