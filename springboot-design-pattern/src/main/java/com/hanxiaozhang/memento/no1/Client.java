package com.hanxiaozhang.memento.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈客户端角色〉
 *
 * @author hanxinghua
 * @create 2022/7/10
 * @since 1.0.0
 */
public class Client {

    public static void main(String[] args) {
        // 创建发起人角色
        Originator originator = new Originator();
        originator.setState("状态1");
        System.out.println("初始状态:" + originator.getState());
        // 创建负责人角色
        Caretaker caretaker = new Caretaker();
        // 负责人存储发起人的备忘录
        caretaker.setMemento(originator.createMemento());
        // 改变发起人状态
        originator.setState("状态2");
        System.out.println("改变后状态:" + originator.getState());
        // 恢复发起人状态
        originator.restoreMemento(caretaker.getMemento());
        System.out.println("恢复后状态:" + originator.getState());
    }

}
