package com.hanxiaozhang.memento.no1;

/**
 * 〈一句话功能简述〉<br>
 * 〈备忘录角色〉
 *
 * @author hanxinghua
 * @create 2022/7/10
 * @since 1.0.0
 */
public class Memento {

    private String state = "";

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
