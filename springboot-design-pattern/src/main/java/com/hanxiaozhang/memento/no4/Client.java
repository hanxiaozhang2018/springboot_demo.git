package com.hanxiaozhang.memento.no4;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public class Client {

    public static void main(String[] args) {

        Originator o = new Originator();
        o.changeState("state 1");
        o.printState();
        // 创建备忘录
        MementoIF memento = o.createMemento();
        // 修改状态
        o.changeState("state 2");
        o.restoreMemento(memento);
        o.printState();

    }


}
