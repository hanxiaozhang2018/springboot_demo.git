package com.hanxiaozhang.memento.no4;

/**
 * 〈一句话功能简述〉<br>
 * 〈发起人〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public class Originator {

    private String state;

    public Originator() {

    }

    public void changeState(String state) {
        this.state = state;
        System.out.println("State has been changed to: " + state);
    }

    public Memento createMemento() {
        return new Memento(this);
    }

    public void restoreMemento(MementoIF memento) {
        Memento m = (Memento) memento;
        changeState(m.getState());
    }

    public void printState() {
        System.out.println("Current state is: " + state);
    }


    class Memento implements MementoIF {


        private String state;

        public Memento(Originator o) {
            this.state = o.state;
        }

        public String getState() {
            return state;
        }

    }

}
