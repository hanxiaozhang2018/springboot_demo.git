package com.hanxiaozhang.memento.no3;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public class Client {

    public static void main(String[] args) {
        Originator o = new Originator();
        Caretaker c = new Caretaker(o);
        // 设置初始状态
        o.setStates("state 0");
        // 建立一个检查点
        c.createMemento();
        // 改变初始状态
        o.setStates("state 1");
        // 建立一个检查点
        c.createMemento();
        // 改变初始状态
        o.setStates("state 2");
        // 建立一个检查点
        c.createMemento();
        // 改变初始状态
        o.setStates("state 3");
        // 建立一个检查点
        c.createMemento();
        // 改变初始状态
        o.setStates("state 4");
        // 建立一个检查点
        c.createMemento();
        // 打印出所有的检查点
        o.printStates();

        System.out.println("恢复到第二个检查点");
        c.restoreMemento(2);
        o.printStates();

        System.out.println("恢复到第零个检查点");
        c.restoreMemento(0);
        o.printStates();




    }
}
