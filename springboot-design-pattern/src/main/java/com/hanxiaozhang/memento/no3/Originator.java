package com.hanxiaozhang.memento.no3;

import java.util.ArrayList;

/**
 * 〈一句话功能简述〉<br>
 * 〈发起人〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public class Originator {

    private ArrayList<String> states;

    private Integer index;

    public Originator() {
        this.states = new ArrayList<>();
        this.index = 0;
    }

    /**
     * 设置状态
     *
     * @param state
     */
    public void setStates(String state) {
        states.add(state);
        index++;
    }

    /**
     * 创建备忘录
     *
     * @return
     */
    public Memento createMemento() {
        return new Memento(states, index);
    }

    /**
     * 将发起人恢复到备忘录对象所记载的状态
     *
     * @param memento
     */
    public void restoreMemento(Memento memento) {
        this.states = memento.getStates();
        this.index = memento.getIndex();
    }


    /**
     * 打印所有状态
     */
    public void printStates() {
        System.out.println("Total number of state :" + index);
        if (index > 0) {
            states.forEach(System.out::println);
        }

    }

}
