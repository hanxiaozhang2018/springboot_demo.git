package com.hanxiaozhang.memento.no3;

import java.util.ArrayList;

/**
 * 〈一句话功能简述〉<br>
 * 〈负责人角色〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public class Caretaker {

    private Originator o;
    private ArrayList<Memento> mementos = new ArrayList();
    private Integer current;

    public Caretaker(Originator o) {
        this.o = o;
        this.current = 0;
    }

    /**
     * 创建一个新的检查点（备忘录点）
     *
     * @return
     */
    public int createMemento() {
        Memento memento = o.createMemento();
        mementos.add(memento);
        return current++;
    }

    /**
     * 将发起人恢复到某一个检查点
     *
     * @param index
     */
    public void restoreMemento(Integer index) {
        o.restoreMemento(mementos.get(index));
    }

    /**
     * 删除某一个检查点
     *
     * @param index
     */
    public void removeMemento(Integer index) {
        mementos.remove(index);
    }

}
