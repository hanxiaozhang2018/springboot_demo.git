package com.hanxiaozhang.memento.no3;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈备忘录对象〉
 *
 * @author hanxinghua
 * @create 2022/7/17
 * @since 1.0.0
 */
public class Memento {


    private ArrayList<String> states;

    private Integer index;

    public Memento(ArrayList<String> states, Integer index) {
        this.states = (ArrayList<String>) states.clone();
        this.index = index;
    }

    public ArrayList<String> getStates() {
        return states;
    }

    public Integer getIndex() {
        return index;
    }

}
