package com.hanxiaozhang.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hanxiaozhang.api.ProviderService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/4/24
 * @since 1.0.0
 */
@RestController
public class TestDubboController {

    @Reference(version = "1.0.0")
    private ProviderService providerService;

    @RequestMapping("/test")
    public String test(){
        String result = providerService.get();
        return result;
    }

}
