package com.hanxiaozhang.api;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/4/24
 * @since 1.0.0
 */
public interface ProviderService {

    String get();

}
