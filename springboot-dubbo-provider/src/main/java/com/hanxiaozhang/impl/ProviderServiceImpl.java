package com.hanxiaozhang.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.hanxiaozhang.api.ProviderService;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/4/24
 * @since 1.0.0
 */
@Component
@Service(version = "1.0.0", timeout = 15000)
public class ProviderServiceImpl implements ProviderService {

    @Override
    public String get() {
        return "ok";
    }


}
