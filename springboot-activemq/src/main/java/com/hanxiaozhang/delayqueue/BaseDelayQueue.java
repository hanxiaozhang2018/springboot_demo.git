package com.hanxiaozhang.delayqueue;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ScheduledMessage;

import javax.jms.*;


/**
 * 〈一句话功能简述〉<br>
 * 〈基础的延迟队列使用方法〉
 *
 * @author hanxinghua
 * @create 2019/7/6
 * @since 1.0.0
 */
public class BaseDelayQueue {

    public static final String BROKER_URL = "tcp://192.168.1.10:61616";


    /**
     * 发送消息
     */
    public static void sendMessage() {
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;
        try {
            ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("admin", "admin", BROKER_URL);
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue("queue.delay.base");
            producer = session.createProducer(queue);
            // 创建发送消息
            TextMessage textMessage = session.createTextMessage("发送延迟消息");
            // 设置延迟 10S
            textMessage.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, 10 * 1000);
            // 设置重复投递间隔（非必要，根据实际情况）
            // textMessage.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_PERIOD, 3000L)
            // 重复投递次数（非必要，根据实际情况）
            // textMessage.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_REPEAT, 5L)

            // 发送消息
            producer.send(textMessage);
        } catch (JMSException e) {
            e.printStackTrace();
        } finally {
            try {
                //关闭资源
                producer.close();
                session.close();
                connection.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 接收消息
     */
    public static void receiveMessage() {
        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;
        try {
            //第一步 创建ConnectionFactory工厂对象
            ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("admin", "admin", BROKER_URL);
            //第二步 从工厂获取一个Connection链接对象
            connection = connectionFactory.createConnection();
            //第三步 连接MQ服务
            connection.start();
            //第四步 创建Session会话 createSession(是否启动事务, 消息确认机制)
            session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
            //第五步 通过Session创建Queue（P2P模板）
            Queue queue = session.createQueue("queue.delay.base");
            //第六步 通过Session创建consumer接收者
            MessageConsumer consumer = session.createConsumer(queue);
            //第七步 指定消息监听器
            consumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        TextMessage textMessage = (TextMessage) message;
                        String text = textMessage.getText();
                        System.out.println("接收的消息：" + text);
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            });
            //第八步 测试需要，监听器一直启动
            while (true) {
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }


}
