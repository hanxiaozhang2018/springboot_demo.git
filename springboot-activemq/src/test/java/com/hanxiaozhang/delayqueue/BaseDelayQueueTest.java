package com.hanxiaozhang.delayqueue;

import org.junit.Test;

/**
 * 〈一句话功能简述〉<br>
 * 〈传统延迟队列测试类〉
 *
 * @author hanxinghua
 * @create 2022/5/14
 * @since 1.0.0
 */
public class BaseDelayQueueTest {

    @Test
    public void sendMessage(){
        BaseDelayQueue.sendMessage();
    }

    @Test
    public void receiveMessage(){
        BaseDelayQueue.receiveMessage();
    }



}
