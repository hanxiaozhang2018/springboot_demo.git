package com.hanxiaozhang.sourcecode.aqs.lock;

import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/11/16
 * @since 1.0.0
 */
public class Test {

    public static void main(String[] args) {
        MyReentrantLock lock = new MyReentrantLock(true);
        new Thread(() -> {
            lock.lock();
            try {
                TimeUnit.HOURS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();

        lock.lock();


    }

}
