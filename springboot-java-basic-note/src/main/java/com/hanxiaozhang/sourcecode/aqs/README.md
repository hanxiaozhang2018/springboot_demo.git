

private static final Unsafe unsafe = Unsafe.getUnsafe() 不能获取的问题?
```
Exception in thread "main" java.lang.ExceptionInInitializerError
at com.hanxiaozhang.sourcecode.aqs.lock.MyReentrantLock.<init>(MyReentrantLock.java:239)
at com.hanxiaozhang.sourcecode.aqs.lock.Test.main(Test.java:14)
Caused by: java.lang.SecurityException: Unsafe
at sun.misc.Unsafe.getUnsafe(Unsafe.java:90)
at com.hanxiaozhang.sourcecode.aqs.MyAbstractQueuedSynchronizer.<clinit>(MyAbstractQueuedSynchronizer.java:2416)
... 2 more
```
解决，通过反射获取

```
  private static final Unsafe unsafe = reflectGetUnsafe();
    public static Unsafe reflectGetUnsafe() {
        try {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            return (Unsafe) field.get(null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

```
