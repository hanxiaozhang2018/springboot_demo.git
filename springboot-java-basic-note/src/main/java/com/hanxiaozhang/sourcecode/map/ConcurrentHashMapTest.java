package com.hanxiaozhang.sourcecode.map;

import java.util.HashMap;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/4/20
 * @since 1.0.0
 */
public class ConcurrentHashMapTest {

    public static void main(String[] args) {

        MyConcurrentHashMap<String,String> map = new MyConcurrentHashMap();

        // map.put(null,null);
        map.put("1","1");
        System.out.println(map.get("1"));


        HashMap<String,String> map1 =new HashMap<>();
        System.out.println(map1.get("1"));

    }

}
