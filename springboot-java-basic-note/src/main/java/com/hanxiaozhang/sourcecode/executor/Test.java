package com.hanxiaozhang.sourcecode.executor;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 〈一句话功能简述〉<br>
 * 〈测试类〉
 *
 * @author hanxinghua
 * @create 2021/11/29
 * @since 1.0.0
 */
public class Test {

    public static void main(String[] args) {

        /*
        负数知识点：
        原码：二进制的数；
        反码：二进制取反的数；
        补码：二进制取反+1的数，负数用补码表示。
        */
        // baseKnowledge();


        MyThreadPoolExecutor threadPool = new MyThreadPoolExecutor(5, 10, 60, TimeUnit.MINUTES,
                new LinkedBlockingQueue(), Executors.defaultThreadFactory(),
                new MyThreadPoolExecutor.MyAbortPolicy());

        threadPool.execute(()->{
            System.out.println("测试一下");
        });


    }

    private static void baseKnowledge() {
        System.out.println("1的原码: " + "00000000000000000000000000000001");
        System.out.println("1的反码: " + Integer.toBinaryString(~1));
        System.out.println("1的补码: " + Integer.toBinaryString(~1 + 1) + " 即,它是-1的二进制");

        System.out.println("CAPACITY is " + Integer.toBinaryString((1 << 29) - 1));
        System.out.println("RUNNING is " + Integer.toBinaryString(-1 << 29));
        System.out.println(Integer.toBinaryString(0 << 29));
    }
}
