package com.hanxiaozhang.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * 〈一句话功能简述〉<br>
 * 〈两个集合取交集、并集、差集〉
 *
 * @author hanxinghua
 * @create 2023/3/14
 * @since 1.0.0
 */
public class No2InterUnionDiff {

    public static void main(String[] args) {

        List<String> list1 = new ArrayList<String>();
        list1.add("1");
        list1.add("2");
        list1.add("3");
        list1.add("5");
        list1.add("6");
        System.out.println("---- list1 ----");
        System.out.println(list1);

        List<String> list2 = new ArrayList<String>();
        list2.add("2");
        list2.add("3");
        list2.add("7");
        list2.add("8");
        System.out.println("---- list2 ----");
        System.out.println(list2);


        // 交集
        List<String> intersection = list1.stream().filter(item -> list2.contains(item)).collect(Collectors.toList());
        System.out.println("---- 交集 ----");
        System.out.println(intersection);

        // 差集 (list1 - list2)
        List<String> reduce = list1.stream().filter(item -> !list2.contains(item)).collect(Collectors.toList());
        System.out.println("---- 差集(list1 - list2)----");
        System.out.println(reduce);

        // 并集
        List<String> listAll = list1.parallelStream().collect(Collectors.toList());
        listAll.addAll(list2);
        System.out.println("---- 并集 ----");
        System.out.println(listAll);


        // 去重并集
        List<String> listAllDistinct = listAll.stream().distinct().collect(Collectors.toList());
        System.out.println("---- 去重并集 ----");
        System.out.println(listAllDistinct);



    }

}
