package com.hanxiaozhang.collection;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/1/16
 * @since 1.0.0
 */
public class No1Map {

    public static void main(String[] args) {

        Map<String, Object> map = new HashMap<>(8);
        map.put("abc", 123);
        map.put("qwe", "QQQ");
        // map.toString()与JSON.toJSONString(map)的区别：
        System.out.println("map.toString() :" + map.toString());
        System.out.println("JSON.toJSONString(map)) :" + JSON.toJSONString(map));

        // 有双引号，没有双引号
        String str = "abc";
        System.out.println("JSONObject.toJSONString(str) String类型有双引号： " + JSONObject.toJSONString(str));
        System.out.println("JSONObject.toJSON(str).toString() String类型没有双引号： " + JSONObject.toJSON(str).toString());

    }
}
