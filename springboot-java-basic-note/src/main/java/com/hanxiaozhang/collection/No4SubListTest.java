package com.hanxiaozhang.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈List的SubList测试〉
 *
 * @author hanxinghua
 * @create 2023/3/20
 * @since 1.0.0
 */
public class No4SubListTest {

    public static void main(String[] args) {


        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println(list);
        int offset = 2, size = 2, total = list.size();
        System.out.println(list.subList(offset, (offset + size) < total ? offset + size : total));
    }

}
