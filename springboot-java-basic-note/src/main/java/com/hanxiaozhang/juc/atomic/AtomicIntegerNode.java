package com.hanxiaozhang.juc.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/1/4
 * @since 1.0.0
 */
public class AtomicIntegerNode {


    public static void main(String[] args) {
        AtomicInteger i = new AtomicInteger(0);
        // ++i
        System.out.println("++i: "+i.incrementAndGet());
        // i++
        System.out.println("i++: "+i.getAndIncrement());
        // 先获取i，再加5
        System.out.println("先获取i，再加5: "+i.getAndAdd(5));
        // 先加5，在获取i
        System.out.println("先加5，在获取i: "+i.addAndGet(5));
    }
}
