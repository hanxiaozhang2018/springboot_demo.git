package com.hanxiaozhang.juc.atomic;

import java.util.concurrent.atomic.LongAdder;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/1/4
 * @since 1.0.0
 */
public class LongAdderNode {

    public static void main(String[] args) {

        LongAdder longAdder = new LongAdder();
        System.out.println(longAdder);
        System.out.println(longAdder.doubleValue());
        longAdder.add(20);
        System.out.println(longAdder);
        longAdder.reset();
        System.out.println(longAdder);
    }

}
