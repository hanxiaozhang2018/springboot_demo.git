package com.hanxiaozhang.juc.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 〈一句话功能简述〉<br>
 * 〈Object.wait(long timeout)与Condition.await(long time, TimeUnit unit) 的使用〉
 * <p>
 * 单机锁，要声明为成员变量，不能放在方法中。放在方法中，只能锁自己，在多线程下解决不了问题。
 *
 * @author hanxinghua
 * @create 2023/3/17
 * @since 1.0.0
 */
public class No1WaitAndAwaitUse {

    private static ReentrantLock lock1 = new ReentrantLock();

    private static Condition condition = lock1.newCondition();

    private static Object lock2 = new Object();


    public static void main(String[] args) {

        lock1.lock();
        try {
            condition.await(2, TimeUnit.SECONDS);
            System.out.println("await可以执行");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            lock1.unlock();
        }

        synchronized (lock2) {
            try {
                lock2.wait(100);
                System.out.println("wait可以执行");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
