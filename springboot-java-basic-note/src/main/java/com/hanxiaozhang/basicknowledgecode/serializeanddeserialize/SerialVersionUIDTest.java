package com.hanxiaozhang.basicknowledgecode.serializeanddeserialize;

import java.io.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈serialVersionUID的取值的作用〉
 * <p>
 * serialVersionUID的取值是Java运行时环境根据类的内部细节自动生成的。如果对类的源代码作了修改，再重新编译，
 * 新生成的类文件的serialVersionUID的取值有可能也会发生变化。
 * <p>
 * 类的serialVersionUID的默认值完全依赖于Java编译器的实现，对于同一个类，用不同的Java编译器编译，有可能会导致不同的serialVersionUID，
 * 也有可能相同。为了提高serialVersionUID的独立性和确定性，强烈建议在一个可序列化类中显示的定义serialVersionUID，为它赋予明确的值。
 * <p>
 * 显式地定义serialVersionUID有两种用途：
 * 1、 在某些场合，希望类的不同版本对序列化兼容，因此需要确保类的不同版本具有相同的serialVersionUID；
 * 2、 在某些场合，不希望类的不同版本对序列化兼容，因此需要确保类的不同版本具有不同的serialVersionUID。
 *
 * @author hanxinghua
 * @create 2024/5/13
 * @since 1.0.0
 */
public class SerialVersionUIDTest {


    public static final String FILE_PATH = "D:/Customer.txt";

    public static void main(String[] args) throws Exception {
        // 序列化Customer对象
        // serializeCustomer();
        // 反序列Customer对象
        Customer customer = deserializeCustomer();
        System.out.println(customer);
    }


    /**
     * 序列化Customer对象
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void serializeCustomer() throws FileNotFoundException,
            IOException {
        Customer customer = new Customer("gacl", 25);
        // ObjectOutputStream 对象输出流
        ObjectOutputStream oo = new ObjectOutputStream(new FileOutputStream(
                new File(FILE_PATH)));
        oo.writeObject(customer);
        System.out.println("Customer对象序列化成功！");
        oo.close();
    }

    /**
     * 反序列Customer对象
     *
     * @return
     * @throws Exception
     * @throws IOException
     */
    private static Customer deserializeCustomer() throws Exception, IOException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
                new File(FILE_PATH)));
        Customer customer = (Customer) ois.readObject();
        System.out.println("Customer对象反序列化成功！");
        return customer;
    }
}


class Customer implements Serializable {

    /**
     * Customer类中定义的serialVersionUID(序列化版本号)
     */
    private static final long serialVersionUID = -1L;

    // Customer类中没有定义serialVersionUID

    private String name;
    private int age;

    // 新添加的sex属性
    private String sex;

    public Customer(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "name=" + name + ", age=" + age;
    }
}