package com.hanxiaozhang.basicknowledgecode;

import java.util.UUID;

/**
 * 〈一句话功能简述〉<br>
 * 〈静态代码块与非静态代码块测试〉
 *
 * @author hanxinghua
 * @create 2021/11/20
 * @since 1.0.0
 */
public class CodeBlock {

    static int count = 100;
    static UUID[] keys = new UUID[count];

    /*static*/ {
        for (int i = 0; i < count; i++) {
            keys[i] = UUID.randomUUID();
        }
    }

    public static void main(String[] args) {

        // new CodeBlock();

        System.out.println(keys[99]);

    }

}
