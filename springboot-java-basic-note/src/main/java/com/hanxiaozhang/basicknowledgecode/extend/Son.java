package com.hanxiaozhang.basicknowledgecode.extend;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/10/18
 * @since 1.0.0
 */
public class Son extends Parent {


    public void method2() {
        method1();
    }
}
