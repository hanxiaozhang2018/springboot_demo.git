package com.hanxiaozhang.basicknowledgecode;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈封箱与拆箱〉
 *
 * @author hanxinghua
 * @create 2023/2/22
 * @since 1.0.0
 */
public class PackAndUnpack {

    /*
     拆箱与封箱概念：
        以Integer与int为例，int类型值转换成Integer对象，这个过程叫做装箱；
     将Integer对象转换成int类型值，这个过程叫做拆箱；装箱和拆箱是自动进行的，非人为转换。

     为什么要有拆箱和装箱？
        Java早年设计缺陷。基础类型是数据，不是对象，也不是Object的子类。
     需要把一个基本类型包装成一个类。

     Integer使用==时：
     1. Integer与new Integer不会相等；
     2. 两个都是非new出来的Integer，如果数在-128到127之间，则是true,否则为false；
     3. 两个都是new出来的Integer，都为false；
     4. int和Integer(无论是否new)比，都为true，因为会把Integer自动拆箱为int再去比。
     Tips：
     1. Integer在-128到127的范围内，不会创建新的对象，而是从IntegerCache中获取的。例如：
        Integer i1 = 120，从缓存中获取；Integer a = 128，
        通过new Integer（128）创建对象，进行装箱。
     2. == 是比较内存地址。

     */


    public static void main(String[] args) {


        Integer i1 = new Integer(188);
        int i2 = 188;
        Integer i3 = new Integer(188);

        Long l1 = new Long(188);
        Long l2 = 188L;

        System.out.println("i1.equals(i2) is " + i1.equals(i2));
        System.out.println("i1.equals(l1) is " + i1.equals(l1));
        System.out.println("i1.equals(l2) is " + i1.equals(l2));
        System.out.println("i1.equals(l2.intValue()) is " + i1.equals(l2.intValue()));
        System.out.println("---- ---- ---- ----");

        //
        ArrayList<Long> list = new ArrayList<>();
        Long long1 = new Long(20230228250111L);
        list.add(20230228250111L);
        list.add(20230228250222L);
        list.add(20230228250333L);
        // 判断集合中是否存在该值
        System.out.println(list.contains(long1.longValue()));

        System.out.println(list.contains(long1));
        System.out.println("---- ---- ---- ----");


        // equals 一定相等  == 会出现不相等
        ArrayList<Long> list2 = new ArrayList<>();
        Long long2 = new Long(20230228250111L);
        Long long3 = new Long(20230228250111L);
        list2.add(long3);
        list2.add(new Long(20230228250222L));
        list2.add(new Long(20230228250333L));

        System.out.println(long2.equals(long3));

        // 判断集合中是否存在该值
        System.out.println(list2.contains(long2.longValue()));

        System.out.println(list2.contains(long2));


    }

}
