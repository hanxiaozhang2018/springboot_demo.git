package com.hanxiaozhang.basicknowledgecode.serializer;

import com.caucho.hessian.io.*;
import com.hanxiaozhang.commomentity.Student;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 〈一句话功能简述〉<br>
 * 〈Hessian序列化工具〉
 *
 * 验证
 *
 * @author hanxinghua
 * @create 2023/12/25
 * @since 1.0.0
 */
@Slf4j
public class HessianSerializerUtil {

    private static SerializerFactory serializerFactory = new SerializerFactory();

    private static final Logger log1
            = Logger.getLogger(HessianSerializerUtil.class.getName());



    public static void main(String[] args) throws Exception {

        Student student = new Student("1", "1");

        byte[] serialize = HessianSerializerUtil.serialize(student);

        // 打印到文件
//        PrintWriter dbg1 = new PrintWriter("D:\\test.txt", "UTF-8");
//        dbg1.write("你好");
//        dbg1.close();

        // 打印到控制台
//        PrintWriter dbg2 = new PrintWriter(System.out);
//        dbg2.write("你好");
//        dbg2.close();


        PrintWriter dbg = new PrintWriter(System.out);

        InputStream is = new ByteArrayInputStream(serialize);

        HessianDebugInputStream in1 = new HessianDebugInputStream(is, dbg);
        in1.startTop2();

//        Object obj = new HessianSerializerInput(in1).readObject();
//        in1.read();


        int code = is.read();

        if (code == 'H') {
            int major = is.read();
            int minor = is.read();

//            in = _factory.getHessian2Input(is);
//
//            Object value = in.readReply(method.getReturnType());
        }
        else if (code == 'r') {
            int major = is.read();
            int minor = is.read();

//            in = _factory.getHessianInput(is);
//            in.startReplyBody();
//
//            Object value = in.readObject(method.getReturnType());
//
//            if (value instanceof InputStream) {
//                value = new ResultInputStream(conn, is, in, (InputStream) value);
//                is = null;
//                conn = null;
//            }
//            else
//                in.completeReply();

        }



        HessianDebugInputStream in = new HessianDebugInputStream(new ByteArrayInputStream(serialize), log1, Level.ALL);
        in.read();
        in.startData1();
        in.startTop2();
        in.startStreaming();
        in.close();


    }


    /**
     * 序列化
     *
     * @param obj
     * @return
     */
    public static byte[] serialize(Object obj) {

        ByteArrayOutputStream ops = new ByteArrayOutputStream();
        HessianOutput out = new HessianOutput(ops);
        out.setSerializerFactory(serializerFactory);
        try {
            out.writeObject(obj);
            out.close();
        } catch (IOException e) {
            log.error("hessian序列化失败", e);
            throw new RuntimeException("hessian序列化失败", e);
        }

        return ops.toByteArray();
    }

    /**
     * 反序列化
     *
     * @param bytes
     * @return
     */
    public static Object deserialize(byte[] bytes) {
        ByteArrayInputStream ips = new ByteArrayInputStream(bytes);
        HessianInput in = new HessianInput(ips);
        in.setSerializerFactory(serializerFactory);

        Object value = null;
        try {
            value = in.readObject();
            in.close();
        } catch (IOException e) {
            log.error("hessian反序列化失败", e);
            throw new RuntimeException("hessian反序列化失败", e);
        }

        return value != null ? value : bytes;
    }

}
