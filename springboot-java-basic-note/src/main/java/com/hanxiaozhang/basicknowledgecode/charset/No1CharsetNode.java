package com.hanxiaozhang.basicknowledgecode.charset;

import java.nio.charset.Charset;

/**
 * 〈一句话功能简述〉<br>
 * 〈字符集〉
 *
 * @author hanxinghua
 * @create 2023/5/6
 * @since 1.0.0
 */
public class No1CharsetNode {

    public static void main(String[] args) {

        Charset defaultCharset = Charset.defaultCharset();
        System.out.println("查询默认的字符集：" + defaultCharset);

    }



}
