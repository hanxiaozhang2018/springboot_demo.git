package com.hanxiaozhang.basicknowledgecode.number;

/**
 * 〈一句话功能简述〉<br>
 * 〈double类型〉
 *
 * @author hanxinghua
 * @create 2023/12/19
 * @since 1.0.0
 */
public class DoubleNode {


    public static void main(String[] args) {

        double d = 65.383;

        System.out.println("double类型值：" + d);
        // 根据IEEE754浮点"double format"位布局，返回指定浮点值的表示
        System.out.println("根据IEEE754浮点\"double format\"位布局，返回指定浮点值的表示：" + Double.doubleToRawLongBits(d));
        // double类型二进制表示
        System.out.println("double类型二进制表示： " + Long.toBinaryString(Double.doubleToLongBits(d)));
        // double类型16进制表示
        System.out.println("double类型16进制表示： " + Long.toHexString(Double.doubleToRawLongBits(d)));

        System.out.println("---- ---- double类型，15或16位十进制有效数 ---- ----");
        // double类型，15或16位十进制有效数
        double d1 = 123456.1234567890;
        double d2 = 123456.12345678901;
        System.out.println("d1：" + d1);
        System.out.println("d2：" + d2);
        System.out.println("d1 二进制：" + Long.toBinaryString(Double.doubleToRawLongBits(d1)));
        System.out.println("d2 二进制：" + Long.toBinaryString(Double.doubleToRawLongBits(d2)));
        System.out.println("d1 16制：" + Double.doubleToRawLongBits(d1));
        System.out.println("d2 16制：" + Double.doubleToRawLongBits(d2));
    }
}
