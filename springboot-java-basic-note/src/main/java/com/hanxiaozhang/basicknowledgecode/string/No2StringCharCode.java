package com.hanxiaozhang.basicknowledgecode.string;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/6/12
 * @since 1.0.0
 */
public class No2StringCharCode {

    static char b;

    public static void main(String[] args) {

        System.out.println("---- char 语法 ----");
        char a = 'a';
        char c = 0;
        System.out.println("---- char 默认值是0 ----");
        System.out.println(c);
        System.out.println(b == c);


        System.out.println("---- charAt 与 (int)char ----");
        String str = "123abcABC";
        char c1 = str.charAt(0);
        System.out.println(c1);
        System.out.println(c1 + " 对应的int值 " + (int) c1);
        // a 97
        char c2 = str.charAt(3);
        System.out.println(c2);
        System.out.println(c2 + " 对应的int值 " + (int) c2);
        // A 65
        char c3 = str.charAt(6);
        System.out.println(c3);
        System.out.println(c2 + " 对应的int值 " + (int) c3);


    }
}
