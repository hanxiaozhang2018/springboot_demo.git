package com.hanxiaozhang.basicknowledgecode.map;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/28
 * @since 1.0.0
 */
public class TreeMapNode {


    public static void main(String[] args) {

        TreeMap<Integer, String> treeMap = new TreeMap();

        treeMap.put(2, "two");
        treeMap.put(1, "one");
        treeMap.put(3, "three");
        treeMap.put(6, "six");
        treeMap.put(5, "five");

        // tailMap(K fromKey)：返回大于或等于fromKey的键值对。
        SortedMap<Integer, String> sortedMap=treeMap.tailMap(3);
        System.out.println("Tail map values: "+sortedMap);


    }

}
