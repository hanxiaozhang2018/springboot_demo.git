package com.hanxiaozhang.basicknowledgecode.whileknowledge;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/10/8
 * @since 1.0.0
 */
public class No1WhileNode {

    static boolean a;

    public static void main(String[] args) {


        System.out.println(a);

        whileNode(10);
        doWhileNode(0);

    }

    private static void whileNode(int i) {
        System.out.println("---- ---- ---- ----");
        while (i > 0) {
            System.out.println(i);
            i--;
        }
    }

    private static void doWhileNode(int i) {
        System.out.println("---- ---- ---- ----");
        do {
            System.out.println(i);
            i--;
        } while (i > 0);
    }


}
