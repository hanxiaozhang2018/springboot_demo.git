package com.hanxiaozhang.basicknowledgecode.list;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/5/13
 * @since 1.0.0
 */
public class SubListTest {

    public static void main(String[] args) {

        List<Object> lists = new ArrayList<Object>();

        lists.add("1");
        lists.add("2");
        lists.add("3");
        lists.add("4");

        List<Object> tempList = lists.subList(2, lists.size());

        // 添加6：尽管提前subList操作，但是添加6，子集合也包含6
        tempList.add("6");

        // [3, 4, 6]
        System.out.println(tempList);

        // [1, 2, 3, 4, 6]
        System.out.println(lists);

    }


}
