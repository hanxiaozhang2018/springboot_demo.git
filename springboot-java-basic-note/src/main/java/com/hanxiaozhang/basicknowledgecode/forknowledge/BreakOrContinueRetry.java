package com.hanxiaozhang.basicknowledgecode.forknowledge;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2021/11/30
 * @since 1.0.0
 */
public class BreakOrContinueRetry {

    public static void main(String[] args) {

        breakSign();
        breakNoSign();
        continueSign();
        continueNoSign();
        breakSignByThreeLayers();
    }

    private static void breakSign() {
        System.out.print("breakSign start => ");
        sign:
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 3) {
                    System.out.print(j + " || ");
                    break sign;
                } else {
                    System.out.print(j + ", ");
                }
            }
            System.out.print("come here");
        }
        System.out.println(" <= end");
    }

    private static void breakNoSign() {
        System.out.print("breakNoSign start => ");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 3) {
                    System.out.print(j + " || ");
                    break;
                } else {
                    System.out.print(j + ", ");
                }
            }
        }
        System.out.println(" <= end");
    }

    private static void continueSign() {
        System.out.print("continueSign start  => ");
        sign:
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 3) {
                    System.out.print(j + " || ");
                    continue sign;
                } else {
                    System.out.print(j + ", ");
                }
            }
            System.out.print("come here");
        }
        System.out.println(" <= end");
    }

    private static void continueNoSign() {
        System.out.print("continueNoSign start  => ");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 3) {
                    continue;
                } else {
                    System.out.print(j + ", ");
                }
            }
        }
        System.out.println(" <= end");
    }


    private static void breakSignByThreeLayers() {
        System.out.print("breakSignByThreeLayers start => ");
        sign:
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 6; k++) {
                    if (k == 3) {
                        System.out.print(k + " || ");
                        break sign;
                    } else {
                        System.out.print(k + ", ");
                    }
                }
            }
        }
        System.out.println(" <= end");
    }


}
