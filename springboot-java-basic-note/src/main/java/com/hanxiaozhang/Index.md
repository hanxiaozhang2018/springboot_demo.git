# 目录
+ basicknowledgecode 基础知识相关代码
+ collection 集合相关代码
+ commomentity 共用实体
+ exception 异常相关代码
+ jdk8 jdk8新特性相关代码
+ juc 并发包相关代码
+ regularexpression 正则
+ smallfunction 小功能
+ sourcecode 源码相关代码
+ test 测试
+ test1 测试