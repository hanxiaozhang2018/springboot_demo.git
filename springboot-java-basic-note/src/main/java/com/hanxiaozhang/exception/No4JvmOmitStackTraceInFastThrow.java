package com.hanxiaozhang.exception;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 〈一句话功能简述〉<br>
 * 〈验证JVM Fast Throw机制，测试该机制是否持久生效〉
 * <p>
 *  第一天，某一时间使用Fast Throw机制。
 *  验证，第二天，会持久出现吗？
 *
 * @author hanxinghua
 * @create 2023/5/5
 * @since 1.0.0
 */
public class No4JvmOmitStackTraceInFastThrow {

    public static void main(String[] args) throws Exception {
        LocalTime time = LocalTime.now();
        LocalDate day = LocalDate.now();

        No4FastThrowThread npeThread = new No4FastThrowThread();
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        while (true) {
            LocalTime now = LocalTime.now();
            LocalDate dayNow = LocalDate.now();
            if (time.getHour() == now.getHour()) {
                executorService.execute(npeThread);
                // 稍微sleep一下
                Thread.sleep(200);
            }
            if ((day.getDayOfMonth()+1) == dayNow.getDayOfMonth()) {
                executorService.execute(npeThread);
                // 稍微sleep一下
                Thread.sleep(200);
            }
        }
    }
}


class No4FastThrowThread extends Thread {
    private static int count = 0;

    public No4FastThrowThread() {

    }

    @Override
    public void run() {
        try {
            System.out.println(this.getClass().getSimpleName() + "--" + (++count));
            String str = null;
            System.out.println(str.length());
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}