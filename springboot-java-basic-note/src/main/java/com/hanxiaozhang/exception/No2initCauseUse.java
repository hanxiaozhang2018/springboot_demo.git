package com.hanxiaozhang.exception;

import java.awt.*;

/**
 * 〈一句话功能简述〉<br>
 * 〈initCause的使用〉
 * <p>
 * 在catch中throw一个新的异常，但是又不希望丢失引起异常的原始异常，
 * 可以使用initCause()设置原始异常到我们的新异常中。
 * 在堆栈信息中，以Caused by: XXX 展示
 *
 *
 * @author hanxinghua
 * @create 2023/4/25
 * @since 1.0.0
 */
public class No2initCauseUse {

    public static void main(String[] args) throws Exception {
        test1();

    }


    private static void test() {
        try {
            Object object = null;
            object.toString();
        } catch (Exception e) {
            RuntimeException runtimeException = new RuntimeException("测试一下");
            runtimeException.initCause(e);
            throw runtimeException;
        }
    }
    private static void test1() {
        try {
            throw new AWTException("hhh");
        } catch (Exception e) {
            throw new RuntimeException("测试一下",e);
        }
    }


}
