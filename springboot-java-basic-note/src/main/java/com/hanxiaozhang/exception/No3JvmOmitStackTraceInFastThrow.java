package com.hanxiaozhang.exception;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 〈一句话功能简述〉<br>
 * 〈验证JVM Fast Throw机制〉
 * <p>
 * -XX:-OmitStackTraceInFastThrow
 *
 * @author hanxinghua
 * @create 2023/5/5
 * @since 1.0.0
 */
public class No3JvmOmitStackTraceInFastThrow {

    public static void main(String[] args) throws Exception {
        FastThrowThread npeThread = new FastThrowThread();
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            executorService.execute(npeThread);
            // 稍微sleep一下
            Thread.sleep(2);
        }
    }
}


class FastThrowThread extends Thread {
    private static int count = 0;

    private Throwable t;

    public FastThrowThread() {

    }

    @Override
    public void run() {
        try {
            System.out.println(this.getClass().getSimpleName() + "--" + (++count));
            String str = null;

            if (count < 20000 || count > 30000) {
                System.out.println(str.length());
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}