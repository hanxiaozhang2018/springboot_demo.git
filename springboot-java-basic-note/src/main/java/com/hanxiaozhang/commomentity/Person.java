package com.hanxiaozhang.commomentity;

import lombok.Data;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2024/8/28
 * @since 1.0.0
 */
@Data
public class Person {
    private String personNo;

    private String name;

    public Person(String personNo, String name) {
        this.personNo = personNo;
        this.name = name;
    }
}

