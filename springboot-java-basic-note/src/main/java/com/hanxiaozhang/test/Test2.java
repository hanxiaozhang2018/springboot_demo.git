package com.hanxiaozhang.test;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/3/19
 * @since 1.0.0
 */
public class Test2 {

    public static void main(String[] args) {
        Integer i = 5;

        int[] arr = {1};
        change(arr);
        System.out.println(arr[0]);
        try {
            test();
        } catch (Exception e) {
            System.out.println("Exception from main");
        }

    }

    private static void change(int[] arr) {
        arr = new int[]{100};
    }


    public static boolean test() throws Exception {
        try {
            // 1.抛出异常
            throw new Exception("Something error");
            // 2.捕获的异常匹配(声明类或其父类），进入控制块
        } catch (Exception e) {
            // 3.打印
            System.out.println("Exception from e");
            // 4. return前控制转移到finally块,执行完后再返回
            return false;
        } finally {
            // 5. 控制转移，直接返回，吃掉了异常
            return true;
        }
    }
}
