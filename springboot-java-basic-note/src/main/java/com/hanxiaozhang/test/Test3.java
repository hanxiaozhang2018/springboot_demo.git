package com.hanxiaozhang.test;

import com.hanxiaozhang.commomentity.Student;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/7/6
 * @since 1.0.0
 */
public class Test3 {


    public static void main(String[] args) {

        System.out.println(BigDecimal.ZERO.compareTo(BigDecimal.valueOf(-1)));
        System.out.println(BigDecimal.ZERO.compareTo(BigDecimal.ZERO));
        System.out.println(BigDecimal.ZERO.compareTo(BigDecimal.ONE));
        System.out.println(BigDecimal.valueOf(1000).stripTrailingZeros().toPlainString());

        String lockPath = "/carcommons_lock/curator_recipes_lock/";

        System.out.println(lockPath.substring(0, lockPath.length() - 1));

        System.out.println(Integer.valueOf("00000001", 2));

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        Iterator<Integer> iterator = list.iterator();
        System.out.println(iterator.next());
        System.out.println(iterator.next());

        List<Integer> integerList = list.stream().filter(x -> x > 10).collect(Collectors.toList());
        System.out.println("integerList size"+integerList.size());

        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("1", "1", "1"));
        studentList.add(new Student("2", "2", "2"));

        list.stream().filter(x -> studentList.stream().map(y -> {
            System.out.println(x + "-" + y.getId());
            return Integer.parseInt(y.getId());
        }).collect(Collectors.toList()).contains(x)).collect(Collectors.toList());

        List<Integer> collect = studentList.stream().map(y -> {
            return Integer.parseInt(y.getId());
        }).collect(Collectors.toList());
        list.stream().filter(x -> collect.contains(x)).collect(Collectors.toList());

    }

}
