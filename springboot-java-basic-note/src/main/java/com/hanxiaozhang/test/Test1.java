package com.hanxiaozhang.test;

import com.hanxiaozhang.commomentity.Student;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/3/17
 * @since 1.0.0
 */
public class Test1 {

    public static void main(String[] args) {
        Student student1 = new Student("1", "小张", "beijing");
        Student student2 = new Student("1", "小张", "tianjin");

        System.out.println(student1 == student2);
        System.out.println(student1.equals(student2));

        int a = 10;
        int length = 8;
        System.out.println(a % length);
        System.out.println((length - 1) & a);


        String str1 = new String("123") + new String("456");
        String str2 = new String("123456");
        String intern = str1.intern();
        System.out.println(intern == str1);
        System.out.println(intern == str2.intern());


    }


}
