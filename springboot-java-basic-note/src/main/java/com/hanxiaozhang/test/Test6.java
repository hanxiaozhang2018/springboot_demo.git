package com.hanxiaozhang.test;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/8/3
 * @since 1.0.0
 */
public class Test6 {


    String url = "2";


    public static void main(String[] args) {

        Test6 test6 = new Test6();
        System.out.println(test6.url);
        test6.test1(test6.url);
        System.out.println(test6.url);
        test6.test2();
        System.out.println(test6.url);
        test6.test();
        System.out.println(test6.url);

        Test6 test7 = new Test6();
        System.out.println(test7.url);

//        System.out.println(String.format("%s你好%%哈%s", 1, 2));
//
//        List<String> list = null;
//        for (String s : list) {
//
//        }
//
//
//        System.out.println(Integer.valueOf("00000100", 2));
//        test(null, 1);
//
//
//        int a = 3, b = 4;
//        System.out.println("a is " + a + " b is " + b);
//        swap(a, b);
//        System.out.println("a is " + a + " b is " + b);
    }


    private static void swap(int a, int b) {
        int temp;
        temp = a;
        a = b;
        b = temp;
        System.out.println("a is " + a + " b is " + b);
    }


    private static void test(Boolean notAddFlag, int i) {
        if (notAddFlag) {
            System.out.println(1);
        }

        Boolean flag = notAddFlag && i == 1;
        System.out.println(flag);
    }


    private void test() {
        url = url + 1;
        System.out.println("test() is " + url);
    }


    private void test1(String url) {
        url = url + 1;
        System.out.println("test1() is " + url);
    }

    private void test2() {
        String url1 = url + 1;
        System.out.println("test2() is " + url1);
    }
}
