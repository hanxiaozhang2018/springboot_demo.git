package com.hanxiaozhang.test;

import java.util.HashMap;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/1/21
 * @since 1.0.0
 */
public class ConstructorTest {

    public static void main(String[] args) {
        Map<String,String> map =new HashMap(4);
        map.put("1","1");
        A a = new A(map);
        a.method();
        map.put("1","2");
        a.method();

    }

}


class A {
    public Map map;

    public A(Map map) {
        this.map = map;
    }

    public void method() {
        System.out.println(map.get("1"));
    }
}