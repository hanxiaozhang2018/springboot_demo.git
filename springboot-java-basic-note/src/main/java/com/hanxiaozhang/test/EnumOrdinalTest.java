package com.hanxiaozhang.test;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/14
 * @since 1.0.0
 */
public class EnumOrdinalTest {

    public static void main(String[] args) {
        System.out.println(Ordinal.ONE.ordinal());
        System.out.println(Ordinal.TWO.ordinal());

    }
}
 enum Ordinal {
     ONE("1"),

     TWO("2");

    private String desc;

    private Ordinal(String desc){
        this.desc = desc;
    }

    public String getDesc(){
        return desc;
    }

}
