package com.hanxiaozhang.test;

/**
 * 〈一句话功能简述〉<br>
 * 〈抽象类测试〉
 *
 * @author hanxinghua
 * @create 2023/4/8
 * @since 1.0.0
 */
public abstract class AbstractClassTest {


    protected abstract void test();
}
