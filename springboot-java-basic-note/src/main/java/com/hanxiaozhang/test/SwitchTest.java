package com.hanxiaozhang.test;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/11/30
 * @since 1.0.0
 */
public class SwitchTest {

    public static void main(String[] args) {

        switchTest(4);
    }

    private static void switchTest(int type) {
        switch (type) {
            case 0:
            case 1:
            case 2:
                System.out.println("0 1 2");
                break;
            case 3:
                System.out.println("3");
                break;
            default:
                System.out.println("default");
                break;
        }
    }
}
