package com.hanxiaozhang.test;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/8/17
 * @since 1.0.0
 */
public class Test4 {

    public static void main(String[] args) {

        String userNoStr ="435,325,";
        if (userNoStr.endsWith(",")) {
            userNoStr = userNoStr.substring(0, userNoStr.lastIndexOf(","));
        }

        System.out.println(userNoStr);

        String errMsg ="12243";

        errMsg = String.format("%s,异常信息是:%s", errMsg, "33333");
        System.out.println(errMsg);

        Object o =null;
        HashMap map1 =(HashMap)o;


        System.out.println(true || 1/0==1);


        String launchStr = "https://openapi.alipay.com/gateway.do?44444";
        if (StringUtils.isNotEmpty(launchStr)) {
            launchStr = launchStr.substring(launchStr.indexOf("?")+1);
        }
        System.out.println(launchStr);

        JSON.toJSONString(null);

        if(true|| (1/0)==0){

        }

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        int size = list.size();
        for (int i = 0; i < size - 1; i++) {
            System.out.println(list.get(i));
        }
        System.out.println("------");
        System.out.println(new BigDecimal(10).compareTo(BigDecimal.ZERO));

        Map<String, String> map = new HashMap(4);
        map.put("1", null);
        map.put("2", null);
        System.out.println(map.size());

        // 不能转换String类型的非整数
        System.out.println(Integer.valueOf("2.11"));


    }


}
