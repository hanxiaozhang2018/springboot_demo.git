package com.hanxiaozhang.test;

import com.hanxiaozhang.commomentity.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/12/25
 * @since 1.0.0
 */
public class Test7 {

    public static void main(String[] args) {


        Student student1 = new Student("1", "1");


        method4(student1);

        method5(student1);

        if (student1.getFlag()) {

        }

        List<Student> studentList = new ArrayList<>();
        studentList.add(student1);
        Student student2 = new Student("2", "1");
        studentList.add(student2);
        Student student3 = new Student("3", "1");
        studentList.add(student3);
        Student student4 = new Student("4", "1");
        studentList.add(student4);

        System.out.println(studentList.indexOf(student2));
        System.out.println(studentList.indexOf(student3));


        List<String> list = null;

        list.stream().collect(Collectors.toList());

        method1();

        // 包装类型的Boolean为null，会报空指针
        Student student = new Student();
        student.setFlag(null);
        if (student.getFlag()) {

        }


        System.out.println("test7");
    }


    private static void method4(Student student) {

        student.setAddress("1");
    }

    private static void method5(Student student) {
        System.out.println(student);
    }

    private static void method1() {
        method2();
    }

    private static void method2() {
        method3();
    }

    private static void method3() {

        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        String className = stack[2].getClassName();
        int indexOf = className.lastIndexOf(".");
        if (indexOf > -1) {
            className = className.substring(className.lastIndexOf("."));
        }
        System.out.println(className);
    }

}
