package com.hanxiaozhang.test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/12
 * @since 1.0.0
 */
public class StreamDateSoreTest {

    public static void main(String[] args) {

        List<Date> list =new ArrayList<>();
        list.add(new Date(2022,1,2,12,22,22));
        list.add(new Date(2022,1,2,13,22,22));
        list.add(new Date(2022,1,2,14,22,22));

        list.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()).forEach(x->{
            System.out.println(x);
        });
        System.out.println("-----");
        list.stream().sorted().collect(Collectors.toList()).forEach(x->{
            System.out.println(x);
        });

    }

}
