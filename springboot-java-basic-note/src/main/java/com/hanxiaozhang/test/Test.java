package com.hanxiaozhang.test;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.hanxiaozhang.commomentity.Student;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/3/13
 * @since 1.0.0
 */
public class Test {


    public static void main(String[] args) throws CloneNotSupportedException {



        // 验证Json序列化null的情况
        Student student1 = JSON.parseObject("{}", Student.class);
        System.out.println(student1);
        student1.getName();


        Student student = new Student();
        student.clone();

        int y = 2;
        int nextY=++y > 8 ? 1 : y;
        System.out.println(nextY);

        String ip ="/127.9.2.1:34678";
        System.out.println(ip.substring(1, ip.indexOf(":")));

        System.out.println((8 - 1) & 8);

//        List<String> list = getList1();
//        List<Integer> result = new ArrayList<>();
//        list.forEach(x -> {
//            result.add(countBucketIndex(x, 8));
//        });
//        result.stream().collect(Collectors.groupingBy(x -> x)).forEach((k, v) -> {
//            System.out.println(k + ": " + v);
//        });



//
//
//        System.out.println(Math.ceil(12.3));
//
//        double d = 10;
//        System.out.println(d / 3);
//        System.out.println(Math.ceil(d / 3));
//        System.out.println(10 / 3);
//
//        int j = 1;
//        System.out.println("_" + ++j);
//
//        int z = 1;
//        System.out.println("_" + z++);
//
//
//        for (int i = 0; i < 200; i++) {
//            if (check2Power(i)) {
//                System.out.println(i);
//            }
//        }


    }


    /**
     * 检测是否2的幂次方
     *
     * @param value
     * @return
     */
    public static boolean check2Power(Integer value) {
        if (value == null || value < 1) {
            return false;
        }
        return (value & (value - 1)) == 0;
    }

    private static int countBucketIndex(Object key, int clickBucketSize) {
        int h, hash;
        hash = (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
        return ((clickBucketSize - 1) & hash) + 1;
    }

    private static List<String> getList() {
        List list = new ArrayList();
        list.add("ins-l9x2taqx");
        list.add("ins-5mchywat");
        list.add("ins-bk4tvtgv");
        list.add("ins-odcbd8i5");
        list.add("ins-hsdve8ht");
        list.add("ins-fhxxmua1");
        list.add("ins-461il7wp");
        list.add("ins-hca06kh1");
        list.add("ins-brj3atzj");
        list.add("ins-mmhp2lqd");
        list.add("ins-k74aa30l");
        list.add("ins-0lhanhe5");
        list.add("ins-ohkbobk7");
        list.add("ins-9k3i7oi5");
        list.add("ins-k7daqxs9");
        list.add("ins-ajuy4yxv");
        list.add("ins-qsgo6nlz");
        list.add("ins-qcecz7mt");
        list.add("ins-aj3w2mnb");
        list.add("ins-fgrdzz5b");
        list.add("ins-h83mglpn");
        list.add("ins-e6tozbov");
        list.add("ins-bidd9xj5");
        list.add("ins-bqarty5l");
        list.add("ins-0s5nv06b");
        list.add("ins-afpqp4j3");
        list.add("ins-85ljekn7");
        list.add("ins-qjkwyeh5");
        list.add("ins-jrey0xjb");
        list.add("ins-46i9q0ef");
        list.add("ins-5m3jca4t");
        list.add("ins-ehh44vs1");
        list.add("ins-14khggdr");
        list.add("ins-d5wlwy4v");
        list.add("ins-j37oo54f");
        list.add("ins-5y3wg2o3");
        list.add("ins-1clgaocv");
        list.add("ins-biw9gqn7");
        list.add("ins-eu1gbmzl");
        list.add("ins-eh7pm0iv");
        list.add("ins-9v5yhcf7");
        list.add("ins-2v75ersd");
        list.add("ins-n9l30sv7");
        list.add("ins-7x4fk8nj");
        list.add("ins-0hlj8m4t");
        list.add("ins-ex9zu6tf");
        list.add("ins-71b1z3zj");
        list.add("ins-ldsbp03t");
        list.add("ins-l1xnx36t");
        list.add("ins-cuhyahlv");
        list.add("ins-d2mcdsjt");
        list.add("ins-aay4w7rh");
        list.add("ins-gk7uy7sb");
        list.add("ins-poeh87af");
        list.add("ins-r0ajb9er");
        list.add("ins-ovwd8pqn");
        list.add("ins-1rot451n");
        list.add("ins-mqfprekn");
        list.add("ins-6eq5lifd");
        list.add("ins-aoawrtc1");
        list.add("ins-pfm34srv");
        list.add("ins-rn8v1u4v");
        list.add("ins-fgqra3yb");
        list.add("ins-4qh8m5tx");
        list.add("ins-m2j84m9b");
        list.add("ins-6lucxlhf");
        list.add("ins-nopxwjux");
        list.add("ins-hnmboxlt");
        list.add("ins-owj0y469");
        list.add("ins-7i3eq5or");
        list.add("ins-jv1eg8kn");
        list.add("ins-pd3lk0rl");
        list.add("ins-raoo0qcn");
        list.add("ins-341kt4sv");
        list.add("ins-cme2mgw9");
        list.add("ins-arafei13");
        list.add("ins-p48yuhtt");
        list.add("ins-mha9har3");
        list.add("ins-fgj0li4p");
        list.add("ins-9s1eklvp");
        list.add("ins-3ywl4rkz");
        list.add("ins-2qxd0pq9");
        list.add("ins-fq1wnmhb");
        list.add("ins-4qzmuczn");
        list.add("ins-jis4brvp");
        list.add("ins-d3fx9d5v");
        list.add("ins-et16anyd");
        list.add("ins-3v7vjd1l");
        list.add("ins-89k31uwb");
        list.add("ins-5uzp2lcz");
        list.add("ins-rqpktvmr");
        list.add("ins-afqxajfr");
        list.add("ins-a7rbugsn");
        list.add("ins-rry80hnp");
        list.add("ins-mu39vnev");
        list.add("ins-h8tqnrxd");
        list.add("ins-ngrpdsmd");
        list.add("ins-g5ubvf43");
        list.add("ins-8dezkt7v");
        list.add("ins-9wedrvhj");
        list.add("ins-oohgeqx5");
        list.add("ins-3bf0cj4d");
        list.add("ins-rjs2cdfx");
        list.add("ins-6otiljrh");
        list.add("ins-o9ersrxr");
        list.add("ins-hcblf25d");
        list.add("ins-mt3d7s5d");
        list.add("ins-h7u57arr");
        list.add("ins-cf90sfez");
        list.add("ins-2rxjcvk5");
        list.add("ins-nwbi11t3");
        list.add("ins-90oc4bhp");
        list.add("ins-pcwyabqj");
        list.add("ins-18f8ob1h");
        list.add("ins-mejxpo4t");
        list.add("ins-n2fk6wbp");
        list.add("ins-c26zv3pl");
        list.add("ins-nct36w99");
        list.add("ins-46ahzbfv");
        list.add("ins-anvvugox");
        list.add("ins-8xgdujhr");
        list.add("ins-9ffddunb");
        list.add("ins-iftfnrev");
        list.add("ins-95d1f1ah");
        list.add("ins-14kao1gt");
        list.add("ins-a0xxz6pz");
        list.add("ins-76aqod8v");
        return list;
    }

    private static List<String> getList1() {
        List list = new ArrayList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        list.add("11");
        list.add("12");
        list.add("13");
        list.add("14");
        list.add("15");
        list.add("16");
        list.add("17");
        list.add("18");
        list.add("19");
        list.add("20");
        list.add("21");
        list.add("22");
        list.add("23");
        list.add("24");
        list.add("25");
        list.add("26");
        list.add("27");
        list.add("28");
        list.add("29");
        list.add("30");
        list.add("31");
        list.add("32");
        list.add("33");
        list.add("34");
        list.add("35");
        list.add("36");
        list.add("37");
        list.add("38");
        list.add("39");
        list.add("40");
        list.add("41");
        list.add("42");
        list.add("43");
        list.add("44");
        list.add("45");
        list.add("46");
        list.add("47");
        list.add("48");
        list.add("49");
        list.add("50");
        list.add("51");
        list.add("52");
        list.add("53");
        list.add("54");
        list.add("55");
        list.add("56");
        list.add("57");
        list.add("58");
        list.add("59");
        list.add("60");
        list.add("61");
        list.add("62");
        list.add("63");
        list.add("64");
        list.add("65");
        list.add("66");
        list.add("67");
        list.add("68");
        list.add("69");
        list.add("70");
        list.add("71");
        list.add("72");
        list.add("73");
        list.add("74");
        list.add("75");
        list.add("76");
        list.add("77");
        list.add("78");
        list.add("79");
        list.add("80");
        list.add("81");
        list.add("82");
        list.add("83");
        list.add("84");
        list.add("85");
        list.add("86");
        list.add("87");
        list.add("88");
        list.add("89");
        list.add("90");
        list.add("91");
        list.add("92");
        list.add("93");
        list.add("94");
        list.add("95");
        list.add("96");
        list.add("97");
        list.add("98");
        list.add("99");
        list.add("100");
        list.add("101");
        list.add("102");
        list.add("103");
        list.add("104");
        list.add("105");
        list.add("106");
        list.add("107");
        list.add("108");
        list.add("109");
        list.add("110");
        list.add("111");
        list.add("112");
        list.add("113");
        list.add("114");
        list.add("115");
        list.add("116");
        list.add("117");
        list.add("118");
        list.add("119");
        list.add("120");
        list.add("121");
        list.add("122");
        list.add("123");
        list.add("124");
        list.add("125");
        list.add("126");
        list.add("127");

        return list;
    }

}
