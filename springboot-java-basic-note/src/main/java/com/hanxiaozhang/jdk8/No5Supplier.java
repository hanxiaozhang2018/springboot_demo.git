package com.hanxiaozhang.jdk8;

import java.util.function.Supplier;

/**
 * 〈一句话功能简述〉<br>
 * 〈Supplier的使用〉
 *
 * @author hanxinghua
 * @create 2022/8/22
 * @since 1.0.0
 */
public class No5Supplier {

    public static void main(String[] args) {
        /*
        Supplier接口被称之为生产型接口，指定接口的泛型是什么类型，
        get方法就会生产什么类型的数据。
        接口只有一个方法：T get();
        Tips: BooleanSupplier、DoubleSupplier、IntSupplier...
         */

        // 1.使用
        Supplier<String> word =()->"hello word!";
        System.out.println(word.get());
        System.out.println("---- ---- ---- ----");

        // 2.求数组元素最大值
        int[] arr = {1,-5,7,2,10,3};
        int maxValue = getMax(()->{
            int max = arr[0];
            for (int i = 1; i < arr.length; i++) {
                if (max < arr[i]) {
                    max = arr[i];
                }
            }
            return max;
        });
        System.out.println(maxValue);
    }

    /**
     * 获取最大元素
     *
     * @param supplier
     * @return
     */
    public static int getMax(Supplier<Integer> supplier) {
        return supplier.get();
    }


}
