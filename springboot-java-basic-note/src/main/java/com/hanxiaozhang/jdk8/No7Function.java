package com.hanxiaozhang.jdk8;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 〈一句话功能简述〉<br>
 * 〈Function〉
 *
 * @author hanxinghua
 * @create 2022/8/23
 * @since 1.0.0
 */
public class No7Function {

    public static void main(String[] args) {

        /*
        Function<T,R>接口：一个类型的数据得到另一个类型的数据，前者称为前置条件，后者称为后置条件。
        方法1：R apply(T t)：根据类型T的参数获取类型R的结果
        方法2：compose(Function<? super V, ? extends T> before)
        方法3：andThen(Function<? super R, ? extends V> after)
        方法4：Function<T, T> identity()
        Tips:BiFunction支持两个参数、DoubleToIntFunction、DoubleToLongFunction...
         */

        // 1.apply(T t)
        Function<String, String> toUpperCase = str -> str.toUpperCase();
        System.out.println(toUpperCase.apply("hello"));
        change("10", (str) -> Integer.parseInt(str));
        System.out.println("---- ---- ---- ----");

        Function<Integer, Integer> multiplyFunction = i -> i * 2;
        Function<Integer, Integer> addFunction = i -> i + 3;

        // 2.compose()：先执行入参的addFunction，然后再执行multiplyFunction (2 + 3) * 2
        System.out.println(multiplyFunction.compose(addFunction).apply(2));
        System.out.println("---- ---- ---- ----");

        // 3.andThen()：后执行入参的addFunction，先执行multiplyFunction  (2 * 2) + 3
        System.out.println(multiplyFunction.andThen(addFunction).apply(2));
        System.out.println("---- ---- ---- ----");


        // 4.identity()：静态方法，返回自己本身
        Stream<String> stream = Stream.of("I", "love", "you", "too");
        Map<String, Integer> map = stream.collect(Collectors.toMap(Function.identity(), String::length));
        System.out.println(map);

    }

    public static void change(String str, Function<String, Integer> func) {
        Integer in = func.apply(str);
        System.out.println(in + 10);
    }


}
