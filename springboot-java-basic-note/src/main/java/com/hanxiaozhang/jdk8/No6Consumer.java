package com.hanxiaozhang.jdk8;

import java.util.function.Consumer;

/**
 * 〈一句话功能简述〉<br>
 * 〈Consumer〉
 *
 * @author hanxinghua
 * @create 2022/8/22
 * @since 1.0.0
 */
public class No6Consumer {

    public static void main(String[] args) {

        /*
        Consumer接口则正好与Supplier接口相反，它不是生产一个数据，而是消费一个数据，其数据类型由泛型决定。
        方法1：void accept(T t)：意为消费一个指定泛型的数据，至于如何消费（使用），需要自定义。
        方法2：Consumer<T> andThen(Consumer<? super T> after)：
              可以把两个或多个Consumer接口组合到一起，再对数据进行消费
        Tips:BiConsumer支持两个参数、DoubleConsumer、IntConsumer...
         */

        // 1.使用
        Consumer<String> first = x -> System.out.println("1." + x.toLowerCase());
        Consumer<String> second = y -> System.out.println("2." + y);
        Consumer<String> result = first.andThen(second);
        result.accept("A");
        System.out.println("---- ---- ---- ----");

        // 2.格式化打印信息
        String[] arr = {"朱古力，18", "猪猪侠，20", "猪猪猪，22"};
        printInfo(arr,
                (String str) -> {
                    String name = str.split("，")[0];
                    System.out.print("姓名：" + name + "，");
                },
                (String str) -> {
                    String age = str.split("，")[1];
                    System.out.println("年龄：" + age + "。");
                });

    }

    public static void printInfo(String[] arr, Consumer<String> consumer1, Consumer<String> consumer2) {
        for (String str : arr) {
            consumer1.andThen(consumer2).accept(str);
        }
    }


}
