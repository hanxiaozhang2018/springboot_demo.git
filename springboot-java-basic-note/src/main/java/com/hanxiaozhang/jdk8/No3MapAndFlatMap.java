package com.hanxiaozhang.jdk8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/8/4
 * @since 1.0.0
 */
public class No3MapAndFlatMap {

    /**
     * 组编号
     */
    private static Integer group = 0;

    /**
     * 学生编号
     */
    private static int student = 1;

    public static void main(String[] args) {

        /*
         map方法返回的是一个object，map将流中的当前元素 替换为 此返回值
         flatMap方法返回的是一个stream，flatMap将流中的当前元素 替换为 此返回流拆解的流元素
         */
        List<String[]> eggs = new ArrayList<>();
        // 第一箱鸡蛋
        eggs.add(new String[]{"鸡蛋_1", "鸡蛋_1", "鸡蛋_1", "鸡蛋_1", "鸡蛋_1"});
        // 第二箱鸡蛋
        eggs.add(new String[]{"鸡蛋_2", "鸡蛋_2", "鸡蛋_2", "鸡蛋_2", "鸡蛋_2"});
        System.out.println("---- ---- ---- ----");
        eggs.stream()
                .map(x -> Arrays.stream(x).map(y -> y.replace("鸡", "煎")))
                .forEach(x -> System.out.println("组" + group++ + ":" + Arrays.toString(x.toArray())));
        System.out.println("---- ---- ---- ----");
        eggs.stream()
                .flatMap(x -> Arrays.stream(x).map(y -> y.replace("鸡", "煎")))
                .forEach(x -> System.out.println("学生" + student++ + ":" + x));

    }

}
