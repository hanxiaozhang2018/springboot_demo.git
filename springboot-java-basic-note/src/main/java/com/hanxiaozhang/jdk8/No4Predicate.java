package com.hanxiaozhang.jdk8;

import com.hanxiaozhang.commomentity.Student;

import java.util.function.Predicate;

/**
 * 〈一句话功能简述〉<br>
 * 〈Predicate的使用〉
 *
 * @author hanxinghua
 * @create 2022/8/22
 * @since 1.0.0
 */
public class No4Predicate {

    public static void main(String[] args) {

        /*
         Predicate[pre dɪ keɪt]【谓词】是一个布尔值的函数。
         作用：对某种数据类型的数据进行判断，结果返回一个boolean值
         核心方法：boolean test(T t)
         其他方法：test、and、or、negate、isEqual和not(jdk11才有方法)
         Tips:BiPredicate支持两个参数、DoublePredicate、IntPredicate...
         */

        // 1.test()：
        Predicate<String> isValid = x -> x != null;
        System.out.println("test string not null is" + isValid.test("test"));
        System.out.println("---- ---- ---- ----");

        // 2.and()：表示两个谓词短路逻辑 &&
        Student student1 = new Student("1", "1", "1");
        Student student2 = new Student("2", "2", "2", "2", "2");
        Predicate<Student> isNameValid = x -> null != x.getName();
        Predicate<Student> isPhoneValid = x -> null != x.getPhone();
        System.out.println("student1 and valid is " + isNameValid.and(isPhoneValid).test(student1));
        System.out.println("student2 and valid is " + isNameValid.and(isPhoneValid).test(student2));
        System.out.println("---- ---- ---- ----");

        // 3.or()：表示两个谓词短路逻辑 ||
        System.out.println("student1 or valid is " + isNameValid.or(isPhoneValid).test(student1));
        System.out.println("---- ---- ---- ----");

        // 4.negate()：表示该谓词的逻辑否定的谓词 ！
        System.out.println("test string null is" + isValid.negate().test("test"));
        System.out.println("---- ---- ---- ----");

        // 5.isEqual()：根据Objects.equals(Object, Object)测试两个参数是否相等的谓词
        Predicate<String> isHelloValid = Predicate.isEqual("hello");
        System.out.println("hello isEqual is " + isHelloValid.test("hello"));
        System.out.println("hi isEqual is " + isHelloValid.test("hi"));

        Predicate<Student> isStudentValid = Predicate.isEqual(student1);
        System.out.println("student1 isEqual student2 is " + isStudentValid.test(student2));

    }

}
