package com.hanxiaozhang.jdk8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/28
 * @since 1.0.0
 */
public class No9UnaryOperator {

    public static void main(String[] args) {

        /*
         UnaryOperator接口继承Function接口，它只接收一个泛型参数T，用于处理单个操作数。
         UnaryOperator接口继承Function接口，在传入泛型T类型的参数，调用apply后，返回也T类型的参数。
         UnaryOperator接口中，定义了一个静态方法，返回泛型对象的本身: <T> UnaryOperator<T> identity();
         */

        // 使用1
        UnaryOperator<Integer> unaryOperator1 = i -> i + 100;
        System.out.println(unaryOperator1.apply(100));
        System.out.println("---- ---- ---- ----");

        // 使用2
        UnaryOperator<String> unaryOperator2 = s -> s;
        System.out.println(unaryOperator2.apply("test"));
        System.out.println("---- ---- ---- ----");

        // 使用3
        List<Integer> list = Arrays.asList(10, 20, 30, 40, 50);
        UnaryOperator<Integer> unaryOperator3 = i -> i * i;
        unaryOperatorFunction(unaryOperator3, list).forEach(System.out::println);


    }

    private static List<Integer> unaryOperatorFunction(UnaryOperator<Integer> unaryOperator, List<Integer> list) {
        List<Integer> uniList = new ArrayList<>();
        list.forEach(i -> uniList.add(unaryOperator.apply(i)));
        return uniList;
    }
}
