package com.hanxiaozhang.jdk8.lambda.No1methodparamlambda;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * Java8函数式接口的定义，其只能有一个抽象方法
 *
 * @author hanxinghua
 * @create 2023/5/30
 * @since 1.0.0
 */
public class No5LambdaMultiMethod {

    public static void main(String[] args) {
        printString(new PrintableMultiMethod() {
            @Override
            public void print(String s) {
                System.out.println(s);
            }

            @Override
            public void print(String a, String b) {
                System.out.println(a + b);
            }
        });

    }

    public static void printString(PrintableMultiMethod p) {
        p.print("hello word");
        p.print("a", "b");
    }


}
