package com.hanxiaozhang.jdk8.lambda.No1methodparamlambda;

/**
 * 〈一句话功能简述〉<br>
 * 〈Lambda一个方法多个入参〉
 *
 * @author hanxinghua
 * @create 2023/5/30
 * @since 1.0.0
 */
public class No4LambdaMultiParam {

    public static void main(String[] args) {

        printString((a, b) -> {
            System.out.println(a + b);
        });
    }

    public static void printString(PrintableMultiParam p) {
        p.print("a", "b");
    }

}
