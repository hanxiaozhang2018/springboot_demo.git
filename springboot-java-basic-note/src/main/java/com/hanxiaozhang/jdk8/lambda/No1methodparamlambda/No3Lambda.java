package com.hanxiaozhang.jdk8.lambda.No1methodparamlambda;

/**
 * 〈一句话功能简述〉<br>
 * 〈Lambda〉
 * <p>
 * 方法引用是对lambda表达式的补充，让已经存在的逻辑无需再次书写lambda表达式，即简化代码。
 *
 * @author hanxinghua
 * @create 2023/5/29
 * @since 1.0.0
 */
public class No3Lambda {


    public static void main(String[] args) {


        // 写法三：
        printString(s -> {
            System.out.println("写法三：" + s);
        });

    }


    public static void printString(Printable p) {
        p.print("hello word");
    }


}
