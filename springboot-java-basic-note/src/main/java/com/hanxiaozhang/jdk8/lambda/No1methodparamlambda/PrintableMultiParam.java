package com.hanxiaozhang.jdk8.lambda.No1methodparamlambda;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2023/5/29
 * @since 1.0.0
 */
public interface PrintableMultiParam {

    void print(String a ,String b);
}
