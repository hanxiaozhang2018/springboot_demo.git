package com.hanxiaozhang.jdk8.lambda.No1methodparamlambda;

/**
 * 〈一句话功能简述〉<br>
 * 〈原始写法〉
 * <p>
 * 方法引用
 * 方法引用是对lambda表达式的补充，让已经存在的逻辑无需再次书写lambda表达式
 *
 * @author hanxinghua
 * @create 2023/5/29
 * @since 1.0.0
 */
public class No1Original {


    public static void main(String[] args) {

        // 写法一：
        printString(new PrintString());

    }


    public static void printString(Printable p) {
        p.print("hello word");
    }


    static class PrintString implements Printable {

        @Override
        public void print(String s) {
            System.out.println("写法一：" + s);
        }
    }

}
