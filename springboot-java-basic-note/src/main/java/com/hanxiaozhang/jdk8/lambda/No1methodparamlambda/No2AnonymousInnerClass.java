package com.hanxiaozhang.jdk8.lambda.No1methodparamlambda;

/**
 * 〈一句话功能简述〉<br>
 * 〈匿名内部类〉
 *
 * @author hanxinghua
 * @create 2023/5/30
 * @since 1.0.0
 */
public class No2AnonymousInnerClass {

    public static void main(String[] args) {

        printString(new Printable() {
            @Override
            public void print(String s) {
                System.out.println("写法二：" + s);
            }
        });
    }


    public static void printString(Printable p) {
        p.print("hello word");
    }

}
