package com.hanxiaozhang.jdk8;

import java.util.function.BiFunction;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author hanxinghua
 * @create 2022/12/28
 * @since 1.0.0
 */
public class No10BiFunction {

    public static void main(String[] args) {

        /*
        BiFunction<T, U, R>接口：通过T类型和U类型数据，得到R类型的数据。
        R apply(T t, U u);
        <V> BiFunction<T, U, V> andThen(Function<? super R, ? extends V> after);
         */

        // apply()  举例：2 + 3 = 5
        BiFunction<Integer, Integer, Integer> biFunction1 = (v1, v2) -> v1 + v2;
        System.out.println("2 + 3 ="+biFunction1.apply(2, 3));
        System.out.println("---- ---- ---- ----");

        // andThen()  举例：(2 + 3) * 6 =30
        BiFunction<Integer, Integer, Integer> biFunction2 = biFunction1.andThen(x -> x * 6);
        System.out.println("(2 + 3) * 6 ="+biFunction2.apply(2, 3));

    }

}
