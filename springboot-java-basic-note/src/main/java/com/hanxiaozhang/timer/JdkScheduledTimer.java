package com.hanxiaozhang.timer;

import java.time.LocalDateTime;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 〈一句话功能简述〉<br>
 * 〈使用jdk的Timer实现定时〉
 *
 * @author hanxinghua
 * @create 2024/2/25
 * @since 1.0.0
 */
public class JdkScheduledTimer extends TimerTask {

   private static Timer TIMER = new Timer();

    public static void main(String[] args) {

        System.out.println("start time is "+LocalDateTime.now());
        // 创建一个延迟5秒执行的定时任务
        TIMER.schedule(new JdkScheduledTimer(),5*1000);
    }


    @Override
    public void run() {
        System.out.println("delayed execute, time is "+LocalDateTime.now());
        // 关闭结束定时Time
        TIMER.cancel();
    }
}
