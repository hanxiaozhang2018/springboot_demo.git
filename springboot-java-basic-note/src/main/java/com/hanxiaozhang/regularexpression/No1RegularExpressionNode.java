package com.hanxiaozhang.regularexpression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 〈一句话功能简述〉<br>
 * 〈正则表达式知识简介〉
 *
 * @author hanxinghua
 * @create 2023/6/6
 * @since 1.0.0
 */
public class No1RegularExpressionNode {

    /*
    Java正则表达式通过java.util.regex包下的Pattern类与Matcher类实现。
    Pattern类可以创建一个正则表达式。 -> Pattern.complie(String regex)
    Pattern类可以创建Matcher对象。 -> Pattern.matcher(CharSequence input)

	Matcher类代表匹配器，包含“正则表达式字符串”和“需要匹配的字符序列”
	boolean matches()：整个目标字符串与原字符串完全匹配时，才返回true。  -> 类比equals()
    boolean lookingAt()：总是从第一个字符进行匹配，匹配成功了不再继续匹配，匹配失败了，也不继续匹配。 -> 类比startWith()
    boolean find()：从当前位置开始匹配，找到一个匹配的子串，将移动下次匹配的位置。
    int start()：返回当前匹配到的字符串，在此次匹配中，在原目标字符串出现的开始位置。
    int end()：返回当前匹配的字符串，在此次匹配中，在原目标字符串中出现的结束位置。
    String group()：返回匹配到的子字符串。

    Tips：
    start() end() 必须先调用find() 或 matches() 或 lookingAt()  否则抛IllegalStateException("No match available");

     */

    public static void main(String[] args) {

        String target = "ab";
        String src = "123ab3ab456ab";
        // 创建正则表达式，不区分大小写
        Pattern p = Pattern.compile(target, Pattern.CASE_INSENSITIVE);
        // 创建匹配器
        Matcher m = p.matcher(src);
        int count = 0;

        // 对正则字符串进行匹配
        while (m.find()) {
            System.out.println("本次匹配的开始位置：" + m.start() + " 结束位置: " + m.end() + " 匹配的子串：" + m.group());
            count++;
        }
        System.out.println("子串出现的次数：" + count);

        System.out.println("---- ---- lookingAt ---- ----");
        Pattern p1 = Pattern.compile(target, Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(src);
        System.out.println("lookingAt: " + m1.lookingAt());

        String target1 = "123ab";
        Pattern p2 = Pattern.compile(target1, Pattern.CASE_INSENSITIVE);
        Matcher m2 = p2.matcher(src);
        System.out.println("lookingAt: " + m2.lookingAt());

    }

}
