package com.hanxiaozhang.idea;

/**
 * 〈一句话功能简述〉<br>
 * 〈Debug高级使用技巧〉
 *
 * @author hanxinghua
 * @create 2023/11/11
 * @since 1.0.0
 */
public class No1DebugUse {

    private boolean ready;

    No1DebugUse() {
        System.out.println();
    }

    public static void main(String[] args) {
        System.out.println("123");
    }

    void print1() {
        print2();
        System.out.println("print1 Printing to console");
    }

    void print2() {
        System.out.println("print2 Printing to console");
    }

}
